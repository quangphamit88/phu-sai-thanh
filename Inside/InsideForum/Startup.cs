﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(InsideX.Startup))]
namespace InsideX
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
