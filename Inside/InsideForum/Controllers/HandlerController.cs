﻿using System.Web.Mvc;
using BussinessObject;
using InsideX.Helper;

namespace InsideX.Controllers
{
    [InsideAuthorize]
    public class HandlerController : Controller
    {
        #region Admin

        //public JsonResult AdminActive(int id, bool status)
        //{
        //    if (BoFactory.Admin.Admin_UpdateActive(id, status, MySessionManager.SessionData.ID))
        //        return Json(new { code = 1 });
        //    return Json(new { code = 0 });
        //}

        #endregion

        #region Category

        public JsonResult CategoryActive(int id, bool status)
        {
            if (BoFactory.Category.Category_UpdateStaus(id, status, MySessionManager.SessionData.ID))
                return Json(new { code = 1 });
            return Json(new { code = 0 });
        }

        public JsonResult CategoryUserAccess(int id, bool status)
        {
            if (BoFactory.Category.Category_UpdateUserAccess(id, status, MySessionManager.SessionData.ID))
                return Json(new { code = 1 });
            return Json(new { code = 0 });
        }

        #endregion        

        public JsonResult ArticleStatus(int id, bool status)
        {
            if (BoFactory.Article.Article_EditStatus(id, MySessionManager.SessionData.ID, (short)1, status ? (short)2 : (short)3))
                return Json(new { code = 1 });
            return Json(new { code = 0 });
        }
       
    }
}