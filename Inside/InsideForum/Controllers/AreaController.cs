﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using BussinessObject;
using EntitiesObject.Entities.WebEntities;
using InsideX.EF;
using InsideX.Helper;

namespace InsideX.Controllers
{
    [AdminAuthorize]
    public class AreaController : Controller
    {
        public ActionResult Index(int cityId = 0, int districtId = 0)
        {
            if (cityId == 0 && districtId > 0)
            {
                var dis = BoFactory.AddressBook.GetDistrictById(districtId);
                if (dis != null)
                    cityId = dis.CityId ?? 0;
            }
            //=== Chọn tỉnh/tp ===
            var city = BoFactory.AddressBook.GetCityList().Select(c => new SelectListItem
            {
                Value = c.Id + "",
                Text = c.CityName,
                Selected = c.Id == cityId
            }).ToList();
            city.Insert(0, new SelectListItem
            {
                Value = "0",
                Text = "=== Chọn tỉnh/tp ===",
                Selected = false
            });

            var district = BoFactory.AddressBook.GetDistrictByCity(cityId).Select(c => new SelectListItem
            {
                Value = c.Id + "",
                Text = c.DistrictName,
                Selected = c.Id == districtId
            }).ToList();
            district.Insert(0, new SelectListItem
            {
                Value = "0",
                Text = "=== Chọn quận/huyện ===",
                Selected = false
            });

            ViewBag.City = city;
            ViewBag.District = district;
            var db = new PstEntities();
            var xAreas = db.XAreas.Where(c=>districtId==0||c.DistrictId==districtId).OrderBy(c => c.Name).ToList();
            return View(xAreas);
        }
        public ActionResult Edit(int id = 0)
        {
            var db = new PstEntities();
            var xArea = id == 0 ? new XArea { } : db.XAreas.FirstOrDefault(c => c.Id == id);
            if (xArea == null)
                return RedirectToAction("Index");
            return View(xArea);
        }

        [HttpPost]
        public ActionResult Edit(XArea model, int cityId = 0)
        {
            if (string.IsNullOrEmpty(model.Name))
                ModelState.AddModelError("Name", "Chưa nhập tên");

            var db = new PstEntities();
            if (ModelState.IsValid)
            {
                if (model.Id > 0)
                {
                    var a = db.XAreas.FirstOrDefault(c => c.Id == model.Id);
                    if (a != null)
                    {
                        a.Name = model.Name;
                    }
                }
                else
                {
                    db.XAreas.Add(model);
                }
                db.SaveChanges();

                return RedirectToAction("Index", new { cityId = cityId, districtId = model.DistrictId });
            }
            return View(model);
        }

        public ActionResult Delete(int id = 0, int cityId = 0, int districtId = 0)
        {
            var db = new PstEntities();
            db.XAreas.Remove(new XArea() {Id = id});
            db.SaveChanges();
            return RedirectToAction("Index", new { districtId, cityId });
        }

        public ActionResult GetDistrictByCity(int cityId = 0, int districtId = 0)
        {
            var district = BoFactory.AddressBook.GetDistrictByCity(cityId).Select(c => new SelectListItem
            {
                Value = c.Id + "",
                Text = c.DistrictName,
                Selected = c.Id == districtId
            }).ToList();

            district.Insert(0, new SelectListItem
            {
                Value = "0",
                Text = "=== Chọn quận/huyện ===",
                Selected = false
            });

            return PartialView(district);
        }
    }
}