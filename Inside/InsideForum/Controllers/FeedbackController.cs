﻿using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using BussinessObject;
using BussinessObject.Bo.WebBo;
using EntitiesObject.Entities.WebEntities;
using InsideX.EF;
using InsideX.Helper;
using InsideX.Models;
using MyUtility.Extensions;

namespace InsideX.Controllers
{
    [InsideAuthorize]
    public class FeedbackController : Controller
    {
        public ActionResult Index()
        {
            //var model=BoFactory.CustomerRequest.Search()
            return View();
        }
        //string keyword, int adminId, int isReply, int pageIndex, int pageSize,
        public JsonResult Search(DatatableResponseRequest param, string keyword = "", int isReply = -1, int brokerId = -1)
        {
            var pageSize = param.Length;
            if (pageSize == 0)
                pageSize = TinyConfiguration.PageSize;
            var pageIndex = (param.Start + pageSize) / pageSize;

            int totalRow = 0;
            var adminId = MySessionManager.SessionData.RoleId == AdminRoleEnum.Admin ? -1 : MySessionManager.SessionData.ID;
            if (brokerId != -1 && MySessionManager.SessionData.IsAdmin)
            {
                adminId = brokerId;
            }


            var model = BoFactory.UserFeedback.Search(keyword, adminId, isReply, pageIndex, pageSize, out totalRow).ToList();

            return Json(new DatatableResponseModel<dynamic>
            {
                recordsTotal = totalRow,
                recordsFiltered = totalRow,
                data = model.ToList<dynamic>()

            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportToExcel(DatatableResponseRequest param, string keyword = "", string dateForm = "", string dateTo = "", int status = 0, int brokerId = -1)
        {

            const int pageSize = 2000;
            const int pageIndex = 1;
            if (string.IsNullOrEmpty(dateForm) || string.IsNullOrEmpty(dateTo))
            {
                dateForm = "01/01/1970";
                dateTo = dateForm;
            }
            DateTime fromDate = string.IsNullOrEmpty(dateForm) ? DateTime.MinValue : dateForm.Trim().ToDateTimeParseExact("dd/MM/yyyy").StartOfDate();
            DateTime toDate = string.IsNullOrEmpty(dateTo) ? DateTime.MinValue : dateTo.Trim().ToDateTimeParseExact("dd/MM/yyyy").EndOfDate();

            int totalRow = 0;
            var adminId = MySessionManager.SessionData.RoleId == AdminRoleEnum.Admin ? -1 : MySessionManager.SessionData.ID;
            if (brokerId != -1 && MySessionManager.SessionData.IsAdmin)
            {
                adminId = brokerId;
            }
            var model = BoFactory.CustomerRequest.Search(adminId, keyword, fromDate, toDate, status, pageIndex, pageSize, 0, ref totalRow).ToList();

            var products = new System.Data.DataTable("test");
            products.Columns.Add("#Mã", typeof(int));
            products.Columns.Add("Loại KH", typeof(string));
            products.Columns.Add("Tình trạng", typeof(string));
            products.Columns.Add("Tên KH", typeof(string));
            products.Columns.Add("SDT", typeof(string));
            products.Columns.Add("Nhu cầu", typeof(string));
            products.Columns.Add("Khu vực", typeof(string));
            products.Columns.Add("Giá tối đa", typeof(string));
            products.Columns.Add("TS đã dẫn", typeof(string));
            products.Columns.Add("Ngày", typeof(string));
            products.Columns.Add("NV", typeof(string));

            foreach (var item in model)
            {
                products.Rows.Add(
                    item.Id,
                    item.CustomerType.ToEnum<CustomerRequestTypeEnum>().Text(),
                    item.Status.ToEnum<CustomerRequestStatusEnum>().Text(),
                    item.CustomerName,
                    item.Mobile,
                    item.RequestDetail,
                    item.RequestLocation,
                    item.PriceMax,
                    item.RealEstate,
                    item.CreateDate.HasValue ? item.CreateDate.Value.ToString("dd/MM/yyyy") : "",
                    item.DisplayName
                    );
            }

            var grid = new GridView { DataSource = products };
            grid.DataBind();
            var name = "BDS_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss-tt");
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=" + name + ".xls");
            Response.ContentType = "application/ms-excel";
            Response.ContentEncoding = System.Text.Encoding.Unicode;
            Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

            Response.Charset = "";
            var sw = new StringWriter();
            var htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);

            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
            return View("Index");
        }


        public ActionResult Edit(int id = 0)
        {
            var model = new CustomerRequest();
            if (id == 0)
            {

            }
            else
            {
                model = BoFactory.CustomerRequest.GetOne(id);
            }
            if (model.BrokerId.HasValue && model.BrokerId.Value > 0)
            {
                var db = new PstEntities();
                var b = db.Employees.FirstOrDefault(c => c.ID == model.BrokerId);
                if (b != null)
                    ViewBag.Broker = b.DisplayName;
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(CustomerRequest model)
        {
            if (ModelState.IsValid)
            {
                if (model.Id > 0)
                {
                    var cus = BoFactory.CustomerRequest.GetOne(model.Id);
                    if (cus == null)
                        return RedirectToAction("Index");
                    cus.CustomerType = model.CustomerType;
                    cus.AddressName = model.AddressName;
                    cus.CustomerName = model.CustomerName;
                    cus.BrokerId = model.BrokerId;
                    cus.ModifyDate = DateTime.Now;
                    cus.Email = model.Email;
                    cus.Keyword = model.Keyword;
                    cus.Mobile = model.Mobile;
                    cus.PriceMax = model.PriceMax;
                    cus.Status = model.Status;
                    cus.SaleOrRent = model.SaleOrRent;
                    cus.RealEstate = model.RealEstate;
                    cus.RequestDetail = model.RequestDetail;
                    cus.RequestLocation = model.RequestLocation;
                    cus.Notes = model.Notes;
                    cus.Keyword = MyUtility.StringCommon.RemoveUnicode(string.Concat(cus.CustomerName, " ", cus.Email, " ", cus.Mobile, " ", cus.AddressName));

                    BoFactory.CustomerRequest.Save(cus);
                }
                else
                {
                    model.CreateDate = DateTime.Now;
                    model.AdminId = MySessionManager.SessionData.ID;
                    model.Keyword = MyUtility.StringCommon.RemoveUnicode(string.Concat(model.CustomerName, " ", model.Email, " ", model.Mobile, " ", model.AddressName));
                    BoFactory.CustomerRequest.Add(model);
                }

                if (model.BrokerId.HasValue && model.BrokerId.Value > 0)
                {
                    var db = new PstEntities();
                    var b = db.Employees.FirstOrDefault(c => c.ID == model.BrokerId);
                    if (b != null)
                        ViewBag.Broker = b.DisplayName;
                }
                return RedirectToAction("Index");
            }

            return View(model);
        }

        public JsonResult Delete(int id)
        {
            var obj = BoFactory.UserFeedback.GetOne(id);
            if (obj != null)
            {
                BoFactory.UserFeedback.Delete(obj);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}