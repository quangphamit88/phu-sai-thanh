﻿using System.Collections.Generic;
using System.Web.Mvc;
using BussinessObject;
using BussinessObject.Bo.WebBo;
using EntitiesObject.Entities.WebEntities;
using InsideX.Helper;

namespace InsideX.Controllers
{
    [AdminAuthorize]
    public class CityController : Controller
    {
        public ActionResult Index()
        {
            var cities = BoFactory.AddressBook.GetCityList() ?? new List<Xcity>();
            return View(cities);
        }
        public ActionResult Edit(int id = 0)
        {
            var city = id == 0 ? new Xcity { IsPublic = true } : BoFactory.AddressBook.GetCityById(id);
            if (city == null)
                return RedirectToAction("Index");
            return View(city);
        }

        [HttpPost]
        public ActionResult Edit(Xcity model)
        {
            if (string.IsNullOrEmpty(model.CityName))
                ModelState.AddModelError("CityName", "Chưa nhập tên");

            if (ModelState.IsValid)
            {
                if (model.Id > 0)
                    BoFactory.Xcity.Save(model);
                else
                    BoFactory.Xcity.Add(model);
                return RedirectToAction("Index");
            }
            return View(model);
        }
        public ActionResult Delete(int id = 0)
        {
            BoFactory.Xcity.Delete(new Xcity {Id = id});
            return RedirectToAction("Index");
        }
    }
}