﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BussinessObject;
using EntitiesObject.Entities.WebEntities;
using InsideX.EF;
using InsideX.Helper;
using InsideX.Models;

namespace InsideX.Controllers
{
    public class ApiController : Controller
    {
        //public JsonResult BrokerAccount(string keyword = "")
        //{
        //    var db = new PstEntities();
        //    keyword = MyUtility.StringCommon.RemoveUnicode(keyword);
        //    var ls = !MySessionManager.SessionData.IsAdmin ? new List<Employee>() :db.Employees.Where(c=>c.Keyword.Contains(keyword)).ToList() ;

        //    if (MySessionManager.SessionData.IsAdmin)
        //    {
        //        ls.Insert(0, new Employee());
        //        {
        //            AdminId = -1,
        //            DisplayName = "-----Tất cả------",
        //            AvatarUrl = "/Files/Avatar/2/avatar3.png",
        //            Id = -1,
        //            UserName = "-----Tất cả------",
        //        });
        //    }
        //    else
        //    {
        //        var b = BoFactory.BrokerAccount.GetByAdminId(MySessionManager.SessionData.ID);
        //        if (b != null)
        //        {
        //            ls.Insert(0, new Ins_BrokerAccount_SearchAutoComplete_Result
        //            {
        //                AdminId = b.AdminId,
        //                DisplayName = b.DisplayName,
        //                AvatarUrl = b.AvatarUrl,
        //                Id = b.Id,
        //                UserName = b.UserName,
        //            });
        //        }
        //    }
        //    return Json(ls.Select(c => new
        //    {
        //        id = c.Id,
        //        text = HttpUtility.HtmlDecode(c.DisplayName),
        //        c.AdminId,
        //        c.AvatarUrl,
        //        UserName = HttpUtility.HtmlDecode(c.UserName)
        //    }), JsonRequestBehavior.AllowGet);
        //}

        public JsonResult UpdatePermission(int AdminId, int productId, bool IsFullPermission = false, bool IsViewAlbumLegal = false, bool IsViewAddress = false, bool IsViewAlbum = false, bool IsViewContact = false, bool IsViewBroker = false, bool isDelete = false, int id = 0)
        {
            var data = new RealEstatePermission()
            {
                Id = id,
                AdminId = AdminId,
                ProductId = productId,
                IsViewAlbumLegal = IsViewAlbumLegal,
                IsFullPermission = IsFullPermission,
                IsViewAddress = IsViewAddress,
                IsViewAlbum = IsViewAlbum,
                IsViewContact = IsViewContact,
                IsViewBroker = IsViewBroker,
            };
            var per = BoFactory.RealEstatePermission.GetByKeyIds(data.ProductId ?? 0, data.AdminId ?? 0);
            if (per == null)
            {
                data.CreateDate = DateTime.Now;
                BoFactory.RealEstatePermission.Add(data);
            }
            else
            {
                if (isDelete)
                {
                    BoFactory.RealEstatePermission.Delete(data.Id);
                }
                else
                    BoFactory.RealEstatePermission.Save(data);
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult BrokerAccountByDept(int deptId = 0, string keyword = "")
        {
            //keyword = MyUtility.StringCommon.RemoveUnicode(keyword);

            //var ls = !MySessionManager.SessionData.IsAdmin ? new List<Ins_BrokerAccount_SearchAutoComplete_Result>() : BoFactory.BrokerAccount.SearchAutoComplete(keyword);
            var db = new PstEntities();
            //var broker = new List<Ins_DepartmentDetail_List_Result>();
            //if (deptId > 0)
            //    broker = db.Ins_DepartmentDetail_List(deptId).ToList();

            //return Json(ls.Select(c => new
            //{
            //    id = c.Id,
            //    text = HttpUtility.HtmlDecode(c.DisplayName),
            //    c.AdminId,
            //    c.AvatarUrl,
            //    UserName = HttpUtility.HtmlDecode(c.UserName),
            //    selected = broker.Any(v => v.AdminId == c.Id)
            //}), JsonRequestBehavior.AllowGet);

            var data = db.Ins_GetBrokerByDept_List(deptId).ToList().Select(c => new
                {
                    id = c.AdminId,
                    text = HttpUtility.HtmlDecode(c.DisplayName),
                    c.AdminId,
                    c.AvatarUrl,
                    UserName = HttpUtility.HtmlDecode(c.UserName),
                    selected = c.DeptId == deptId,
                    //locked = c.DeptId != null && c.DeptId != deptId
                }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetAddress(string type = "C", int districtId = 0, int cityId = 0, int streetId = 0, int wardId = 0, int areaId = 0)
        {
            type = type.ToUpper();
            ViewBag.Type = type;
            var list = new List<SelectListItem>();
            switch (type)
            {
                case "C":
                    list = PstHelper.DropdownListCity(cityId);
                    break;
                case "D":
                    list = PstHelper.DropdownListDistrict(cityId, districtId);
                    break;
                case "W":
                    list = PstHelper.DropdownListWard(districtId, wardId);
                    break;
                case "S":
                    list = PstHelper.DropdownListStreet(districtId, streetId);
                    break;
                case "A":
                    list = PstHelper.DropdownListArea(districtId, areaId);
                    break;
            }

            return PartialView(list);
        }
    }
}