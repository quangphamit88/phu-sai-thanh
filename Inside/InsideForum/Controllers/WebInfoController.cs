﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BussinessObject;
using BussinessObject.Bo.WebBo;
using EntitiesObject.Entities.WebEntities;
using InsideX.Helper;
using InsideX.Models;

namespace InsideX.Controllers
{
    [AdminAuthorize]
    public class WebInfoController : Controller
    {
        //
        // GET: /WebInfo/
        public ActionResult ContactConfig()
        {
            var model = BoFactory.SystemConfig.GetContactConfig() ?? new SystemConfigBo.ContactConfig();
            //var con1 = (contactConfig.Contact1 ?? string.Empty).Split('|').ToList();
            //var con2 = (contactConfig.Contact2 ?? string.Empty).Split('|').ToList();
            //var model = new ContactConfigModel
            //{
            //    Address = contactConfig.Address,
            //    BackgroundUrl = contactConfig.BackgroundUrl,
            //    Email = contactConfig.Email,
            //    Name1 = con1[0],
            //    Name2 = con2[0],
            //    Phone1 = con1.Count > 1 ? con1[1] : "",
            //    Phone2 = con2.Count > 1 ? con2[1] : ""
            //};
            return View(model);
        }

        [HttpPost]
        public ActionResult ContactConfig(ContactConfigModel model)
        {
            //model.Contact1 = model.Name1 + "|" + model.Phone1;
            //model.Contact2 = model.Name2 + "|" + model.Phone2;

            BoFactory.SystemConfig.SetContactConfig(model);
            return RedirectToAction("ContactConfig");
        }


    }
}