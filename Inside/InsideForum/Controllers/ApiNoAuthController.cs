﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InsideX.Helper;

namespace InsideX.Controllers
{
    public class ApiNoAuthController : Controller
    {
        public JsonResult CheckLogin()
        {
            return Json(MySessionManager.SessionData != null);
        }
    }
}