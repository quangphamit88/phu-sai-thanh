﻿using System;
using System.Web.Mvc;
using InsideX.Helper;
using MyUtility.Extensions;
using PhuSaiThanhLib.Enum;

namespace InsideX.Controllers
{
    [AdminAuthorize]
    public class ReportController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult WebsiteBuy()
        {
            return View();
        }

        public ActionResult WebsiteRent()
        {
            return View();
        }

        public ActionResult EmployeeReal()
        {
            return View();
        }

        public ActionResult EmployeeCustomer()
        {
            return View();
        }

        public ActionResult EmployeeRealSearch(DateTime? dateFrom, DateTime? dateTo, int employeeType = 1, int realBuyOrRent = 1)
        {
            dateFrom = dateFrom ?? DateTime.Now.AddYears(-1);
            dateTo = dateTo ?? DateTime.Now;
            dateFrom = new DateTime(dateFrom.Value.Year, dateFrom.Value.Month, dateFrom.Value.Day, 0, 0, 1);
            dateTo = new DateTime(dateTo.Value.Year, dateTo.Value.Month, dateTo.Value.Day, 23, 59, 59);


            var model = new EmployeeRealSearchParam {
                EmployeeType = employeeType,
                RealBuyOrRent = realBuyOrRent,
                DateFrom = dateFrom,
                DateTo = dateTo
            };

            if (employeeType == EmployeeTypeEnum.Real.Value()) {
                return PartialView("EmployeeRealSearch_Real", model);
            }
            else {
                return PartialView("EmployeeRealSearch_Customer", model);
            }

            return PartialView(string.Format("EmployeeRealSearch_{0}", employeeType.NumberToEnumToText<EmployeeTypeEnum>(), realBuyOrRent.NumberToEnumToText<RealBuyOrRentEnum>()), model);
            //if (employeeType == EmployeeTypeEnum.Real.Value()) {
            //    if (realBuyOrRent == RealBuyOrRentEnum.Buy.Value())
            //        return PartialView("EmployeeRealSearch_Real_Buy");
            //    else
            //        return PartialView("EmployeeRealSearch_Real_Rent");
            //}
            //else {
            //    return PartialView(string.Format("EmployeeRealSearch_{0}",employeeType.NumberToEnumToText<EmployeeTypeEnum>(),realBuyOrRent.NumberToEnumToText<RealBuyOrRentEnum>()));
            //}
            //return PartialView(string.Format("EmployeeRealSearch_{0}_{1}", employeeType.NumberToEnumToText<EmployeeTypeEnum>()));
        }
    }



    public class EmployeeRealSearchParam
    {
        public int EmployeeType { get; set; }
        public int RealBuyOrRent { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
    }
}