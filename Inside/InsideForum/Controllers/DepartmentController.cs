﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BussinessObject;
using InsideX.EF;
using InsideX.Helper;
using InsideX.Models;
using InsideX.Models.Dept;

namespace InsideX.Controllers
{
    [InsideAuthorize]
    public class DepartmentController : Controller
    {
        PstEntities db = new PstEntities();
        public ActionResult Index(int deptId = 0, int brokerId=0)
        {
            //var db = new PstEntities();
            ViewBag.BrokerId = brokerId;
            return View(db.Departments.Where(c => c.IsDelete != true && (deptId == 0 || c.Id == deptId)&&(brokerId==0||c.DepartmentDetails.Any(v=>v.EmployeeId==brokerId))).OrderBy(c => c.Type).ThenBy(c => c.Name).ToList());
        }

        public ActionResult Update(int id = 0)
        {
            //var db = new PstEntities();
            var dept = new Department();
            if (id > 0) {
                dept = db.Departments.FirstOrDefault(c => c.Id == id) ?? new Department();
            }
            //ViewBag.Broker = db.Ins_GetBrokerByDept_List(0).ToList().Select(c=>new 
            //{
            //    id = c.AdminId,
            //    text = HttpUtility.HtmlDecode(c.DisplayName),
            //    c.AdminId,
            //    c.AvatarUrl,
            //    UserName = HttpUtility.HtmlDecode(c.UserName),
            //    selected = c.DeptId
            //}).ToList();
            return View(dept.ToJson().JsonToModel<DepartmentModel>());
        }

        [HttpPost]
        public ActionResult Update(DepartmentModel model)
        {
            if (ModelState.IsValid) {
                Department dept;
                if (model.Id > 0) {
                    dept = db.Departments.FirstOrDefault(c => c.Id == model.Id) ?? new Department();
                    dept.IsActive = model.IsActive;
                    dept.Name = model.Name;
                    dept.Note = model.Note;
                    dept.Type = (short)model.Type;
                    db.SaveChanges();

                }
                else {
                    dept = new Department {
                        IsActive = model.IsActive,
                        Name = model.Name,
                        Note = model.Note,
                        Type = (short)model.Type,
                        CreatedDate = DateTime.Now
                    };
                    db.Departments.Add(dept);
                    db.SaveChanges();
                }
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            //var db = new PstEntities();
            //var detail = db.DepartmentDetails.Where(c => c.DeptId == id);
            //db.DepartmentDetails.RemoveRange(detail);
            //db.SaveChanges();

            var dept = db.Departments.FirstOrDefault(c => c.Id == id);
            dept.DepartmentDetails.Clear();
            db.Departments.Remove(dept);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult DeptDetail(int deptId = 0)
        {
            ViewBag.deptId = deptId;
            var listDept = db.Departments.Where(c => c.IsDelete != true).OrderBy(c => c.Name).ToList().Select(c => new SelectListItem() {
                Selected = deptId == c.Id,
                Text = c.Name,
                Value = c.Id.ToString()
            }).ToList();
            listDept.Insert(0, new SelectListItem() { Text = "== Tất cả ==", Value = "0" });
            ViewBag.ddlDept = listDept;
            var result = db.Departments.FirstOrDefault(c => c.Id == deptId) ?? new Department();
            return View(result);

        }

        [HttpPost]
        public ActionResult UpdateDetail(int deptId, int brokerId, bool isLeader = false, bool isSubLeader = false)
        {
            var em = db.Employees.FirstOrDefault(c => c.ID == brokerId);
            if (em == null)
                return RedirectToAction("DeptDetail", new { deptId = deptId });

            if (isLeader)
                isSubLeader = false;

            var dept = db.Departments.FirstOrDefault(c => c.Id == deptId);
            if (dept == null)
                return RedirectToAction("Index");

            if (isLeader) {
                foreach (var item in dept.DepartmentDetails) {
                    item.IsLeader = false;
                }
            }

            if (isSubLeader) {
                foreach (var item in dept.DepartmentDetails) {
                    item.IsSubLeader = false;
                }
            }

            var emp = dept.DepartmentDetails.FirstOrDefault(c => c.Employee.ID == brokerId);
            if (emp == null) {
                dept.DepartmentDetails.Add(new DepartmentDetail() {
                    DeptId = deptId,
                    EmployeeId = brokerId,
                    IsActive = true,
                    IsLeader = isLeader,
                    IsSubLeader = isSubLeader,
                    CreatedBy = MySessionManager.SessionData.Employee.Username,
                    CreatedDate = DateTime.Now
                });
            }
            else {
                emp.IsLeader = isLeader;
                emp.IsSubLeader = isSubLeader;
            }
            db.SaveChanges();
            return RedirectToAction("DeptDetail", new { deptId = deptId });
        }

        //public ActionResult DeleteDetail(int deptid, int departmentid, int adminid)
        //{

        //    var detail = db.DepartmentDetails.FirstOrDefault(c => c.AdminId == adminid && c.DeptId == departmentid);
        //    if (detail != null)
        //    {
        //        db.DepartmentDetails.Remove(detail);
        //        db.SaveChanges();
        //    }
        //    return RedirectToAction("DeptDetail", new { deptId = deptid });

        //}

        //public ActionResult SetLeader(object deptid, int departmentid, int adminid)
        //{
        //    var detail = db.DepartmentDetails.Where(c => c.DeptId == departmentid).ToList();
        //    if (detail.Any())
        //    {
        //        foreach (var item in detail)
        //        {
        //            item.IsLeader = item.AdminId == adminid;
        //        }
        //        db.SaveChanges();
        //    }
        //    return RedirectToAction("DeptDetail", new { deptId = deptid });
        //}

        //void UpdateEmployee(int departmentId, List<int> brokerId)
        //{
        //    if (brokerId == null)
        //        return;
        //    var list = db.Employees.Where(c => brokerId.Contains(c.ID)).ToList();
        //    foreach (var item in list) {
        //        item.DeptId = departmentId;
        //    }
        //    db.SaveChanges();
        //}

        //void UpdateDepartmentDetail(int departmentId, List<int> brokerId)
        //{
        //    if (brokerId==null)
        //        brokerId=new List<int>();

        //    brokerId = brokerId.Distinct().ToList();

        //    var detailList = db.DepartmentDetails.Where(c => c.DeptId == departmentId).ToList();
        //    var brokerList = db.BrokerAccounts.Where(c => brokerId.Contains(c.Id) && c.AdminId > 0).ToList();
        //    brokerList = brokerList.Where(c => detailList.All(v => v.AdminId != c.AdminId)).ToList();
        //    var deleteLlist = detailList.Where(c => !brokerId.Contains(c.AdminId)).ToList();
        //    if (deleteLlist.Any())
        //    {
        //        db.DepartmentDetails.RemoveRange(deleteLlist);
        //        db.SaveChanges();
        //    }

        //    if (brokerList.Any())
        //    {
        //        var list = brokerList.Select(c => new DepartmentDetail()
        //        {
        //            AdminId = c.AdminId ?? 0,
        //            DeptId = departmentId,
        //            IsActive = true,
        //            IsLeader = false
        //        }).ToList();


        //        db.DepartmentDetails.AddRange(list);
        //        db.SaveChanges();
        //    }

        //}
        public ActionResult DeleteDeptDetail(int deptid, int employeeid)
        {
            var item = db.DepartmentDetails.FirstOrDefault(c => c.DeptId == deptid && c.EmployeeId == employeeid);
            if (item != null) {
                db.DepartmentDetails.Remove(item);
                db.SaveChanges();
            }

            return RedirectToAction("DeptDetail", new { deptId = deptid });
        }
    }



}








































