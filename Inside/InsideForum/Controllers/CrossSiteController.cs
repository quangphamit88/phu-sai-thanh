﻿using System.Linq;
using System.Web.Mvc;
using InsideX.EF;
using InsideX.Fillter;
using InsideX.Helper;
using InsideX.Models;
using MyUtility.Extensions;

namespace InsideX.Controllers
{
    public class CrossSiteController : Controller
    {
        [AllowCrossSite]
        public ActionResult Login(string username, string password)
        {
            if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password) && !MySessionManager.IsAuthen) {
                var db = new PstEntities();
                var employee = db.Employees.FirstOrDefault(c => c.Username == username.ToLower());

                if (employee != null && employee.Password == password) {
                    var myUserData = new UserData {
                        ID = employee.ID,
                        RoleId = employee.Role.ToEnum<AdminRoleEnum>(),
                        LoginName = employee.Username,
                        DisplayName = employee.DisplayName,
                        Avatar = employee.Avatar,
                        Sign = MyUtility.Common.MD5_encode(string.Format("{0}{1}{2}{3}", employee.ID, employee.Username, employee.DisplayName, TinyConfiguration.KeySign)),
                        Employee = employee,
                        //IsLeader = employee.IsLeader,
                        //IsSubLeader = employee.IsSubLeader,
                    };

                    MySessionManager.SessionData = myUserData;
                }
            }

            if (MySessionManager.IsAuthen)
                return Content("Is Authen");
            return Content("Is Not Authen");

        }
    }
}