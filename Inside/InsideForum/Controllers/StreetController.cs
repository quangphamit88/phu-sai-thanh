﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BussinessObject;
using EntitiesObject.Entities.WebEntities;
using InsideX.Helper;
using MyUtility.Extensions;

namespace InsideX.Controllers
{
    [AdminAuthorize]
    public class StreetController : Controller
    {
        public ActionResult Index(int cityId = 0, int districtId = 0, int wardId = 0)
        {
            if (cityId == 0 && districtId > 0)
            {
                var dis = BoFactory.AddressBook.GetDistrictById(districtId);
                if (dis != null)
                    cityId = dis.CityId ?? 0;
            }
            //=== Chọn tỉnh/tp ===
            var city = BoFactory.AddressBook.GetCityList().Select(c => new SelectListItem
            {
                Value = c.Id + "",
                Text = c.CityName,
                Selected = c.Id == cityId
            }).ToList();
            city.Insert(0, new SelectListItem
            {
                Value = "0",
                Text = "=== Chọn tỉnh/tp ===",
                Selected = false
            });

            var district = BoFactory.AddressBook.GetDistrictByCity(cityId).Select(c => new SelectListItem
            {
                Value = c.Id + "",
                Text = c.DistrictName,
                Selected = c.Id == districtId
            }).ToList();
            district.Insert(0, new SelectListItem
            {
                Value = "0",
                Text = "=== Chọn quận/huyện ===",
                Selected = false
            });

            //var ward = BoFactory.AddressBook.GetWardByDistrict(districtId).Select(c => new SelectListItem
            //{
            //    Value = c.Id + "",
            //    Text = c.WardName,
            //    Selected = c.Id == wardId
            //}).ToList();
            //ward.Insert(0, new SelectListItem
            //{
            //    Value = "0",
            //    Text = "=== Chọn phường/xã ===",
            //    Selected = false
            //});

            ViewBag.City = city;
            ViewBag.District = district;
            //ViewBag.Ward = ward;

            List<Xstreet> xstreets;
            if (wardId > 0)
                xstreets = BoFactory.AddressBook.GetStreetByWard(wardId);
            else if (districtId > 0)
                xstreets = BoFactory.AddressBook.GetStreetByDistrict(districtId);
            else xstreets = new List<Xstreet>();

            return View(xstreets);
        }
        public ActionResult Edit(int id = 0, int cityId = 0)
        {
            ViewBag.cityId = cityId;
            var xstreet = id == 0 ? new Xstreet { IsPublic = true } : BoFactory.AddressBook.GetStreetById(id);
            if (xstreet == null)
                return RedirectToAction("Index");
            return View(xstreet);
        }

        [HttpPost]
        public ActionResult Edit(Xstreet model, int cityId = 0)
        {
            ViewBag.cityId = cityId;
            if (model.DistrictId <= 0)
                ModelState.AddModelError("StreetName", "Chưa nhập quận/huyện");

            //if (model.WardId <= 0)
            //    ModelState.AddModelError("StreetName", "Chưa nhập phường/xã");

            if (string.IsNullOrEmpty(model.StreetName))
                ModelState.AddModelError("StreetName", "Chưa nhập tên");

            if (ModelState.IsValid)
            {
                model.Remarks = model.StreetName.ToAlias();
                if (model.Id > 0)
                    BoFactory.Xstreet.Save(model);
                else
                    BoFactory.Xstreet.Add(model);
                return RedirectToAction("Index", new { cityId, model.DistrictId, model.WardId });
            }
            return View(model);
        }

        public ActionResult Delete(int id = 0, int cityId = 0, int districtId = 0)
        {
            BoFactory.Xstreet.Delete(new Xstreet { Id = id });
            return RedirectToAction("Index", new { districtId, cityId });
        }

        public ActionResult GetDistrictByCity(int cityId = 0, int districtId = 0)
        {
            var district = BoFactory.AddressBook.GetDistrictByCity(cityId).Select(c => new SelectListItem
            {
                Value = c.Id + "",
                Text = c.DistrictName,
                Selected = c.Id == districtId
            }).ToList();

            district.Insert(0, new SelectListItem
            {
                Value = "0",
                Text = "=== Chọn quận/huyện ===",
                Selected = false
            });

            return PartialView(district);
        }
        public ActionResult GetWardByDistrict(int districtId = 0, int wardId = 0)
        {
            var district = BoFactory.AddressBook.GetWardByDistrict(districtId).Select(c => new SelectListItem
            {
                Value = c.Id + "",
                Text = c.WardName,
                Selected = c.Id == wardId
            }).ToList();

            district.Insert(0, new SelectListItem
            {
                Value = "0",
                Text = "=== Chọn phường/xã ===",
                Selected = false
            });

            return PartialView(district);
        }
    }
}