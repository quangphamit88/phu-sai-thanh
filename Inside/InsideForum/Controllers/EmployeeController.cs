﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InsideX.EF;
using InsideX.Helper;
using InsideX.Models;
using InsideX.Models.Customer;
using log4net.Core;
using MyUtility.Extensions;
using PhuSaiThanhLib.Enum;

namespace InsideX.Controllers
{
    [InsideAuthorize]
    public class EmployeeController : Controller
    {
        public ActionResult Index()
        {
            var db = new PstEntities();
            var dept = db.Departments.Where(c => c.IsDelete != true).ToList().Select(c => new SelectListItem {
                Text = c.Name,
                Value = c.Id.ToString()
            }).ToList();

            dept.Insert(0, new SelectListItem {
                Text = "== Chọn nhóm ==",
                Value = "0"
            });
            ViewBag.dept = dept;
            //ViewBag.Type = type;
            return View();
        }

        public ActionResult IndexTable(string keyword, int? deptId, int? level, int? empType, bool? active, bool? delete)
        {
            //var db = new PstEntities();
            //var data = db.Ins_Employee_ByDept_List(keyword, deptId, level, empType, active, delete).ToList();
            return PartialView();
        }

        public ActionResult AdminTable()
        {
            var db = new PstEntities();
            int admin = AdminRoleEnum.Admin.Value();
            var data = db.Employees.Where(c => c.Role == admin).ToList();
            return PartialView(data);
        }

        public ActionResult Index2()
        {
            var db = new PstEntities();
            var dept = db.Departments.Where(c => c.IsDelete != true).ToList().Select(c => new SelectListItem {
                Text = c.Name,
                Value = c.Id.ToString()
            }).ToList();

            dept.Insert(0, new SelectListItem {
                Text = "== Chọn nhóm ==",
                Value = "0"
            });
            ViewBag.dept = dept;
            //ViewBag.Type = type;
            return View();
        }


        public JsonResult EmployeeList(DatatableResponseRequest param, string keyword, int? deptId, int? level, int? empType, bool? active, bool? delete)
        {
            var start = param.Start + 1;

            var db = new PstEntities();
            var data = db.Ins_Employee_List(keyword, deptId, level, empType, active, delete, start, param.Length).ToList();
            var totalRow = data.Any() ? data[0].TotalRow ?? 0 : 0;
            return Json(new DatatableResponseModel<dynamic> {
                recordsTotal = totalRow,
                recordsFiltered = totalRow,
                data = data.Select(c => new {
                    c.ID,
                    c.DeptId,
                    c.Avatar,
                    Address = c.Address ?? "-",
                    c.CreateBy,
                    c.CreateDate,
                    c.Deleted,
                    c.DeptName,
                    c.DisplayName,
                    Email = c.Email ?? "-",
                    c.Gender,
                    c.IsActive,
                    c.IsLeader,
                    c.IsSubLeader,
                    c.Keyword,
                    c.Level,
                    LevelString = c.Level.NumberToEnumToText<EmployeeLevelEnum>(),
                    c.ModifyBy,
                    c.ModifyDate,
                    c.Note,
                    Phone = c.Phone ?? "-",
                    c.Role,
                    RoleString = c.Role.NumberToEnumToText<AdminRoleEnum>(),
                    c.RowId,
                    c.TotalRow,
                    c.Type,
                    TypeString = c.Role == (int)AdminRoleEnum.Employer ? c.Type.NumberToEnumToText<EmployeeTypeEnum>() : "-",
                    c.Username
                }).ToList<dynamic>()
            }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Update(int id = 0)
        {
            var db = new PstEntities();
            Employee em;
            if (id > 0) {
                em = db.Employees.FirstOrDefault(c => c.ID == id);
                if (em == null)
                    return RedirectToAction("Index");
            }
            else {
                em = new Employee();
            }


            return View(em);
        }

        [HttpPost]
        public ActionResult Update(Employee model, bool isChangePassword = false, bool isChangeUsername = false)
        {
            if (isChangeUsername && string.IsNullOrEmpty(model.Username))
                ModelState.AddModelError("Username", "Chưa nhập Username.");

            if (isChangePassword && string.IsNullOrEmpty(model.Password))
                ModelState.AddModelError("Password", "Chưa nhập password.");



            if (ModelState.IsValid) {

                if (isChangePassword && !string.IsNullOrEmpty(model.Password))
                    model.Password = MyUtility.Common.MD5_encode(string.Format("{0}{1}", model.Password, TinyConfiguration.KeySign));

                if (isChangeUsername && !string.IsNullOrEmpty(model.Username))
                    model.Username = model.Username.Trim().ToLower();


                if (string.IsNullOrEmpty(model.DisplayName))
                    model.DisplayName = model.Username;

                var db = new PstEntities();
                var list = db.Employees.Where(c => c.ID != model.ID && c.Username == model.Username).ToList();
                if (list.Any()) {
                    ModelState.AddModelError("Username", "Username đã tồn tại.");
                    return View(model);
                }

                //if (model.IsLeader) {
                //    var ds = db.Employees.Where(c => c.DeptId == model.DeptId && c.IsLeader && c.ID != model.ID).ToList();
                //    if (ds.Any()) {
                //        foreach (var employee in ds) {
                //            employee.IsLeader = false;
                //        }
                //        db.SaveChanges();
                //    }
                //}

                //if (model.IsSubLeader) {
                //    var ds = db.Employees.Where(c => c.DeptId == model.DeptId && c.IsSubLeader && c.ID != model.ID).ToList();
                //    if (ds.Any()) {
                //        foreach (var employee in ds) {
                //            employee.IsSubLeader = false;
                //        }
                //        db.SaveChanges();
                //    }
                //}

                if (model.ID > 0) {
                    var em = db.Employees.FirstOrDefault(c => c.ID == model.ID);
                    if (em == null)
                        return RedirectToAction("Index");
                    em.DisplayName = model.DisplayName;
                    em.Type = model.Type;
                    em.Gender = model.Gender;
                    em.Avatar = model.Avatar;
                    em.Address = model.Address;
                    em.Email = model.Email;
                    em.IsActive = model.IsActive;
                    //em.DeptId = model.DeptId;
                    em.Level = model.Level;
                    //if (model.IsLeader) {
                    //    em.IsLeader = model.IsLeader;
                    //    em.IsSubLeader = false;
                    //}
                    //else
                    //    em.IsLeader = false;

                    //if (model.IsSubLeader) {
                    //    em.IsLeader = false;
                    //    em.IsSubLeader = model.IsSubLeader;
                    //}
                    //else
                    //    em.IsSubLeader = false;

                    em.IsActive = model.IsActive;
                    //em.Role = model.Role;
                    em.Phone = model.Phone;
                    em.Note = model.Note;
                    em.ModifyBy = MySessionManager.SessionData.ID;
                    em.ModifyDate = DateTime.Now;
                    if (isChangePassword)
                        em.Password = model.Password;

                    if (isChangeUsername)
                        em.Username = model.Username;
                }
                else {
                    model.CreateBy = MySessionManager.SessionData.ID;
                    model.CreateDate = DateTime.Now;
                    model.Role = AdminRoleEnum.Employer.Value();
                    db.Employees.Add(model);
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(model);

        }

        public ActionResult Delete(int id)
        {
            var db = new PstEntities();
            var e = db.Employees.FirstOrDefault(c => c.ID == id);
            if (e != null && e.Role != AdminRoleEnum.Admin.Value())
            {
                e.DepartmentDetails.Clear();
                e.Deleted = true;
                e.Username = "deleted_" + e.ID + "_" + e.Username;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }


        #region chang pass

        public ActionResult UpdateProfile()
        {
            var db = new PstEntities();
            var emp = db.Employees.FirstOrDefault(c => c.ID == MySessionManager.SessionData.ID);
            if (emp == null)
                return RedirectToAction("Index", "Home");
            return View(emp);

        }

        [HttpPost]
        public ActionResult UpdateProfile(Employee model, bool isChangePassword = false)
        {
            if (isChangePassword && string.IsNullOrEmpty(model.Password))
                ModelState.AddModelError("Password", "Chưa nhập password.");


            if (isChangePassword && !string.IsNullOrEmpty(model.Password))
                model.Password = MyUtility.Common.MD5_encode(string.Format("{0}{1}", model.Password, TinyConfiguration.KeySign));

            if (string.IsNullOrEmpty(model.DisplayName))
                model.DisplayName = model.Username;

            var db = new PstEntities();

            if (model.ID > 0) {
                var em = db.Employees.FirstOrDefault(c => c.ID == MySessionManager.SessionData.ID);
                if (em == null)
                    return RedirectToAction("Index", "Home");
                em.DisplayName = model.DisplayName;
                
                em.Gender = model.Gender;
                em.Avatar = model.Avatar;
                em.Address = model.Address;
                em.Email = model.Email;
                em.Phone = model.Phone;
                em.Note = model.Note;
                em.ModifyBy = MySessionManager.SessionData.ID;
                em.ModifyDate = DateTime.Now;
                if (isChangePassword)
                    em.Password = model.Password;

                db.SaveChanges();
                return RedirectToAction("UpdateProfile");
            }

            return View(model);

        }


        #endregion
    }
}