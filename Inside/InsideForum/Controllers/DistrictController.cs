﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using BussinessObject;
using EntitiesObject.Entities.WebEntities;
using InsideX.Helper;
using MyUtility.Extensions;

namespace InsideX.Controllers
{
    [AdminAuthorize]
    public class DistrictController : Controller
    {
        public ActionResult Index(int cityId = 0)
        {
            //=== Chọn tỉnh/tp ===
            var city = BoFactory.AddressBook.GetCityList().Select(c => new SelectListItem
            {
                Value = c.Id + "",
                Text = c.CityName,
                Selected = c.Id == cityId
            }).ToList();
            city.Insert(0, new SelectListItem
            {
                Value = "0",
                Text = "=== Chọn tỉnh/tp ===",
                Selected = false
            });
            ViewBag.City = city;
            var xdistricts = BoFactory.AddressBook.GetDistrictByCity(cityId) ?? new List<Xdistrict>();
            return View(xdistricts);
        }
        public ActionResult Edit(int id = 0)
        {
            var xdistrict = id == 0 ? new Xdistrict { IsPublic = true } : BoFactory.AddressBook.GetDistrictById(id);
            if (xdistrict == null)
                return RedirectToAction("Index");
            return View(xdistrict);
        }

        [HttpPost]
        public ActionResult Edit(Xdistrict model)
        {
            if (string.IsNullOrEmpty(model.DistrictName))
                ModelState.AddModelError("DistrictName", "Chưa nhập tên");

            if (ModelState.IsValid)
            {
                model.Remarks = model.DistrictName.ToAlias();

                if (model.Id > 0)
                    BoFactory.Xdistrict.Save(model);
                else
                    BoFactory.Xdistrict.Add(model);
                return RedirectToAction("Index", new { cityId = model.CityId });
            }
            return View(model);
        }

        public ActionResult Delete(int id = 0, int cityId = 0)
        {
            BoFactory.Xdistrict.Delete(new Xdistrict{ Id = id });
            return RedirectToAction("Index", new { cityId = cityId });
        }
    }
}