﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using InsideX.EF;
using InsideX.Helper;
using InsideX.Models;
using InsideX.Models.Real;
using MyUtility.Extensions;
using PhuSaiThanhLib;
using PhuSaiThanhLib.Enum;
using PhuSaiThanhLib.Real;

namespace InsideX.Controllers
{
    [InsideAuthorize]
    [RealAuthorize]
    public class RealController : Controller
    {
        #region view

        public ActionResult BuyLead()
        {
            return Buy(true);
        }

        public ActionResult RentList()
        {
            return Rent(true);
        }


        public ActionResult Buy(bool isLead = false)
        {
            return Index(RealBuyOrRentEnum.Buy, isLead);
        }
        public ActionResult Rent(bool isLead = false)
        {
            return Index(RealBuyOrRentEnum.Rent, isLead);
        }
        public ActionResult Index(RealBuyOrRentEnum buyOrRent, bool isLead)
        {
            var db = new PstEntities();
            var dept = db.Departments.Where(c => c.IsDelete != true).ToList().Select(c => new SelectListItem {
                Text = c.Name,
                Value = c.Id.ToString()
            }).ToList();

            dept.Insert(0, new SelectListItem {
                Text = "== Chọn nhóm ==",
                Value = "0"
            });
            ViewBag.dept = dept;
            ViewBag.BuyOrRent = buyOrRent;
            ViewBag.IsLead = isLead;
            return View("Index");
        }

        #endregion

        #region Create
        public ActionResult Create(RealBuyOrRentEnum buyOrRent = RealBuyOrRentEnum.Buy)
        {
            ViewBag.BuyOrRent = buyOrRent;
            return View(new CreateRealModel());
        }

        [HttpPost]
        public ActionResult Create(CreateRealModel model, RealBuyOrRentEnum buyOrRent = RealBuyOrRentEnum.Buy)
        {
            ViewBag.BuyOrRent = buyOrRent;
            if (ModelState.IsValid) {
                var db = new PstEntities();

                //check address
                if (PstHelper.IsExistAddress(model.AddressNo.Trim(), model.StreetId, model.WardId)) {
                    ModelState.AddModelError("Id", string.Format("Địa chỉ này đã tồn tại."));
                    return View(model);
                }

                var real = new Real {
                    Title = model.Title,
                    BuyOrRent = buyOrRent.Value(),
                    Status = RealStatusEnum.Choduyet.Value(),
                    AddressNo = model.AddressNo.Trim(),
                    DistrictId = model.DistrictId,
                    WardId = model.WardId,
                    StreetId = model.StreetId,
                    CityId = model.CityId,
                    BrokerId = model.BrokerId,
                    CreatedBy = MySessionManager.SessionData.LoginName,
                    CreatedDate = DateTime.Now,
                    PublicDate = DateTime.Now,
                    GiaNha = 0,
                    UocLuongGiaBan = 0,
                    DienTichSuDung = 0,

                    IsAllowEditInfo = true,
                    IsAllowEditAddress = true,
                    IsAllowEditProductDetail = true,
                    IsAllowEditContact = true,
                    IsAllowEditHoaHong = true,
                    IsAllowEditNote = true,
                    IsAllowEditLocationDescription = true,
                    IsAllowEditImage = true,

                };

                db.Reals.Add(real);
                db.SaveChanges();

                return RedirectToAction("Update" + buyOrRent, new { id = real.Id });
            }

            return View(model);
        }


        #endregion

        #region update
        public ActionResult UpdateBuy(int id = 0)
        {
            return Update(id, RealBuyOrRentEnum.Buy);
        }

        public ActionResult UpdateRent(int id = 0)
        {
            return Update(id, RealBuyOrRentEnum.Rent);
        }

        public ActionResult Update(int id = 0, RealBuyOrRentEnum buyOrRent = RealBuyOrRentEnum.Buy)
        {
            if (id == 0)
                return RedirectToAction("Create", new { buyOrRent = buyOrRent });

            var db = new PstEntities();
            var real = db.Reals.FirstOrDefault(c => c.Id == id);
            if (real == null)
                return RedirectToAction("Index");

            buyOrRent = real.BuyOrRent.ToEnum<RealBuyOrRentEnum>();

            if (!MySessionManager.SessionData.IsAdmin && MySessionManager.SessionData.ID != real.BrokerId)
                return RedirectToAction(buyOrRent.ToString());

            ViewBag.BuyOrRent = buyOrRent;
            real.Hinh = string.Join("", (real.Hinh ?? "").Split(';').Select(c => c.GetFullUrlImage2()));
            real.HinhChiTiet = string.Join(";", (real.HinhChiTiet ?? "").Split(';').Select(c => c.GetFullUrlImage2()));
            real.HinhGiayTo = string.Join(";", (real.HinhGiayTo ?? "").Split(';').Select(c => c.GetFullUrlImage2()));
            real.HinhNoiThat = string.Join(";", (real.HinhNoiThat ?? "").Split(';').Select(c => c.GetFullUrlImage2()));

            return View("Update", real);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpdateBuy(Real model)
        {
            return Update(model, RealBuyOrRentEnum.Buy);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpdateRent(Real model)
        {
            return Update(model, RealBuyOrRentEnum.Rent);
        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(Real model, RealBuyOrRentEnum buyOrRent = RealBuyOrRentEnum.Buy)
        {
            ViewBag.BuyOrRent = buyOrRent;

            if (ModelState.IsValid) {
                var db = new PstEntities();

                if (model.Id == 0)
                    return RedirectToAction("Create", new { buyOrRent = buyOrRent });

                var real = db.Reals.FirstOrDefault(c => c.Id == model.Id);
                if (real == null)
                    return RedirectToAction("Update" + buyOrRent, new { type = buyOrRent });


                //check quyền
                if (MySessionManager.SessionData.IsEmployer && real.Status != RealStatusEnum.Choduyet.Value()) {
                    if (MySessionManager.SessionData.ID != real.BrokerId) {
                        ModelState.AddModelError("", "Bạn không có quyền chỉnh sửa nội dung này.");
                        return View("Update", real);
                    }
                }


                real.UpdatedDate = DateTime.Now;
                real.UpdatedBy = MySessionManager.SessionData.LoginName;


                if (real.AddressNo.Trim() != model.AddressNo.Trim() || real.WardId != model.WardId || real.StreetId != model.StreetId) {
                    if (PstHelper.IsExistAddress(model.AddressNo.Trim(), model.StreetId ?? 0, model.WardId ?? 0)) {
                        ModelState.AddModelError("", string.Format("Địa chỉ này đã tồn tại."));
                        return View("Update", real);
                    }
                    else {
                        real.AddressNo = model.AddressNo;
                        real.DistrictId = model.DistrictId;
                        real.CityId = model.CityId;
                        real.WardId = model.WardId;
                        real.StreetId = model.StreetId;
                    }
                }

                if (MySessionManager.SessionData.IsAdmin) {
                    real.BrokerId = model.BrokerId;
                    real.Status = model.Status;
                }


                if (MySessionManager.SessionData.IsAdmin || (real.Status == RealStatusEnum.Choduyet.Value() || real.IsAllowEditInfo == true)) {
                    real.Type = model.Type;
                    real.StatusNow = model.StatusNow;
                    real.StatusNowPercent = model.StatusNowPercent;

                    real.Price = model.Price;
                    real.PriceType = buyOrRent == RealBuyOrRentEnum.Buy ? PriceUnitEnum.Billion.Value() : PriceUnitEnum.Million.Value();
                    real.PriceMonthType = model.PriceMonthType;
                    real.PriceSold = model.PriceSold;
                    real.PriceSoldType = buyOrRent == RealBuyOrRentEnum.Buy ? PriceUnitEnum.Billion.Value() : PriceUnitEnum.Million.Value();

                    real.Title = model.Title;
                    real.IsPriceHot = model.IsPriceHot;
                    real.Vip = model.Vip;
                }

                if (MySessionManager.SessionData.IsAdmin || (real.Status == RealStatusEnum.Choduyet.Value() || real.IsAllowEditAddress == true)) {
                    real.AddressNo = model.AddressNo;
                    real.DistrictId = model.DistrictId;
                    real.CityId = model.CityId;
                    real.WardId = model.WardId;
                    real.StreetId = model.StreetId;
                }

                if (MySessionManager.SessionData.IsAdmin || (real.Status == RealStatusEnum.Choduyet.Value() || real.IsAllowEditProductDetail == true)) {
                    real.StreetType = model.StreetType;
                    real.DuongRong = model.DuongRong;
                    real.DienTich = model.DienTich;
                    real.DienTichSuDung = model.DienTichSuDung;
                    real.ChieuRong = model.ChieuRong;
                    real.ChieuDai = model.ChieuDai;
                    real.SoPhongNgu = model.SoPhongNgu;
                    real.SoWc = model.SoWc;
                    real.KetCau = model.KetCau;
                    real.SoMatTien = model.SoMatTien;
                    real.Direction = model.Direction;
                    real.LegalStatus = model.LegalStatus;

                    real.HinhDangDat = model.HinhDangDat;
                    real.HinhDangDatNumber = model.HinhDangDatNumber;
                }

                if (MySessionManager.SessionData.IsAdmin || (real.Status == RealStatusEnum.Choduyet.Value() || real.IsAllowEditContact == true)) {
                    real.ContactName = model.ContactName;
                    real.ContactPhone = model.ContactPhone;
                    real.ContactAddress = model.ContactAddress;
                    real.ContactResource = model.ContactResource;
                    real.ContactType = model.ContactType;
                }

                if (MySessionManager.SessionData.IsAdmin || (real.Status == RealStatusEnum.Choduyet.Value() || real.IsAllowEditHoaHong == true)) {
                    real.HoaHongBan = model.HoaHongBan;
                    real.HoaHongBanUnitType = model.HoaHongBanUnitType;
                    real.HoaHongThue = model.HoaHongThue;
                }

                if (MySessionManager.SessionData.IsAdmin || (real.Status == RealStatusEnum.Choduyet.Value() || real.IsAllowEditNote == true)) {
                    real.Note1 = model.Note1;
                    real.Note2 = model.Note2;
                    real.Note3 = model.Note3;
                    real.Note4 = model.Note4;
                    real.Note5 = model.Note5;
                    real.Note6 = model.Note6;
                    real.Note7 = model.Note7;
                    real.Note8 = model.Note8;
                    real.Note9 = model.Note9;
                    real.Note10 = model.Note10;
                    real.UocLuongGiaBan = model.UocLuongGiaBan;
                    real.GiaNha = model.GiaNha;
                    real.GiaTriDat = model.GiaTriDat;

                }
                if (MySessionManager.SessionData.IsAdmin || (real.Status == RealStatusEnum.Choduyet.Value() || real.IsAllowEditLocationDescription == true)) {
                    real.Description = model.Description;
                }
                if (MySessionManager.SessionData.IsAdmin || (real.Status == RealStatusEnum.Choduyet.Value() || real.IsAllowEditImage == true)) {
                    real.Hinh = model.Hinh;
                    real.HinhNoiThat = model.HinhNoiThat;
                    real.HinhChiTiet = model.HinhChiTiet;
                    real.HinhGiayTo = model.HinhGiayTo;
                }

                real.Keyword = MyUtility.StringCommon.RemoveUnicode(model.Title);

                real.IsAllowEditInfo = false;
                real.IsAllowEditAddress = false;
                real.IsAllowEditProductDetail = false;
                real.IsAllowEditContact = false;
                real.IsAllowEditHoaHong = false;
                real.IsAllowEditNote = false;
                real.IsAllowEditLocationDescription = false;
                real.IsAllowEditImage = false;

                if (MySessionManager.SessionData.IsAdmin || real.BrokerId == MySessionManager.SessionData.ID)
                    db.SaveChanges();
                return RedirectToAction(buyOrRent.ToString());
            }
            return View("Update", model);
        }

        #endregion

        #region list
        public JsonResult RealList(DatatableResponseRequest param, RealBuyOrRentEnum buyOrRent, bool isLead, DateTime? dateFrom, DateTime? dateTo, int? realType, int? realStatus, int? streetView, int? realStatusNow, decimal? minPrice, decimal? MaxPrice, bool IsHotPrice, int? deptId, int? realDirection, int? roomNumber, int? brokerId, string keyword, bool isSold = false)
        {
            //deptId = null;
            Thread.Sleep(1000);
            if (dateFrom.HasValue)
                dateFrom = new DateTime(dateFrom.Value.Year, dateFrom.Value.Month, dateFrom.Value.Day, 0, 0, 1);

            if (dateTo.HasValue)
                dateTo = new DateTime(dateTo.Value.Year, dateTo.Value.Month, dateTo.Value.Day, 23, 59, 59);

            //var direction = realDirection.ToString();
            //if (realDirection == 9)
            //    direction = "1,3,4,5";

            //if (realDirection == 10)
            //    direction = "2,6,7,8";

            var direction = BasicHelper.Direction(realDirection ?? 0);

            var start = param.Start + 1;

            var db = new PstEntities();

            var empList = "";
            if (MySessionManager.SessionData.IsAdmin)
                empList = "";
            else {
                if (isLead)
                    empList = MySessionManager.SessionData.IdsAccess();
                else
                    empList = MySessionManager.SessionData.ID.ToString();
            }


            var data = db.Ins_Real_Search(empList, brokerId, buyOrRent.Value(), realType, realStatus, realStatusNow, streetView, deptId, direction, dateFrom, dateTo, minPrice, MaxPrice, IsHotPrice, roomNumber, keyword, keyword.ToAlias(), isSold, start, param.Length).ToList();
            var totalRow = data.Any() ? data[0].TotalRow ?? 0 : 0;
            return Json(new DatatableResponseModel<dynamic> {
                recordsTotal = totalRow,
                recordsFiltered = totalRow,
                data = data.Select(c => {

                    return new {
                        c.Id,
                        c.TempId,
                        c.PublicDate,
                        c.Status,
                        RealStatusEnum = c.Status.NumberToEnumToText<RealStatusEnum>(),
                        RealStatusNowEnum = (c.StatusNow ?? 2).NumberToEnumToText<RealStatusNowEnum>(),
                        c.StatusNow,
                        StatusNowPercent = c.StatusNowPercent.HasValue ? c.StatusNowPercent + " %" : "",
                        c.Type,
                        RealTypeEnum = c.Type.NumberToEnumToText<RealTypeEnum>(),
                        c.BuyOrRent,
                        RealBuyOrRentEnum = c.BuyOrRent.NumberToEnumToText<RealBuyOrRentEnum>(),
                        Price = (c.Price ?? new decimal(0)).ToString("0.##"),
                        c.PriceType,
                        c.PriceMonthType,
                        c.PriceSold,
                        c.PriceSoldType,
                        c.Title,
                        c.IsPriceHot,
                        c.Vip,
                        c.AddressNo,
                        c.DistrictId,
                        c.CityId,
                        c.WardId,
                        c.StreetId,
                        c.StreetType,
                        StreetTypeEnum = c.StreetType.NumberToEnumToText<StreetTypeEnum>(),
                        DuongRong = c.DuongRong ?? 0,
                        DienTich = c.DienTich ?? 0,
                        DienTichSuDung = c.DienTichSuDung ?? 0,
                        ChieuRong = c.ChieuRong ?? 0,
                        ChieuDai = c.ChieuDai ?? 0,
                        SoPhongNgu = c.SoPhongNgu ?? 0,
                        SoWc = c.SoWc ?? 0,
                        KetCau=c.KetCau??string.Empty,
                        SoMatTien = c.SoMatTien ?? 0,
                        c.Direction,
                        DirectionString = c.Direction.NumberToEnumToText<RealDirectionEnum>(),
                        c.LegalStatus,
                        c.HinhDangDat,
                        RealHinhDangDatEnum = c.HinhDangDat.NumberToEnumToText<RealHinhDangDatEnum>(),
                        HinhDangDatNumber = c.HinhDangDatNumber ?? 0,
                        c.ContactName,
                        c.ContactPhone,
                        c.ContactAddress,
                        c.ContactResource,
                        c.ContactType,
                        c.HoaHongBan,
                        c.HoaHongBanUnitType,
                        c.HoaHongThue,
                        c.Note1,
                        c.Note2,
                        c.Note3,
                        c.Note4,
                        c.Note5,
                        c.Note6,
                        c.Note7,
                        c.Note8,
                        c.Note9,
                        c.Note10,
                        UocLuongGiaBan = c.UocLuongGiaBan ?? 0,
                        GiaNha = c.GiaNha ?? 0,
                        GiaTriDat = (c.GiaTriDat ?? 0).DecimalFormat(),
                        c.BrokerId,
                        c.Description,
                        Hinh = c.Hinh.GetFullUrlImage2(),
                        c.HinhNoiThat,
                        c.HinhChiTiet,
                        c.HinhGiayTo,
                        c.CreatedDate,
                        c.CreatedBy,
                        c.UpdatedDate,
                        c.UpdatedBy,
                        c.Keyword,
                        c.ViewCount,
                        c.Deleted,
                        c.DistrictName,
                        c.CityName,
                        c.WardName,
                        c.RowId,
                        c.TotalRow,
                        c.StreetName,
                        c.Username,
                        c.DisplayName,
                        c.Avatar,
                        UnitTypeString = c.BuyOrRent == RealBuyOrRentEnum.Buy.Value() ? "tỷ" : "triệu",
                        _GiaNha = (((decimal?)c.DienTichSuDung ?? 0) * (c.GiaNha ?? 0)).ToString("F2"),
                        CreatedDateString = c.CreatedDate.ToString("dd/MM/yy"),
                        UpdatedDateString = c.UpdatedDate.HasValue ? c.UpdatedDate.Value.ToString("dd/MM/yy") : "",
                        HinhNoiThatNumber = (c.HinhNoiThat ?? "").Split(';').Count(v => v.Trim().Length > 0),
                        HinhChiTietNumber = (c.HinhChiTiet ?? "").Split(';').Count(v => v.Trim().Length > 0),
                        HinhGiayToNumber = (c.HinhGiayTo ?? "").Split(';').Count(v => v.Trim().Length > 0),
                        HinhNoiThatView = string.Join(" ", (c.HinhNoiThat ?? "").Split(';').Where(b => b.Length > 0).Select(v => string.Format("<a href='{0}'></a>", v.GetFullUrlImage2()))),
                        HinhChiTietView = string.Join(" ", (c.HinhChiTiet ?? "").Split(';').Where(b => b.Length > 0).Select(v => string.Format("<a href='{0}'></a>", v.GetFullUrlImage2()))),
                        HinhGiayToView = string.Join(" ", (c.HinhGiayTo ?? "").Split(';').Where(b => b.Length > 0).Select(v => string.Format("<a href='{0}'></a>", v.GetFullUrlImage2())))
                    };
                }).ToList<dynamic>()
            }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult ExportToExcel(RealBuyOrRentEnum buyOrRent, bool isLead, DateTime? dateFrom, DateTime? dateTo, int? realType, int? realStatus, int? streetView, int? realStatusNow, decimal? minPrice, decimal? MaxPrice, bool IsHotPrice, int? deptId, int? realDirection, int? roomNumber, int? brokerId, string keyword, bool isSold = false)
        {
            if (dateFrom.HasValue)
                dateFrom = new DateTime(dateFrom.Value.Year, dateFrom.Value.Month, dateFrom.Value.Day, 0, 0, 1);

            if (dateTo.HasValue)
                dateTo = new DateTime(dateTo.Value.Year, dateTo.Value.Month, dateTo.Value.Day, 23, 59, 59);

            var direction = realDirection.ToString();
            if (realDirection == 9)
                direction = "1,3,4,5";

            if (realDirection == 10)
                direction = "2,6,7,8";

            const int start = 0;

            var db = new PstEntities();
            var data = db.Ins_Real_Search(MySessionManager.SessionData.IsAdmin ? "" : MySessionManager.SessionData.ID.ToString(), brokerId, buyOrRent.Value(), realType, realStatus, realStatusNow, streetView, deptId, direction, dateFrom, dateTo, minPrice, MaxPrice, IsHotPrice, roomNumber, keyword, keyword.ToAlias(), isSold, start, 100000).ToList();

            #region level
            if (isLead && MySessionManager.SessionData.IsLeaderOrSubLeader)
            {
                var level = db.LevelReals.FirstOrDefault(c => c.Level == MySessionManager.SessionData.Employee.Level);
                if (level != null)
                {
                    foreach (var item in data)
                    {
                        if (!level.IsViewAddress)
                        {
                            item.AddressNo = "";
                            item.StreetName = "";
                            item.DistrictName = "";
                        }
                        
                        if (!level.IsViewBroker)
                        {
                            item.DisplayName = "";
                            item.Username = "";
                        }

                        if (!level.IsViewContact)
                        {
                            item.ContactName = "";
                            item.ContactPhone = "";
                            item.ContactAddress = "";
                            item.ContactResource = "";
                            item.ContactType = 0;
                        }
                    }
                }
            }
            #endregion

            var products = new System.Data.DataTable("BDS");

            #region Columns
            products.Columns.Add("Mã số							", typeof(string));
            products.Columns.Add("Loại                          ", typeof(string));
            products.Columns.Add("Hình thức                     ", typeof(string));
            products.Columns.Add("Tình trạng                    ", typeof(string));
            products.Columns.Add("BĐS giá tốt                   ", typeof(string));
            products.Columns.Add("VIP                           ", typeof(string));
            products.Columns.Add("Địa chỉ                       ", typeof(string));
            products.Columns.Add("Tỉnh/TP                       ", typeof(string));
            products.Columns.Add("Quận                          ", typeof(string));
            products.Columns.Add("Phường                        ", typeof(string));
            products.Columns.Add("Đường                         ", typeof(string));
            products.Columns.Add("Mặt tiền                      ", typeof(string));
            products.Columns.Add("Đường rộng                    ", typeof(string));
            products.Columns.Add("Số Mặt tiền                   ", typeof(string));
            products.Columns.Add("Chiều rộng                    ", typeof(string));
            products.Columns.Add("Chiều dài                     ", typeof(string));
            products.Columns.Add("DT đất                        ", typeof(string));
            products.Columns.Add("DT sử dụng                    ", typeof(string));
            products.Columns.Add("Kết cấu                       ", typeof(string));
            products.Columns.Add("Hiện trạng                    ", typeof(string));
            products.Columns.Add("Số phòng ngủ                  ", typeof(string));
            products.Columns.Add("Số toilet                     ", typeof(string));
            products.Columns.Add("Hướng                         ", typeof(string));
            products.Columns.Add("Pháp lý                       ", typeof(string));
            products.Columns.Add("Tên chủ nhà                   ", typeof(string));
            products.Columns.Add("Chính chủ/Môi giới/Đầu tư     ", typeof(string));
            products.Columns.Add("SĐT                           ", typeof(string));
            products.Columns.Add("Nguồn                         ", typeof(string));
            products.Columns.Add("Giá                           ", typeof(string));
            products.Columns.Add("Giá đã bán                    ", typeof(string));
            products.Columns.Add("Hoa hồng                      ", typeof(string));
            products.Columns.Add("Nhân viên                     ", typeof(string));
            products.Columns.Add("1. Khoảng cách đến MT         ", typeof(string));
            products.Columns.Add("2. Hình dáng đất              ", typeof(string));
            products.Columns.Add("3. Tình trạng pháp lý         ", typeof(string));
            products.Columns.Add("4. Lộ giới                    ", typeof(string));
            products.Columns.Add("5. Khuyết điểm                ", typeof(string));
            products.Columns.Add("6. Lịch sử giá                ", typeof(string));
            products.Columns.Add("7. Ước lượng giá bán          ", typeof(string));
            products.Columns.Add("8. Giá nhà                    ", typeof(string));
            products.Columns.Add("9. Giá trị đất                ", typeof(string));
            products.Columns.Add("10. Ghi chú                   ", typeof(string));
            products.Columns.Add("Tiêu đề                       ", typeof(string));
            products.Columns.Add("Mô tả                         ", typeof(string));
            products.Columns.Add("Ngày đăng                     ", typeof(string));
            products.Columns.Add("Ngày sửa                      ", typeof(string));
            #endregion


            foreach (var item in data) {
                #region mo ta
                var des = @"";
                des += "MS: #" + item.Id;
                des += "\n";
                des += HttpUtility.HtmlDecode((item.Description ?? "").Replace("<br />", "\n"));
                des += "\n";
                des += "- Diện tích: " + (item.ChieuRong ?? 0) + " x " + (item.ChieuDai??0) + "m  (" + (item.DienTich ?? 0) + " m2)";
                if (item.HinhDangDat.HasValue)
                    des += "- " + item.HinhDangDat.NumberToEnumToText<RealHinhDangDatEnum>();
                if (item.HinhDangDatNumber.HasValue)
                    des += "- " + (item.HinhDangDatNumber ?? 0) + "m";
                des += "\n";
                des += "- Kết cấu: " + item.KetCau;
                if (item.DienTichSuDung.HasValue)
                    des += "- DTSD: " + item.DienTichSuDung + " m2";
                des += "\n";
                if (item.StatusNow.HasValue && item.StatusNow.Value > 0)
                    des += "- Hiện trạng nhà: " + item.StatusNow.NumberToEnumToText<RealStatusNowEnum>();
                if (item.Direction.HasValue)
                    des += "- Hướng: " + item.Direction.NumberToEnumToText<RealDirectionEnum>();
                des += "\n";
                des += "- Giá: " + item.Price.GetValueOrDefault().DecimalFormat() + item.BuyOrRent.ToUnitBuyOrRent() + "  còn thương lượng. Công ty BĐS Phú Sài Thành, 362 Trần Văn Kiểu, Phường 11, Quận 6";

                #endregion

                #region add
                products.Rows.Add(
                    //*Mã số					
                    item.Id,
                    //*Loại                     
                    item.Type.NumberToEnumToText<RealTypeBuyEnum>(),
                    //*Hình thức                
                    item.BuyOrRent.NumberToEnumToText<RealBuyOrRentEnum>(),
                    //*Tình trạng               
                    item.Status.NumberToEnumToText<RealStatusEnum>(),
                    //*BĐS giá tốt              
                    item.IsPriceHot.BoolToYesNo(),
                    //*VIP                      
                    item.Vip,
                    //*Địa chỉ                  
                    item.AddressNo,
                    //*Tỉnh/TP                  
                    item.CityName,
                    //*Quận                     
                    item.DistrictName,
                    //*Phường                   
                    item.WardName,
                    //*Đường                    
                    item.StreetName,
                    //*Mặt tiền                 
                    item.StreetType.NumberToEnumToText<StreetTypeEnum>(),
                    //*Đường rộng               
                    item.DuongRong,
                    //*Số Mặt tiền              
                    item.SoMatTien,
                    //*Chiều rộng               
                    item.ChieuRong,
                    //*Chiều dài                
                    item.ChieuDai,
                    //*DT đất                   
                    item.DienTich,
                    //*DT sử dụng               
                    item.DienTichSuDung,
                    //*Kết cấu                  
                    item.KetCau,
                    //*Hiện trạng               
                    item.StatusNow.NumberToEnumToText<RealStatusNowEnum>(),
                    //*Số phòng ngủ             
                    item.SoPhongNgu,
                    //*Số toilet                
                    item.SoWc,
                    //*Hướng                    
                    item.Direction.NumberToEnumToText<RealDirectionEnum>(),
                    //*Pháp lý                  
                    item.LegalStatus.NumberToEnumToText<LegalFormEnum>(),
                    //*Tên chủ nhà              
                    item.ContactName,
                    //*Chính chủ/Môi giới/Đầu tư
                    item.ContactType.NumberToEnumToText<RealContactTypeEnum>(),
                    //*SĐT                      
                    item.ContactPhone,
                    //*Nguồn                    
                    item.ContactResource,
                    //*Giá                      
                    item.Price.DecimalFormat() + item.BuyOrRent.ToUnitBuyOrRent(),
                    //*Giá đã bán               
                    item.PriceSold.DecimalFormat() + item.BuyOrRent.ToUnitBuyOrRent(),
                    //*Hoa hồng                 
                    item.BuyOrRent == RealBuyOrRentEnum.Buy.Value()
                        ? item.HoaHongBan.DecimalFormat() + " " + item.HoaHongBanUnitType.NumberToEnumToText<RealUnitHoaHongEnum>()
                        : item.HoaHongThue,
                    //*Nhân viên                
                    item.DisplayName,
                    //*1. Khoảng cách đến MT    
                    item.Note1,
                    //*2. Hình dáng đất         
                    item.Note2,
                    //*3. Tình trạng pháp lý    
                    item.Note3,
                    //*4. Lộ giới               
                    item.Note4,
                    //*5. Khuyết điểm           
                    item.Note5,
                    //*6. Lịch sử giá           
                    item.Note6,
                    //*7. Ước lượng giá bán     
                    item.UocLuongGiaBan.DecimalFormat(),
                    //*8. Giá nhà               
                    item.GiaNha.DecimalFormat(),
                    //*9. Giá trị đất           
                    item.GiaTriDat.DecimalFormat(),
                    //*10. Ghi chú              
                    item.Note10,
                    //*Tiêu đề                  
                    item.Title,
                    //*Mô tả                    
                    des,
                    //*Ngày đăng                
                    item.CreatedDate.ToString("dd/MM/yyyy HH:mm:ss"),
                    //*Ngày sửa                 
                    item.UpdatedDate.HasValue ? item.UpdatedDate.Value.ToString("dd/MM/yyyy HH:mm:ss") : ""
                    );
                #endregion
            }
            var grid = new GridView { DataSource = products };
            grid.DataBind();

            for (int i = 0; i < grid.Rows.Count; i++) {
                for (int j = 0; j < grid.Rows[i].Cells.Count; j++) {
                    grid.Rows[i].Cells[j].Text = grid.Rows[i].Cells[j].Text.Replace("\n", "");
                    //grid.Rows[i].Cells[j].Text = grid.Rows[i].Cells[j].Text.Replace("\n", "<br/>");
                }
            }

            var name = "BDS_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss-tt");
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=" + name + ".xls");
            Response.ContentType = "application/ms-excel";
            Response.ContentEncoding = System.Text.Encoding.Unicode;
            Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

            Response.Charset = "";
            var sw = new StringWriter();
            var htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);

            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();

            return Content("");

        }

        #endregion

        #region permission Khách hàng

        public ActionResult CustomerPermissionList(bool isView = false, int id = 0)
        {
            var db = new PstEntities();
            var list = db.Ins_RealPermission_View(id).ToList();
            list = list.GroupBy(c => new { c.BrokerId, c.RealId }).Select(g => g.OrderByDescending(c => c.CreatedDate).First()).Where(c => c.IsDeleted == false && c.TimeLimit >= DateTime.Now).ToList();
            ViewBag.List = list;
            var model = new RealPermission { RealId = id };
            ViewBag.isView = isView;
            if (isView)
                return PartialView("CustomerPermissionList_OnlyView", model);

            return PartialView(model);
        }


        [HttpPost]
        public ActionResult CustomerPermissionList(RealPermission model, List<int> brokerPermission, int addHour = 2, int addMinutes = 15)
        {
            brokerPermission = brokerPermission ?? new List<int>();
            brokerPermission = brokerPermission.Where(c => c > 0).ToList();
            var db = new PstEntities();
            foreach (var item in brokerPermission) {
                model.BrokerId = item;
                db.RealPermissions.Add(new RealPermission {
                    CreatedBy = MySessionManager.SessionData.LoginName,
                    CreatedDate = DateTime.Now,
                    UpdatedBy = MySessionManager.SessionData.LoginName,
                    UpdatedDate = DateTime.Now,
                    IsDeleted = false,
                    BrokerId = item,

                    IsViewImageGiayTo = model.IsViewImageGiayTo,
                    IsViewImageDetail = model.IsViewImageDetail,
                    IsViewAddress = model.IsViewAddress,
                    IsViewContact = model.IsViewContact,
                    IsViewBroker = model.IsViewBroker,
                    RealId = model.RealId,
                    TimeLimit = DateTime.Now.AddMinutes(addMinutes)
                });
            }
            db.SaveChanges();
            return RedirectToAction("CustomerPermissionList", new { id = model.RealId });
        }

        public JsonResult CustomerPermissionUpdate(int id, bool isViewImageGiayTo, bool isViewImageDetail, bool isViewBroker, bool isViewAddress, bool isViewContact)
        {
            var db = new PstEntities();
            var per = db.RealPermissions.FirstOrDefault(c => c.Id == id);
            if (per == null) return Json(false, JsonRequestBehavior.AllowGet);
            per.IsViewImageGiayTo = isViewImageGiayTo;
            per.IsViewImageDetail = isViewImageDetail;
            per.IsViewAddress = isViewAddress;
            per.IsViewContact = isViewContact;
            per.IsViewBroker = isViewBroker;
            db.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CustomerPermissionDelete(int id)
        {
            var db = new PstEntities();
            var per = db.RealPermissions.FirstOrDefault(c => c.Id == id);
            if (per != null) {
                per.IsDeleted = true;
                db.SaveChanges();
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region permission Nhân viên bds (nhân viên tạo)
        //id bds
        public ActionResult RealPermission(int id)
        {
            var db = new PstEntities();
            var model = db.Reals.FirstOrDefault(c => c.Id == id);
            if (model != null)
                return PartialView(model);
            ModelState.AddModelError("", "Không tìm thấy bds");
            return PartialView();
        }

        [HttpPost]
        public ActionResult RealPermission(Real model)
        {
            var db = new PstEntities();
            var real = db.Reals.FirstOrDefault(c => c.Id == model.Id);
            if (real != null) {
                real.IsAllowEditInfo = model.IsAllowEditInfo;
                real.IsAllowEditAddress = model.IsAllowEditAddress;
                real.IsAllowEditProductDetail = model.IsAllowEditProductDetail;
                real.IsAllowEditContact = model.IsAllowEditContact;
                real.IsAllowEditHoaHong = model.IsAllowEditHoaHong;
                real.IsAllowEditNote = model.IsAllowEditNote;
                real.IsAllowEditLocationDescription = model.IsAllowEditLocationDescription;
                real.IsAllowEditImage = model.IsAllowEditImage;
                db.SaveChanges();

                ViewBag.Success = true;
                return View(real);
            }
            ModelState.AddModelError("", "Không tìm thấy bds");
            return View(model);
        }


        #endregion

        #region Delete
        public JsonResult Delete(int id = 0)
        {
            if (MySessionManager.SessionData.IsAdmin) {
                var db = new PstEntities();
                var real = db.Reals.FirstOrDefault(c => c.Id == id);
                if (real != null) {
                    real.Deleted = true;
                    real.UpdatedDate = DateTime.Now;
                    real.UpdatedBy = MySessionManager.SessionData.LoginName;
                    db.SaveChanges();
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json("Cần quyền admin để thực hiện thao tác này.", JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}