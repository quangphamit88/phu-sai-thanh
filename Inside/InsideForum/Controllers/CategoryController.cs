﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BussinessObject;
using EntitiesObject.Entities.WebEntities;
using InsideX.Helper;
using InsideX.Models;
//using MyConfig;

namespace InsideX.Controllers
{
    [AdminAuthorizeAttribute]
    public class CategoryController : Controller
    {
        #region List

        [InsideAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        [InsideAuthorize]
        [HttpPost]
        public JsonResult GetListCategory()
        {
            var myList = BoFactory.Category.Category_GetAll();

            if (myList == null || (myList.Count == 0))
            {
                return Json(new { code = -1, message = "Không có dữ liệu" });
            }

            return Json(new
            {
                code = 1,
                data = myList.Select(dt => new
                {
                    ID = dt.ID,
                    Name = HttpUtility.HtmlDecode(dt.Name),
                    IsActive = dt.IsActive,
                    IsUserAccess = dt.IsUserAccess,
                    IsNotDelete = dt.IsNotDelete == true ? "hidden" : ""
                })
            });
        }

        #endregion

        #region Create

        [HttpGet]
        [InsideAuthorize]
        public ActionResult Create()
        {
            return View(new CategoryModel());
        }

        [InsideAuthorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CategoryModel model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.VALID = "Vui lòng kiểm tra dữ liệu vừa nhập";
                return View(model);
            }

            var myCategory = new Category
            {
                GameID = 1,
                Name = (model.Name),
                Position = model.Position,
                IsActive = model.IsActive,
                IsUserAccess = model.IsUserAccess,
                CreateBy = MySessionManager.SessionData.ID,
                ModifyBy = MySessionManager.SessionData.ID,
                CreateDate = DateTime.Now,
                ModifyDate = DateTime.Now,
                MetaTitle = (model.MetaTitle),
                MetaKeyword = (model.MetaKeyword),
                MetaDescription = (model.MetaDescription),
                OgTitle = (model.OgTitle),
                OgDescription = (model.OgDescription),
                OgUrl = (model.OgUrl),
                OgImage = (model.OgImage),
                TwitterTitle = (model.TwitterTitle),
                TwitterDescription = (model.TwitterDescription),
                TwitterUrl = (model.TwitterUrl),
                TwitterImage = (model.TwitterImage),
                MetaH1 = (model.MetaH1),
                MetaH2 = (model.MetaH2),
                MetaH3 = (model.MetaH3)
            };

            #region SEO

            #endregion

            if (BoFactory.Category.Category_Insert(myCategory) > 0)
            {
                ModelState.Clear();
                ViewBag.SUCCESS = "Tạo mới thành công";
                return View(new CategoryModel());
            }

            ViewBag.ERROR = "Có lỗi xảy ra, vui lòng thử lại sau";
            return View(model);
        }
        #endregion

        #region Edit

        private CategoryModel EditView(Ins_Category_GetById_Result item)
        {
            return new CategoryModel
            {
                ID = item.ID,
                GameID = item.GameID,
                Name = HttpUtility.HtmlDecode(item.Name),
                Position = item.Position,
                IsActive = item.IsActive ?? false,
                MetaTitle = item.MetaTitle,
                MetaKeyword = item.MetaKeyword,
                MetaDescription = item.MetaDescription,
                OgTitle = item.OgTitle,
                OgDescription = item.OgDescription,
                OgUrl = item.OgUrl,
                OgImage = item.OgImage,
                TwitterTitle = item.TwitterTitle,
                TwitterDescription = item.TwitterDescription,
                TwitterUrl = item.TwitterUrl,
                TwitterImage = item.TwitterImage,
                MetaH1 = item.MetaH1,
                MetaH2 = item.MetaH2,
                MetaH3 = item.MetaH3,
                IsUserAccess = item.IsUserAccess ?? false
            };
        }

        [HttpGet]
        [InsideAuthorize]
        public ActionResult Edit(int? id)
        {
            if ((id ?? 0) <= 0)
            {
                return RedirectToAction("Index");
            }

            var myCategoryCheck = BoFactory.Category.Category_GetById((int)id);

            if (myCategoryCheck == null || (myCategoryCheck != null && myCategoryCheck.ID <= 0))
            {
                return RedirectToAction("Index");
            }

            return View(this.EditView(myCategoryCheck));
        }

        [InsideAuthorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CategoryModel model, int? id)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.VALID = "Vui lòng kiểm tra dữ liệu vừa nhập";
                return View(model);
            }

            if ((id ?? 0) <= 0)
            {
                return RedirectToAction("Index");
            }

            var myCategoryCheck = BoFactory.Category.Category_GetById((int)id);

            if (myCategoryCheck == null || (myCategoryCheck != null && myCategoryCheck.ID <= 0))
            {
                return RedirectToAction("Index");
            }

            Category myCategory = new Category();
            myCategory.ID = (int)id;
            myCategory.GameID = model.GameID;
            myCategory.Name = (model.Name);
            myCategory.Position = model.Position;
            myCategory.IsActive = model.IsActive;
            myCategory.IsUserAccess = model.IsUserAccess;
            myCategory.ModifyBy = MySessionManager.SessionData.ID;
            myCategory.ModifyDate = DateTime.Now;

            #region SEO

            myCategory.MetaTitle = (model.MetaTitle);
            myCategory.MetaKeyword = (model.MetaKeyword);
            myCategory.MetaDescription = (model.MetaDescription);
            myCategory.OgTitle = (model.OgTitle);
            myCategory.OgDescription = (model.OgDescription);
            myCategory.OgUrl = (model.OgUrl);
            myCategory.OgImage = (model.OgImage);
            myCategory.TwitterTitle = (model.TwitterTitle);
            myCategory.TwitterDescription = (model.TwitterDescription);
            myCategory.TwitterUrl = (model.TwitterUrl);
            myCategory.TwitterImage = (model.TwitterImage);
            myCategory.MetaH1 = (model.MetaH1);
            myCategory.MetaH2 = (model.MetaH2);
            myCategory.MetaH3 = (model.MetaH3);

            #endregion

            if (BoFactory.Category.Category_Update(myCategory))
            {
                ModelState.Clear();
                ViewBag.SUCCESS = "Cập nhật thành công";
                return View(model);
            }

            ViewBag.ERROR = "Có lỗi xảy ra, vui lòng thử lại sau";
            return View(model);
        }
        #endregion

        #region Delete
        [InsideAuthorize]
        [HttpPost]
        public JsonResult Delete(int? id)
        {
            if ((id ?? 0) <= 0)
            {
                return Json(new { code = -1, message = "Chuyên mục không tồn tại" });
            }

            var myCategoryCheck = BoFactory.Category.GetOne(id);

            if (myCategoryCheck == null || (myCategoryCheck != null && myCategoryCheck.ID <= 0))
            {
                return Json(new { code = -2, message = "Chuyên mục không tồn tại" });
            }

            if (!BoFactory.Category.Delete(myCategoryCheck))
            {
                return Json(new { code = -3, message = "Có lỗi xảy ra, vui lòng thử lại sau" });
            }

            return Json(new { code = 1 });
        }
        #endregion
    }
}