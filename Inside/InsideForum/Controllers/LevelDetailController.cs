﻿
using System.Linq;
using System.Web.Mvc;
using InsideX.EF;
using InsideX.Helper;

namespace InsideX.Controllers
{
    [InsideAuthorize]
    public class LevelDetailController : Controller
    {
        public ActionResult LevelReal()
        {
            var db = new PstEntities();
            var model = db.LevelReals.OrderBy(c => c.Id).ToList();
            return View(model);
        }

        public JsonResult UpdateLevelReal(int levelDetailId, bool? isViewImageGiayTo, bool? isViewImageDetail, bool? isViewAddress, bool? isViewContact, bool? isViewBroker
            , bool? isviewCusInfo
            , bool? isViewCusCare
            , bool? isViewCusImages
            , bool? isViewCusLetterInfo

            )
        {

            var db = new PstEntities();
            var model = db.LevelReals.FirstOrDefault(c => c.Id == levelDetailId);
            if (model != null) {
                #region kinh doanh

                if (isviewCusInfo != null) model.IsviewCusInfo = isviewCusInfo.Value;

                if (isViewCusCare != null)
                    model.IsViewCusCare = isViewCusCare.Value;

                if (isViewCusImages != null)
                    model.IsViewCusImages = isViewCusImages.Value;

                if (isViewCusLetterInfo != null)
                    model.IsViewCusLetterInfo = isViewCusLetterInfo.Value;

                #endregion

                #region khai thác
                if (isViewImageGiayTo != null)
                    model.IsViewImageGiayTo = (bool)isViewImageGiayTo;

                if (isViewImageDetail != null)
                    model.IsViewImageDetail = (bool)isViewImageDetail;

                if (isViewAddress != null)
                    model.IsViewAddress = (bool)isViewAddress;

                if (isViewContact != null)
                    model.IsViewContact = (bool)isViewContact;

                if (isViewBroker != null)
                    model.IsViewBroker = (bool)isViewBroker;
                #endregion

                db.SaveChanges();
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

    }
}