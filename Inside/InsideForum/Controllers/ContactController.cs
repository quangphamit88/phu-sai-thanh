﻿using System;
using System.Linq;
using System.Web.Mvc;
using BussinessObject;
using InsideX.Helper;
using InsideX.Models;

namespace InsideX.Controllers
{
    [AdminAuthorizeAttribute]
    public class ContactController : Controller
    {
        public ActionResult Index()
        {
            //var data= BoFactory.ContactBookBo.Search()

            return View();
        }


        public JsonResult Search(string keyWord = "", string phone = "", string email = "", int cityId = 0, int currentPage = 1)
        {
            int itemsPerPage = TinyConfiguration.PageSize;
            int totalRecord = 0;
            var startPos = (currentPage - 1) * itemsPerPage;
            var myList = BoFactory.ContactBook.Search(keyWord, phone, email, cityId, currentPage, itemsPerPage, ref  totalRecord);

            var myListView = myList;


            if (myList == null || (myList.Count == 0))
            {
                return Json(new { code = -1, message = "Không có dữ liệu" });
            }

            return Json(new
            {
                code = 1,
                data = myListView,
                totalRecord,
                currentPage,
                itemsPerPage

            }, JsonRequestBehavior.AllowGet);
        }

    }
}