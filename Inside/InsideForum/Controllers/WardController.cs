﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using BussinessObject;
using EntitiesObject.Entities.WebEntities;
using InsideX.Helper;
using MyUtility.Extensions;

namespace InsideX.Controllers
{
    [AdminAuthorize]
    public class WardController : Controller
    {
        public ActionResult Index(int cityId = 0, int districtId = 0)
        {
            if (cityId == 0 && districtId > 0)
            {
                var dis = BoFactory.AddressBook.GetDistrictById(districtId);
                if (dis != null)
                    cityId = dis.CityId ?? 0;
            }
            //=== Chọn tỉnh/tp ===
            var city = BoFactory.AddressBook.GetCityList().Select(c => new SelectListItem
            {
                Value = c.Id + "",
                Text = c.CityName,
                Selected = c.Id == cityId
            }).ToList();
            city.Insert(0, new SelectListItem
            {
                Value = "0",
                Text = "=== Chọn tỉnh/tp ===",
                Selected = false
            });

            var district = BoFactory.AddressBook.GetDistrictByCity(cityId).Select(c => new SelectListItem
            {
                Value = c.Id + "",
                Text = c.DistrictName,
                Selected = c.Id == districtId
            }).ToList();
            district.Insert(0, new SelectListItem
            {
                Value = "0",
                Text = "=== Chọn quận/huyện ===",
                Selected = false
            });

            ViewBag.City = city;
            ViewBag.District = district;

            var xwards = BoFactory.AddressBook.GetWardByDistrict(districtId) ?? new List<Xward>();
            return View(xwards);
        }
        public ActionResult Edit(int id = 0)
        {
            var xward = id == 0 ? new Xward { IsPublic = true } : BoFactory.AddressBook.GetWardById(id);
            if (xward == null)
                return RedirectToAction("Index");
            return View(xward);
        }

        [HttpPost]
        public ActionResult Edit(Xward model, int cityId = 0)
        {
            if (string.IsNullOrEmpty(model.WardName))
                ModelState.AddModelError("DistrictName", "Chưa nhập tên");

            if (ModelState.IsValid)
            {
                model.Remarks = model.WardName.ToAlias();
                if (model.Id > 0)
                    BoFactory.Xward.Save(model);
                else
                    BoFactory.Xward.Add(model);
                return RedirectToAction("Index", new { cityId = cityId, districtId = model.DistrictId });
            }
            return View(model);
        }

        public ActionResult Delete(int id = 0, int cityId = 0, int districtId = 0)
        {
            BoFactory.Xward.Delete(new Xward { Id = id });
            return RedirectToAction("Index", new { districtId, cityId });
        }

        public ActionResult GetDistrictByCity(int cityId = 0, int districtId = 0)
        {
            var district = BoFactory.AddressBook.GetDistrictByCity(cityId).Select(c => new SelectListItem
            {
                Value = c.Id + "",
                Text = c.DistrictName,
                Selected = c.Id == districtId
            }).ToList();

            district.Insert(0, new SelectListItem
            {
                Value = "0",
                Text = "=== Chọn quận/huyện ===",
                Selected = false
            });

            return PartialView(district);
        }
    }
}