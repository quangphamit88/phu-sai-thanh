﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using BussinessObject;
using InsideX.EF;
using InsideX.Helper;
using InsideX.Models;
using InsideX.Models.Customer;
using MyUtility.Extensions;
using PhuSaiThanhLib;
using PhuSaiThanhLib.Enum;
using PhuSaiThanhLib.Real;
using RealDirectionEnum = PhuSaiThanhLib.Enum.RealDirectionEnum;

namespace InsideX.Controllers
{
    [InsideAuthorize]
    [CustomerAuthorize]
    public class CustomerController : Controller
    {
        #region view


        public ActionResult BuyLead()
        {
            return Buy(true);
        }

        public ActionResult RentLead()
        {
            return Rent(true);
        }

        public ActionResult Buy(bool isLead = false)
        {
            return NewIndex(CustomerTypeEnum.Buy, isLead);
        }

        public ActionResult Rent(bool isLead = false)
        {
            return NewIndex(CustomerTypeEnum.Rent, isLead);
        }

        public ActionResult NewIndex(CustomerTypeEnum type, bool isLead = false)
        {
            var db = new PstEntities();
            var dept = db.Departments.Where(c => c.IsDelete != true).ToList().Select(c => new SelectListItem {
                Text = c.Name,
                Value = c.Id.ToString()
            }).ToList();

            dept.Insert(0, new SelectListItem {
                Text = "== Chọn nhóm ==",
                Value = "0"
            });
            ViewBag.dept = dept;
            ViewBag.Type = type;
            ViewBag.IsLead = isLead;
            return View("NewIndex");
        }

        #endregion

        #region update
        public ActionResult UpdateBuy(int id = 0)
        {
            return Update(id, CustomerTypeEnum.Buy);
        }

        public ActionResult UpdateRent(int id = 0)
        {
            return Update(id, CustomerTypeEnum.Rent);
        }

        public ActionResult Update(int id = 0, CustomerTypeEnum type = CustomerTypeEnum.Buy)
        {
            var db = new PstEntities();
            var cus = new Customer() {
                Type = type.Value()
            };
            if (id > 0) {
                cus = db.Customers.FirstOrDefault(c => c.Id == id);
                if (cus == null)
                    return RedirectToAction(type.ToString());
                type = cus.Type.ToEnum<CustomerTypeEnum>();

                if (!MySessionManager.SessionData.IsAdmin && MySessionManager.SessionData.ID != cus.BrokerId)
                    return RedirectToAction(type.ToString());
            }


            var districtList = (cus.RequireDistrict ?? "").Split(';').Where(c => c.Length > 0).ToList();
            ViewBag.District = BoFactory.AddressBook.GetDistrictByCity(1).Select(c => new SelectListItem {
                Value = c.Id.ToString(),
                Text = c.DistrictName,
                Selected = districtList.Contains(c.Id.ToString())
            }).ToList();

            var area = cus.RequireArea.StringToListInt();
            ViewBag.Area = db.Ins_GetAreaListById("")
                .GroupBy(group => new { group.DistrictId, group.DistrictName })
                .Select(group =>
                    new GroupSelectListItem {
                        GroupId = group.Key.DistrictId.ToString(),
                        GroupName = group.Key.DistrictName,
                        SelectListItems = group.Select(c => new SelectListItem {
                            Text = c.Name,
                            Value = c.Id.ToString(),
                            Selected = area.Contains(c.Id)
                        }).ToList()
                    }


                ).ToList();


            ViewBag.Type = type;
            if (id == 0)
                return View("FirstUpdate", cus);
            return View("Update", cus);
        }

        [HttpPost]
        public ActionResult UpdateBuy(Customer model, InsertCustomerModel model2)
        {
            return Update(model, model2, CustomerTypeEnum.Buy);
        }

        [HttpPost]
        public ActionResult UpdateRent(Customer model, InsertCustomerModel model2)
        {
            return Update(model, model2, CustomerTypeEnum.Rent);
        }


        [HttpPost]
        public ActionResult Update(Customer model, InsertCustomerModel model2, CustomerTypeEnum type = CustomerTypeEnum.Buy)
        {
            //model.RequireDistrict = model2.RequireDistrict.ListIntToString(";");
            //model.RequireArea = model2.RequireArea.ListIntToString(";");
            model.RequireStreetView = model2.RequireStreetView.ListIntToString(";");
            model.RequireBdsType = model2.RequireBdsType.ListIntToString(";");
            model.RequireBdsStatus = model2.RequireBdsStatus.ListIntToString(";");
            model.RequireBdsStatusNow = model2.RequireBdsStatusNow.ListIntToString(";");
            model.RequireDirection = model2.RequireDirection.ListIntToString(";");

            ViewBag.Type = type;

            if (!ModelState.IsValid) {
                return View(model.Id == 0 ? "FirstUpdate" : "Update", model);
            }

            var db = new PstEntities();
            if (model.Id == 0) {
                model.CreatedDate = DateTime.Now;
                model.CreatorId = MySessionManager.SessionData.ID;
                model.BrokerId = MySessionManager.SessionData.IsAdmin ? model.BrokerId : MySessionManager.SessionData.ID;
                model.Type = type.Value();

                model.IsAllowEditCustomerInfo = true;
                model.IsAllowEditCustomerType = true;
                model.IsAllowEditNhuCau = true;
                model.IsAllowEditImage = true;

                db.Customers.Add(model);
                db.SaveChanges();
                return RedirectToAction("Update" + type, new { id = model.Id, type });
            }



            var customer = db.Customers.FirstOrDefault(c => c.Id == model.Id);
            if (customer == null)
                return RedirectToAction("Update" + type, new { type });

            //check quyền
            if (MySessionManager.SessionData.IsEmployer) {
                if (MySessionManager.SessionData.ID != customer.BrokerId) {
                    ModelState.AddModelError("", "Bạn không có quyền chỉnh sửa nội dung này.");
                    return View("Update", model);
                }
            }

            customer.UpdatedDate = DateTime.Now;
            customer.UpdatorId = MySessionManager.SessionData.ID;
            if (MySessionManager.SessionData.IsAdmin)
                customer.BrokerId = model.BrokerId;

            #region được thêm dt email nếu null
            if (string.IsNullOrEmpty(customer.Phone1)) customer.Phone1 = model.Phone1;
            if (string.IsNullOrEmpty(customer.Phone2)) customer.Phone2 = model.Phone2;
            if (string.IsNullOrEmpty(customer.Phone3)) customer.Phone3 = model.Phone3;

            if (string.IsNullOrEmpty(customer.Address1)) customer.Address1 = model.Address1;
            if (string.IsNullOrEmpty(customer.Address2)) customer.Address2 = model.Address2;
            if (string.IsNullOrEmpty(customer.Email)) customer.Email = model.Email;
            #endregion

            if (MySessionManager.SessionData.IsAdmin || customer.IsAllowEditCustomerInfo) {
                customer.PrefixName = model.PrefixName;
                customer.FullName = model.FullName;
                customer.Phone1 = model.Phone1;
                customer.Phone2 = model.Phone2;
                customer.Phone3 = model.Phone3;
                customer.Address1 = model.Address1;
                customer.Address2 = model.Address2;
                customer.Email = model.Email;
            }

            if (MySessionManager.SessionData.IsAdmin || customer.IsAllowEditCustomerType) {
                customer.Type2 = model.Type2;
                customer.Status = model.Status;
            }

            if (MySessionManager.SessionData.IsAdmin || customer.IsAllowEditNhuCau) {
                //customer.RequireDistrict = model.RequireDistrict;
                //customer.RequireArea = model.RequireArea;
                customer.RequireBdsStatus = model.RequireBdsStatus;
                customer.RequireBdsStatusNow = model.RequireBdsStatusNow;
                customer.RequireBdsType = model.RequireBdsType;
                customer.RequireDirection = model.RequireDirection;
                customer.RequireNote = model.RequireNote;
                //customer.RequireNote1 = model.RequireNote1;
                //customer.RequireNote2 = model.RequireNote2;
                //customer.RequireNote3 = model.RequireNote3;
                //customer.RequireNote4 = model.RequireNote4;
                //customer.RequireNote5 = model.RequireNote5;
                customer.RequireStreetView = model.RequireStreetView;

                customer.PriceFrom = model.PriceFrom;
                customer.PriceTo = model.PriceTo;
                customer.PriceUnit = model.PriceUnit;

                customer.RoomNumber = model.RoomNumber;
                customer.ToiletsNumber = model.ToiletsNumber;

                if (type == CustomerTypeEnum.Rent) {
                    customer.TimeLimit = model.TimeLimit;
                    customer.TimeUnit = model.TimeUnit;
                }
            }

            if (MySessionManager.SessionData.IsAdmin || customer.IsAllowEditImage) {
                customer.Image = model.Image;
                customer.ImageListCustomer = model.ImageListCustomer;
                customer.ImageListInfo = model.ImageListInfo;
            }

            customer.IsAllowEditCustomerInfo = false;
            customer.IsAllowEditCustomerType = false;
            customer.IsAllowEditNhuCau = false;
            customer.IsAllowEditImage = false;

            db.SaveChanges();
            return RedirectToAction(type.ToString());
        }
        #endregion

        #region list

        public JsonResult CustomerList(DatatableResponseRequest param,bool isLead, DateTime? dateFrom, DateTime? dateTo, int? type, int? type2, string bdsType, string customerStatus, int? deptId, int direction, int? roomNumber, decimal? price, string streetView, string keyword, int? brokerId)
        {
            if (dateFrom.HasValue)
                dateFrom = new DateTime(dateFrom.Value.Year, dateFrom.Value.Month, dateFrom.Value.Day, 0, 0, 1);

            if (dateTo.HasValue)
                dateTo = new DateTime(dateTo.Value.Year, dateTo.Value.Month, dateTo.Value.Day, 23, 59, 59);

            var strDirection = BasicHelper.Direction(direction);
           

            var start = param.Start + 1;

            var empList = "";
            if (MySessionManager.SessionData.IsAdmin)
                empList = "";
            else {
                if (isLead)
                    empList = MySessionManager.SessionData.IdsAccess();
                else
                    empList = MySessionManager.SessionData.ID.ToString();
            }

            var db = new PstEntities();
            var data = db.Ins_Customer_List(empList, brokerId, dateFrom, dateTo, type, type2, bdsType, customerStatus, deptId, strDirection, roomNumber, price, streetView, keyword, start, param.Length).ToList();
            var totalRow = data.Any() ? data[0].TotalRow ?? 0 : 0;
            var areaList = db.XAreas.ToList();
            return Json(new DatatableResponseModel<dynamic> {
                recordsTotal = totalRow,
                recordsFiltered = totalRow,
                data = data.Select(c => {
                    var strPrice = string.Format("{0}-{1}", c.PriceFrom ?? 0, c.PriceTo ?? 0);
                    var priceUnit = c.PriceUnit != null
                        ? string.Format("({0})", c.PriceUnit.ToEnum<PriceUnitEnum>().Text())
                        : "";

                    var a = new {
                        c.Id,
                        CustomerType2Enum = c.Type2.NumberToEnumToText<CustomerType2Enum>(),
                        c.Image,
                        c.FullName,
                        Phone = c.Phone1 + "<br/>" + c.Phone2 + "<br/>" + c.Phone3,
                        Phone1 = c.Phone1 ?? "",
                        Phone2 = c.Phone2 ?? "",
                        Phone3 = c.Phone3 ?? "",
                        Address1 = c.Address1 ?? "",
                        Address2 = c.Address2 ?? "",
                        CustomerPrefixNameEnum = c.PrefixName.NumberToEnumToText<CustomerPrefixNameEnum>(),

                        RequireStreetViewList = c.RequireStreetView.StringToListInt().Select(v => v.NumberToEnumToText<StreetTypeEnum>()).ToList().JoinToString(", "),
                        RequireBdsStatusNowList = c.RequireBdsStatusNow.StringToListInt().Select(v => v.NumberToEnumToText<RealStatusNowEnum>()).ToList().JoinToString(", "),
                        RequireBdsTypeList = c.RequireBdsType.StringToListInt().Select(v => v.NumberToEnumToText<RealTypeEnum>()).ToList().JoinToString(", "),
                        RequireDirectionList = c.RequireDirection.StringToListInt().Select(v => v.NumberToEnumToText<RealDirectionEnum>()).ToList().JoinToString(", "),
                        RequiredAreaList = GetArea(areaList, c.RequireArea.StringToListInt()),

                        RequireNote = c.RequireNote ?? "",
                        RequireNote1 = c.RequireNote1 ?? "",
                        RequireNote2 = c.RequireNote2 ?? "",
                        RequireNote3 = c.RequireNote3 ?? "",
                        RequireNote4 = c.RequireNote4 ?? "",
                        RequireNote5 = c.RequireNote5 ?? "",

                        c.RoomNumber,
                        PublicDate = c.PublicDate != null ? c.PublicDate.Value.ToString("dd/MM/yyyy") : "",
                        Price = string.Format("{0:F1} - {1:F1}", c.PriceFrom ?? 0, c.PriceTo ?? 0),
                        PriceUnit = c.Type == CustomerTypeEnum.Buy.Value() ? "tỷ" : "triệu",
                        c.EmpAvatar,
                        c.DisplayName,
                        c.Username,
                        CustomerStatusEnum = c.Status.NumberToEnumToText<CustomerStatusEnum>(),
                        PriceString = strPrice + " " + priceUnit,
                        CreatedDateString = c.CreatedDate.Value.ToString("dd/MM/yyyy"),
                        UpdatedDateString = c.UpdatedDate.HasValue ? c.UpdatedDate.Value.ToString("dd/MM/yyyy") : "",
                        ImageListInfoNumber = (c.ImageListInfo ?? "").Split(';').Count(v => v.Trim().Length > 0),
                        ImageListInfoView = string.Join(" ", (c.ImageListInfo ?? "").Split(';').Where(b => b.Length > 0).Select(v => string.Format("<a href='{0}'></a>", v.GetFullUrlImage2()))),
                        ImageListCustomerNumber = (c.ImageListCustomer ?? "").Split(';').Count(v => v.Trim().Length > 0),
                        ImageListCustomerView = string.Join(" ", (c.ImageListCustomer ?? "").Split(';').Where(b => b.Length > 0).Select(v => string.Format("<a href='{0}'></a>", v.GetFullUrlImage2()))),

                    };
                    return a;
                }).ToList<dynamic>()
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportToExcel(DateTime? dateFrom, DateTime? dateTo, int? type, int? type2, string bdsType, string customerStatus, int? deptId, int direction, int? roomNumber, decimal? price, string streetView, string keyword, int? brokerId)
        {
            /*
Mã số
Loại KH
Tình trạng
Họ và tên
SĐT
Địa chỉ
Email
Khu vực
Lưu ý
Mặt tiền/Hẻm
Lưu ý
Loại BĐS
Lưu ý
Hiện trạng
Lưu ý
Hướng
Lưu ý
Giá
Số phòng
Số toilet
Nhân viên
Ngày tạo
Ngày sửa
             
             
             */


            if (dateFrom.HasValue)
                dateFrom = new DateTime(dateFrom.Value.Year, dateFrom.Value.Month, dateFrom.Value.Day, 0, 0, 1);

            if (dateTo.HasValue)
                dateTo = new DateTime(dateTo.Value.Year, dateTo.Value.Month, dateTo.Value.Day, 23, 59, 59);

            var strDirection = direction.ToString();
            if (direction == 9)
                strDirection = "1,3,4,5";

            if (direction == 10)
                strDirection = "2,6,7,8";

            const int start = 0;

            var db = new PstEntities();
            var data = db.Ins_Customer_List(MySessionManager.SessionData.IsAdmin ? "" : MySessionManager.SessionData.ID.ToString(), brokerId, dateFrom, dateTo, type, type2, bdsType, customerStatus, deptId, strDirection, roomNumber, price, streetView, keyword, 0, 100000).ToList();
            var products = new DataTable();
            products.Columns.Add("Mã số", typeof(string));
            products.Columns.Add("Loại KH", typeof(string));
            products.Columns.Add("Tình trạng", typeof(string));
            products.Columns.Add("Họ và tên", typeof(string));
            products.Columns.Add("SĐT", typeof(string));
            products.Columns.Add("Địa chỉ", typeof(string));
            products.Columns.Add("Email", typeof(string));
            products.Columns.Add("Khu vực", typeof(string));
            products.Columns.Add("Lưu ý 1", typeof(string));
            products.Columns.Add("Mặt tiền/Hẻm", typeof(string));
            products.Columns.Add("Lưu ý 2", typeof(string));
            products.Columns.Add("Loại BĐS", typeof(string));
            products.Columns.Add("Lưu ý 3", typeof(string));
            products.Columns.Add("Hiện trạng", typeof(string));
            products.Columns.Add("Lưu ý 4", typeof(string));
            products.Columns.Add("Hướng", typeof(string));
            products.Columns.Add("Lưu ý 5", typeof(string));
            products.Columns.Add("Giá", typeof(string));
            products.Columns.Add("Số phòng", typeof(string));
            products.Columns.Add("Số toilet", typeof(string));
            products.Columns.Add("Nhân viên", typeof(string));
            products.Columns.Add("Ngày tạo", typeof(string));
            products.Columns.Add("Ngày sửa", typeof(string));



            var districts = db.MyDistricts.ToList();
            var areas = db.XAreas.ToList();
            var listIds = data.Select(c => c.Id).ToList();
            var cskh = db.CustomerCares.Where(c => listIds.Contains(c.CustomerId)).ToList();
            var proIds = cskh.Select(c => c.RealId).Distinct().ToList();
            var reals = db.Reals.Where(c => proIds.Contains(c.Id)).ToList();

            foreach (var item in data) {

                var dis = item.RequireDistrict.StringToListInt().Select(c => {
                    var d = districts.FirstOrDefault(v => v.Id == c);
                    if (d != null)
                        return d.DistrictName;
                    return string.Empty;
                }).Where(c => c.Length > 0).ToList();

                var are = item.RequireArea.StringToListInt().Select(c => {
                    var d = areas.FirstOrDefault(v => v.Id == c);
                    if (d != null)
                        return d.Name;
                    return string.Empty;
                }).Where(c => c.Length > 0).ToList();

                dis.AddRange(are);

                products.Rows.Add(
                    //Mã số
                    item.Id,
                    //Loại KH
                    item.Type.NumberToEnumToText<CustomerTypeEnum>(),
                    //Tình trạng
                    item.Status.NumberToEnumToText<CustomerStatusEnum>(),
                    //Họ và tên
                    string.Format("{0} {1}", item.PrefixName.NumberToEnumToText<CustomerPrefixNameEnum>(), item.FullName),
                    //SĐT
                    string.Format("{0} \n {1} \n {2}", item.Phone1, item.Phone2, item.Phone3),
                    //Địa chỉ
                    string.Format("{0} \n {1}", item.Address1, item.Address2),
                    //Email
                    string.Format("{0}", item.Email),
                    //Khu vực
                    string.Join(", ", dis),
                    //Lưu ý
                    item.RequireNote1,
                    //Mặt tiền/Hẻm
                    string.Join(", ", item.RequireStreetView.StringToListInt().Select(c => c.NumberToEnumToText<StreetTypeEnum>())),
                    //Lưu ý
                    item.RequireNote2,
                    //Loại BĐS
                    string.Join(", ", item.RequireBdsType.StringToListInt().Select(c => c.NumberToEnumToText<RealTypeEnum>())),
                    //Lưu ý
                    item.RequireNote3,
                    //Hiện trạng
                    string.Join(", ", item.RequireBdsStatusNow.StringToListInt().Select(c => c.NumberToEnumToText<RealStatusNowEnum>())),
                    //Lưu ý
                    item.RequireNote4,
                    //Hướng
                    string.Join(", ", item.RequireDirection.StringToListInt().Select(c => c.NumberToEnumToText<RealDirectionEnum>())),
                    //Lưu ý
                    item.RequireNote5,
                    //Giá
                    string.Format("{0} ~ {1} {2}", item.PriceFrom ?? 0, item.PriceTo ?? 0, item.Type == CustomerTypeEnum.Buy.Value() ? "tỷ" : "triệu"),
                    //Số phòng
                    item.RoomNumber,
                    //Số toilet
                    item.ToiletsNumber,
                    //Nhân viên
                    item.DisplayName,
                    //Ngày tạo
                    item.CreatedDate.HasValue ? item.CreatedDate.Value.ToString("dd/MM/yyyy HH:mm:ss") : "",
                    //Ngày sửa
                    item.UpdatedDate.HasValue ? item.UpdatedDate.Value.ToString("dd/MM/yyyy HH:mm:ss") : ""
                    );

                //kiem tra xem có cs hay ko
                var cs = db.Ins_CustomerCare_List_Buy_CusId(item.Id).ToList();
                if (cs.Any()) {
                    #region add title

                    products.Rows.Add(null, "Mã TS", "TS", "Giá", "Ghi chú", "Nhân viên hỗ trợ", "Ngày tạo");

                    #endregion

                    foreach (var care in cs)
                    {
                        products.Rows.Add(null, care.RealId, care.RealTitle,
                            string.Format("{0} {1}", care.Price.DecimalFormat("0"),
                                care.RealBuyOrRent == 1 ? "tỷ" : "triệu"), care.Note, care.DisplayName,
                            care.CreatedDate.HasValue ? care.CreatedDate.Value.ToString("dd/MM/yyyy") : "");
                    }
                    products.Rows.Add("");
                }




            }
            var grid = new GridView { DataSource = products };
            //grid.datai
            grid.DataBind();
            //grid.Rows[0].BackColor = Color.Yellow;
            for (int i = 0; i < grid.Rows.Count; i++) {
                grid.Rows[i].Cells[0].HorizontalAlign = HorizontalAlign.Center;
                grid.Rows[i].Cells[0].VerticalAlign = VerticalAlign.Middle;

                grid.Rows[i].Cells[1].HorizontalAlign = HorizontalAlign.Center;
                grid.Rows[i].Cells[1].VerticalAlign = VerticalAlign.Middle;

                //grid.Rows[i].Cells[4].Text = grid.Rows[i].Cells[4].Text.Replace("\n", "<br/>");
                //grid.Rows[i].Cells[5].Text = grid.Rows[i].Cells[5].Text.Replace("\n", "<br/>");
                for (int m = 0; m < grid.Rows[i].Cells.Count; m++)
                {
                    grid.Rows[i].Cells[m].Text = grid.Rows[i].Cells[m].Text.Replace("\n", "");
                    //grid.Rows[i].Cells[m].Text = grid.Rows[i].Cells[m].Text.Replace("\n", "<br/>");
                }


                if (string.IsNullOrEmpty(grid.Rows[i].Cells[0].Text) || grid.Rows[i].Cells[0].Text == "&nbsp;") {
                    grid.Rows[i].Cells[3].HorizontalAlign = HorizontalAlign.Left;

                    for (int j = 1; j <= 6; j++) {
                        //grid.Rows[i].Cells[j].BorderColor = Color.Blue;
                        //grid.Rows[i].Cells[j].HorizontalAlign = HorizontalAlign.Center;
                    }


                    if (i - 1 >= 0 && !string.IsNullOrEmpty(grid.Rows[i - 1].Cells[0].Text) && grid.Rows[i - 1].Cells[0].Text != "&nbsp;") {
                        for (int j = 1; j <= 6; j++) {
                            grid.Rows[i].Cells[j].Font.Bold = true;
                            grid.Rows[i].Cells[j].HorizontalAlign = HorizontalAlign.Center;
                        }
                    }
                }
                else {
                    grid.Rows[i].ForeColor = Color.Blue;
                    grid.Rows[i].BackColor = Color.Gainsboro;
                }
            }
            var name = "BDS_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss-tt");
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=" + name + ".xls");
            Response.ContentType = "application/ms-excel";
            Response.ContentEncoding = System.Text.Encoding.Unicode;
            Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

            Response.Charset = "";
            var sw = new StringWriter();
            var htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);

            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();

            return Content("");

        }

        #endregion

        #region Care
        public ActionResult CustomerDelete(int id)
        {
            if (MySessionManager.SessionData.IsAdmin) {

                var db = new PstEntities();
                var cus = db.Customers.FirstOrDefault(c => c.Id == id);
                if (cus != null) {
                    db.Customers.Remove(cus);
                    db.SaveChanges();
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json("Cần quyền admin để thực hiện thao tác này.", JsonRequestBehavior.AllowGet);
        }

        public ActionResult CustomerCare(int customerId = 0, bool isShow = true)
        {
            ViewBag.isShow = isShow;
            if (customerId > 0) {
                var db = new PstEntities();
                return PartialView(db.Ins_CustomerCare_List_Buy_CusId(customerId).ToList());
            }
            return Content("");
        }

        public ActionResult CustomerCare_OnlyShow(int customerId = 0)
        {
            return RedirectToAction("CustomerCare", new { customerId, isShow = false });
        }

        public ActionResult CareUpdate(int customerId, int id = 0)
        {
            var db = new PstEntities();
            CustomerCare care;
            if (id > 0) {
                care = db.CustomerCares.FirstOrDefault(c => c.Id == id);
            }
            else
                care = new CustomerCare { CustomerId = customerId, Date = DateTime.Now };

            return PartialView(care);
        }

        [HttpPost]
        public ActionResult CareUpdate(CustomerCare model)
        {
            if (ModelState.IsValid) {
                var db = new PstEntities();
                if (model.Id > 0) {
                    var care = db.CustomerCares.FirstOrDefault(c => c.Id == model.Id);
                    if (care != null) {
                        care.Date = model.Date;
                        care.Note = model.Note;
                        care.RealId = model.RealId;
                        care.UpdatedDate = DateTime.Now;
                        care.UpdatorId = MySessionManager.SessionData.ID;
                        care.CareImageLIst = model.CareImageLIst;
                    }
                }
                else {
                    db.CustomerCares.Add(model);
                }
                db.SaveChanges();
                ViewBag.Success = true;
            }
            return PartialView(model);
        }

        public ActionResult CareDelete(int id)
        {
            var db = new PstEntities();
            var care = db.CustomerCares.FirstOrDefault(c => c.Id == id);
            if (care != null) {
                db.CustomerCares.Remove(care);
                db.SaveChanges();
            }
            return Content("");
        }
        #endregion


        #region permission Nhân viên bds (nhân viên tạo)
        //id bds
        public ActionResult EditPermission(int id)
        {
            var db = new PstEntities();
            var model = db.Customers.FirstOrDefault(c => c.Id == id);
            if (model != null)
                return PartialView(model);
            return Content("Không tìm thấy bds");
        }

        [HttpPost]
        public ActionResult EditPermission(Customer model)
        {
            var db = new PstEntities();
            var real = db.Customers.FirstOrDefault(c => c.Id == model.Id);
            if (real != null) {

                real.IsAllowEditCustomerInfo = model.IsAllowEditCustomerInfo;
                real.IsAllowEditCustomerType = model.IsAllowEditCustomerType;
                real.IsAllowEditNhuCau = model.IsAllowEditNhuCau;
                real.IsAllowEditImage = model.IsAllowEditImage;

                db.SaveChanges();

                ViewBag.Success = true;
                return View(real);
            }
            ModelState.AddModelError("", "Không tìm thấy kh");
            return View(model);
        }


        #endregion

        public ActionResult ArealView(int id)
        {
            var db = new PstEntities();
            var cus = db.Customers.FirstOrDefault(c => c.Id == id);
            var areaIds = cus.RequireArea.StringToListInt();
            var areaList = db.Ins_GetAreaList().Where(c => areaIds.Contains(c.Id)).ToList();

            var dis = cus.RequireDistrict.StringToListInt();
            if (dis.Any()) {
                var listDis = db.MyDistricts.Where(c => dis.Contains(c.Id)).ToList();
                areaList.InsertRange(0, listDis.Select(c => new Ins_GetAreaList_Result {
                    DistrictId = c.Id,
                    DistrictName = c.DistrictName
                }).ToList());

            }
            ViewBag.Customer = cus;

            return PartialView(areaList);

        }

        string GetArea(List<XArea> listArea, List<int> listId)
        {
            if (!listArea.Any() || !listId.Any())
                return string.Empty;
            var list = listArea.Where(c => listId.Contains(c.Id)).Select(c => c.Name);
            return string.Join(", ", list);
        }


        public JsonResult DeleteArea(int id, int idArea)
        {
            var db = new PstEntities();
            var cus = db.Customers.FirstOrDefault(c => c.Id == id);
            var ids = cus.RequireArea.StringToListInt().Where(c => c != idArea);
            cus.RequireArea = string.Join(";", ids);
            db.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddArea(int id, int? areaId, int? districtId)
        {
            var db = new PstEntities();
            var cus = db.Customers.FirstOrDefault(c => c.Id == id);
            if (cus == null)
                return Json(true, JsonRequestBehavior.AllowGet);

            if (areaId.HasValue && areaId.Value > 0) {
                var areas = cus.RequireArea.StringToListInt().Where(c => c != areaId.Value).ToList();
                areas.Add(areaId.Value);
                cus.RequireArea = string.Join(";", areas);
            }
            else {
                if (districtId.HasValue) {
                    var districts = cus.RequireDistrict.StringToListInt().Where(c => c != districtId).ToList();
                    districts.Add(districtId.Value);
                    cus.RequireDistrict = string.Join(";", districts);
                }
            }



            db.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }


    }
}