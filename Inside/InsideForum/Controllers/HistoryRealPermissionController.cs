﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InsideX.EF;
using InsideX.Helper;
using InsideX.Models;
using MyUtility.Extensions;
using PhuSaiThanhLib;
using PhuSaiThanhLib.Enum;
using PhuSaiThanhLib.Real;

namespace InsideX.Controllers
{
    [AdminAuthorize]
    public class HistoryRealPermissionController : Controller
    {
        //
        // GET: /HistoryRealPermission/
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult List(DatatableResponseRequest param, RealBuyOrRentEnum buyOrRent, DateTime? dateFrom, DateTime? dateTo, int? realType, int? realStatus, int? streetView, int? realStatusNow, decimal? minPrice, decimal? MaxPrice, bool IsHotPrice, int? deptId, int? realDirection, int? roomNumber, int? brokerId, string keyword, bool isSold = false)
        {
            if (dateFrom.HasValue)
                dateFrom = new DateTime(dateFrom.Value.Year, dateFrom.Value.Month, dateFrom.Value.Day, 0, 0, 1);

            if (dateTo.HasValue)
                dateTo = new DateTime(dateTo.Value.Year, dateTo.Value.Month, dateTo.Value.Day, 23, 59, 59);

            var direction = realDirection.ToString();
            if (realDirection == 9)
                direction = "1,3,4,5";

            if (realDirection == 10)
                direction = "2,6,7,8";

            var start = param.Start + 1;

            var db = new PstEntities();
            var data = db.Ins_HistoryRealPermission_Search(brokerId, buyOrRent.Value(), realType, realStatus, realStatusNow, streetView, deptId, direction, dateFrom, dateTo, minPrice, MaxPrice, IsHotPrice, roomNumber, keyword, keyword.ToAlias(), isSold, start, param.Length).ToList();
            var totalRow = data.Any() ? data[0].TotalRow ?? 0 : 0;
            return Json(new DatatableResponseModel<dynamic> {
                recordsTotal = totalRow,
                recordsFiltered = totalRow,
                data = data.Select(c => new {
                    c.Id,
                    c.TempId,
                    c.PublicDate,
                    c.Status,
                    RealStatusEnum = c.Status.NumberToEnumToText<RealStatusEnum>(),
                    RealStatusNowEnum = (c.StatusNow ?? 2).NumberToEnumToText<RealStatusNowEnum>(),
                    c.StatusNow,
                    StatusNowPercent = c.StatusNowPercent.HasValue ? c.StatusNowPercent + " %" : "",
                    c.Type,
                    RealTypeEnum = c.Type.NumberToEnumToText<RealTypeEnum>(),
                    c.BuyOrRent,
                    RealBuyOrRentEnum = c.BuyOrRent.NumberToEnumToText<RealBuyOrRentEnum>(),
                    Price = (c.Price ?? new decimal(0)).ToString("0.##"),
                    c.PriceType,
                    c.PriceMonthType,
                    c.PriceSold,
                    c.PriceSoldType,
                    c.Title,
                    c.IsPriceHot,
                    c.Vip,
                    c.AddressNo,
                    c.DistrictId,
                    c.CityId,
                    c.WardId,
                    c.StreetId,
                    c.StreetType,
                    StreetTypeEnum = c.StreetType.NumberToEnumToText<StreetTypeEnum>(),
                    DuongRong = c.DuongRong ?? 0,
                    DienTich = c.DienTich ?? 0,
                    DienTichSuDung = c.DienTichSuDung ?? 0,
                    ChieuRong = c.ChieuRong ?? 0,
                    ChieuDai = c.ChieuDai ?? 0,
                    SoPhongNgu = c.SoPhongNgu ?? 0,
                    SoWc = c.SoWc ?? 0,
                    c.KetCau,
                    SoMatTien = c.SoMatTien ?? 0,
                    c.Direction,
                    DirectionString = c.Direction.NumberToEnumToText<RealDirectionEnum>(),
                    c.LegalStatus,
                    c.HinhDangDat,
                    RealHinhDangDatEnum = c.HinhDangDat.NumberToEnumToText<RealHinhDangDatEnum>(),
                    HinhDangDatNumber = c.HinhDangDatNumber ?? 0,
                    //c.ContactName,
                    //c.ContactPhone,
                    //c.ContactAddress,
                    //c.ContactResource,
                    //c.ContactType,
                    //c.HoaHongBan,
                    //c.HoaHongBanUnitType,
                    //c.HoaHongThue,
                    //c.Note1,
                    //c.Note2,
                    //c.Note3,
                    //c.Note4,
                    //c.Note5,
                    //c.Note6,
                    //c.Note7,
                    //c.Note8,
                    //c.Note9,
                    //c.Note10,
                    UocLuongGiaBan = c.UocLuongGiaBan ?? 0,
                    GiaNha = c.GiaNha ?? 0,
                    GiaTriDat = (c.GiaTriDat ?? 0).DecimalFormat(),
                    c.BrokerId,
                    c.Description,
                    Hinh = c.Hinh.GetFullUrlImage2(),
                    //c.HinhNoiThat,
                    //c.HinhChiTiet,
                    //c.HinhGiayTo,
                    //c.CreatedDate,
                    //c.CreatedBy,
                    //c.UpdatedDate,
                    //c.UpdatedBy,
                    c.Keyword,
                    c.ViewCount,
                    c.Deleted,
                    c.DistrictName,
                    c.CityName,
                    c.WardName,
                    c.RowId,
                    c.TotalRow,
                    c.StreetName,
                    c.Username,
                    c.DisplayName,
                    c.Avatar,
                    UnitTypeString = c.BuyOrRent == RealBuyOrRentEnum.Buy.Value() ? "tỷ" : "triệu",
                    _GiaNha = (((decimal?)c.DienTichSuDung ?? 0) * (c.GiaNha ?? 0)).ToString("F2"),
                    //CreatedDateString = c.CreatedDate.Value.ToString("dd/MM/yy"),
                    //UpdatedDateString = c.UpdatedDate.HasValue ? c.UpdatedDate.Value.ToString("dd/MM/yy") : "",
                    //HinhNoiThatNumber = (c.HinhNoiThat ?? "").Split(';').Count(v => v.Trim().Length > 0),
                    //HinhChiTietNumber = (c.HinhChiTiet ?? "").Split(';').Count(v => v.Trim().Length > 0),
                    //HinhGiayToNumber = (c.HinhGiayTo ?? "").Split(';').Count(v => v.Trim().Length > 0),
                    //HinhNoiThatView = string.Join(" ", (c.HinhNoiThat ?? "").Split(';').Where(b => b.Length > 0).Select(v => string.Format("<a href='{0}'></a>", v.GetFullUrlImage2()))),
                    //HinhChiTietView = string.Join(" ", (c.HinhChiTiet ?? "").Split(';').Where(b => b.Length > 0).Select(v => string.Format("<a href='{0}'></a>", v.GetFullUrlImage2()))),
                    //HinhGiayToView = string.Join(" ", (c.HinhGiayTo ?? "").Split(';').Where(b => b.Length > 0).Select(v => string.Format("<a href='{0}'></a>", v.GetFullUrlImage2())))
                    HistoryCreatedDateString = c.HistoryCreatedDate.ToString("dd/MM/yy HH:mm:ss"),
                    HistoryUpdatedDateString = c.HistoryUpdatedDate.ToString("dd/MM/yy HH:mm:ss"),
                    HistoryTimeLimitString = c.HistoryTimeLimit.ToString("dd/MM/yy HH:mm:ss"),
                    Time = (c.HistoryTimeLimit - c.HistoryUpdatedDate).TotalMinutes,
                    Role = Role(c)
                }).ToList<dynamic>()
            }, JsonRequestBehavior.AllowGet);
        }

        public string Role(Ins_HistoryRealPermission_Search_Result item)
        {
            var list = new List<string>();

            if (item.HistoryIsViewImageGiayTo)
                list.Add("Xem giấy tờ");
            if (item.HistoryIsViewImageDetail)
                list.Add("Xem chi tiết");
            if (item.HistoryIsViewAddress)
                list.Add("Xem địa chỉ");
            if (item.HistoryIsViewContact)
                list.Add("Xem liên hệ");
            if (item.HistoryIsViewBroker)
                list.Add("Xem nhân viên");

            return string.Join("<br/>", list);
        }
    }
}