﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BussinessObject;
using EntitiesObject.Entities.WebEntities;
using InsideX.EF;
using InsideX.Helper;
using InsideX.Models;
using Article = EntitiesObject.Entities.WebEntities.Article;

namespace InsideX.Controllers
{
    [ValidateInput(false)]
    [AdminAuthorizeAttribute]
    public class ArticleController : Controller
    {
        [InsideAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        [InsideAuthorize]
        [HttpPost]
        public JsonResult SearchArticle(int category, string title, int? status, string accountDisplayName, string dateFrom, string dateTo, int currentPage = 1)
        {
            int itemsPerPage = TinyConfiguration.PageSize;
            int totalRecord = 0;
            var startPos = (currentPage - 1) * itemsPerPage;
            if (string.IsNullOrEmpty(dateFrom) || string.IsNullOrEmpty(dateTo))
            {
                dateFrom = "01/01/1970";
                dateTo = dateFrom;
            }
            DateTime stardatetime = string.IsNullOrEmpty(dateFrom) ? DateTime.MinValue : dateFrom.Trim().ToDateTimeParseExact("dd/MM/yyyy").StartOfDate();
            DateTime enddatetime = string.IsNullOrEmpty(dateTo) ? DateTime.MinValue : dateTo.Trim().ToDateTimeParseExact("dd/MM/yyyy").EndOfDate();
            var myList = BoFactory.Article.Article_List_Paging(category, -1, accountDisplayName, title, status ?? 0, -1, -1, -1, -1, -1, -1, -1, stardatetime, enddatetime, startPos, itemsPerPage, out  totalRecord);

            var myListView = myList.Select(x => new
                    {
                        x.RowNumber,
                        Title = x.Title,
                        Summary = x.Summary,
                        x.AccountID,
                        PublishDate = x.PublishDate.ToVNdate("dd/MM/yyyy HH:mm"),
                        CreateDate = x.CreateDate.ToVNdate("dd/MM/yyyy HH:mm"),
                        Status = x.Status >= 3 ? false : true,
                        x.ID,
                        x.AccountDisplayName,
                        CategoryName = HttpUtility.HtmlDecode(x.CategoryName)
                    }
                );


            if (myList == null || (myList != null && myList.Count == 0))
            {
                return Json(new { code = -1, message = "Không có dữ liệu" });
            }

            return Json(new { code = 1, data = myListView, totalRecord = totalRecord, currentPage = currentPage, itemsPerPage = itemsPerPage });
        }

        #region Create

        [HttpGet]
        [InsideAuthorize]
        public ActionResult CreateArticle()
        {
            return View(new ArticleModel());
        }

        [InsideAuthorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateArticle(ArticleModel model)
        {

            if (!ModelState.IsValid)
            {
                ViewBag.VALID = "Vui lòng kiểm tra dữ liệu vừa nhập";
                return View(model);
            }

            Article article = new Article();
            article.Title = model.Title;
            article.Status = model.Status;

            #region Info
            article.CategoryID = model.CategoryID;
            article.AccountType = 1;
            article.Avatar = model.Avatar;
            article.Summary = model.Summary;
            article.Content = model.Content;
            article.Status = model.Status;
            article.IsComment = model.IsComment;
            article.IsCommentFacebook = model.IsCommentFacebook;
            article.IsFollow = model.IsFollow;
            article.IsShare = model.IsShare;
            article.IsLikeFacebook = model.IsLikeFacebook;
            article.IsNotify = model.IsNotify;
            article.IsHot = model.IsHot;
            article.ViewType = model.IsGuildView ? (short)1 : (short)0;
            #endregion

            #region SEO

            article.MetaTitle = model.MetaTitle;
            article.MetaKeyword = model.MetaKeyword;
            article.MetaDescription = model.MetaDescription;
            article.OgTitle = model.OgTitle;
            article.OgDescription = model.OgDescription;
            article.OgUrl = model.OgUrl;
            article.OgImage = model.OgImage;
            article.TwitterTitle = model.TwitterTitle;
            article.TwitterDescription = model.TwitterDescription;
            article.TwitterUrl = model.TwitterUrl;
            article.TwitterImage = model.TwitterImage;
            article.MetaH1 = (model.MetaH1);
            article.MetaH2 = (model.MetaH2);
            article.MetaH3 = (model.MetaH3);

            #endregion

            #region other

            article.AccountID = MySessionManager.SessionData.ID;
            article.AccountDisplayName = MySessionManager.SessionData.DisplayName;
            article.ModifyBy = MySessionManager.SessionData.ID;
            article.PublishDate = model.PublishDate.ToDateTimeParseExact("dd/MM/yyyy HH:mm");
            article.CreateDate = DateTime.Now;
            article.ModifyDate = DateTime.Now;

            #endregion

            if (BoFactory.Article.Article_Insert(article) > 0)
            {
                ModelState.Clear();
                ViewBag.SUCCESS = "Tạo mới thành công";
                return View(new ArticleModel());
            }

            ViewBag.ERROR = "Có lỗi xảy ra, vui lòng thử lại sau";
            return View(model);
        }
        #endregion

        #region Edit

        [HttpGet]
        [InsideAuthorize]
        public ActionResult EditArticle(int id = 0)
        {
            if (id == 0)
            {
                return RedirectToAction("Index");
            }

            var myCategoryCheck = BoFactory.Article.Article_GetById(id);

            if (myCategoryCheck == null || (myCategoryCheck.ID <= 0))
            {
                return RedirectToAction("Index");
            }

            return View(DecodeHTML(myCategoryCheck));
        }

        [InsideAuthorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditArticle(ArticleModel model, int? id)
        {

            if (!ModelState.IsValid)
            {
                ViewBag.VALID = "Vui lòng kiểm tra dữ liệu vừa nhập";
                return View(model);
            }

            if ((id ?? 0) <= 0)
            {
                return RedirectToAction("Index");
            }

            var myArticleCheck = BoFactory.Article.Article_GetById(id.GetValueOrDefault(0));

            if (myArticleCheck == null || (myArticleCheck != null && myArticleCheck.ID <= 0))
            {
                return RedirectToAction("Index");
            }

            if (model.IsShowEmployee && id > 0)
            {
                var db = new PstEntities();
                var ar = db.Articles.FirstOrDefault(c => c.ID == id);
                if (ar != null)
                {
                    ar.EmpName = model.EmpName;
                    ar.EmpPhone = model.EmpPhone;
                    ar.EmpText = model.EmpText;
                    ar.IsShowEmployee = model.IsShowEmployee;
                    db.SaveChanges();
                }
            }

            Article article = new Article();

            article.ID = id.GetValueOrDefault(0);
            article.Title = model.Title;
            article.Status = model.Status;

            #region Info
            article.CategoryID = model.CategoryID;
            article.AccountType = 1;
            article.Title = model.Title;
            article.Avatar = model.Avatar;
            article.Summary = model.Summary;
            article.Content = model.Content;
            article.Status = model.Status;
            article.IsComment = model.IsComment;
            article.IsCommentFacebook = model.IsCommentFacebook;
            article.IsFollow = model.IsFollow;
            article.IsShare = model.IsShare;
            article.IsLikeFacebook = model.IsLikeFacebook;
            article.IsNotify = model.IsNotify;
            article.IsHot = model.IsHot;
            article.ViewType = model.IsGuildView ? (short)1 : (short)0;
            #endregion

            #region SEO

            article.MetaTitle = model.MetaTitle;
            article.MetaKeyword = model.MetaKeyword;
            article.MetaDescription = model.MetaDescription;
            article.OgTitle = model.OgTitle;
            article.OgDescription = model.OgDescription;
            article.OgUrl = model.OgUrl;
            article.OgImage = model.OgImage;
            article.TwitterTitle = model.TwitterTitle;
            article.TwitterDescription = model.TwitterDescription;
            article.TwitterUrl = model.TwitterUrl;
            article.TwitterImage = model.TwitterImage;
            article.MetaH1 = (model.MetaH1);
            article.MetaH2 = (model.MetaH2);
            article.MetaH3 = (model.MetaH3);

            #endregion

            #region other

            article.AccountID = MySessionManager.SessionData.ID;
            article.ModifyBy = MySessionManager.SessionData.ID;
            article.PublishDate = model.PublishDate.ToDateTimeParseExact("dd/MM/yyyy HH:mm");
            article.CreateDate = DateTime.Now;
            article.ModifyDate = DateTime.Now;

            #endregion

            if (BoFactory.Article.Article_UpdateContent(article))
            {
                ModelState.Clear();
                ViewBag.SUCCESS = "Cập nhật thành công";
                return View(model);
            }

            ViewBag.ERROR = "Có lỗi xảy ra, vui lòng thử lại sau";
            return View(model);
        }
        #endregion

        #region Delete
        [InsideAuthorize]
        [HttpPost]
        public JsonResult Delete(int? id)
        {

            if ((id ?? 0) <= 0)
            {
                return Json(new { code = -1, message = "Bài viết không tồn tại" });
            }

            var list = new List<int> { 0, 3, 4, 5, 7, 8, 9, 11 };
            if (list.Contains(id ?? 0))
            {

                return Json(new { code = -1, message = "Bài này không được xóa" });
            }


            var myArticleCheck = BoFactory.Article.Article_GetById((int)id);

            if (myArticleCheck == null || (myArticleCheck != null && myArticleCheck.ID <= 0))
            {
                return Json(new { code = -2, message = "Bài viết không tồn tại" });
            }

            if (!BoFactory.Article.Article_EditStatus(myArticleCheck.ID, MySessionManager.SessionData.ID, 1, -1))
            {
                return Json(new { code = -3, message = "Có lỗi xảy ra, vui lòng thử lại sau" });
            }

            return Json(new { code = 1 });
        }
        #endregion

        private ArticleModel DecodeHTML(Ins_Article_GetById_Result item)
        {
            return new ArticleModel
            {
                ID = item.ID,
                CategoryID = item.CategoryID,
                Title = item.Title,
                Avatar = item.Avatar,
                Summary = item.Summary,
                Content = item.Content,
                Status = item.Status,

                IsComment = item.IsComment,
                IsCommentFacebook = item.IsCommentFacebook,
                IsFollow = item.IsFollow,
                IsShare = item.IsShare,
                IsLikeFacebook = item.IsLikeFacebook,
                IsNotify = item.IsNotify,
                IsHot = item.IsHot,
                IsGuildView = item.ViewType == 1,

                MetaTitle = item.MetaTitle,
                MetaKeyword = item.MetaKeyword,
                MetaDescription = item.MetaDescription,
                OgTitle = item.OgTitle,
                OgDescription = item.OgDescription,
                OgUrl = item.OgUrl,
                OgImage = item.OgImage,
                TwitterTitle = item.TwitterTitle,
                TwitterDescription = item.TwitterDescription,
                TwitterUrl = item.TwitterUrl,
                TwitterImage = item.TwitterImage,
                MetaH1 = item.MetaH1,
                MetaH2 = item.MetaH2,
                MetaH3 = item.MetaH3,
                PublishDate = item.PublishDate.ToString("dd/MM/yyyy HH:mm:ss"),
                IsShowEmployee = item.IsShowEmployee == true,
                EmpName = item.EmpName,
                EmpPhone = item.EmpPhone,
                EmpText = item.EmpText
            };
        }
    }
}