﻿using System.Web.Optimization;

namespace InsideX
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.IgnoreList.Clear();
            AddDefaultIgnorePatterns(bundles.IgnoreList);

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            #region css            
            bundles.Add(new StyleBundle("~/content/login-css").Include(
                      "~/content/css/bootstrap.min.css",
                      "~/content/css/font-awesome.min.css",
                      "~/content/css/nprogress.css",
                      "~/content/css/animate.min.css",
                      "~/content/css/custom.min.css",
                      "~/content/css/site.css"));

            bundles.Add(new StyleBundle("~/content/master-css").Include(
                      "~/content/css/bootstrap.min.css",
                      "~/content/css/font-awesome.min.css",
                      "~/content/css/nprogress.css",
                      "~/content/css/icheck.green.css",
                      "~/content/css/bootstrap-progressbar-3.3.4.min.css",
                      "~/content/css/daterangepicker.css",
                      "~/content/css/custom.min.css",
                      "~/content/css/site.css"));
            #endregion

            #region js
            bundles.Add(new ScriptBundle("~/bundles/login-js").Include(
                        "~/scripts/jquery.min.js",
                        "~/scripts/bootstrap.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/master-js").Include(
                        "~/scripts/jquery.min.js",
                        "~/scripts/bootstrap.min.js",
                        "~/scripts/fastclick.js",
                        "~/scripts/nprogress.js",
                        "~/scripts/bootstrap-progressbar.min.js",
                        "~/scripts/icheck.min.js",
                        "~/scripts/moment.js",
                        "~/scripts/daterangepicker.js",
                        "~/scripts/angular.min.js",
                        "~/scripts/ui-bootstrap-tpls-0.11.0.js",
                        "~/scripts/custom.js",
                        "~/scripts/angular-sanitize.js",
                        "~/scripts/function.js"));

            bundles.Add(new ScriptBundle("~/bundles/ckeditor-js").Include(
                        "~/content/ckeditor/ckeditor.js",
                        "~/content/ckfinder/ckfinder.js"));
            #endregion
        }

        public static void AddDefaultIgnorePatterns(IgnoreList ignoreList)
        {
            if (ignoreList == null)
                throw new System.ArgumentNullException("ignoreList");
            ignoreList.Ignore("*.intellisense.js");
            ignoreList.Ignore("*-vsdoc.js");
            ignoreList.Ignore("*.debug.js", OptimizationMode.WhenEnabled);
            //ignoreList.Ignore("*.min.js", OptimizationMode.WhenDisabled);
            //ignoreList.Ignore("*.min.css", OptimizationMode.WhenDisabled);
        }
    }
}
