﻿using System.ComponentModel.DataAnnotations;

namespace InsideX.Models
{
    public class CategoryModel
    {
        public int ID { get; set; }
        public int? GameID { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập")]
        [StringLength(250, MinimumLength = 4, ErrorMessage = "Tên chuyên mục phải từ {2} đến {1} ký tự")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập")]
        [Range(0, short.MaxValue, ErrorMessage = "Thứ tự không đúng")]
        public short? Position { get; set; }

        public bool IsActive { get; set; }

        public bool IsUserAccess { get; set; }

        #region SEO

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string MetaTitle { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string MetaDescription { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string MetaKeyword { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string OgTitle { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string OgDescription { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string OgUrl { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string OgImage { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string TwitterTitle { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string TwitterDescription { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string TwitterUrl { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string TwitterImage { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string MetaH1 { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string MetaH2 { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string MetaH3 { get; set; }

        #endregion
    }
}