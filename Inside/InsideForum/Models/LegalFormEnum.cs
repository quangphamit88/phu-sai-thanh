﻿using System.ComponentModel;

namespace InsideX.Models
{
    public enum LegalFormEnum
    {
        [Description("Sổ hồng")]
        SoHong = 1,

        [Description("Sổ đỏ")]
        SoDo = 2,

        [Description("Chưa có sổ")]
        ChuaCoSo = 3
    }
}