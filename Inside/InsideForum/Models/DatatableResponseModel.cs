﻿using System.Collections.Generic;

namespace InsideX.Models
{
    public class DatatableResponseModel<T>
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<T> data { get; set; }
    }
}