﻿namespace InsideX.Models
{
    public enum HistoryPageEnum
    {

        BdsView = 20,
        BdsSale = 21,
        BdsCreate = 22,
        BdsEdit = 23,
        BdsDelete = 24,
        BdsViewExcel = 25,
        BdsViewSaleExcel = 26,

        KhView = 30,
        KhCreate = 31,
        KhEdit = 32, 
        KhDelete = 33,
        KhViewExcel = 34,

    }
}