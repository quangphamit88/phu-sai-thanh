﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BussinessObject.Bo.WebBo;

namespace InsideX.Models
{
    public class ContactConfigModel : SystemConfigBo.ContactConfig
    {
        public string Name1 { get; set; }
        public string Name2 { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }

    }
}