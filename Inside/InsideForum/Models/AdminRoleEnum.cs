﻿using System.ComponentModel;

namespace InsideX.Models
{
    public enum AdminRoleEnum
    {
        [Description("Quản lý")]
        Admin = 1,

        //[Description("Manager")]
        //Manager = 2,

        [Description("Nhân viên")]
        Employer = 3
    }
}