﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace InsideX.Models.Real
{
    public class CreateRealModel
    {
        //[Required(ErrorMessage = "Vui lòng nhập tiêu đề.")]
        //[MaxLength(60, ErrorMessage = "Tối đa 60 ký tự.")]
        public string Title { get; set; }
        public int Id { get; set; }

        //[Required]
        public int Status { get; set; }

        [Required]
        public string AddressNo { get; set; }

        [Required]
        [Range(1, 1000, ErrorMessage = "Chưa chọn data")]
        public int CityId { get; set; }

        [Required]
        [Range(1, 1000, ErrorMessage = "Chưa chọn data")]
        public int DistrictId { get; set; }

        [Required]
        [Range(1, 1000, ErrorMessage = "Chưa chọn data")]
        public int WardId { get; set; }

        [Required]
        [Range(1, 1000, ErrorMessage = "Chưa chọn data")]
        public int StreetId { get; set; }

        [Required]
        [Range(1, 1000, ErrorMessage = "Chưa chọn data")]
        public int BrokerId { get; set; }
    }
}