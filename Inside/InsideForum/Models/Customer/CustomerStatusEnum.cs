﻿using System.ComponentModel;

namespace InsideX.Models.Customer
{
    public enum CustomerStatusEnum
    {
        [Description("Chưa xem tài sản")]
        NotView = 1,

        [Description("Đã xem tài sản")]
        Viewed = 2,

        [Description("Đã mua")]
        Bought = 3
    }
}