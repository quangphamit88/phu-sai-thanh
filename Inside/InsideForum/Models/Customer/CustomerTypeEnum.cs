﻿using System.ComponentModel;

namespace InsideX.Models.Customer
{
    public enum CustomerTypeEnum
    {
        [Description("Khách hàng mua")]
        Buy = 1,

        [Description("Khách hàng thuê")]
        Rent = 2,
    }
}