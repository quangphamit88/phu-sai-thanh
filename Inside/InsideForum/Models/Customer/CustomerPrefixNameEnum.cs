﻿
using System.ComponentModel;

namespace InsideX.Models.Customer
{
    public enum CustomerPrefixNameEnum
    {
        [Description("None")]
        None = 0,

        [Description("Anh")]
        Anh = 1,

        [Description("Chị")]
        Chi = 2,

        [Description("Cô")]
        Co = 3,

        [Description("Chú")]
        Chu = 4,

        [Description("Bác")]
        Bac = 5,
        
        [Description("Ông")]
        Ong = 6,

        [Description("Bà")]
        Ba=7

    }
}