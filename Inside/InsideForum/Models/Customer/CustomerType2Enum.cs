﻿using System.ComponentModel;

namespace InsideX.Models.Customer
{
    public enum CustomerType2Enum
    {
        //[Description("KH1")]
        //Kh1 = 1,

        //[Description("KH2")]
        //Kh2 = 2,

        [Description("KH1")]
        Kh1 = 1,
        [Description("KH2")]
        Kh2 = 2,
        [Description("KH3")]
        Kh3 = 3,
        [Description("ĐT1")]
        Dt1 = 4,
        [Description("ĐT2")]
        Dt2 = 5,
        [Description("ĐT3")]
        Dt3 = 6,
        [Description("MG")]
        Mg = 7,
        [Description("Cò")]
        Co = 8
    }
}