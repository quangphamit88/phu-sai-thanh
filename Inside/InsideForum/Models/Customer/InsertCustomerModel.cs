﻿
using System.Collections.Generic;

namespace InsideX.Models.Customer
{
    public class InsertCustomerModel
    {


        //public List<int> RequireDistrict { get; set; }
        //public List<int> RequireArea { get; set; }
        public List<int> RequireStreetView { get; set; }
        public List<int> RequireBdsType { get; set; }
        public List<int> RequireBdsStatus { get; set; }
        public List<int> RequireBdsStatusNow { get; set; }
        public List<int> RequireDirection { get; set; }
    }
}