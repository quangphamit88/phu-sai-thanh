﻿using System.ComponentModel;

namespace InsideX.Models.Customer
{
    public enum TimeUnitEnum
    {
        [Description("Tháng")]
        Month = 1,

        [Description("Năm")]
        Year = 12
    }
}