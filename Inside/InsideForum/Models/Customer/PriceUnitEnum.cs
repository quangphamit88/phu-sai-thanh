﻿using System.ComponentModel;

namespace InsideX.Models.Customer
{
    public enum PriceUnitEnum
    {

        [Description("Triệu")]
        Million = 1000000,

        [Description("Tỷ")]
        Billion = 1000000000
    }
}