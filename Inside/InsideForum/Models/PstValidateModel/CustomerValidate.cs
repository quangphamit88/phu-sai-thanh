﻿using System;
using System.ComponentModel.DataAnnotations;

namespace InsideX.Models.PstValidateModel
{
    public  class CustomerValidate
    {
        //public int Id { get; set; }
        //public int? Type { get; set; }
        //public DateTime? CreatedDate { get; set; }
        //public int? CreatorId { get; set; }
        //public int PrefixName { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập tên")]
        public string FullName { get; set; }

        //public string Phone1 { get; set; }
        //public string Phone2 { get; set; }
        //public string Phone3 { get; set; }
        //public string Address1 { get; set; }
        //public string Address2 { get; set; }

        [EmailAddress]
        public string Email { get; set; }
        //public DateTime? PublicDate { get; set; }
        //public int Type2 { get; set; }
        //public int Status { get; set; }
        //[Required(ErrorMessage = "Vui lòng nhập ")]
        //public string RequireDistrict { get; set; }
        //[Required(ErrorMessage = "Vui lòng nhập ")]
        //public string RequireArea { get; set; }
        //public string RequireNote1 { get; set; }
        //public string RequireNote2 { get; set; }
        //public string RequireNote3 { get; set; }
        //public string RequireNote4 { get; set; }
        //public string RequireStreetView { get; set; }
        //public string RequireBdsType { get; set; }
        //public string RequireBdsStatus { get; set; }
        //[Required(ErrorMessage = "Vui lòng nhập ")]
        //public string RequireBdsStatusNow { get; set; }
        //public string RequireDirection { get; set; }
        //public decimal PriceFrom { get; set; }
        //[Required(ErrorMessage = "Vui lòng nhập ")]
        //public decimal PriceTo { get; set; }
        //public int PriceUnit { get; set; }
        //public int? TimeLimit { get; set; }
        //public int? TimeUnit { get; set; }
        //public int? RoomNumber { get; set; }
        //public int? ToiletsNumber { get; set; }
        //public string Image { get; set; }
        //public string ImageListCustomer { get; set; }
        //public string ImageListInfo { get; set; }
        //public int? BrokerId { get; set; }

        //public DateTime? UpdatedDate { get; set; }
        //public int? UpdatorId { get; set; }
    }
}