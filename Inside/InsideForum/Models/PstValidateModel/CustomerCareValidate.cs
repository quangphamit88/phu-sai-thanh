﻿using System;
using System.ComponentModel.DataAnnotations;

namespace InsideX.Models.PstValidateModel
{
    public class CustomerCareValidate
    {
        [Required]
        public int CustomerId { get; set; }

        public DateTime? Date { get; set; }
        public string Note { get; set; }

        //[Required(ErrorMessage = "Nhập # tài sản")]
        [Range(1, 20000, ErrorMessage = "Nhập #mã tài sản. Vd: 565")]
        public string RealId { get; set; }

        [Required(ErrorMessage = "Chọn nhân viên")]
        [Range(1, 20000, ErrorMessage = "Chọn nhân viên")]
        public int? BrockerId { get; set; }

    }
}