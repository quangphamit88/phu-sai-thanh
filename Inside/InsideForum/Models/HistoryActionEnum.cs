﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InsideX.Models
{
    public enum HistoryActionEnum
    {
        View = 1,
        Create = 2,
        Edit = 3,
        ExportToExcel = 4
    }
}