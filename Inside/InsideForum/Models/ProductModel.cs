﻿using System.ComponentModel.DataAnnotations;

namespace InsideX.Models
{
    public class ProductModel
    {
        

        public ProductModel()
        {
            Id = 0;
            Image = string.Empty;
        }
        //public int CategoryId { get; set; }

        #region 
        public int Id { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập")]
        [StringLength(250, MinimumLength = 3, ErrorMessage = "Tiêu đề phải từ {2} đến {1} ký tự")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập")]
        public int Type { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập")]
        public string SoNha { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập")]
        public string Duong { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập")]
        public string PhuongXa { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập")]
        public string QuanHuyen { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập")]
        public string TinhTp { get; set; }

        public bool ViTriMatTien { get; set; }
        //public bool ViTriTrungTam { get; set; }
        public bool HemXeHoi { get; set; }
        //public string HemRong { get; set; }


        #region Thong tin lien he

        [Required(ErrorMessage = "Vui lòng nhập")]
        public string Phone { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập")]
        public string HomeName { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập")]
        [EmailAddress(ErrorMessage = "Email không đúng định dạng")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập")]
        public string Address { get; set; }
        public string ThongTinMoiGioi { get; set; }
        public bool ChinhChu { get; set; }
        #endregion

        #region Đặc điểm sản phẩm

        [Required(ErrorMessage = "Vui lòng nhập")]
        public int? PhongNgu { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập")]
        public int? ChieuRong { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập")]
        public int? ChieuDai { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập")]
        public int? Toilet { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập")]
        public int? Level { get; set; }

        public int? KetCau { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập")]
        public int? DtSuDung { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập")]
        public int? DtDat { get; set; }

        //public bool CoSanThuong { get; set; }
        //public bool DaCoSo { get; set; }
        //public bool CuTienXayMoi { get; set; }
        public string Huong { get; set; }

        #endregion

        #region Thông tin giá bán

        public int LoaiBds { get; set; }
        public int Status { get; set; }
        [Required(ErrorMessage = "Vui nhập giá")]
        [MaxLength(13, ErrorMessage = "Vui lòng nhập đúng giá")]
        public decimal? Price { get; set; }

        public decimal? PhiMoiGioi { get; set; }
        public bool LockTransition { get; set; }
        public bool IsHot { get; set; }
        public bool IsNew { get; set; }
        public string DatePublic { get; set; }
        public string Description { get; set; }
        #endregion

        #region Vi tri va hinh anh
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string Image { get; set; }
        public string ImageDetail { get; set; }

        public string Content { get; set; }
        #endregion

        #endregion

        #region SEO

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string MetaTitle { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string MetaDescription { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string MetaKeyword { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string OgTitle { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string OgDescription { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string OgUrl { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string OgImage { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string TwitterTitle { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string TwitterDescription { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string TwitterUrl { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string TwitterImage { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string MetaH1 { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string MetaH2 { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string MetaH3 { get; set; }


        #endregion


    }
}