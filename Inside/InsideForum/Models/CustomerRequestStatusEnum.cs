﻿using System.ComponentModel;

namespace InsideX.Models
{
    public enum CustomerRequestStatusEnum
    {
        [Description("Chưa xem tài sản")]
        GoiDien=1,
        [Description("Đã xem tài sản")]
        DaDan=2,
        [Description("Đã mua")]
        HoTro=3
    }
}