﻿using System.ComponentModel.DataAnnotations;

namespace InsideX.Models
{
    public class ArticleModel
    {
        public int ID { get; set; }

        public int CategoryID { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập")]
        [StringLength(250, MinimumLength = 3, ErrorMessage = "Tiêu đề phải từ {2} đến {1} ký tự")]
        public string Title { get; set; }

        //[Required(ErrorMessage = "Vui lòng nhập")]
        //[StringLength(250, ErrorMessage = "Avatar phải từ {2} đến {1} ký tự")]
        public string Avatar { get; set; }


        [Required(ErrorMessage = "Vui lòng nhập")]
        [StringLength(1000, ErrorMessage = "Tóm tắt phải từ {2} đến {1} ký tự")]
        public string Summary { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập")]
        public string Content { get; set; }

        public short Status { get; set; }

        public bool IsComment { get; set; }

        public bool IsCommentFacebook { get; set; }

        public bool IsFollow { get; set; }

        public bool IsShare { get; set; }

        public bool IsLikeFacebook { get; set; }

        public bool IsNotify { get; set; }

        public bool IsHot { get; set; }
                
        public bool IsGuildView { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập")]
        public string PublishDate { get; set; }

        public bool IsShowEmployee { get; set; }
        public string EmpName { get; set; }
        public string EmpPhone { get; set; }
        public string EmpText { get; set; }

        #region SEO

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string MetaTitle { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string MetaDescription { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string MetaKeyword { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string OgTitle { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string OgDescription { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string OgUrl { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string OgImage { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string TwitterTitle { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string TwitterDescription { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string TwitterUrl { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string TwitterImage { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string MetaH1 { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string MetaH2 { get; set; }

        [StringLength(1000, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string MetaH3 { get; set; }

        #endregion
    }
}