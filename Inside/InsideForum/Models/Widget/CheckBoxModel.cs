﻿namespace InsideX.Models.Widget
{
    public class CheckBoxModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }

        public bool Selected { get; set; }

        public string Text { get; set; }
    }
}