﻿namespace InsideX.Models
{
    public class CommentModel
    {
        public int ID { get; set; }

        public string Content { get; set; }

        public int ArticleID { get; set; }
    }
}