﻿using System.ComponentModel.DataAnnotations;

namespace InsideX.Models
{
    public class AdminLoginModel
    {
        [Required(ErrorMessage = "Vui lòng nhập Tên đăng nhập")]
        [StringLength(20, MinimumLength = 4, ErrorMessage = "Tên tài khoản phải từ {2} đến {1} ký tự")]
        public string LoginName { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập Mật khẩu")]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "Mật khẩu phải từ {2} đến {1} ký tự")]
        public string Password { get; set; }
    }

    public class AdminChangePasswordModel
    {
        [Required(ErrorMessage = "Vui lòng nhập")]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "Mật khẩu phải từ {2} đến {1} ký tự")]
        public string PasswordOld { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập")]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "Mật khẩu phải từ {2} đến {1} ký tự")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập")]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "Mật khẩu phải từ {2} đến {1} ký tự")]
        [Compare("Password", ErrorMessage = "Nhập lại mật khẩu không đúng")]
        public string ConfirmPassword { get; set; }
    }

    public class AdminCreateModel
    {
        [Required(ErrorMessage = "Vui lòng nhập")]
        [StringLength(20, MinimumLength = 4, ErrorMessage = "Tên tài khoản phải từ {2} đến {1} ký tự")]
        [RegularExpression(@"([A-Za-z0-9])\w+", ErrorMessage = "Tên đăng nhập không đúng")]
        public string LoginName { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập")]
        [StringLength(250, ErrorMessage = "Số ký tự vượt quá giới hạn qui định")]
        public string DisplayName { get; set; }

        //[Required(ErrorMessage = "Vui lòng nhập")]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "Mật khẩu phải từ {2} đến {1} ký tự")]
        public string Password { get; set; }

        //[Required(ErrorMessage = "Vui lòng nhập")]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "Mật khẩu phải từ {2} đến {1} ký tự")]
        [Compare("Password", ErrorMessage = "Nhập lại mật khẩu không đúng")]
        public string ConfirmPassword { get; set; }

        public string AvatarUrl { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public bool IsActive { get; set; }

        public int RoleId { get; set; }

        public int Id { get; set; }
    }
}