﻿using System.ComponentModel;

namespace InsideX.Models
{
    public enum UnitEnum
    {
        [Description("%")]
        Percent = 0,

        [Description("Triệu")]
        Million = 1000,

        [Description("Tỷ")]
        Billion = 1000000,
    }
}