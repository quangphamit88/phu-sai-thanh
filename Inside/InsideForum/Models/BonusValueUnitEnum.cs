﻿using System.ComponentModel;

namespace InsideX.Models
{
    public enum BonusValueUnitEnum
    {
        [Description("%")]
        PhanTram=0,
        [Description("triệu")]
        Trieu=1000,
        [Description("tỷ")]
        Ty=1000000
    }
}