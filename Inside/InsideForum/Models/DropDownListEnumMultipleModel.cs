﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace InsideX.Models
{
    public class DropDownListEnumMultipleModel
    {
        public List<SelectListItem> SelectListItems { get; set; }

        public string Name { get; set; }
        public string Class { get; set; }

        public List<GroupSelectListItem> GroupSelectListItems { get; set; }
    }

    public class GroupSelectListItem
    {
        public List<SelectListItem> SelectListItems { get; set; }
        public string GroupName { get; set; }
        public string GroupId { get; set; }
    }
}