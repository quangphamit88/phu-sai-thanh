﻿using System;
using System.ComponentModel.DataAnnotations;
using InsideX.EF;

namespace InsideX.Models.Dept
{
    public class DepartmentModel : Department
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Nhập tên nhóm")]
        public string Name { get; set; }

        public int Type { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public bool? IsDelete { get; set; }
        public string Note { get; set; }
    }
}