﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InsideX.Models.Dept;
using MyUtility.Extensions;
using Newtonsoft.Json;
using PhuSaiThanhLib;
using PhuSaiThanhLib.Enum;
using PhuSaiThanhLib.Real;

namespace InsideX.Models
{
    public static class TinyHelper
    {

        public static List<SelectListItem> ToSelectListItems(this List<EnumToList> list, int selected = 0, string firstValue = null, string firstText = null)
        {
            var listItems = list.Select(c => new SelectListItem {
                Text = c.Value,
                Value = c.Key.ToString(),
                Selected = c.Key == selected
            }).ToList();

            if (!string.IsNullOrEmpty(firstText) && !string.IsNullOrEmpty(firstValue)) {
                listItems.Insert(0, new SelectListItem {
                    Text = firstText,
                    Value = firstValue
                });
            }
            return listItems;
        }

        public static string BoolToYesNo(this bool? value)
        {
            if (value.HasValue && value.Value)
                return "yes";
            return "no";
        }

        public static List<SelectListItem> DropdownListProductType(int selected = 0, string defaultText = "Loại BĐS")
        {
            var productType = typeof(RealTypeEnum).ToList().Select(c => new SelectListItem() {
                Text = c.Value,
                Value = c.Key.ToString(),
                Selected = selected == c.Key
            }).ToList();

            productType.Insert(0, new SelectListItem {
                Text = defaultText,
                Value = "-1"
            });


            return productType;
        }

        public static List<SelectListItem> DropdownListDeptType(int selected = 0, string defaultText = "")
        {
            var productType = typeof(DeptTypeEnum).ToList().Select(c => new SelectListItem() {
                Text = c.Value,
                Value = c.Key.ToString(CultureInfo.InvariantCulture),
                Selected = selected == c.Key
            }).Where(c => c.Value != "0").ToList();

            //productType.Insert(0, new SelectListItem
            //{
            //    Text = defaultText,
            //    Value = "-1"
            //});

            return productType;
        }

        public static List<SelectListItem> DropdownListSaleStatus(int selected = 0, bool useDefault = true, string defaultText = "=== Trạng thái ===", string defaultValue = "-1")
        {
            var saleOrRentEnum = typeof(RealStatusEnum).ToList().OrderBy(c => {
                switch (c.Key) {
                    case 5:
                        return 1;
                    case 1:
                        return 2;
                    case 6:
                        return 3;
                    case 4:
                        return 4;
                    case 2:
                        return 5;
                    case 3:
                        return 6;
                    default:
                        return 10;

                }
            }).Select(c => new SelectListItem() {
                Text = c.Value,
                Value = c.Key.ToString(),
                Selected = selected == c.Key
            }).ToList();

            if (useDefault)
                saleOrRentEnum.Insert(0, new SelectListItem {
                    Text = defaultText,
                    Value = defaultValue
                });


            return saleOrRentEnum;
        }

        public static List<SelectListItem> ToSelectList(this Enum e, string selected = "")
        {
            var kaka = typeof(Enum).ToList().Select(c => new SelectListItem() {
                Text = c.Value,
                Value = c.Key.ToString(),
                Selected = selected == c.Value
            }).ToList();
            return kaka;
        }

        public static List<SelectListItem> ToMultiSelectList(this Enum e, List<string> selected)
        {
            if (selected == null)
                selected = new List<string>();
            var kaka = typeof(Enum).ToList().Select(c => new SelectListItem() {
                Text = c.Value,
                Value = c.Key.ToString(),
                Selected = selected.Contains(c.Value)
            }).ToList();
            return kaka;
        }

        public static Int32 ToIn32(this string text, int defaultNumber = 0)
        {
            Int32.TryParse(text, out defaultNumber);
            return defaultNumber;
        }
        public static Int32 ToIn32(this long text, int defaultNumber = 0)
        {
            return text.ToString(CultureInfo.InvariantCulture).ToIn32(defaultNumber);
        }

        public static string ToJson(this object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        public static T JsonToModel<T>(this string str)
        {
            return JsonConvert.DeserializeObject<T>(str);
        }

        public static bool KeyInListString(this string key, string listStr, char spe = ';')
        {
            listStr = listStr ?? string.Empty;
            var list = listStr.Split(spe).Where(c => c.Length > 0);
            return list.Contains(key);
        }

        //public static List<SelectListItem> DdlBroker(int selected)
        //{
        //    var list = BoFactory.BrokerAccount.SearchAutoComplete("") ?? new List<Ins_BrokerAccount_SearchAutoComplete_Result>();
        //    var rt = list.Select(c => new SelectListItem {
        //        Value = c.AdminId.ToString(),
        //        Text = c.DisplayName,
        //        Selected = c.AdminId == selected
        //    }).ToList();
        //    rt.Insert(0, new SelectListItem() { Text = "== Chọn nhân viên ==", Value = "0" });
        //    return rt;
        //}

        public static string ListIntToString(this List<int> list, string sep = ";")
        {
            if (list == null)
                return "";
            return string.Join(sep, list);
        }

        public static List<int> StringToListInt(this string str, char sep = ';')
        {
            if (str == null)
                return new List<int>();
            return str.Split(sep).Select(c => c.Trim()).Where(c => c.Length > 0).Select(c => c.ToIn32()).ToList();
        }

        //public static List<SelectListItem> StringToDdlListMulti(this string str, Enum eEnum, char sep = ';')
        //{
        //    var list = str.StringToListInt(sep).Select(c => c.ToString()).ToList();
        //    return eEnum.ToMultiSelectList(list);
        //}

        //public static string StringToEnumText(this string str, Enum eEnum, char sep = ';',string join=";")
        //{
        //    if (string.IsNullOrEmpty(str))
        //        return "";

        //    var listE = typeof(eEnum).ToList();
        //    var listSelected = str.StringToListInt(sep);
        //    var listReturn = listSelected.Select(c =>
        //     {
        //         var tem = listE.FirstOrDefault(v => v.Key == c);
        //         return tem == null ? c.ToString(CultureInfo.InvariantCulture) : tem.Value;
        //     }).ToList();

        //    return string.Join(join, listReturn);
        //}

        public static string JoinToString(this List<string> list, string sep = ";")
        {
            return string.Join(sep, list);
        }

        public static string ToStringDefault(this DateTime? date, string patern, string defaultText = "")
        {
            return date == null ? defaultText : date.Value.ToString(patern);
        }


        public static string FirstLetterAlias(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return string.Empty;

            var list = str.Split(' ').Select(c => c.Trim()).Where(c => !string.IsNullOrEmpty(c)).ToList();
            return String.Join("", list.Select(c => c.Substring(0, 1)));

        }

        public static dynamic ToDynamic<T>(this T obj)
        {
            //var a=  obj.ToJson().JsonToModel<dynamic>();
            dynamic expando = new ExpandoObject();
            dynamic a = JsonConvert.DeserializeObject(obj.ToJson());

            //expando["aa"] = 3;
            foreach (var propertyInfo in typeof(T).GetProperties()) {
                var currentValue = propertyInfo.GetValue(obj);
                //expando.Add(propertyInfo.Name, currentValue);
                //a[propertyInfo.Name] = currentValue;
                //expando["aaa"] = 33;
            }
            return a as dynamic;
        }

        public static string DecimalFormat(this decimal? money, string defaultString = "")
        {
            if (money.HasValue)
                return money.Value.DecimalFormat();
            return defaultString;
        }

        public static string DecimalFormat(this decimal money)
        {
            return money.ToString("0.##");
        }

        public static List<string> ToImageList(this string str, bool isFullUrl = false, char separator = ';')
        {
            if (string.IsNullOrEmpty(str))
                return new List<string>();

            var list = str.Split(separator).Where(c => c.Length > 0).ToList();
            if (isFullUrl)
                return list.Select(c => c.GetFullUrlImage()).ToList();
            return list;
        }

    }
}