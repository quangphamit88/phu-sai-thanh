﻿namespace InsideX.Models
{
    public class DatatableResponseRequest
    {
        public int Start { get; set; }
        public int Length { get; set; }
        public string Search { get; set; }
    }
}