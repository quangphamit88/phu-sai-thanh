﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using InsideX.EF;
using InsideX.Models;
using InsideX.Models.Customer;
using InsideX.Models.Dept;
using Microsoft.Ajax.Utilities;
using MyUtility.Extensions;
using Newtonsoft.Json;
using PhuSaiThanhLib.Enum;
using PhuSaiThanhLib.Real;
using RealTypeEnum = PhuSaiThanhLib.Real.RealTypeEnum;

namespace InsideX.Helper
{
    public static class PstHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="brokerId"></param>
        /// <param name="roleEnum">AdminRoleEnum</param>
        /// <param name="employeeType">EmployeeTypeEnum</param>
        /// <param name="isInsertFirst">True = [== Chọn nhân viên ==]</param>
        /// <returns></returns>
        public static List<SelectListItem> BrokerList(this int brokerId, int roleEnum, int employeeType, bool isInsertFirst = true)
        {
            var db = new PstEntities();
            var list = db.Employees.Where(c => (
                (roleEnum == 0 || c.Role == roleEnum) &&
                (employeeType == 0 || c.Type == employeeType)) && c.Deleted != true).OrderBy(c => c.Username).ToList();
            var result = list.Select(c => new SelectListItem {
                Text = string.Format("{0} ({1})", c.DisplayName, c.Username),
                Value = c.ID.ToString(),
                Selected = c.ID == brokerId
            }).ToList();
            if (isInsertFirst)
                result.Insert(0, new SelectListItem() { Value = "0", Text = "== Chọn nhân viên ==" });
            return result;
        }

        public static List<SelectListItem> DropdownListCity(int cityId = 0)
        {
            var db = new PstEntities();
            var list = db.Cities.OrderBy(c=>c.CityName).ToList();
            var result = list.Select(c => new SelectListItem {
                Text = string.Format("{0}", c.CityName),
                Value = c.Id.ToString(),
                Selected = c.Id == cityId
            }).ToList();
            result.Insert(0, new SelectListItem() { Value = "0", Text = "== Chọn tỉnh/TP ==" });
            return result;
        }

        public static List<SelectListItem> DropdownListDistrict(int cityId, int dictrictId = 0)
        {
            var db = new PstEntities();
            var list = cityId > 0 ? db.MyDistricts.Where(c => c.CityId == cityId).OrderBy(c=>c.DistrictName).ToList() : new List<MyDistrict>();
            var result = list.Select(c => new SelectListItem {
                Text = string.Format("{1}", c.Id, c.DistrictName),
                Value = c.Id.ToString(),
                Selected = c.Id == dictrictId
            }).ToList();
            result.Insert(0, new SelectListItem() { Value = "0", Text = "== Chọn quận/huyện ==" });
            return result;
        }

        public static List<SelectListItem> DropdownListWard(int dictrictId, int wardId = 0)
        {
            var db = new PstEntities();
            var list = dictrictId > 0 ? db.MyWards.Where(c => c.DistrictId == dictrictId).OrderBy(c=>c.WardName).ToList() : new List<MyWard>();
            var result = list.Select(c => new SelectListItem {
                Text = string.Format("{1}", c.Id, c.WardName),
                Value = c.Id.ToString(),
                Selected = c.Id == wardId
            }).ToList();
            result.Insert(0, new SelectListItem() { Value = "0", Text = "== Chọn phường/xã ==" });
            return result;
        }

        public static List<SelectListItem> DropdownListStreet(int dictrictId, int streetId = 0)
        {
            var db = new PstEntities();
            var list = dictrictId > 0 ? db.MyStreets.Where(c => c.DistrictId == dictrictId).OrderBy(c=>c.StreetName).ToList() : new List<MyStreet>();
            var result = list.Select(c => new SelectListItem {
                Text = string.Format("{1}", c.Id, c.StreetName),
                Value = c.Id.ToString(),
                Selected = c.Id == streetId
            }).ToList();
            result.Insert(0, new SelectListItem() { Value = "0", Text = "== Chọn đường ==" });
            return result;
        }

        public static List<SelectListItem> DropdownListArea(int dictrictId, int areaId = 0)
        {
            var db = new PstEntities();
            var list = dictrictId > 0 ? db.XAreas.Where(c => c.DistrictId == dictrictId).OrderBy(c=>c.Name).ToList() : new List<XArea>();
            var result = list.Select(c => new SelectListItem {
                Text = string.Format("{1}", c.Id, c.Name),
                Value = c.Id.ToString(),
                Selected = c.Id == areaId
            }).ToList();
            result.Insert(0, new SelectListItem() { Value = "0", Text = "== Chọn khu vực ==" });
            return result;
        }

        public static bool IsExistAddress(string addressNo, int streetId, int wardId)
        {
            var db = new PstEntities();
            var statusList = new int[] { RealStatusEnum.DangBan.Value(), RealStatusEnum.DangChoThue.Value() };
            //var item =
            //    db.Reals.Where(
            //        c =>
            //            statusList.Contains(c.Id) && c.AddressNo == addressNo && c.WardId == wardId &&
            //            c.StreetId == streetId);
            return db.Reals.FirstOrDefault(c => statusList.Contains(c.Status) && c.AddressNo == addressNo && c.WardId == wardId && c.StreetId == streetId) != null;
        }

        public static Dictionary<int, string> EmployeeAvatarDic(AdminRoleEnum adminType, int empType = 0)
        {
            var db = new PstEntities();
            var a = db.Employees.Where(c => empType == 0 || c.Type == empType).ToDictionary(v => v.ID, b => b.Avatar);
            return a;
        }

        public static string EmployeeAvatarJson(AdminRoleEnum adminType, int empType = 0)
        {
            return JsonConvert.SerializeObject(EmployeeAvatarDic(adminType, empType));
        }

        public static List<EnumToList> GetListRealStatusEnum(RealBuyOrRentEnum buyOrRent)
        {
            if (buyOrRent == RealBuyOrRentEnum.Buy)
                return typeof(RealStatusBuyEnum).ToList();
            return typeof(RealStatusRentEnum).ToList();
        }

        public static List<EnumToList> GetListRealTypeEnum(RealBuyOrRentEnum buyOrRent)
        {
            if (buyOrRent == RealBuyOrRentEnum.Buy)
                return typeof(RealTypeBuyEnum).ToList();
            return typeof(RealTypeEnum).ToList();
        }

        public static List<EnumToList> GetListRealTypeEnum(CustomerTypeEnum buyOrRent)
        {
            if (buyOrRent == CustomerTypeEnum.Buy)
                return typeof(RealTypeBuyEnum).ToList();
            return typeof(RealTypeEnum).ToList();
        }


        public static List<SelectListItem> DeptList(int deptType = 0, int deptId = 0, bool isInsertFirst = true)
        {
            var db = new PstEntities();
            var list = db.Departments.Where(c => ((deptType == 0 || c.Type == deptType)) && c.IsDelete != true).OrderBy(c => c.Name).ToList();
            var result = list.Select(c => new SelectListItem {
                Text = string.Format("{0} ({1})", c.Name, c.Type.NumberToEnumToText<DeptType2Enum>()),
                Value = c.Id.ToString(),
                Selected = c.Id == deptId
            }).ToList();
            if (isInsertFirst)
                result.Insert(0, new SelectListItem() { Value = "0", Text = "== Chọn nhóm ==" });
            return result;
        }

        public static string ToUnitBuyOrRent(this int buyOrRent)
        {
            if (buyOrRent == 1)
                return " tỷ";
            return " triệu";
        }
    }
}