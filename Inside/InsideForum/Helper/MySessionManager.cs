﻿using System.Collections.Generic;
using System.Linq;
using EntitiesObject.Entities.WebEntities;
using System.Web;
using InsideX.EF;
using InsideX.Models;
using InsideX.Models.Customer;
using MyUtility.Extensions;
using PhuSaiThanhLib.Enum;

namespace InsideX.Helper
{
    public class MySessionManager
    {
        private const string UserSessionKey = "UserData";
        private const string PermissionKey = "PermissionKey";

        public static UserData SessionData
        {
            get
            {
                if (HttpContext.Current.Session[UserSessionKey] == null)
                    return null;

                var checkUserData = HttpContext.Current.Session[UserSessionKey] as UserData;

                if (checkUserData == null)
                    return null;

                if (checkUserData.ID <= 0)
                    return null;

                if (checkUserData.Sign != MyUtility.Common.MD5_encode(string.Format("{0}{1}{2}{3}", checkUserData.ID, checkUserData.LoginName, checkUserData.DisplayName, TinyConfiguration.KeySign)))
                    return null;

                return checkUserData;
            }
            set
            {
                HttpContext.Current.Session[UserSessionKey] = value;
            }
        }

        public static bool IsAuthen
        {
            get { return SessionData != null; }
        }

        public static void SignOut()
        {
            // ReSharper disable once RedundantCheckBeforeAssignment
            if (SessionData != null)
                SessionData = null;
        }

        public static void ClearAllSession()
        {
            HttpContext.Current.Session.Abandon();
        }
    }

    public class UserData
    {
        public int ID { get; set; }
        public AdminRoleEnum RoleId { get; set; }
        public string LoginName { get; set; }
        public string DisplayName { get; set; }
        public string Avatar { get; set; }
        public string Sign { get; set; }

        public EmployeeTypeEnum EmployeeTypeEnum { get { return Employee.Type.ToEnum<EmployeeTypeEnum>(); } }

        public bool IsAdmin { get { return RoleId == AdminRoleEnum.Admin; } }
        public bool IsSupperAdmin { get { return RoleId == AdminRoleEnum.Admin && ID == 38; } }
        //public bool IsManager { get { return RoleId == AdminRoleEnum.Manager; } }
        public bool IsEmployer { get { return RoleId == AdminRoleEnum.Employer; } }
        public bool IsMaster { get { return LoginName.ToLower() == "master"; } }
        public bool IsTuVan { get { return LoginName.ToLower() == "tuvankhachhang"; } }

        public EF.Employee Employee { get; set; }
        


        public bool IsLeaderOrSubLeader
        {
            get { return Employee.DepartmentDetails.Any(c => c.IsLeader || c.IsSubLeader); }
        }


        public bool IsBds { get { return EmployeeTypeEnum == EmployeeTypeEnum.Real; } }
        public bool IsCustomer { get { return EmployeeTypeEnum == EmployeeTypeEnum.Customer; } }

        public List<int> IdsAllowAccess()
        {
            var db = new PstEntities();
            if (IsAdmin)
                return new List<int>();
            ////if (IsLeader)
            ////    return db.Employees.Where(c => c.Role == 3 && c.DeptId == Employee.DeptId && c.Deleted != true && c.ID != ID).Select(c => c.ID).ToList();
            ////if (IsSubLeader) {
            ////    var list = db.Employees.Where(c => c.Role == 3 && c.DeptId == Employee.DeptId && c.Deleted != true && !c.IsLeader && !c.IsSubLeader && c.ID != ID).Select(c => c.ID).ToList();
            ////    list.Add(ID);
            //return GetIdAccessForLeader(db,ID);
            return GetIdsByEmp(Employee);
            //    return list;
            //}
            return new List<int> { ID };
        }

        public string IdsAccess(string spe = ",")
        {
            if (IsAdmin)
                return "";
            return string.Join(spe, IdsAllowAccess());
        }

        public LevelReal GetLevelReal()
        {
            var db = new PstEntities();
            var id = string.Format("{0}{1}", Employee.Type, Employee.Level).ToIn32();

            var data = db.LevelReals.FirstOrDefault(c => c.Id == id);
            return data ?? new LevelReal();
        }

        //public List<int> GetIdAccessForLeader(PstEntities db, int employeeId)
        //{
        //    var list = new List<int>() { employeeId };
        //    var em = db.Employees.FirstOrDefault(c => c.ID == employeeId);
        //    if (em == null)
        //        return list;

        //    if (em.DepartmentDetails.Any(c => c.IsLeader)) {
        //        foreach (var departmentDetail in em.DepartmentDetails.Where(c => c.IsLeader)) {
        //            list.AddRange(departmentDetail.Department.DepartmentDetails.Select(c => c.EmployeeId));
        //            if (departmentDetail.IsLeader || departmentDetail.IsSubLeader)
        //                list.AddRange(GetIdAccessForLeader(db, departmentDetail.EmployeeId));
        //        }
        //    }
        //    if (em.DepartmentDetails.Any(c => c.IsSubLeader)) {
        //        foreach (var departmentDetail in em.DepartmentDetails.Where(c => c.IsSubLeader)) {
        //            list.AddRange(departmentDetail.Department.DepartmentDetails.Where(c => !c.IsLeader).Select(c => c.EmployeeId));
        //            if (departmentDetail.IsLeader || departmentDetail.IsSubLeader)
        //                list.AddRange(GetIdAccessForLeader(db, departmentDetail.EmployeeId));
        //        }
        //    }


        //    return list.Distinct().ToList();
        //}

        public List<int> GetIdsByEmp(Employee employee)
        {
            var list = new List<int> { employee.ID };
            var leads = employee.DepartmentDetails.Where(c => c.IsLeader || c.IsSubLeader).ToList();
            foreach (var detail in leads) {
                var listEmp = new List<Employee>();

                if (detail.IsLeader)
                    listEmp.AddRange(detail.Department.DepartmentDetails.Select(c => c.Employee).ToList());

                if (detail.IsSubLeader)
                    listEmp.AddRange(detail.Department.DepartmentDetails.Where(c => !c.IsLeader).Select(c => c.Employee).ToList());

                var child = listEmp.Where(c => !list.Contains(c.ID)).ToList();

                list.AddRange(listEmp.Select(c => c.ID).ToList());

                foreach (var emp in child.Where(c => c.DepartmentDetails.Any(v => v.IsLeader || v.IsSubLeader))) {
                    list.AddRange(GetIdsByEmp(emp));
                }
            }

            return list.Distinct().ToList();

        }

    }
}