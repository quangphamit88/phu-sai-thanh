﻿using System;
using System.Globalization;
using System.Web;

namespace InsideX.Helper
{
    public static class DataTypeHelper
    {
        public static string HtmlEncode(this String html)
        {
            return HttpUtility.HtmlEncode(html);
        }

        public static string HtmlDecode(this String html)
        {
            return HttpUtility.HtmlDecode(html);
        }
        /// <summary>
        /// dd/MM/yyyy
        /// </summary>
        public static string ToVNdate(this DateTime? date, string format = "dd/MM/yyyy")
        {
            if (date.HasValue)
                return date.Value.ToString(format);
            return "";
        }

        public static DateTime ToDateTimeParseExact(this string text, string format = "dd/MM/yyyy HH:mm:ss")
        {
            return DateTime.ParseExact(text, format, CultureInfo.InvariantCulture);
        }

        public static DateTime StartOfDate(this DateTime dt)
        {
            return new DateTime(dt.Year, dt.Month, dt.Day);
        }

        public static DateTime EndOfDate(this DateTime dt)
        {
            return new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 59);
        }

    }
}