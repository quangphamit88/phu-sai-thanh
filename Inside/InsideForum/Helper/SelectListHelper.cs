﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using BussinessObject;
using BussinessObject.Enum;
using MyUtility.Extensions;

namespace InsideX.Helper
{
    public class SelectListHelper
    {
        #region Status

        public static List<SelectListCustom> ArticleStatus(SelectListCustom first = null)
        {
            List<SelectListCustom> myList = new List<SelectListCustom>();

            if (first != null)
            {
                myList.Add(first);
            }

            var list = typeof(ArticleStatusEnum).ToList();

            myList.AddRange(list.Select(dt => new SelectListCustom
            {
                Value = dt.Key.ToString(),
                Text = dt.Value
            }));

            return myList;
        }

        public static List<SelectListCustom> ProductStatus(SelectListCustom first = null)
        {
            List<SelectListCustom> myList = new List<SelectListCustom>();

            if (first != null)
            {
                myList.Add(first);
            }

            var list = typeof(ArticleStatusEnum).ToList();

            myList.AddRange(list.Select(dt => new SelectListCustom
            {
                Value = dt.Key.ToString(),
                Text = dt.Value
            }));

            return myList;
        }

        #endregion

        #region Category

        public static List<SelectListCustom> Category(SelectListCustom first = null)
        {
            List<SelectListCustom> myList = new List<SelectListCustom>();

            if (first != null)
            {
                myList.Add(first);
            }

            var list = BoFactory.Category.Category_GetAll();

            if (list == null || (list != null && list.Count == 0))
            {
                return myList;
            }

            myList.AddRange(list.Select(dt => new SelectListCustom
            {
                Value = dt.ID.ToString(),
                Text = HttpUtility.HtmlDecode(dt.Name)
            }));

            return myList;
        }

        #endregion
    }

    public class SelectListCustom
    {
        public string Value { get; set; }
        public string Text { get; set; }
    }
}