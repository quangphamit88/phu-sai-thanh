﻿using System;
using System.Web;
using System.Web.Mvc;
using InsideX.Models;
using MyUtility.Extensions;
using PhuSaiThanhLib.Enum;

namespace InsideX.Helper
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class InsideAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null) throw new ArgumentNullException("httpContext");
            if (MySessionManager.SessionData == null)
                return false;
            return true;
        }
    }

    public class AdminAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null) throw new ArgumentNullException("httpContext");
            if (MySessionManager.SessionData == null)
                return false;
            
            return MySessionManager.SessionData.RoleId == AdminRoleEnum.Admin;
        }
    }

    public class RealAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null) throw new ArgumentNullException("httpContext");
            if (MySessionManager.SessionData == null)
                return false;

            return MySessionManager.SessionData.RoleId == AdminRoleEnum.Admin||MySessionManager.SessionData.Employee.Type==EmployeeTypeEnum.Real.Value();
        }
    }

    public class CustomerAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null) throw new ArgumentNullException("httpContext");
            if (MySessionManager.SessionData == null)
                return false;

            return MySessionManager.SessionData.RoleId == AdminRoleEnum.Admin || MySessionManager.SessionData.Employee.Type == EmployeeTypeEnum.Customer.Value();
        }
    }
}