//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace InsideX.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class CustomerCare
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public Nullable<int> RealId { get; set; }
        public Nullable<int> BrockerId { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string Note { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> CreatorId { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<int> UpdatorId { get; set; }
        public string CareImageLIst { get; set; }
    }
}
