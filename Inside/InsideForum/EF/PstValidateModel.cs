﻿using System;
using InsideX.Models.PstValidateModel;

namespace InsideX.EF
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(CustomerValidate))]
    public partial class Customer
    {
    }


    [System.ComponentModel.DataAnnotations.MetadataType(typeof(CustomerCareValidate))]
    public partial class CustomerCare
    {
       
    }

    [System.ComponentModel.DataAnnotations.MetadataType(typeof(EmployeeValidate))]
    public partial class Employee
    {

    }
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(RealPermissionValidate))]
    public partial class RealPermission
    {

    }

    [System.ComponentModel.DataAnnotations.MetadataType(typeof(RealValidate))]
    public partial class Real
    {

    }
    
}