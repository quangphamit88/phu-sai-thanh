﻿function changeActive(id, type) {
    NProgress.start();
    var control = $('#' + type + id);

    $.ajax({
        type: 'POST',
        url: '/handler/' + type,
        data: { id: id, status: control.is(':checked') },
        dataType: 'json',
        success: function (result) {
            NProgress.done();

            if (result.code != 1) {
                ErrorMessage();
            }
        },
        error: function (result) {
            NProgress.done();

            if (control.is(':checked')) {
                control.prop('checked', false);
            } else {
                control.prop('checked', true);
            }

            ErrorMessage();
        }
    });
};

function changeStatus(id, type) {
    NProgress.start();
    var activeID = $('#' + type + id);

    $.ajax({
        type: 'POST',
        url: '/handler/' + type,
        data: { id: id, value: activeID.val() },
        dataType: 'json',
        success: function (result) {
            NProgress.done();

            if (result.code != 1) {
                ErrorMessage();
            }
        },
        error: function (result) {
            NProgress.done();
            ErrorMessage();
        }
    });
};

function ErrorMessage() {
    alert('Có lỗi xảy ra, vui lòng thử lại.');
};

function ConfirmDelete() {
    if (confirm('Bạn có muốn xóa không?'))
        return true;
    return false;
};

function NumbersOnly(evt) {
    var e = evt
    if (window.event) {
        var charCode = e.keyCode;
    } else if (e.which) {
        var charCode = e.which
    }

    if (charCode == 46)
        return true;

    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 190)
        return false;
    return true;
};

/* ANGULAR - START */

function AngularCall(control, app, controller, urlPost, dataPost, callback, scopeCalback) {
    control.find('table tbody').hide();
    control.find('.pagination').hide();

    var insideForum = angular.module(app, ['ui.bootstrap', 'ngSanitize'])
        .filter('trusted', ['$sce', function ($sce) {
            return function (html) {
                return $sce.trustAsHtml(html);
            }
        }]);

    insideForum.controller(controller, ['$scope', '$http', function ($scope, $http) {
        GetAngularData();
        if (typeof scopeCalback === 'function') {
            scopeCalback($scope);
        }

        function GetAngularData() {
            $http({
                method: 'POST',
                url: urlPost,
                data: JSON.stringify(dataPost)
            }).then(function successCallback(response) {

                var angularData = response.data;

                if (angularData != null && angularData.code == 1) {
                    if (angularData.data != null && angularData.data.length > 0) {
                        $scope.Models = angularData.data;
                        $scope.itemsPerPage = angularData.itemsPerPage;
                        $scope.currentPage = angularData.currentPage;
                        $scope.totalRecord = angularData.totalRecord;

                        if (dataPost != null && dataPost.showPaging == false) {
                            $scope.showPaging = 0;
                            $scope.indexNo = 1;
                            control.find('.pagination').hide();
                        }
                        else if (angularData.totalRecord <= angularData.itemsPerPage) {
                            $scope.showPaging = 0;
                            $scope.indexNo = 1;
                            control.find('.pagination').hide();
                        }
                        else {
                            $scope.showPaging = 1;
                            $scope.indexNo = (angularData.currentPage - 1) * angularData.itemsPerPage + 1;
                            control.find('.pagination').show();
                        }

                        control.find('table tbody').show();
                        control.find('.x_content .alert').remove();

                        if (typeof callback === 'function') {
                            callback();
                        }

                        return;
                    }
                }

                control.find('table tbody').hide();
                control.find('.pagination').hide();

                ShowErrorMessage(control.find('.x_content'), 'Chưa có dữ liệu');
            });
        }

        $scope.pageChanged = function () {
            if (dataPost != null && dataPost.hasOwnProperty('currentPage')) {
                delete dataPost.currentPage;
            }

            if (dataPost == null || dataPost == undefined) {
                dataPost = {};
            }
            $('html,body').animate({
                scrollTop: 0
            }, 1000);
            dataPost.currentPage = $scope.currentPage;
            GetAngularData();
        };

        $scope.changeActive = function (id, style) {
            changeActive(id, style);
        }

        $scope.deleteItem = function (id, urlDelete, data) {
            if (!ConfirmDelete()) {
                return;
            }
            NProgress.start();
            if (!data) {
                data = {
                    id:id
                }
            }
            $.ajax({
                type: 'POST',
                url: urlDelete,
                data: data,
                dataType: 'json',
                success: function (result) {
                    if (result.code == 1) {
                        GetAngularData();
                    }

                    NProgress.done();

                    if (result.code != 1) {
                        ErrorMessage();
                    }
                },
                error: function (result) {
                    NProgress.done();
                    ErrorMessage();
                }
            });
        }

        $scope.AngularSearch = function () {
            dataPost = getAngularSearchData();
            GetAngularData();
        }
    }]);
};

/* ANGULAR - END */

function ShowErrorMessage(control, message) {
    $('<div class="alert alert-danger alert-dismissible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' + message + ' </div>').prependTo(control);
};