﻿(function ($) {
    $.fn.extend({

        multiImg: function (customOptions) {

            // Đặt các giá trị mặc định
            var defaults = {
                seperate: ";",
                defaultImg: "",
                showDefaultItem: 4,
                type: 'Other',
                path: 'Path',
                id: 'id'
            };

            var template = function (src, isHideDelete) {
                var hide = "";
                if (src.length==0)
                    hide = "ng-hide";
                return '<a class="ckfinder_item ng-scope" href="javascript:void(0)">' +
                    '   <span onclick="return false;" class="color-ed1c24 glyphicon glyphicon-trash del-img ' + hide + '" ></span>' +
                    '   <img class="display-block" width="150" height="150" src="' + src + '" alt=" Thêm hình" >' +
                    '</a>';
            }

            var options = $.extend(defaults, customOptions);

            var imgToString = function (obj) {
                var input = $("input[type=text]", obj);
                var str = '';
                $("img", obj).each(function () {
                    if ($(this).attr('src').length > 0)
                        str += defaults.seperate + $(this).attr('src');
                });
                input.val(str.substr(1));
            };


            var stringToImg = function (obj, str) {
                var html = '';
                var list = [];

                if (str != null && str.length > 0) {
                    list = str.split(defaults.seperate);
                    if (list.length > 0) {
                        for (var i = 0; i < list.length; i++) {
                            html += template(list[i]);
                        }
                    }
                }

                if (list.length < options.showDefaultItem - 1) {
                    for (var j = list.length; j < options.showDefaultItem - 1; j++) {
                        html += template("");
                    }
                }
                html += template("");
                $(".img-list", obj).html(html);
            };

            var addMoreIfFull = function (obj) {
                var isHas = false;
                $(".img-list > a > img", obj).each(function () {
                    var img = $(this);
                    if (img.attr("src").length == 0)
                        isHas = true;
                });

                if (!isHas)
                    $(".img-list", obj).append(template("", true));

            };



            return this.each(function () {
                var opts = options;
                var obj = $(this);// Đặt tên biến cho element (ul)
                var input = $("input[type=text]", obj);
                stringToImg(obj, input.val());
                // Lấy tất cả thẻ li trong ul
                var del = $("span.del-img", obj);
                var add = $(".img-list > a > img", obj);
                del.click(function () {
                    $(this).parent("a").find("img").attr("src", "");
                    $(this).addClass("ng-hide");
                    imgToString(obj);

                });

                obj.on("click", ".img-list > a > img", function (e) {
                    e.preventDefault();

                    var finder = new CKFinder();
                    finder.connectorInfo = 't=' + options.type + '&p=' + options.path + '&id=' + options.id;
                    var self = $(this);
                    finder.selectActionFunction = function (fileUrl) {
                        //item.url = fileUrl;
                        self.attr("src", fileUrl);
                        self.parent("a").find('span').removeClass("ng-hide");
                        imgToString(obj);
                        addMoreIfFull(obj);
                    };
                    finder.popup();

                });
                //add.click(function () {
                //    var finder = new CKFinder();
                //    var self = $(this);
                //    finder.selectActionFunction = function (fileUrl) {
                //        //item.url = fileUrl;
                //        self.attr("src", fileUrl);
                //        self.parent("a").find('span').removeClass("ng-hide");
                //        imgToString(obj);
                //        addMoreIfFull(obj);
                //    };
                //    finder.popup();
                //});

                // Thêm sự kiện mouseover và mouseout vào thẻ a
                //items.mouseover(function () {
                //    // lúc này this chính là thẻ a
                //    $(this).animate({ paddingLeft: opts.animatePadding }, 500);
                //}).mouseout(function () {
                //    $(this).animate({ paddingLeft: '0' }, 500);
                //});
            });
        }
    });
})(jQuery);