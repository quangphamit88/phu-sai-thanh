﻿var datatableLanguage = {
    "emptyTable": "Không có dữ liệu",
    "lengthMenu": "Hiển thị _MENU_ dòng 1 trang",
    "zeroRecords": "Không có dữ liệu",
    "info": "Trang _PAGE_ / _PAGES_",
    "infoEmpty": "Không có dữ liệu",
    "infoFiltered": "(filtered from _MAX_ total records)",
    "processing": "Đang tải dữ liệu."
};
function FullUrl(url) {
    if (url != null && url.length > 0) {
        if (url.substr(0, 4) == "http")
            return url;
        return FullDomain + url;
    }
    return "";
}

function openModal(url, width) {
    $.get(url, function (data) {
        if (!width)
            width = 600;
        $("#myModal .modal-dialog").css("width", width);

        $("#contentMyModel").html(data);
        $("#myModal").modal("show");
    });
}

function GetAddress(type, c, d, w, s, callback) {
    $.post('/Api/GetAddress', {
        type: type,
        cityId: c,
        districtId: d,
        wardId: w,
        streetId: s,
        areaId: 0,
    }, function (data) {
        callback && callback(data);
    });
}
function formatAvatar(item) {
    if (!item.id || item.id == "0") {
        return item.text;
    }
    var $item = $('<span><img width="30" height="30" src="' + window.EmployeeAvatarJson[item.id] + '" class="img-flag" alt="" /> ' + item.text + '</span>');
    return $item;
};

function templateSelectAll(item) {
    if (!item.id || item.id == "0") {
        return item.text;
    }
    if (item.id == "-1") {
        var $item = $('<strong style="color:red;"> ' + item.text + '</strong>');
        return $item;
    }
    return item.text;
};

function KeepLogin() {
    $.ajax({
        type: 'POST',
        url: 'http://pst.com/account/CrossLogin',
        crossDomain: true,
        data: {
            username: Window.MyUsername,
            password: window.MyPassword
        },
        dataType: 'json',
        success: function (responseData, textStatus, jqXHR) {
            var value = responseData.someKey;
        },
        //error: function (responseData, textStatus, errorThrown) {
        //    alert('POST failed.');
        //}
    });
}


$(document).ready(function () {
    //$.post('http://phusaithanh.com/Login/KeepLogin', { username: Window.MyUsername, password: window.MyPassword });
    //KeepLogin();





    $('body form').on('click', 'span.image_view_slide', function () {
        $(this).children('a').remove();
        var inputText = $(this).attr("input");
        var input = $('#' + inputText).val();
        var list = input.split(';');
        for (var i = 0; i < list.length; i++) {
            if (list[i].length > 0)
                $(this).append('<a href="' + list[i] + '"></a>');
        }


        if ($(this).children('a').length > 0) {
            $(this).imageview();
            $(this).children('a')[0].click();

        }
    });

    $(document).on("click", "button[type=submit]", function (e) {
        var self = $(this);
        e.preventDefault();
        $.post("/ApiNoAuth/CheckLogin", function (data) {
            if (data != true) {
                alert("no auth");
            } else {
                self.closest("form").submit();
            }
        });
    });



});


