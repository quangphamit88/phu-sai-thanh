﻿/*
Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckfinder.com/license
*/

CKFinder.customConfig = function (config) {
    config.language = 'vi';
    config.removePlugins = 'help';
    config.filebrowserBrowseUrl = window.currentId;
};

