/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

//CKEDITOR.editorConfig = function( config ) {
//	// Define changes to default configuration here. For example:
//	// config.language = 'fr';
//	// config.uiColor = '#AADC6E';
//};
CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here. For example:
    config.language = 'vi';
    // config.uiColor = '#AADC6E';
    //config.skin = 'office2013';
    config.enterMode = CKEDITOR.ENTER_BR;
    config.extraPlugins = 'jwplayer';
    config.toolbar = [['Source', '-', 'Save', 'NewPage', 'Preview', '-', 'Templates'],
    ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Print', 'SpellChecker', 'Scayt'],
    ['Undo', 'Redo', '-', 'Find', 'Replace', '-', 'SelectAll', 'RemoveFormat'],

    ['BidiLtr', 'BidiRtl'], ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],
    '/',
    ['Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript'],
    ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote', 'CreateDiv'],
    ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
    ['Link', 'Unlink', 'Anchor'],
    ['Image', 'jwplayer', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak'],
    '/',
    ['Styles', 'Format', 'Font', 'FontSize'],
    ['TextColor', 'BGColor'],
    ['Maximize', 'ShowBlocks', '-', 'About']];

    config.filebrowserBrowseUrl = '/Content/ckfinder/ckfinder.html';
    config.filebrowserImageBrowseUrl = '/Content/ckfinder/ckfinder.html?Type=Images';
    config.filebrowserFlashBrowseUrl = '/Content/ckfinder/ckfinder.html?Type=Flash';
    config.filebrowserUploadUrl = '/Content/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Files';
    config.filebrowserImageUploadUrl = '/Content/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Images';
    config.filebrowserFlashUploadUrl = '/Content/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Flash';

    CKFinder.setupCKEditor(null, '/Content/ckfinder');
};


//CKEDITOR.editorConfig = function (config) {
//    // Define changes to default configuration here.
//    // For the complete reference:
//    // x
//    //config.width = '75%';
//    //config.height = '200px';
//    // The toolbar groups arrangement, optimized for two toolbar rows.
//    // Toolbar configuration generated automatically by the editor based on config.toolbarGroups.
//    config.extraPlugins = 'youtube';
//    config.toolbar = [
//        { name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates'] },
//        { name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
//        { name: 'editing', groups: ['find', 'selection', 'spellchecker'], items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt'] },
//        { name: 'forms', items: ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'] },
//        '/',
//        { name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Strike', '-', 'RemoveFormat'] },
//        { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl'] },
//        { name: 'links', items: ['Link', 'Unlink', 'Anchor'] },
//        { name: 'insert', items: ['Image', 'youtube', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe'] },
//        '/',
//        { name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize'] },
//        { name: 'colors', items: ['TextColor', 'BGColor'] },
//        { name: 'tools', items: ['Maximize', 'ShowBlocks'] },
//        { name: 'others', items: ['-'] },
//        { name: 'about', items: ['About'] },
//    ];

//    // Toolbar groups configuration.
//    config.toolbarGroups = [
//        { name: 'document', groups: ['mode', 'document', 'doctools'] },
//        { name: 'clipboard', groups: ['clipboard', 'undo'] },
//        { name: 'editing', groups: ['find', 'selection', 'spellchecker'] },
//        { name: 'forms' },
//        '/',
//        { name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
//        { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'] },
//        { name: 'links' },
//        { name: 'insert' },
//        '/',
//        { name: 'styles' },
//        { name: 'colors' },
//        { name: 'tools' },
//        { name: 'others' },
//        { name: 'about' }
//    ];

//    // Remove some buttons, provided by the standard plugins, which we don't
//    // need to have in the Standard(s) toolbar.
//    config.removeButtons = 'Underline,Subscript,Superscript';

//    // Se the most common block elements.
//    config.format_tags = 'p;h1;h2;h3;pre';

//    // Make dialogs simpler.
//    config.removeDialogTabs = 'image:advanced;link:advanced';

//    // xuong dong khong them the <p>
//    config.enterMode = CKEDITOR.ENTER_BR;


//    // ckfinder
//    config.filebrowserBrowseUrl = '/Content/ckfinder/ckfinder.html';
//    config.filebrowserImageBrowseUrl = '/Content/ckfinder/ckfinder.html?type=Images';
//    config.filebrowserFlashBrowseUrl = '/Content/ckfinder/ckfinder.html?type=Flash';
//    config.filebrowserUploadUrl = '/Content/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
//    config.filebrowserImageUploadUrl = '/Content/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
//    config.filebrowserFlashUploadUrl = '/Content/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
//};
