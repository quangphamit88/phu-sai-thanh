
SELECT a.ID,
       a.LoginName,
       a.[Password],
       a.DisplayName,
       a.Avatar,
       a.IsActive,
       a.CreateBy,
       a.CreateDate,
       a.ModifyBy,
       a.ModifyDate,
       ar.RoleId,
       x.RoleName
        INTO dbo.Employee
FROM   Admin a
       JOIN BrokerAccount ba
            ON  a.ID = ba.AdminId
       LEFT JOIN AdminRole ar
            ON  a.id = ar.AdminId
LEFT JOIN Xrole x ON ar.RoleId=x.Id