﻿

$(document).ready(function () {
    if (AllowRightClick != true) {
        document.addEventListener("contextmenu", function (e) {
            e.preventDefault();
        }, false);
    } else {
        $("body").removeClass("noselect");
    }


    $(document).on('click', '#search_button', function (e) {
        e.preventDefault();
        var tab = $('.search_box .nav-tabs li.active').attr("tab");
        $("#TabIndex").val(tab);
        $("#PriceType").val($("#PriceTypeList" + tab).val());

        //if (tab == 1) {
        //} else {
            
        //}


        $('#form_search_header').submit();


    });

    var qtyItemDisplay = 4;
    var width = window.innerWidth;
    if (width <= 768)
        qtyItemDisplay = 3;
    if (width <= 550)
        qtyItemDisplay = 2;
    if (width <= 300)
        qtyItemDisplay = 1;

    if ($(".box_gia_tot .tab-pane.active .home-slider li").length > 0) {
        $(".box_gia_tot .tab-pane.active .home-slider").lightSlider({
            loop: true,
            keyPress: true,
            item: qtyItemDisplay,
            pager: false,
            speed: 5000,
            pause: 10000,
            enableTouch: false,
            enableDrag: false,
            freeMove: false,
            auto: true,
        });
    }

    $('#myTabs a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    var showHot = false;



    if ($("#chothuegiatot .home-slider li").length > 0) {
        $("#showHot").on("shown.bs.tab", function (e) {
            if (!showHot) {
                setTimeout(function () {
                    $("#chothuegiatot .home-slider").lightSlider({
                        loop: true,
                        keyPress: true,
                        item: qtyItemDisplay,
                        speed: 5000,
                        pause: 10000,
                        pager: false,
                        enableTouch: false,
                        enableDrag: false,
                        freeMove: false,
                        auto: true,
                    });

                }, 0);

                showHot = true;
            }
        });

    };

    if ($('#back-to-top').length) {
        var scrollTrigger = 100, // px
            backToTop = function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $('#back-to-top').addClass('show');
                } else {
                    $('#back-to-top').removeClass('show');
                }
            };
        backToTop();
        $(window).on('scroll', function () {
            backToTop();
        });
        $('#back-to-top').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
        });
    }

    //var getDistrictUrl = '@Url.Action("GetDistrict","Api")';
    //var getWardUrl = '@Url.Action("GetWard","Api")';
    //var getStreetUrl = '@Url.Action("GetStreet","Api")';

    var getStreet = function () {
        $.post(window.getStreetUrl, { id: $(".search_box #DistrictId").val() }, function (data) {
            $(".search_box #StreetId").html(data);
        });
    }

    var getWard = function () {
        $.post(window.getWardUrl, { id: $(".search_box #DistrictId").val() }, function (data) {
            $(".search_box #WardId").html(data);
            getStreet();
        });
    }
    var getDistrict = function () {
        $.post(window.getDistrictUrl, { id: $(".search_box #CityId").val() }, function (data) {
            $(".search_box #DistrictId").html(data);
            getWard();
            getStreet();
        });
    }

    $(document).on("change", ".search_box #CityId", function () {
        getDistrict();
        //$(".search_box #CityId option.remove-data,").remove();
        //$(".search_box #DistrictId option.remove-data,").remove();
        //$(".search_box #WardId option.remove-data,").remove();
        //$(".search_box #StreetId option.remove-data").remove();
    });
    $(document).on("change", ".search_box #DistrictId", function () {
        getWard();
        getStreet();
    });
    $(document).on("change", ".search_box #WardId", function () {
        //getStreet();
    });

    //$("img.img_err").addEventListener("error", function() {

    //    $(this).attr("src", window.imgError);
    //});
    ////$(document).on("error", "img.img_err", function(e) {
    //});

    $('input.only_number').keyup(function (e) {
        if (/\D/g.test(this.value)) {
            // Filter non-digits from input value.
            this.value = this.value.replace(/\D/g, '');
        }
    });

    $('.search_box a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        //e.target // newly activated tab
        var tab = $(this).parent("li").attr("tab");
        if (tab == 1) {
            $("#PriceTypeList1").show();
            $("#PriceTypeList2").hide();
        } else {
            $("#PriceTypeList1").hide();
            $("#PriceTypeList2").show();
        }
        //alert(e.target.attr("tab"));
        //e.relatedTarget // previous active tab
    });
    $(document).on('keydown', '#current_page_number', function (e) {
        var key = e.which;
        if (key == 13) {
            $('#go_page_btn').click();
            return false;
        }
    });
});