﻿using Microsoft.Owin;
using OutsideX;
using Owin;

[assembly: OwinStartup(typeof(Startup))]
namespace OutsideX
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
