﻿namespace OutsideX.Models
{
    public class WebCounter
    {
        public int TotalCount { get; set; }
        public int ToDayCount { get; set; }
        public int OnlineCount { get; set; }
        public string Date { get; set; }
    }
}