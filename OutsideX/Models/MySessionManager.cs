﻿using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using OutsideX.EF;

namespace OutsideX.Models
{
    public class MySessionManager
    {
        private const string UserSessionKey = "UserData";
        private const string PermissionKey = "PermissionKey";

        public static UserData SessionData
        {
            get
            {
                if (HttpContext.Current.Session[UserSessionKey] == null)
                    return null;

                var checkUserData = HttpContext.Current.Session[UserSessionKey] as UserData;

                if (checkUserData == null)
                    return null;

                if (checkUserData.ID <= 0)
                    return null;

                if (checkUserData.Sign != MyUtility.Common.MD5_encode(string.Format("{0}{1}{2}{3}", checkUserData.ID, checkUserData.LoginName, checkUserData.DisplayName, MyConfig.MyConfiguration.OutsideAuthen.KeySign)))
                    return null;

                return checkUserData;
            }
            set
            {
                HttpContext.Current.Session[UserSessionKey] = value;
            }
        }

        public static bool IsAuthen
        {
            get { return SessionData != null; }
        }

        public static bool IsAdmin
        {
            get
            {
                if (!IsAuthen) return false;
                return SessionData.RoleId == AdminRoleEnum.Admin;
            }
        }

        public static void SignOut()
        {
            if (SessionData != null)
                SessionData = null;
        }

        public static void ClearAllSession()
        {
            HttpContext.Current.Session.Abandon();
        }

    }

    public class UserData
    {
        public int ID { get; set; }
        public long BrokerId { get; set; }
        public AdminRoleEnum RoleId { get; set; }
        public string LoginName { get; set; }
        public string DisplayName { get; set; }
        public string Avatar { get; set; }
        public string Sign { get; set; }

        public bool IsAdmin { get { return RoleId == AdminRoleEnum.Admin; } }
        //public bool IsManager { get { return RoleId == AdminRoleEnum.Manager; } }
        public bool IsEmployer { get { return RoleId == AdminRoleEnum.Employer; } }
        public bool IsTuVan { get { return LoginName.ToLower() == "tuvankhachhang"; } }

        public EF.Employee Employee { get; set; }

        //public bool IsLeader { get; set; }

        //public bool IsSubLeader { get; set; }

        public List<int> GetIdsByEmp(Employee employee)
        {
            var list = new List<int> { employee.ID };
            var leads = employee.DepartmentDetails.Where(c => c.IsLeader || c.IsSubLeader).ToList();
            foreach (var detail in leads) {
                var listEmp = new List<Employee>();

                if (detail.IsLeader)
                    listEmp.AddRange(detail.Department.DepartmentDetails.Select(c => c.Employee).ToList());

                if (detail.IsSubLeader)
                    listEmp.AddRange(detail.Department.DepartmentDetails.Where(c => !c.IsLeader).Select(c => c.Employee).ToList());

                var child = listEmp.Where(c => !list.Contains(c.ID)).ToList();

                list.AddRange(listEmp.Select(c => c.ID).ToList());

                foreach (var emp in child.Where(c => c.DepartmentDetails.Any(v => v.IsLeader || v.IsSubLeader))) {
                    list.AddRange(GetIdsByEmp(emp));
                }
            }

            return list.Distinct().ToList();

        }
    }

}