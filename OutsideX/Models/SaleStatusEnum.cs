﻿using System.ComponentModel;

namespace OutsideX.Models
{
    public enum SaleStatusEnum
    {
        [Description("Chờ duyệt")]
        Choduyet = 5,

        [Description("Đang Bán")]
        OnSale = 1,

        [Description("Đầu tư")]
        Khongduyet = 6,

        [Description("Đang Cho Thuê")]
        Renting = 4,

        [Description("Đã Bán")]
        HaveSold = 2,

        [Description("Đã Cho Thuê")]
        HaveRented = 3,
    }
}