﻿using System.Collections.Generic;
using System.Linq;
using OutsideX.EF;

namespace OutsideX.Models
{
    public static class PstHelper
    {

        public static List<Out_Real_Search_Result> Real_Search(RealSearchModel model)
        {
            //if(model.MaxPrice==100)

            var db = new PstEntities();
            return db.Out_Real_Search(
                model.Title,
                model.TitleAlias,
                model.BuyOrRent,
                model.RealStatus ?? 0,
                model.WardId,
                model.DistrictId,
                model.CityId,
                model.StreetId,
                model.RealTypeMulti,
                model.StreetTypeMulti,
                model.DirectionMulti,
                model.RealStatusNowMulti,
                model.WardIdMulti,
                model.StreetIdMulti,
                model.RoomNumber,
                model.MinPrice,
                model.MaxPrice,
                model.IsHotPrice,
                model.SortType,
                model.PageIndex,
                model.PageSize
                ).ToList();
        }


        public static List<Out_Real_Search_Result> Real_Search(RealSearchModel model, ref int total)
        {
            total = 0;
            var data = Real_Search(model);
            if (data.Any())
                total = data[0].TotalRow ?? 0;
            return data;
        }



        public static List<Out_Real_Search_Result> ConvertToRealSearchModel(List<Out_Real_SelectTopHot_Result> list)
        {
            return list.Select(c => new Out_Real_Search_Result {
                Id = c.Id,
                TempId = c.TempId,
                PublicDate = c.PublicDate,
                Status = c.Status,
                StatusNow = c.StatusNow,
                Type = c.Type,
                BuyOrRent = c.BuyOrRent,
                Price = c.Price,
                PriceType = c.PriceType,
                PriceMonthType = c.PriceMonthType,
                PriceSold = c.PriceSold,
                PriceSoldType = c.PriceSoldType,
                Title = c.Title,
                IsPriceHot = c.IsPriceHot,
                Vip = c.Vip,
                AddressNo = c.AddressNo,
                DistrictId = c.DistrictId,
                CityId = c.CityId,
                WardId = c.WardId,
                StreetId = c.StreetId,
                StreetType = c.StreetType,
                DuongRong = c.DuongRong,
                DienTich = c.DienTich,
                DienTichSuDung = c.DienTichSuDung,
                ChieuRong = c.ChieuRong,
                ChieuDai = c.ChieuDai,
                SoPhongNgu = c.SoPhongNgu,
                SoWc = c.SoWc,
                KetCau = c.KetCau,
                SoMatTien = c.SoMatTien,
                Direction = c.Direction,
                LegalStatus = c.LegalStatus,
                HinhDangDat = c.HinhDangDat,
                HinhDangDatNumber = c.HinhDangDatNumber,
                ContactName = c.ContactName,
                ContactPhone = c.ContactPhone,
                ContactAddress = c.ContactAddress,
                ContactResource = c.ContactResource,
                ContactType = c.ContactType,
                HoaHongBan = c.HoaHongBan,
                HoaHongBanUnitType = c.HoaHongBanUnitType,
                HoaHongThue = c.HoaHongThue,
                Note1 = c.Note1,
                Note2 = c.Note2,
                Note3 = c.Note3,
                Note4 = c.Note4,
                Note5 = c.Note5,
                Note6 = c.Note6,
                Note7 = c.Note7,
                Note8 = c.Note8,
                Note9 = c.Note9,
                Note10 = c.Note10,
                UocLuongGiaBan = c.UocLuongGiaBan,
                GiaNha = c.GiaNha,
                GiaTriDat = c.GiaTriDat,
                BrokerId = c.BrokerId,
                Description = c.Description,
                Hinh = c.Hinh,
                HinhNoiThat = c.HinhNoiThat,
                HinhChiTiet = c.HinhChiTiet,
                HinhGiayTo = c.HinhGiayTo,
                CreatedDate = c.CreatedDate,
                CreatedBy = c.CreatedBy,
                UpdatedDate = c.UpdatedDate,
                UpdatedBy = c.UpdatedBy,
                Keyword = c.Keyword,
                ViewCount = c.ViewCount,
                Deleted = c.Deleted,
                IsAllowEditInfo = c.IsAllowEditInfo,
                IsAllowEditAddress = c.IsAllowEditAddress,
                IsAllowEditProductDetail = c.IsAllowEditProductDetail,
                IsAllowEditContact = c.IsAllowEditContact,
                IsAllowEditHoaHong = c.IsAllowEditHoaHong,
                IsAllowEditNote = c.IsAllowEditNote,
                //IsAllowEditLocationDescription    =c.IsAllowEditLocationDescrip                 ,
                IsAllowEditImage = c.IsAllowEditImage,
                //CityName                          =c.CityName                                   ,
                //DistrictName                      =c.DistrictName                               ,
                //StreetName                        =c.StreetName                                 ,
                //WardName                          =c.WardName                                   ,
                //RowId                             =c.RowId                                      ,
                //TotalRow                          =c.TotalRow                                   ,

            }).ToList();
        }

        public static string ToUnitBuyOrRent(this int buyOrRent)
        {
            if (buyOrRent == 1)
                return " tỷ";
            return " triệu";
        }

    }
}