﻿using System.ComponentModel;

namespace OutsideX.Models
{
    public enum ProductStatusNowEnum
    {
        [Description("Nhà nát")]
        NhaNat = 1,

        [Description("Ở được")]
        ODuoc = 2,

        [Description("Nhà mới")]
        NhaMoi = 3
    }
}