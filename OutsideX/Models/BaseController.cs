﻿using System.Web.Mvc;
using BussinessObject;
using BussinessObject.Bo.WebBo;

namespace OutsideX.Models
{
    public class BaseController : Controller
    {
        public BaseController()
        {
            if (HttpContext.Session != null && HttpContext.Session["ViewCount"] == null)
                Session["ViewCount"] = "";
        }
    }
}