﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using MyUtility.Extensions;
using PhuSaiThanhLib;
using PhuSaiThanhLib.Enum;
using PhuSaiThanhLib.Real;

namespace OutsideX.Models
{
    public static class TinyHelper
    {
        public static string ToTextTime(this DateTime? date)
        {
            if (date == null || date.Value > DateTime.Now)
                return "Cách đây vài phút";
            var now = DateTime.Now;
            var time = now - date.Value;
            var days = time.TotalDays;

            var year = Math.Floor(days / 365);
            var month = Math.Floor(days / 30);
            var week = Math.Floor(days / 7);
            var day = Math.Floor(days);
            var hour = Math.Floor(time.TotalHours);
            var minus = Math.Floor(time.TotalMinutes);
            if (year > 1)
                return string.Format("Cách đây {0} năm", year);
            if (month > 1)
                return string.Format("Cách đây {0} tháng", month);
            if (week > 1)
                return string.Format("Cách đây {0} tuần", week);
            if (day > 1)
                return string.Format("Cách đây {0} ngày", day);
            if (hour > 1)
                return string.Format("Cách đây {0} giờ", hour);
            if (minus > 1)
                return string.Format("Cách đây {0} phút", minus);
            return "Vừa mới đăng";
        }


        //public static string GetFullUrlImage(this string str)
        //{
        //    if (string.IsNullOrEmpty(str))
        //        return string.Empty;
        //    if (str.ToLower().StartsWith("http"))
        //        return str;
        //    if (str.StartsWith("/"))
        //        str = str.Substring(1);

        //    return BasicHelper.FullDomainAdmin + str;
        //}

        public static string GetFullContent(this string content)
        {
            return content.Replace("\"/Images/ckfinder", "\"" + BasicHelper.FullDomainAdmin + "/Images/ckfinder");
        }



        public static string HtmlDecode(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return string.Empty;
            return HttpUtility.HtmlDecode(str);
        }

        public static string DecimalToText(this decimal? money, string template = "{0} {1}")
        {
            return money.HasValue && money.Value != 0 ? money.Value.DecimalToText(template) : "0 đ";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="money"></param>
        /// <param name="template">{1.2} {tỷ}</param>
        /// <returns></returns>
        public static string DecimalToText(this decimal money, string template = "{0} {1}")
        {
            var m = string.Empty;
            var u = "đ";
            if (money >= 1000000000) {
                m = (money / 1000000000).ToString("0.##");
                u = " Tỷ";
            }
            else if (money >= 1000000) {
                m = (money / 1000000).ToString("0.##");
                u = " Triệu";
            }
            else if (money >= 1000) {
                m = (money / 1000).ToString("0.##");
                u = " Nghìn";
            }
            else m = money.ToString("0.##");
            return string.Format(template, m, u);
        }

        public static string DisplayDecimal(this decimal? money, string defaultText = "0")
        {
            if (money.HasValue)
                return money.Value.ToString("0.##");
            return defaultText;
        }

        public static string ConvertFloor(this int? num)
        {
            if (num == null || num < 1)
                return "--";
            return num == 1 ? "1 trệt" : string.Format("1 trệt {0} lầu", num - 1);
        }

        public static string ToDirection(this int? dire)
        {
            if (dire == null)
                return "-";
            return dire.Value.ToEnum<RealDirectionEnum>().Text();
        }

        public static string ToProductType(this int? type)
        {
            if (type == null)
                return "-";
            return type.Value.ToEnum<RealTypeEnum>().Text();
        }

        public static string ToDetault(this double? num, string def = "-")
        {
            return num == null ? def : num.ToString();
        }
        public static string ToDetault(this int? num, string def = "-")
        {
            return num == null ? def : num.ToString();
        }
        public static string ToIntDetault(dynamic num, string def = "-")
        {
            if (num == null) return def;
            return num.ToString();
        }


        public static string ToDetault(this long? num, string def = "-")
        {
            return num == null ? def : num.ToString();
        }

        public static string ToString(this DateTime? date, string temp, string def = "-")
        {
            if (date.HasValue)
                return date.Value.ToString(temp);
            return def;
        }

        public static string ToTitleGiaTot(this string text, int maxLength = 40)
        {
            if (string.IsNullOrEmpty(text))
                return string.Empty;
            if (text.Length > maxLength)
                return text.Substring(0, maxLength - 3) + "...";
            return text;
        }
        public static string MaxLength(this string text, int maxLength = 40)
        {
            if (string.IsNullOrEmpty(text))
                return string.Empty;
            if (text.Length > maxLength)
                return text.Substring(0, maxLength - 3) + "...";
            return text;
        }

        public static string CleanNotNumber(this string text)
        {
            var digitsOnly = new Regex(@"[^\d]");
            return digitsOnly.Replace(text, "");
        }

        public static string DecimalFormat(this decimal? money, string defaultString = "")
        {
            if (money.HasValue)
                return money.Value.DecimalFormat();
            return defaultString;
        }

        public static string DecimalFormat(this decimal money)
        {
            return money.ToString("0.##");
        }

        public static string DecimalFormat(this double money)
        {
            return money.ToString("0.##");
        }

        public static IHtmlString DropdownListMulti(this HtmlHelper helper, string name, List<SelectListItem> data, IDictionary<string, object> htmlAttributes)
        {
            var tb = new TagBuilder("select");
            tb.Attributes.Add("id", name);
            tb.Attributes.Add("name", name);

            tb.Attributes.Add("multiple", "multiple");
            foreach (var item in htmlAttributes) {
                //tb.Attributes.Add(item.Key, item.Value.ToString());
                tb.Attributes[item.Key] = item.Value.ToString();
            }

            foreach (var item in data) {
                var option = new TagBuilder("option");
                option.Attributes.Add("value", item.Value);
                if (item.Selected)
                    option.Attributes.Add("selected", "selected");
                option.InnerHtml = item.Text;

                tb.InnerHtml += option.ToString();
            }
            return new MvcHtmlString(tb.ToString());
        }


        public static string ToStringJoin(this List<int> list, string sp = ",")
        {
            if (list != null && list.Any()) {
                return string.Join(sp, list);

            }
            return string.Empty;
        }
    }
}