﻿using System.ComponentModel;

namespace OutsideX.Models
{
    public enum OwnerEnum
    {
        [Description("Chính chủ")]
        Master = 1,

        [Description("Môi giới")]
        Broker = 2,

        [Description("Đầu tư")]
        Invester = 3,
    }
}