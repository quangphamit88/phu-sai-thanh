﻿using System;
using System.Collections.Generic;

namespace OutsideX.Models
{
    public class PagingModel
    {
        public PagingModel(int total, int? pageSize, int? currentPage, int maxDisplay = 5)
        {
            TotalRow = total;
            PageSize = pageSize ?? 2;
            CurrentPage = currentPage ?? 1;
            Items = new List<PagingItems>();
            if (maxDisplay % 2 == 0)
                maxDisplay = maxDisplay - 1;
            IsHasNext = false;
            IsHasPreview = false;

            if (pageSize > 0)
            {
                TotalPage = (int)Math.Ceiling((double)TotalRow / (double)pageSize);
            }
            //if (CurrentPage > TotalPage)
            //    CurrentPage = TotalPage;
            if (TotalPage > 1)
            {
                if (TotalPage <= maxDisplay)
                {
                    for (var i = 1; i <= TotalPage; i++)
                    {
                        Items.Add(new PagingItems()
                        {
                            IsCurrent = CurrentPage == i,
                            Num = i
                        });
                    }
                }
                else
                {
                    var left = (maxDisplay + 1) / 2;
                    if (CurrentPage <= left)
                    {
                        IsHasNext = true;
                        for (var i = 1; i <= maxDisplay; i++)
                        {
                            Items.Add(new PagingItems()
                            {
                                IsCurrent = CurrentPage == i,
                                Num = i
                            });
                        }
                    }
                    else if (CurrentPage >= TotalPage - left)
                    {
                        IsHasPreview = true;
                        for (var i = TotalPage - maxDisplay + 1; i <= TotalPage; i++)
                        {
                            Items.Add(new PagingItems()
                            {
                                IsCurrent = CurrentPage == i,
                                Num = i
                            });
                        }
                    }
                    else
                    {
                        IsHasNext = true;
                        IsHasPreview = true;
                        for (var i = CurrentPage - left + 1; i <= CurrentPage + left - 1; i++)
                        {
                            Items.Add(new PagingItems()
                            {
                                IsCurrent = CurrentPage == i,
                                Num = i
                            });
                        }
                    }
                }
            }


        }

        public int TotalRow { get; set; }
        public int PageSize { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPage { get; set; }
        public bool IsHasNext { get; set; }
        public bool IsHasPreview { get; set; }

        public List<PagingItems> Items { get; set; }
    }

    public class PagingItems
    {
        public int Num { get; set; }
        public bool IsCurrent { get; set; }
    }
}