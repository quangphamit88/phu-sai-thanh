﻿using System;
using System.Web;
using Newtonsoft.Json;

namespace OutsideX.Models
{
    public static class CookieHelper
    {
        public static void Add(string name, string value, DateTime? expires = null)
        {
            expires = expires ?? DateTime.Now.AddMonths(1);
            var myCookie =  new HttpCookie(name)
            {
                Value = value,
                Expires = expires.Value
            };
            HttpContext.Current.Response.SetCookie(myCookie);
        }

        public static void Add<T>(string name, T value, DateTime? expires = null)
        {
            var str = JsonConvert.SerializeObject(value);
            Add(name, str, expires);
        }

        public static string Get(string name)
        {
            var httpCookie = HttpContext.Current.Request.Cookies[name];
            if (httpCookie != null)
                return httpCookie.Value;
            return string.Empty;
        }
    }
}