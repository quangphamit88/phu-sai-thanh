﻿


using System;
using System.Linq;
using MyUtility.Extensions;
using OutsideX.EF;
using PhuSaiThanhLib.Enum;

namespace OutsideX.Models
{
    public class LevelAccessModel
    {
        public LevelAccessModel(int realBrokerId, int realId)
        {
            IsViewAlbum = false;
            IsViewAlbumLegal = false;
            IsViewAddress = false;
            IsViewBroker = false;
            IsViewContact = false;

            var db = new PstEntities();

            if (MySessionManager.IsAuthen) {
                var isOwner = realBrokerId == MySessionManager.SessionData.ID;
                if (MySessionManager.SessionData.IsAdmin || isOwner) {
                    IsViewAlbum = true;
                    IsViewAlbumLegal = true;
                    IsViewAddress = true;
                    IsViewBroker = true;
                    IsViewContact = true;
                }
                else {

                    if (MySessionManager.SessionData.Employee.Type == EmployeeTypeEnum.Real.Value()) {

                        var listLeader =
                            MySessionManager.SessionData.Employee.DepartmentDetails.Where(
                                c => c.IsLeader || c.IsSubLeader);


                        if (listLeader.Any()) {
                            var empChild =
                                MySessionManager.SessionData.GetIdsByEmp(MySessionManager.SessionData.Employee);
                            if (empChild.Contains(realBrokerId)) {

                                var levelReal = db.LevelReals.FirstOrDefault(c => c.Level == MySessionManager.SessionData.Employee.Level);
                                if (levelReal != null) {
                                    IsViewAlbum = levelReal.IsViewImageDetail;
                                    IsViewAlbumLegal = levelReal.IsViewImageGiayTo;
                                    IsViewAddress = levelReal.IsViewAddress;
                                    IsViewBroker = levelReal.IsViewBroker;
                                    IsViewContact = levelReal.IsViewContact;
                                }

                            }
                        }
                    }

                    if (MySessionManager.SessionData.Employee.Type == EmployeeTypeEnum.Customer.Value()) {
                        var per = db.RealPermissions.FirstOrDefault(c => c.RealId == realId && c.BrokerId == MySessionManager.SessionData.ID && c.IsDeleted != true && c.TimeLimit >= DateTime.Now);
                        if (per != null) {
                            IsViewAlbum = per.IsViewImageDetail;
                            IsViewAlbumLegal = per.IsViewImageGiayTo;
                            IsViewAddress = per.IsViewAddress;
                            IsViewBroker = per.IsViewBroker;
                            IsViewContact = per.IsViewContact;
                        }

                    }

                }




            }
        }


        public bool IsViewAlbum { get; set; }

        public bool IsViewAlbumLegal { get; set; }

        public bool IsViewAddress { get; set; }

        public bool IsViewBroker { get; set; }

        public bool IsViewContact { get; set; }
    }
}