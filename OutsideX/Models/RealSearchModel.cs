﻿using System.Collections.Generic;
using MyUtility.Extensions;

namespace OutsideX.Models
{
    public class RealSearchModel
    {
        public string Title { get; set; }

        public string TitleAlias
        {
            get { return (Title ?? "").ToAlias(); }
        }

        public int? BuyOrRent { get; set; }
        //public int? RealType { get; set; }
        public int? RealStatus { get; set; }
        //public int? RealStatusNow { get; set; }
        //public int? StreetView { get; set; }
        //public string RealDirection { get; set; }
        public int? WardId { get; set; }
        public int? DistrictId { get; set; }
        public int? CityId { get; set; }
        public int? StreetId { get; set; }
        public int? RoomNumber { get; set; }
        public decimal? MinPrice { get; set; }
        public decimal? MaxPrice { get; set; }
        public bool? IsHotPrice { get; set; }
        public int? SortType { get; set; }
        public int? PageIndex { get; set; }
        public int? PageSize { get; set; }

        public string RealTypeMulti { get; set; }
        public string RealStatusNowMulti { get; set; }
        public string DirectionMulti { get; set; }
        public string StreetTypeMulti { get; set; }
        public string WardIdMulti { get; set; }
        public string StreetIdMulti { get; set; }
    }
}
