﻿using System.ComponentModel;

namespace OutsideX.Models
{
    public enum AdminRoleEnum
    {
        [Description("Administrator")]
        Admin = 1,

        //[Description("Manager")]
        //Manager = 2,

        [Description("Employer")]
        Employer = 3
    }
}