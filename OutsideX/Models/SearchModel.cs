﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using BussinessObject;
using EntitiesObject.Entities.WebEntities;
using EntitiesObject.Message.Model;
using MyUtility.Extensions;
using OutsideX.EF;
using PhuSaiThanhLib.Enum;
using PhuSaiThanhLib.Real;

namespace OutsideX.Models
{
    public class SearchModel
    {
        public SearchModel()
        {
            PageIndex = 1;
            PageSize = 2;
            CityId = 1;
            DistrictId = 0;
            Result = new List<Out_Real_Search_Result>();
            TabIndex = 1;
            SortType = 3;
            IsPriceHot = false;

            RealTypeMulti = new List<int>();
            RealStatusNowMulti = new List<int>();
            DirectionMulti = new List<int>();
            StreetTypeMulti = new List<int>();
            WardIdMulti = new List<int>();
            StreetIdMulti = new List<int>();

        }

        public string Keyword { get; set; }
        public bool SaleOrRent { get; set; }
        public int DistrictId { get; set; }
        public int CityId { get; set; }
        public int WardId { get; set; }
        public int StreetId { get; set; }
        //public int DirectionId { get; set; }
        //public int ProductType { get; set; }
        //public int ProductStatusNow { get; set; }
        public bool IsShowAdvance { get; set; }
        public int TabIndex { get; set; }
        public int SortType { get; set; }
        public int RoomNumber { get; set; }
        public bool IsPriceHot { get; set; }

        public List<int> RealTypeMulti { get; set; }
        public List<int> RealStatusNowMulti { get; set; }
        public List<int> DirectionMulti { get; set; }
        public List<int> StreetTypeMulti { get; set; }
        public List<int> WardIdMulti { get; set; }
        public List<int> StreetIdMulti { get; set; }

        /// <summary>
        /// 1: tang dan; 2: giam dan
        /// </summary>
        public int PriceType { get; set; }
        /// <summary>
        /// 1:nha mat tien; 2: hem
        /// </summary>
        public int LaneType { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int TotalRow { get; set; }
        public List<Out_Real_Search_Result> Result { get; set; }

        public List<SelectListItem> CityList()
        {
            var model = new List<SelectListItem> { new SelectListItem { Value = "0", Text = "Tỉnh/Thành Phố" } };
            var city = BoFactory.AddressBook.GetCityList() ?? new List<Xcity>();
            model.AddRange(city.Select(c => new SelectListItem {
                Value = c.Id + "",
                Text = c.CityName,
                Selected = c.Id == CityId
            }));
            return model;
        }

        public List<SelectListItem> DistrictList()
        {
            var model = new List<SelectListItem> { new SelectListItem { Value = "0", Text = "Quận/Huyện" } };
            if (CityId > 0) {
                var dis = BoFactory.AddressBook.GetDistrictByCity(CityId) ?? new List<Xdistrict>();
                model.AddRange(dis.Select(item => new SelectListItem {
                    Value = item.Id + "",
                    Text = item.DistrictName,
                    Selected = item.Id == DistrictId
                }));
            }
            return model;
        }

        //public List<SelectListItem> WardList()
        //{
        //    var model = new List<SelectListItem> { new SelectListItem { Value = "0", Text = "Phường/Xã" } };
        //    if (DistrictId > 0) {
        //        var dis = BoFactory.AddressBook.GetWardByDistrict(DistrictId) ?? new List<Xward>();
        //        model.AddRange(dis.Select(item => new SelectListItem {
        //            Value = item.Id + "",
        //            Text = item.WardName,
        //            Selected = item.Id == WardId
        //        }));
        //    }
        //    return model;
        //}

        //public List<SelectListItem> StreetList()
        //{
        //    var model = new List<SelectListItem> { new SelectListItem { Value = "0", Text = "Đường" } };
        //    if (DistrictId > 0) {
        //        var dis = BoFactory.AddressBook.GetStreetByDistrict(DistrictId) ?? new List<Xstreet>();
        //        model.AddRange(dis.Select(item => new SelectListItem {
        //            Value = item.Id + "",
        //            Text = item.StreetName,
        //            Selected = item.Id == StreetId
        //        }));
        //    }
        //    return model;
        //}

        //public List<SelectListItem> DirectionList()
        //{
        //    var model = typeof(RealDirectionEnum).ToList().Select(c => new SelectListItem() {
        //        Text = c.Value,
        //        Value = c.Key.ToString(CultureInfo.InvariantCulture),
        //        Selected = c.Key == DirectionId
        //    }).ToList();
        //    model.Insert(0, new SelectListItem { Text = "Hướng", Value = "0" });
        //    return model;
        //}

        //public string DirectionValue
        //{
        //    get { return DirectionId.ToString(); }
        //}

        //public List<SelectListItem> ProductTypeList()
        //{
        //    var model = typeof(RealTypeEnum).ToList().Select(c => new SelectListItem() {
        //        Text = c.Value,
        //        Value = c.Key.ToString(CultureInfo.InvariantCulture),
        //        Selected = c.Key == ProductType
        //    }).ToList();
        //    //model.Remove(model.FirstOrDefault(x => x.Value == ((int)RealTypeEnum.NhaMatPho).ToString()));
        //    //model.Insert(0, new SelectListItem { Text = "Loại nhà đất", Value = "0", Selected = true });
        //    return model;
        //}

        //public List<SelectListItem> ProductStatusNowList()
        //{
        //    var model = typeof(ProductStatusNowEnum).ToList().Select(c => new SelectListItem() {
        //        Text = c.Value,
        //        Value = c.Key.ToString(CultureInfo.InvariantCulture),
        //        Selected = c.Key == ProductType
        //    }).ToList();
        //    //model.Remove(model.FirstOrDefault(x => x.Value == ((int)RealTypeEnum.NhaMatPho).ToString()));
        //    model.Insert(0, new SelectListItem { Text = "Hiện trạng", Value = "0", Selected = true });
        //    return model;
        //}

        public List<SelectListItem> LaneList()
        {
            var lst = typeof(RealStreetTypeEnum).ToList().Select(c => new SelectListItem {
                Text = c.Value,
                Value = c.Key.ToString(CultureInfo.InvariantCulture),
                Selected = c.Key == LaneType

            }).ToList();
            lst.Insert(0, new SelectListItem {
                Value = "0",
                Text = "Mặt tiền/Hẻm"
            });
            return lst;
        }

        public List<SelectListItem> PriceTypeList1()
        {
            var model = new List<SelectListItem>()
            {
                new SelectListItem {Value = "0", Text = "Giá tối đa"},
                new SelectListItem {Value = "2", Text = "2 tỷ", Selected = PriceType == 2&TabIndex==1},
                new SelectListItem {Value = "4", Text = "4 tỷ", Selected = PriceType == 4&TabIndex==1},
                new SelectListItem {Value = "6", Text = "6 tỷ", Selected = PriceType == 6&TabIndex==1},
                new SelectListItem {Value = "8", Text = "8 tỷ", Selected = PriceType == 8&TabIndex==1},
                new SelectListItem {Value = "10", Text = "10 tỷ", Selected = PriceType == 10&TabIndex==1},
                new SelectListItem {Value = "12", Text = "12 tỷ", Selected = PriceType == 12&TabIndex==1},
                new SelectListItem {Value = "14", Text = "14 tỷ", Selected = PriceType == 14&TabIndex==1},
                new SelectListItem {Value = "16", Text = "16 tỷ", Selected = PriceType == 16&TabIndex==1},
                new SelectListItem {Value = "18", Text = "18 tỷ", Selected = PriceType == 18&TabIndex==1},
                new SelectListItem {Value = "20", Text = "20 tỷ", Selected = PriceType == 2010&TabIndex==1},
                new SelectListItem {Value = "1000", Text = "trên 20 tỷ", Selected = PriceType == 1000&TabIndex==1},
            };
            return model;
        }

        public List<SelectListItem> PriceTypeList2()
        {
            var model = new List<SelectListItem>()
            {
                new SelectListItem {Value = "0", Text = "Giá tối đa"},
                new SelectListItem {Value = "5", Text = "5 triệu", Selected = PriceType == 5&TabIndex==2},
                new SelectListItem {Value = "10", Text = "10 triệu", Selected = PriceType == 10&TabIndex==2},
                new SelectListItem {Value = "15", Text = "15 triệu", Selected = PriceType == 15&TabIndex==2},
                new SelectListItem {Value = "20", Text = "20 triệu", Selected = PriceType == 20&TabIndex==2},
                new SelectListItem {Value = "25", Text = "25 triệu", Selected = PriceType == 25&TabIndex==2},
                new SelectListItem {Value = "30", Text = "30 triệu", Selected = PriceType == 30&TabIndex==2},
                new SelectListItem {Value = "35", Text = "35 triệu", Selected = PriceType == 35&TabIndex==2},
                new SelectListItem {Value = "40", Text = "40 triệu", Selected = PriceType == 40&TabIndex==2},
                new SelectListItem {Value = "45", Text = "45 triệu", Selected = PriceType == 45&TabIndex==2},
                new SelectListItem {Value = "50", Text = "50 triệu", Selected = PriceType == 50&TabIndex==2},
                new SelectListItem {Value = "1000", Text = "trên 50 triệu", Selected = PriceType == 1000&TabIndex==2},
            };
            return model;
        }

        public List<SelectListItem> RooomNumberList()
        {
            var model = new List<SelectListItem>()
            {
                new SelectListItem {Value = "0", Text = "Số phòng"},
                new SelectListItem {Value = "1", Text = "1 +", Selected = RoomNumber == 1},
                new SelectListItem {Value = "2", Text = "2 +", Selected = RoomNumber == 2},
                new SelectListItem {Value = "3", Text = "3 +", Selected = RoomNumber == 3},
                new SelectListItem {Value = "4", Text = "4 +", Selected = RoomNumber == 4},
                new SelectListItem {Value = "5", Text = "5 +", Selected = RoomNumber == 5},
                new SelectListItem {Value = "6", Text = "6 +", Selected = RoomNumber == 6},
                new SelectListItem {Value = "7", Text = "7 +", Selected = RoomNumber == 7},
                new SelectListItem {Value = "8", Text = "8 +", Selected = RoomNumber == 8},
                new SelectListItem {Value = "9", Text = "9 +", Selected = RoomNumber == 9},
                new SelectListItem {Value = "10", Text = "10 +", Selected = RoomNumber == 10},
            };
            return model;
        }



        #region region Multi

        public List<SelectListItem> RealTypeMultiList()
        {
            var model = typeof(RealTypeEnum).ToList().Select(c => new SelectListItem() {
                Text = c.Value,
                Value = c.Key.ToString(CultureInfo.InvariantCulture),
                Selected = RealTypeMulti.Contains(c.Key)
            }).ToList();
            return model;
        }

        public List<SelectListItem> RealStatusNowMultiList()
        {
            var model = typeof(ProductStatusNowEnum).ToList().Select(c => new SelectListItem() {
                Text = c.Value,
                Value = c.Key.ToString(CultureInfo.InvariantCulture),
                Selected = RealStatusNowMulti.Contains(c.Key)
            }).ToList();
            return model;
        }

        public List<SelectListItem> DirectionMultiList()
        {
            var model = typeof(RealDirectionEnum).ToList().Select(c => new SelectListItem() {
                Text = c.Value,
                Value = c.Key.ToString(CultureInfo.InvariantCulture),
                Selected = DirectionMulti.Contains(c.Key)
            }).ToList();
            return model;
        }

        public List<SelectListItem> StreetTypeMultiList()
        {
            var lst = typeof(RealStreetTypeEnum).ToList().Select(c => new SelectListItem {
                Text = c.Value,
                Value = c.Key.ToString(CultureInfo.InvariantCulture),
                Selected = StreetTypeMulti.Contains(c.Key)

            }).ToList();
            return lst;
        }

        public List<SelectListItem> WardMultiList()
        {
            var model = new List<SelectListItem> {
                //new SelectListItem { Value = "0", Text = "Phường/Xã" }
            };
            if (DistrictId > 0) {
                var dis = BoFactory.AddressBook.GetWardByDistrict(DistrictId) ?? new List<Xward>();
                model.AddRange(dis.Select(item => new SelectListItem {
                    Value = item.Id + "",
                    Text = item.WardName,
                    Selected = WardIdMulti.Contains(item.Id)
                }));
            }
            else {
                //model.Add(new SelectListItem { Value = "0", Text = "Phường/Xã" });
            }
            return model;
        }

        public List<SelectListItem> StreetMultiList()
        {
            var model = new List<SelectListItem> {
                //new SelectListItem { Value = "0", Text = "Đường" }
            };
            if (DistrictId > 0) {
                var dis = BoFactory.AddressBook.GetStreetByDistrict(DistrictId) ?? new List<Xstreet>();
                model.AddRange(dis.Select(item => new SelectListItem {
                    Value = item.Id + "",
                    Text = item.StreetName,
                    Selected = StreetIdMulti.Contains(item.Id)
                }));
            }
            return model;
        }


        #endregion

    }
}