﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OutsideX.Models
{
    public class ContactToStaff
    {
        [Required(ErrorMessage = "Nhập họ tên.")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "Nhập số điện thoại.")]
        public string Phone { get; set; }

        //[Required(ErrorMessage = "Nhập email.")]
        [EmailAddress(ErrorMessage = "Không đúng định dạng email.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Nhập nội dung.")]
        public string Content { get; set; }

        public int BrokerId { get; set; }

        public int RealId { get; set; }
    }
}