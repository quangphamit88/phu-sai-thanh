﻿using System;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using BussinessObject;
using BussinessObject.Bo.WebBo;
using OutsideX.Models;

namespace OutsideX
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Session_Start()
        {
            Application.Lock();
            var stringDate = DateTime.Now.ToString("yyyyMMdd");

            var model = BoFactory.SystemConfig.GetConfig<WebCounter>(SystemConfigBo.SystemConfigEnum.KEY_WEB_COUNTER) ?? new WebCounter { Date = stringDate, OnlineCount = 0, ToDayCount = 0, TotalCount = 0 };
            model.TotalCount++;
            if (model.Date == stringDate)
            {
                model.ToDayCount++;
                model.OnlineCount++;
            }
            else
            {
                model.Date = stringDate;
                model.ToDayCount = 1;
                model.OnlineCount = 1;
            }
            BoFactory.SystemConfig.SetConfig(model, SystemConfigBo.SystemConfigEnum.KEY_WEB_COUNTER);

            Application.UnLock();
        }
        protected void Session_End()
        {
            Application.Lock();
            var model = BoFactory.SystemConfig.GetConfig<WebCounter>(SystemConfigBo.SystemConfigEnum.KEY_WEB_COUNTER);
            if (model.OnlineCount > 1)
                model.OnlineCount = model.OnlineCount - 1;
            BoFactory.SystemConfig.SetConfig(model, SystemConfigBo.SystemConfigEnum.KEY_WEB_COUNTER);
            Application.UnLock();
        }

    }
}
