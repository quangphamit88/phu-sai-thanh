﻿using System.Linq;
using BussinessObject;
using System.Web.Mvc;
using MyConfig;
using MyUtility.Extensions;
using OutsideX.EF;
using OutsideX.Models;
using PhuSaiThanhLib.Enum;

namespace OutsideX.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var pageSize = MyConfiguration.Default.PageSize;
            const int pageIndex = 1;

            ViewBag.NavBar = "Index";
            int total = 0;
            var request = new RealSearchModel {
                PageIndex = pageIndex,
                PageSize = pageSize,
                BuyOrRent = RealBuyOrRentEnum.Buy.Value(),
                SortType = 1
            };
            var model = PstHelper.Real_Search(request);
            ViewBag.Total = model.Any() ? model[0].TotalRow ?? 0 : 0;
            ViewBag.PageSize = pageSize;
            ViewBag.PageIndex = pageIndex;
            return View(model);
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Faq()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.HeaderType = "HeaderType2";
            ViewBag.FooterType = "FooterType2";
            ViewBag.NavBar = "About";

            var db = new PstEntities();
            var model = db.Out_Article_GetDataById(8, null).FirstOrDefault() ?? new Out_Article_GetDataById_Result();
            return View(model);
        }

        public ActionResult KyGui()
        {

            ViewBag.HeaderType = "HeaderType2";
            ViewBag.FooterType = "FooterType2";
            ViewBag.NavBar = "KyGui";
            var db = new PstEntities();
            var model = db.Out_Article_GetDataById(11, null).FirstOrDefault() ?? new Out_Article_GetDataById_Result();
            return View(model);
        }
    }
}