﻿using System.Collections.Generic;
using System.Web.Mvc;
using BussinessObject;
using EntitiesObject.Entities.WebEntities;
using MyConfig;
using MyUtility.Extensions;
using OutsideX.Models;
using PhuSaiThanhLib.Enum;

namespace OutsideX.Controllers
{
    public class ApiController : Controller
    {
        /// <summary>
        /// tab = 0 nha dat gia tot,1 cho thue gia tot,2 nha dat da ban
        /// </summary>
        /// <param name="tabName"></param>
        /// <param name="tab"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <param name="priceType"></param>
        /// <returns></returns>
        public ActionResult HomeBds(string tabName, int tab = 1, int pageSize = 10, int pageIndex = 1, int priceType = 1)
        {
            pageSize = MyConfiguration.Default.PageSize;
            var request = new RealSearchModel {
                PageIndex = pageIndex,
                PageSize = pageSize,
                BuyOrRent = tab == 3 ? 0 : tab,
                SortType = priceType,
            };

            var total = 0;
            var model = PstHelper.Real_Search(request, ref total);
            ViewBag.Total = total;
            ViewBag.PageSize = pageSize;
            ViewBag.PageIndex = pageIndex;
            return PartialView(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tabName"></param>
        /// <param name="tab">tab: 1 & 2</param>
        /// <param name="pageIndex"></param>
        /// <param name="priceType">1: giam dan; 2: tang dan</param>
        /// <returns></returns>
        public ActionResult NhaDatBan(string tabName, int tab = 1, int pageSize = 10, int pageIndex = 1, int priceType = 1, bool isHot = false)
        {
            pageSize = MyConfiguration.Default.PageSize;
            var request = new RealSearchModel {
                PageIndex = pageIndex,
                PageSize = pageSize,
                BuyOrRent = 1,
                RealStatus = 0,
                SortType = priceType,
                IsHotPrice = isHot,
            };
            switch (tab) {
                case 2:
                    request.RealStatus = 2;
                    break;
                case 3:
                    request.RealStatus = 7;
                    break;
                default:
                    request.RealStatus = 0;
                    break;
            }


            var total = 0;
            var model = PstHelper.Real_Search(request, ref total);
            ViewBag.Total = total;
            ViewBag.PageSize = pageSize;
            ViewBag.PageIndex = pageIndex;
            return PartialView("HomeBds", model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tabName"></param>
        /// <param name="tab">tab: 1 & 2</param>
        /// <param name="pageIndex"></param>
        /// <param name="priceType">1: giam dan; 2: tang dan</param>
        /// <returns></returns>
        public ActionResult NhaDatChoThue(string tabName, int tab = 1, int pageSize = 10, int pageIndex = 1, int priceType = 1)
        {
            pageSize = MyConfiguration.Default.PageSize;
            var request = new RealSearchModel {
                PageIndex = pageIndex,
                PageSize = pageSize,
                BuyOrRent = 2,
                RealStatus = 0,
                SortType = priceType,
            };
            switch (tab) {
                case 2:
                    request.RealStatus = 2;
                    break;
                case 3:
                    request.RealStatus = 7;
                    break;
                default:
                    request.RealStatus = 0;
                    break;
            }
            var total = 0;
            var model = PstHelper.Real_Search(request, ref total);
            ViewBag.Total = total;
            ViewBag.PageSize = pageSize;
            ViewBag.PageIndex = pageIndex;
            return PartialView("HomeBds", model);
        }
        public ActionResult NhaDatTuongTu(RealBuyOrRentEnum saleOrRent, string tabName, int tab = 1, int pageSize = 10, int pageIndex = 1, int priceType = 1, int wardId = 0, double price = 0)
        {
            pageSize = MyConfiguration.Default.PageSize;

            var min = price - 0.5;
            if (min < 0)
                min = 0;
            var max = price + 0.5;

            var request = new RealSearchModel {
                PageIndex = pageIndex,
                PageSize = pageSize,
                BuyOrRent = saleOrRent.Value(),
                RealStatus = tab == 2 ? 3 : 0,
                SortType = priceType,
                MinPrice = (decimal)min,
                MaxPrice = (decimal)max,
                WardId = wardId,

            };
            var total = 0;
            var model = PstHelper.Real_Search(request, ref total);
            ViewBag.Total = total;
            ViewBag.PageSize = pageSize;
            ViewBag.PageIndex = pageIndex;
            return PartialView("HomeBds", model);
        }

        public ActionResult GetDistrict(int id = 0)
        {
            if (id == 0)
                return PartialView(new List<Xdistrict>());

            var model = BoFactory.AddressBook.GetDistrictByCity(id) ?? new List<Xdistrict>();
            return PartialView(model);
        }
        public ActionResult GetWard(int id = 0)
        {
            if (id == 0)
                return PartialView(new List<Xward>());

            var model = BoFactory.AddressBook.GetWardByDistrict(id) ?? new List<Xward>();
            return PartialView(model);
        }
        public ActionResult GetStreet(int id = 0)
        {
            if (id == 0)
                return PartialView(new List<Xstreet>());

            //var model = BoFactory.AddressBook.GetStreetByWard(id) ?? new List<Xstreet>();
            var model = BoFactory.AddressBook.GetStreetByDistrict(id) ?? new List<Xstreet>();
            return PartialView(model);
        }


    }
}