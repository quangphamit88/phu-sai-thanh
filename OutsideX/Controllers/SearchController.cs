﻿using System.Linq;
using System.Web.Mvc;
using MyConfig;
using MyUtility.Extensions;
using OutsideX.Models;

namespace OutsideX.Controllers
{
    public class SearchController : Controller
    {
        public ActionResult Index(SearchModel model)
        {
            ViewBag.HeaderType = "HeaderType3";
            var pageSize = MyConfiguration.Default.PageSize;
            model.PageSize = pageSize;
            var total = 0;
            //model.Keyword = MyUtility.StringCommon.RemoveUnicode(model.Keyword);

            var request = new RealSearchModel {
                WardId = model.WardId,
                CityId = model.CityId,
                DistrictId = model.DistrictId,
                DirectionMulti = model.DirectionMulti.ToStringJoin(),
                //OrderByDesc = model.PriceType == 2,
                BuyOrRent = model.TabIndex,
                //Title = MyUtility.StringCommon.RemoveUnicode(model.Keyword),
                Title = string.IsNullOrEmpty(model.Keyword) ? "" : StringExtension.ConvertToUnSign(model.Keyword ?? "").Replace("-", " "),
                PageSize = model.PageSize,
                PageIndex = model.PageIndex,
                StreetId = model.StreetId,
                RealTypeMulti = model.RealTypeMulti.ToStringJoin(),
                StreetTypeMulti = model.StreetTypeMulti.ToStringJoin(),
                RealStatusNowMulti = model.RealStatusNowMulti.ToStringJoin(),
                WardIdMulti = model.WardIdMulti.ToStringJoin(),
                StreetIdMulti = model.StreetIdMulti.ToStringJoin(),
                MaxPrice = model.PriceType,
                MinPrice = 0,
                SortType = model.SortType,
                IsHotPrice = model.IsPriceHot,
                RoomNumber = model.RoomNumber
            };

            if (model.PriceType == 11)
                request.MaxPrice = 0;
            model.Result = PstHelper.Real_Search(request);
            model.TotalRow = model.Result.Any() ? model.Result[0].TotalRow ?? 0 : 0;

            return View(model);
        }
    }
}