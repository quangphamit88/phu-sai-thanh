﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using BussinessObject;
using EntitiesObject.Entities.WebEntities;
using MyUtility.Extensions;
using OutsideX.EF;
using OutsideX.Models;
using PhuSaiThanhLib;
using PhuSaiThanhLib.Enum;

namespace OutsideX.Controllers
{
    public class LayoutController : Controller
    {

        #region RightBox

        public ActionResult RightBoxXemNhieu()
        {
            //var model = BoFactory.RealEstate.SearchTopView(5) ?? new List<Out_RealEstate_SelectTopView_Result>();
            var model = PstHelper.Real_Search(new RealSearchModel() {
                BuyOrRent = 0,
                PageIndex = 0,
                PageSize = 5,
                SortType = 11
            });
            return PartialView(model);
        }
        public ActionResult RightBoxTinTuc()
        {
            var db = new PstEntities();
            var model = db.Out_Article_GetTopArticle(10, 1).ToList();
            return View(model);
        }

        public ActionResult RightBoxQuan(int cityId = 0, int districtId = 0, int wardId = 0, int saleOrRent = 1)
        {
            if (cityId == 0)
                return Content("");
            ViewBag.cityId = cityId;
            ViewBag.districtId = districtId;
            ViewBag.saleOrRent = saleOrRent;
            var ci = BoFactory.AddressBook.GetCityList().FirstOrDefault(c => c.Id == cityId);
            if (ci != null)
                ViewBag.cityName = ci.CityName;
            var db = new PstEntities();
            var model = db.Out_RealEstate_CountByDistrict(cityId).ToList();
            return View(model.OrderBy(c => {
                if (c.DistrictName.StartsWith("Quận"))
                    return c.DistrictName.CleanNotNumber().ToInt();
                return 1000;
            }).ThenBy(c => c.DistrictName).ToList());
        }

        public ActionResult RightBoxPhuong(int cityId = 0, int districtId = 0, int wardId = 0, int saleOrRent = 1)
        {
            if (districtId == 0 && wardId == 0)
                return Content("");
            ViewBag.cityId = cityId;
            ViewBag.districtId = districtId;
            ViewBag.wardId = wardId;
            ViewBag.saleOrRent = saleOrRent;
            var dis = BoFactory.AddressBook.GetDistrictById(districtId);
            if (dis != null)
                ViewBag.districtName = dis.DistrictName;
            var model = BoFactory.RealEstate.CountByWard(districtId) ?? new List<Out_RealEstate_CountByWard_Result>();
            return View(model.OrderBy(c => {
                if (c.WardName.StartsWith("Phường"))
                    return c.WardName.CleanNotNumber().ToInt();
                return 1000;
            }).ThenBy(c => c.WardName).ToList());
        }
        public ActionResult RightBoxInfo()
        {
            return View();
        }
        #endregion

        #region BoxGiaTot


        public ActionResult BoxGiaTot(int tabIndex = 1)
        {
            var db = new PstEntities();
            ViewBag.model1 = db.Out_Real_SelectTopHot(20, RealBuyOrRentEnum.Buy.Value()).ToList();
            ViewBag.model2 = db.Out_Real_SelectTopHot(20, RealBuyOrRentEnum.Rent.Value()).ToList();
            return View(tabIndex);
        }

        #endregion

        #region ContactStaff

        public ActionResult ContactStaff()
        {
            return View();
        }


        public ActionResult ContactToStaff(int brokerId, int realId)
        {
            var model = new ContactToStaff {
                BrokerId = brokerId,
                RealId = realId
            };
            return PartialView(model);
        }
        public ActionResult ServiceContact()
        {
            var model = new ContactToStaff {
                BrokerId = 0
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult ContactToStaff(ContactToStaff model)
        {
            if (ModelState.IsValid) {
                BoFactory.UserFeedback.Add(new UserFeedback() {
                    CreateDate = DateTime.Now,
                    Mobile = model.Phone,
                    ContactName = model.FullName,
                    Email = model.Email,
                    Feedback = model.Content,
                    BrokerId = model.BrokerId,
                    KeywordS = StringExtension.ConvertToUnSign(string.Concat("#", model.Phone, " #", model.FullName, " #", model.Email)),
                    ProductId = model.RealId
                });
                var gmailSender = new GmailSender {
                    Subject = string.Format("Yêu cầu từ bđs: {0}", model.RealId),
                    Body = string.Format("" +
                                         "Tên: {0}<br/>" +
                                         "Điện thoại: {1}<br/>" +
                                         "Email: {2}<br/>" +
                                         "Nội dung: {3}<br/>" +
                                         "Mã BĐS: {4}<br/>" +
                                         "Thời gian: {5}", model.FullName, model.Phone, model.Email, model.Content, model.RealId,DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"))
                };

                //try {
                gmailSender.Send();
                //}
                //// ReSharper disable once EmptyGeneralCatchClause
                //catch {
                //}
                ViewBag.Success = true;

            }
            return PartialView(model);
        }

        #endregion

        //public ActionResult RenderPaging(int total, int pageSize = 1, int currentPage = 1)
        //{
        //    var model = new PagingModel(total, pageSize, currentPage, 5);
        //    return PartialView(model);
        //}

    }
}