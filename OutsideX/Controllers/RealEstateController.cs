﻿using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using BussinessObject;
using MyConfig;
using MyUtility.Extensions;
using OutsideX.EF;
using OutsideX.Models;
using PhuSaiThanhLib.Enum;
using WebUtility;

namespace OutsideX.Controllers
{
    public class RealEstateController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult NhaDatBan()
        {
            ViewBag.NavBar = "NhaDatBan";
            var pageSize = MyConfiguration.Default.PageSize;
            const int pageIndex = 1;

            int total = 0;
            var request = new RealSearchModel {
                PageIndex = pageIndex,
                PageSize = pageSize,
                BuyOrRent = RealBuyOrRentEnum.Buy.Value()
            };
            var model = PstHelper.Real_Search(request, ref total);
            ViewBag.Total = total;
            ViewBag.PageSize = pageSize;
            ViewBag.PageIndex = pageIndex;
            return View(model);
        }

        public ActionResult NhaDatChoThue()
        {
            ViewBag.NavBar = "NhaDatChoThue";
            var pageSize = MyConfiguration.Default.PageSize;
            const int pageIndex = 1;

            int total = 0;
            var request = new RealSearchModel {
                PageIndex = pageIndex,
                PageSize = pageSize,
                BuyOrRent = RealBuyOrRentEnum.Rent.Value()
            };
            var model = PstHelper.Real_Search(request, ref total);
            ViewBag.Total = total;
            ViewBag.PageSize = pageSize;
            ViewBag.PageIndex = pageIndex;
            return View(model);
        }

        public ActionResult NhaDatTuongTu(RealBuyOrRentEnum saleOrRent, int wardId = 0, double price = 0)
        {
            ViewBag.NavBar = "NhaDatTuongTu";
            var pageSize = MyConfiguration.Default.PageSize;
            const int pageIndex = 1;
            var min = price - 0.5;
            if (min < 0)
                min = 0;
            var max = price + 0.5;

            int total = 0;
            var request = new RealSearchModel {
                PageIndex = pageIndex,
                PageSize = pageSize,
                BuyOrRent = saleOrRent.Value(),
                MinPrice = (decimal)min,
                MaxPrice = (decimal)max,
                SortType = DropdownSortTypeEnum.MoiNhat.Value(),
                WardId = wardId
            };
            var model = PstHelper.Real_Search(request, ref total);
            ViewBag.Total = total;
            ViewBag.PageSize = pageSize;
            ViewBag.PageIndex = pageIndex;
            ViewBag.WardId = wardId;
            ViewBag.Price = price;
            ViewBag.saleOrRent = saleOrRent;
            return View(model);
        }

        public ActionResult CanMuaCanThue()
        {
            ViewBag.NavBar = "CanMuaCanThue";
            return View();
        }

        public ActionResult NhaDatGiaTot()
        {

            ViewBag.NavBar = "NhaDatChoThue";
            var pageSize = MyConfiguration.Default.PageSize;
            const int pageIndex = 1;

            int total = 0;
            var request = new RealSearchModel {
                PageIndex = pageIndex,
                PageSize = pageSize,
                BuyOrRent = RealBuyOrRentEnum.Buy.Value(),
                IsHotPrice = true

            };
            var model = PstHelper.Real_Search(request, ref total);
            ViewBag.Total = total;
            ViewBag.PageSize = pageSize;
            ViewBag.PageIndex = pageIndex;
            ViewBag.NavBar = "GiaTot";
            return View(model);
        }

        public ActionResult ChoThueGiaTot()
        {

            ViewBag.NavBar = "NhaDatChoThue";
            var pageSize = MyConfiguration.Default.PageSize;
            const int pageIndex = 1;

            int total = 0;
            var request = new RealSearchModel {
                PageIndex = pageIndex,
                PageSize = pageSize,
                BuyOrRent = RealBuyOrRentEnum.Rent.Value(),
                IsHotPrice = true

            };
            var model = PstHelper.Real_Search(request, ref total);
            ViewBag.Total = total;
            ViewBag.PageSize = pageSize;
            ViewBag.PageIndex = pageIndex;
            ViewBag.NavBar = "GiaTot";
            return View(model);
        }


        public ActionResult DetailCustomer(int id = 0)
        {
            var db = new PstEntities();
            var real = db.Reals.FirstOrDefault(c => c.Id == id);
            if (real != null) {
                real.ViewCount = (real.ViewCount ?? 0) + 1;
                db.SaveChanges();
            }

            var model = db.Out_Real_Get_By_Id(id).FirstOrDefault();
            if (model == null)
                return RedirectToAction("NhaDatBan");

            var coo = CookieHelper.Get("Recent1");
            if (string.IsNullOrEmpty(coo))
                coo = id.ToString();
            else {
                var list = coo.Split(',').Take(20).ToList();
                //ViewBag.Recent = db.Out_Real_RecentViewed(string.Join(";", list.Where(c => c != id.ToString()))).ToList();
                ViewBag.Recent = PstHelper.Real_Search(new RealSearchModel {
                    PageIndex = 1,
                    PageSize = 20,
                    BuyOrRent = model.BuyOrRent,
                    SortType = DropdownSortTypeEnum.MoiNhat.Value(),
                    Title = string.Join("#", list.Where(c => c != id.ToString()))
                });


                list.Insert(0, id.ToString());
                coo = string.Join(",", list.Distinct().Take(20));
            }
            CookieHelper.Add("Recent1", coo);

            var m = new decimal(0.5);

            ViewBag.Similar = PstHelper.Real_Search(new RealSearchModel {
                PageIndex = 1,
                PageSize = 20,
                BuyOrRent = model.BuyOrRent,
                MinPrice = (model.Price ?? 0) - m,
                MaxPrice = (model.Price ?? 0) + m,
                SortType = DropdownSortTypeEnum.MoiNhat.Value(),
                WardId = model.WardId
            });


            
            ViewBag.NavBar = "DetailCustomer";
            ViewBag.SaleOrRent = model.BuyOrRent;
            ViewBag.Price = model.Price;
            ViewBag.WardId = model.WardId;
            return View("DetailCustomer", model);
        }

        //public ActionResult DetailStaff()
        //{
        //    ViewBag.NavBar = "DetailStaff";
        //    return View();
        //}
    }
}