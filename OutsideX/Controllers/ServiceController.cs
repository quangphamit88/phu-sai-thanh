﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BussinessObject;
using EntitiesObject.Entities.WebEntities;
using OutsideX.EF;
using Out_Article_GetDataById_Result = OutsideX.EF.Out_Article_GetDataById_Result;

namespace OutsideX.Controllers
{
    public class ServiceController : Controller
    {
        public ActionResult Index(int pageIndex = 1)
        {
            ViewBag.HeaderType = "HeaderType2";
            ViewBag.FooterType = "FooterType2";
            ViewBag.NavBar = "Service";
            int total = 0;
            const int pageSize = 20;
            var model = BoFactory.Article.GetService(20, 10) ?? new List<Article>();
            ViewBag.Total = total;
            ViewBag.PageIndex = pageIndex;
            ViewBag.PageSize = pageSize;
            return View(model);
        }
        public ActionResult Detail(int id = 0)
        {
            ViewBag.HeaderType = "HeaderType2";
            ViewBag.FooterType = "FooterType2";
            ViewBag.NavBar = "ServiceDetail";
            var db = new PstEntities();
            var model = db.Out_Article_GetDataById(id, null).FirstOrDefault() ?? new Out_Article_GetDataById_Result();
            return View(model);
        }
    }
}