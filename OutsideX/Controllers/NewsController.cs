﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BussinessObject;
using EntitiesObject.Entities.WebEntities;
using MyConfig;

namespace OutsideX.Controllers
{
    public class NewsController : Controller
    {
        public ActionResult Index(int pageIndex = 1)
        {
            ViewBag.HeaderType = "HeaderType2";
            ViewBag.FooterType = "FooterType2";
            ViewBag.NavBar = "News";
            var pageSize = MyConfiguration.Default.PageSize;

            int total = 0;
            var model = BoFactory.Article.Search(20, 1, pageIndex, pageSize, ref total) ?? new List<Out_Article_Search_Result>();
            ViewBag.Total = total;
            ViewBag.PageIndex = pageIndex;
            ViewBag.PageSize = pageSize;
            return View(model);
        }
        public ActionResult Detail(int id = 0)
        {
            ViewBag.HeaderType = "HeaderType2";
            ViewBag.FooterType = "FooterType2";
            ViewBag.NavBar = "NewsDetail";
            switch (id)
            {
                case 7:
                    ViewBag.NavBar = "Job";
                    break;
                case 8:
                    ViewBag.NavBar = "About";
                    break;
                case 9:
                    ViewBag.NavBar = "FeedBack";
                    break;
            }

            var model = BoFactory.Article.Out_Article_GetDataById(id, null);
            if (model != null && model.Any())
            {
                // count view
                BoFactory.Article.Out_Article_CountView(model[0].ID);
                return View(model[0]);
            }
            return RedirectToAction("Index");
        }
    }
}