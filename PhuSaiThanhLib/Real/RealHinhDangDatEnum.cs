﻿using System.ComponentModel;

namespace PhuSaiThanhLib.Real
{
    public enum RealHinhDangDatEnum
    {
        [Description("Vuông vức")]
        VuongVut = 1,
        [Description("Nở hậu")]
        NoHau = 2,
        [Description("Không vuông vức")]
        KhongVuongVut = 3
    }
}
