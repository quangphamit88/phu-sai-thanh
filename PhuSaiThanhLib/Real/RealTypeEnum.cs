﻿using System.ComponentModel;

namespace PhuSaiThanhLib.Real
{
    public enum RealTypeEnum
    {
        //[Description("Nhà Mặt Phố")]
        //NhaMatPho = 1,
        [Description("Nhà Riêng")]
        NhaRieng = 2,
        [Description("Biệt Thự")]
        BietThu = 3,
        [Description("Căn Hộ")]
        CanHo = 4,
        [Description("Nhà Kho")]
        NhaKho = 5,
        [Description("Nhà Xưởng")]
        NhaXuong = 6,
        [Description("Đất nền")]
        DatNen = 7,

        [Description("Phòng")]
        Phong = 8,
        [Description("Mặt tiền")]
        MatTien = 9,
    }

    public enum RealTypeBuyEnum
    {
        //[Description("Nhà Mặt Phố")]
        //NhaMatPho = 1,
        [Description("Nhà Riêng")]
        NhaRieng = 2,
        [Description("Biệt Thự")]
        BietThu = 3,
        [Description("Căn Hộ")]
        CanHo = 4,
        [Description("Nhà Kho")]
        NhaKho = 5,
        [Description("Nhà Xưởng")]
        NhaXuong = 6,
        [Description("Đất nền")]
        DatNen = 7,
    }

    //public enum RealTypeRentEnum
    //{
    //    NhaRieng = 2,
    //    [Description("Biệt Thự")]
    //    BietThu = 3,
    //    [Description("Căn Hộ")]
    //    CanHo = 4,
    //    [Description("Nhà Kho")]
    //    NhaKho = 5,
    //    [Description("Nhà Xưởng")]
    //    NhaXuong = 6,
    //    [Description("Đất nền")]
    //    DatNen = 7,

    //    [Description("Phòng")]
    //    Phong = 8,
    //    [Description("Mặt tiền")]
    //    MatTien = 9,
    //}
}