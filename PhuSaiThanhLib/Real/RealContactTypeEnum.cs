﻿using System.ComponentModel;

namespace PhuSaiThanhLib.Real
{
    public enum RealContactTypeEnum
    {
        [Description("Chính chủ")]
        Master = 1,

        [Description("Môi giới")]
        Broker = 2,

        [Description("Đầu tư")]
        Invester = 3,
    }
}