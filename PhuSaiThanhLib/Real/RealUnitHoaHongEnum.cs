﻿using System.ComponentModel;

namespace PhuSaiThanhLib.Real
{
    public enum RealUnitHoaHongEnum
    {
        [Description("%")]
        Percent = 0,

        [Description("Triệu")]
        Million = 1000,

        [Description("Tỷ")]
        Billion = 1000000,
    }
}