﻿using System.ComponentModel;

namespace PhuSaiThanhLib.Real
{
    public enum RealStatusNowEnum
    {
        [Description("Nhà nát")]
        NhaNat = 1,

        [Description("Ở được")]
        ODuoc = 2,

        [Description("Nhà mới")]
        NhaMoi = 3
    }
}