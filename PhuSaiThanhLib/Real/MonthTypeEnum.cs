﻿using System.ComponentModel;

namespace PhuSaiThanhLib.Real
{
    public enum MonthTypeEnum
    {
        [Description("Tháng")]
        Month = 1,

        [Description("Năm")]
        Year = 12
    }
}