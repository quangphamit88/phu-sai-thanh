﻿using System.ComponentModel;

namespace PhuSaiThanhLib.Real
{
    public enum RealStatusEnum
    {
        [Description("Đang Bán")]
        DangBan = 1,

        [Description("Đã Bán")]
        DaBan = 2,

        [Description("Đã Cho Thuê")]
        DaChoThue = 3,

        [Description("Đang Cho Thuê")]
        DangChoThue = 4,

        [Description("Chờ duyệt")]
        Choduyet = 5,

        [Description("Đầu tư")]
        DauTu = 6,

        [Description("Ngưng bán")]
        NgungBan = 7,

        [Description("Ngưng cho thuê")]
        NgungThue = 8,

    }

    public enum RealStatusBuyEnum
    {
        [Description("Đang Bán")]
        DangBan = 1,

        [Description("Đã Bán")]
        DaBan = 2,
       
        [Description("Chờ duyệt")]
        Choduyet = 5,

        [Description("Đầu tư")]
        DauTu = 6,

        [Description("Ngưng bán")]
        NgungBan = 7,
    }

    public enum RealStatusRentEnum
    {
        [Description("Đã Cho Thuê")]
        DaChoThue = 3,

        [Description("Đang Cho Thuê")]
        DangChoThue = 4,

        [Description("Chờ duyệt")]
        Choduyet = 5,

        [Description("Đầu tư")]
        DauTu = 6,

        [Description("Ngưng cho thuê")]
        NgungThue = 8,
    }
}
