﻿using System.Net;
using System.Net.Mail;

namespace PhuSaiThanhLib
{
    public class GmailSender
    {
        public string GmailUsername { get; set; }
        public string GmailDisplayName { get; set; }
        public string GmailPassword { get; set; }
        public string GmailHost { get; set; }
        public int GmailPort { get; set; }
        public bool GmailSsl { get; set; }

        private string ToEmail { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsHtml { get; set; }

        public GmailSender()
        {
            GmailHost = "smtp.gmail.com";
            GmailPort = 25; // Gmail can use ports 25, 465 & 587; but must be 25 for medium trust environment.
            GmailSsl = true;
            GmailUsername = "web.phusaithanh@gmail.com";
            GmailPassword = "19001221";

            GmailDisplayName = "Web Phú Sài Thành";
            //ToEmail = "quangphamit88@gmail.com";
            ToEmail = "phusaithanh.pst@gmail.com";
            IsHtml = true;
        }

        //public GmailSender(string username, string pass, string displayName)
        //{
        //    GmailHost = "smtp.gmail.com";
        //    GmailPort = 25; // Gmail can use ports 25, 465 & 587; but must be 25 for medium trust environment.
        //    GmailSsl = true;
        //    GmailUsername = username;
        //    GmailPassword = pass;

        //    GmailDisplayName = displayName;
        //}

        public void Send()
        {
            var smtp = new SmtpClient {
                Host = GmailHost,
                Port = GmailPort,
                EnableSsl = GmailSsl,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(GmailUsername, GmailPassword)
            };

            using (var message = new MailMessage(new MailAddress(GmailUsername, GmailDisplayName), new MailAddress(ToEmail))) {
                message.Subject = Subject;
                message.Body = Body;
                message.IsBodyHtml = IsHtml;
                smtp.Send(message);
            }
        }
    }
}