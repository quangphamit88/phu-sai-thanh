﻿using System.ComponentModel;

namespace PhuSaiThanhLib.Enum
{
    public enum DropdownSortTypeEnum
    {
        [Description("Tin mới nhất")]
        MoiNhat = 1,
        [Description("Bất động sản nổi bật")]
        NoiBat = 2,
        [Description("Giá giảm dần")]
        GiamDan = 3,
        [Description("Giá tăng dần")]
        TangDan = 4,
    }
}
