﻿using System.ComponentModel;

namespace PhuSaiThanhLib.Enum
{
    public enum EmployeeLevelEnum
    {
        [Description("--")]
        Level0 = 0,
        [Description("Cấp 1")]
        Level1 = 1,
        [Description("Cấp 2")]
        Level2 = 2,
        [Description("Cấp 3")]
        Level3 = 3,
        [Description("Cấp 4")]
        Level4 = 4,
    }
}
