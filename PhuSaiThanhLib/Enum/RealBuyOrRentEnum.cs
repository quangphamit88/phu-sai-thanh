﻿using System.ComponentModel;

namespace PhuSaiThanhLib.Enum
{
    public enum RealBuyOrRentEnum
    {
        [Description("Nhà Đất Bán")]
        Buy = 1,

        [Description("Nhà Cho Thuê")]
        Rent = 2
    }
}