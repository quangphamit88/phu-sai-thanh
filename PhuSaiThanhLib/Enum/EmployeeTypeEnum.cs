﻿

using System.ComponentModel;

namespace PhuSaiThanhLib.Enum
{
    public enum EmployeeTypeEnum
    {
        [Description("Chuyên Viên Kinh Doanh")]
        Customer = 1,
        [Description("Chuyên Viên Khai Thác")]
        Real = 2
    }

    public enum EmployeeTypeEnumDisplay2
    {
        [Description("Chăm Sóc Khách Hàng")]
        Customer = 1,
        [Description("Chuyên Viên Khai Thác")]
        Real = 2
    }
}
