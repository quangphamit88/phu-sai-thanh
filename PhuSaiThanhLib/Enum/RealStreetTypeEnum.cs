﻿using System.ComponentModel;

namespace PhuSaiThanhLib.Enum
{
    public enum RealStreetTypeEnum
    {
        [Description("Mặt tiền kinh doanh")]
        MatTien = 1,

        [Description("Mặt tiền nội bộ")]
        MatTienNoiBo = 2,

        [Description("Hẻm trên 5 mét")]
        HemTren5Met = 3,

        [Description("Hẻm dưới 5 mét")]
        HemDuoi5Met = 4,
    }
}