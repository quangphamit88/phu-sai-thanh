﻿using System.ComponentModel;

namespace PhuSaiThanhLib.Enum
{
    public enum RealSaleOrRentEnum
    {
        [Description("Nhà đất bán")]
        Sale = 1,
        [Description("Nhà đất cho thuê")]
        Rent = 2,
    }
}
