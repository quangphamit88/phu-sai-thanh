﻿using System.ComponentModel;

namespace PhuSaiThanhLib.Enum
{
    public enum SaleOrRentEnum
    {
        [Description("Nhà Đất Bán")]
        NhaDatBan = 1,

        [Description("Nhà Cho Thuê")]
        NhaDatChoThue = 2
    }
}