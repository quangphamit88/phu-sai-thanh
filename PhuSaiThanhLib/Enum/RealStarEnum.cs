﻿using System.ComponentModel;

namespace PhuSaiThanhLib.Enum
{
    public enum RealStarEnum
    {
        [Description("Vip 1")]
        Star1 = 1,
        [Description("Vip 2")]
        Star2 = 2,
        [Description("Vip 3")]
        Star3 = 3,
    }
}
