﻿using System.ComponentModel;

namespace PhuSaiThanhLib.Enum
{
    public enum DeptTypeEnum
    {
        [Description("Không xác định")]
        All = 0,

        [Description("Bất động sản")]
        Bds = 2,

        [Description("Khách hàng")]
        Customer = 1
    }
    public enum DeptType2Enum
    {
        [Description("--")]
        All = 0,

        [Description("BĐS")]
        Bds = 2,

        [Description("KH")]
        Customer = 1
    }
    public enum DeptType3Enum
    {
        [Description("Bất động sản")]
        Bds = 2,

        [Description("Khách hàng")]
        Customer = 1
    }
}