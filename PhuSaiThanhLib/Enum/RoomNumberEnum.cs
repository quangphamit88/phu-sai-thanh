﻿using System.ComponentModel;

namespace PhuSaiThanhLib.Enum
{
    public enum RoomNumberEnum
    {
        [Description("1 +")]
        Num1 = 1,

        [Description("2 +")]
        Num2 = 2,
        
        [Description("3 +")]
        Num3 = 3,
        
        [Description("4 +")]
        Num4 = 4,
        
        [Description("5 +")]
        Num5 = 5,
        
        [Description("6 +")]
        Num6 = 6,
        
        [Description("7 +")]
        Num7 = 7,
        
        [Description("8 +")]
        Num8 = 8,
        
        [Description("9 +")]
        Num9 = 9,

        [Description("10 +")]
        Num10 = 10,
    }
}
