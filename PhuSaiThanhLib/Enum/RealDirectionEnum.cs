﻿using System.ComponentModel;

namespace PhuSaiThanhLib.Enum
{
    public enum RealDirectionEnum
    {
        [Description("Đông")]
        Dong = 1,
        [Description("Tây")]
        Tay = 2,
        [Description("Nam")]
        Nam = 3,
        [Description("Bắc")]
        Bac = 4,
        [Description("Đông Nam")]
        DongNam = 5,
        [Description("Đông Bắc")]
        DongBac = 6,
        [Description("Tây Nam")]
        TayNam = 7,
        [Description("Tây Bắc")]
        TayBac = 8,

        [Description("Đông Tứ Trạch")]
        DongTt = 9,
        [Description("Tây Tứ Trạch")]
        TayTt = 10
    }
}