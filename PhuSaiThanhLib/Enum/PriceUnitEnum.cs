﻿using System.ComponentModel;

namespace PhuSaiThanhLib.Enum
{
    public enum PriceUnitEnum
    {

        [Description("Triệu")]
        Million = 1000000,

        [Description("Tỷ")]
        Billion = 1000000000
    }
}