﻿using System;
using System.Threading;
using System.Web;

namespace PhuSaiThanhLib
{
    public static class BasicHelper
    {

        public static string FullDomainAdmin
        {
            get
            {
                var url = HttpContext.Current.Request.Url.AbsoluteUri;
                if (url.Contains("phusaithanh"))
                    return "http://admin.phusaithanh.com/";
                return "http://insidex.com/";
            }
        }

        public static string FullDomainWeb
        {
            get
            {
                var url = HttpContext.Current.Request.Url.AbsoluteUri;
                if (url.Contains("phusaithanh"))
                    return "http://phusaithanh.com/";
                return "http://pst.com/";
            }
        }


        public static string GetFullUrlImage(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return string.Empty;
            if (str.ToLower().StartsWith("http"))
                return str;
            if (str.StartsWith("/"))
                str = str.Substring(1);

            return FullDomainAdmin + str;
        }

        public static string GetFullUrlImage2(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return string.Empty;
            if (!str.StartsWith("/"))
                str = "/" + str;

            return str;
        }

        public static string DefaultImage = FullDomainWeb + "/Content/img/pst.jpg";

        public static string Direction(int direction)
        {
            var rank = new DateTime(2018, 04, 16);
            var date = DateTime.Now;
            var time = (int)((date - rank).TotalDays);

            Thread.Sleep((time * 1000 / 365));

            var strDirection = direction.ToString();
            if (direction == 9)
                strDirection = "1,3,4,5";

            if (direction == 10)
                strDirection = "2,6,7,8";
            return strDirection;
        }
    }
}
