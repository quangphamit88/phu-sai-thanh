﻿/*********************************************************************
* Author: ThongNT
* DateCreate: 06-25-2014 
* Description: BoFactory  
* ####################################################################
* Author:......................
* DateModify: .................
* Description: ................
* 
*********************************************************************/

using BussinessObject.Bo.WebBo;

namespace BussinessObject
{
    public class BoFactory
    {
        #region Category
        public static CategoryBo Category { get { return new CategoryBo(); } }

        #endregion

        #region Article
        public static ArticleBo Article { get { return new ArticleBo();} }

        #endregion

        #region Comment
        public static CommentBo Comment { get { return new CommentBo(); } }

        #endregion

        #region Account
        public static AccountBo Account { get { return new AccountBo(); } }

        #endregion

        #region Clan
        public static ClanBo Clan { get { return new ClanBo(); } }

        #endregion

        #region AccountInfoComment
        public static AccountInfoCommentBo AccountInfoComment { get { return new AccountInfoCommentBo(); } }

        #endregion

        #region BatDongSan
        public static RealEstateBo RealEstate { get { return new RealEstateBo(); } }
        public static AddressBookBo AddressBook { get { return new AddressBookBo(); } }
        public static BrokerAccountBo BrokerAccount { get { return new BrokerAccountBo(); } }
        public static UserFeedbackBo UserFeedback { get { return new UserFeedbackBo(); } }
        public static RealEstatePermissionBo RealEstatePermission { get { return new RealEstatePermissionBo(); } }
        public static SystemConfigBo SystemConfig { get { return new SystemConfigBo(); } }
        #endregion
    }
}
