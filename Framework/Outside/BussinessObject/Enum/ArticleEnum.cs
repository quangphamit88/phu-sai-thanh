﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessObject.Enum
{
    public enum ArticleHomeTopTypeEnum
    {
        [Description("Mới nhất")]
        MoiNhat = 1,

        [Description("Xem nhiều")]
        XemNhieu = 2,


        [Description("Chính sách - Nội quy")]
        ChinhSachNoiQuy = 3
    }

    public enum ArticleStatusEnum
    {
        [Description("Soạn thảo")]
        SoanThao = 0,

        [Description("Đang chờ duyệt")]
        DangChoDuyet = 1,

        [Description("Hiển thị")]
        HienThi = 2,

        [Description("Ẩn")]
        An = 3,
    }

    public enum ArticleGuildView
    {
        [Description("Chỉ cho phép thành viên trong bang xem")]
        GuildView = 1,

        [Description("Cho phép tất cả mọi người cùng xem")]
        AllView = 0
    }
}
