﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessObject.Enum.Account
{
    public enum AccountTypeEnum
    {
        [Description("Tài khoản thường")]
        Normal = 0,
        
        [Description("Mod")]
        Mod = 1,

        [Description("Admin")]
        Admin = 2
    }
}
