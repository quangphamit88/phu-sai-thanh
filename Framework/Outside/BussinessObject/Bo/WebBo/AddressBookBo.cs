﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using BussinessObject.Helper;
using BussinessObject.Models;
using DataAccess;
using EntitiesObject.Entities.WebEntities;
using MyUtility.Extensions;

namespace BussinessObject.Bo.WebBo
{
    public class AddressBookBo : BaseBo<AddressBook>
    {
        public AddressBookBo()
            : base(DaoFactory.AddressBook)
        {
        }

        public ContactBook GetContactBook(long contactId)
        {
            return DaoFactory.ContactBook.GetOne(contactId);
        }

        public Out_AddressBook_SelectById_Result GetById(long addressId)
        {
            return DaoFactory.AddressBook.GetById(addressId);
        }

        public List<Xcity> GetCityList()
        {
            return DaoFactory.Xcity.GetCityList();
        }

        public List<Xdistrict> GetDistrictByCity(int cityId)
        {
            return DaoFactory.Xdistrict.GetDistrictByCity(cityId);
        }

        public List<Xward> GetWardByDistrict(int districtId)
        {
            return DaoFactory.Xward.GetWardByDistrict(districtId);
        }

        public List<Xstreet> GetStreetByWard(int wardId)
        {
            return DaoFactory.Xstreet.GetStreetByWard(wardId);
        }

        public List<Xstreet> GetStreetByDistrict(int districtId)
        {
            return DaoFactory.Xstreet.GetStreetByDistrict(districtId);
        }

        public Xdistrict GetDistrictById(int districtId)
        {
            return DaoFactory.Xdistrict.GetOne(districtId);
        }
    }
}
