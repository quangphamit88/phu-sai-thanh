﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using BussinessObject.Helper;
using BussinessObject.Models;
using DataAccess;
using EntitiesObject.Entities.WebEntities;
using MyUtility.Extensions;

namespace BussinessObject.Bo.WebBo
{
    public class SystemConfigBo : BaseBo<SystemConfig>
    {
        private string KEY_WEB_CONTACT_INFO = "KEY_WEB_CONTACT_INFO";
        public SystemConfigBo()
            : base(DaoFactory.SystemConfig)
        {
        }

        public ContactConfig GetContactConfig()
        {
            var config = DaoFactory.SystemConfig.GetOne("KEY_WEB_CONTACT_INFO");
            if (config == null) return null;
            var contact = Newtonsoft.Json.JsonConvert.DeserializeObject<ContactConfig>(config.ConfigValue);
            return contact;
        }

        public bool SetContactConfig(ContactConfig config)
        {
            var obj = DaoFactory.SystemConfig.GetOne("KEY_WEB_CONTACT_INFO");
            if (obj == null)
            {
                return DaoFactory.SystemConfig.Add(new SystemConfig
                {
                    ConfigKey = KEY_WEB_CONTACT_INFO,
                    ConfigValue = Newtonsoft.Json.JsonConvert.SerializeObject(config),
                    IsEnable = true,
                    Notes = "Thong tin lien he"
                });
            }
            else
            {
                obj.ConfigValue = Newtonsoft.Json.JsonConvert.SerializeObject(config);
                return DaoFactory.SystemConfig.Save(obj);
            }
        }

        public bool SetConfig<T>(T model, SystemConfigEnum keyEnum)
        {
            var key = keyEnum.Text();
            var obj = DaoFactory.SystemConfig.GetOne(key);
            if (obj == null)
            {
                return DaoFactory.SystemConfig.Add(new SystemConfig
                {
                    ConfigKey = key,
                    ConfigValue = Newtonsoft.Json.JsonConvert.SerializeObject(model),
                    IsEnable = true,
                    Notes = ""
                });
            }
            else
            {
                obj.ConfigValue = Newtonsoft.Json.JsonConvert.SerializeObject(model);
                return DaoFactory.SystemConfig.Save(obj);
            }
        }

        public T GetConfig<T>(SystemConfigEnum keyEnum)
        {
            var key = keyEnum.Text();
            var config = DaoFactory.SystemConfig.GetOne(key);
            if (config == null) 
                return default(T);

            var contact = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(config.ConfigValue);
            return contact;
        }
       


        public class ContactConfig
        {
            public string BackgroundUrl { get; set; }
            public string Email { get; set; }
            public string Address { get; set; }
            public string Name1 { get; set; }
            public string Name2 { get; set; }
            public string Phone1 { get; set; }
            public string Phone2 { get; set; }

            public string Morning { get; set; }
            public string Afternoon { get; set; }
            public string Sunday { get; set; }
            public string Saturday { get; set; }
        }

        public enum SystemConfigEnum
        {
            [Description("KEY_WEB_COUNTER")]
            KEY_WEB_COUNTER = 1
        }
    }
}
