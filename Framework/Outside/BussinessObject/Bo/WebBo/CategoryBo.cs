﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using BussinessObject.Helper;
using BussinessObject.Models;
using DataAccess;
using EntitiesObject.Entities.WebEntities;
using MyUtility.Extensions;

namespace BussinessObject.Bo.WebBo
{
    public class CategoryBo : BaseBo<Category>
    {
        public CategoryBo()
            : base(DaoFactory.Category)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameID"></param>
        /// <param name="status">null: bỏ qua user access</param>
        /// <returns></returns>
        public List<Out_Category_Get_Result> Category_Get(int? gameID, bool? status)
        {
            return DaoFactory.Category.Category_Get(gameID, status);
        }

        public List<Out_Category_GetDataById_Result> Out_Category_GetDataById(int gameID, int id)
        {
            return DaoFactory.Category.Out_Category_GetDataById(gameID, id);
        }

        public List<Out_Category_GetByIsUserAccess_Result> GetByIsUserAccess(int gameID, int isUserAccess)
        {
            return DaoFactory.Category.GetByIsUserAccess(gameID, isUserAccess);
        }
    }
}
