﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using BussinessObject.Helper;
using BussinessObject.Models;
using DataAccess;
using EntitiesObject.Custom;
using EntitiesObject.Entities.WebEntities;
using EntitiesObject.Message;
using EntitiesObject.Message.DtoResponse;
using EntitiesObject.Message.Model;
using MyUtility.Extensions;

namespace BussinessObject.Bo.WebBo
{
    public class RealEstatePermissionBo : BaseBo<RealEstatePermission>
    {
        public RealEstatePermissionBo()
            : base(DaoFactory.RealEstatePermission)
        {
        }

        public int GetAdminRole(int adminId)
        {
            return DaoFactory.RealEstatePermission.GetAdminRole(adminId);
        }

        public RealEstatePermission GetPermission(long productId, int adminId)
        {
            return DaoFactory.RealEstatePermission.GetPermission(productId, adminId);
        }

    }
}
