﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using BussinessObject.Helper;
using BussinessObject.Models;
using DataAccess;
using EntitiesObject.Entities.WebEntities;
using MyUtility.Extensions;

namespace BussinessObject.Bo.WebBo
{
    public class UserFeedbackBo : BaseBo<UserFeedback>
    {
        public UserFeedbackBo()
            : base(DaoFactory.UserFeedback)
        {
        }

    }
}
