﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using BussinessObject.Helper;
using BussinessObject.Models;
using DataAccess;
using EntitiesObject.Custom;
using EntitiesObject.Entities.WebEntities;
using EntitiesObject.Message;
using EntitiesObject.Message.DtoResponse;
using EntitiesObject.Message.Model;
using MyUtility.Extensions;

namespace BussinessObject.Bo.WebBo
{
    public class BrokerAccountBo : BaseBo<BrokerAccount>
    {
        public BrokerAccountBo()
            : base(DaoFactory.BrokerAccount)
        {
        }

        public Out_BrokerAccount_SelectById_Result GetById(int brokerId)
        {
            return DaoFactory.BrokerAccount.GetById(brokerId);
        }
        public BrokerAccount GetByAdmin(int adminId)
        {
            return DaoFactory.BrokerAccount.GetByAdmin(adminId);
        }
    }
}
