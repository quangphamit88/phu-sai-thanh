﻿/**********************************************************************
 * Author:      LongNP
 * DateCreate:  2017-05-04
 *********************************************************************/

using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using EntitiesObject.Entities.WebEntities;

namespace BussinessObject.Bo.WebBo
{
    public class AccountBo : BaseBo<Account>
    {
        public AccountBo() : base(DaoFactory.Account) { }

        /// <summary>
        /// Tạo mới
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public int Insert(Account item)
        {
            return DaoFactory.Account.Insert(item);
        }

        /// <summary>
        /// Get account by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="totalArticle"></param>
        /// <param name="totalComment"></param>
        /// <returns></returns>
        public Out_Account_GetById_Result GetInfo(int id)
        {
            return DaoFactory.Account.GetInfo(id);
        }

        /// <summary>
        /// Get account by account game id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Out_Account_GetByGameAccountID_Result GetInfoByGameAccountID(int gameID, int accountID)
        {
            return DaoFactory.Account.GetInfoByGameAccountID(gameID, accountID);
        }

        /// <summary>
        /// Get full account info by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Out_Account_GetFullInfoById_Result GetFullInfo(int id)
        {
            return DaoFactory.Account.GetFullInfo(id);
        }

        /// <summary>
        /// kiểm tra tài khoản game đã tồn tại trong forum hay chưa
        /// </summary>
        /// <param name="gameID">load GameID từ MyConfig</param>
        /// <param name="gameAccountID"></param>
        /// <param name="gameAccountName"></param>
        /// <returns></returns>
        public int CheckIsExist(int gameID, int gameAccountID, string gameAccountName)
        {
            return DaoFactory.Account.CheckIsExist(gameID, gameAccountID, gameAccountName);
        }

        /// <summary>
        /// cập nhật thông tin tài khoản đăng nhập
        /// </summary>
        /// <param name="id"></param>
        /// <param name="avatar"></param>
        /// <param name="totalGold"></param>
        /// <param name="level"></param>
        /// <param name="playTime"></param>
        /// <returns></returns>
        public bool UpdateInfo(int id, string avatar, decimal totalGold, int level, int playTime)
        {
            return DaoFactory.Account.UpdateInfo(id, avatar, totalGold, level, playTime);
        }

        /// <summary>
        /// Get account by GameId , AccountId
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public Out_Account_GetByGameIdAccountId_Result Out_Account_GetByGameIdAccountId(int gameId, int accountId)
        {
            return DaoFactory.Account.Out_Account_GetByGameIdAccountId(gameId, accountId);
        }

        /// <summary>
        /// cập nhật avatar
        /// </summary>
        /// <param name="id"></param>
        /// <param name="avatar"></param>
        /// <returns></returns>
        public bool UpdateAvatar(int id, string avatar)
        {
            return DaoFactory.Account.UpdateAvatar(id, avatar);
        }

        public bool Login(string userName, string password, ref string message, ref Admin admin)
        {
            var isOk = DaoFactory.Account.Login(userName, password, ref admin);
            if (!isOk)
            {
                message = "Tài khoản không tồn tại";
                return false;
            }
            if (!admin.IsActive)
            {
                message = "Tài khoản đã bị khóa. Vui lòng liên hệ Quản Trị!";
                return false;
            }
            if(admin.Password != password)
            {
                message = "Tên đăng nhập hoặc mật khẩu không đúng!";
                return false;
            }
            return isOk;
        }
    }
}
