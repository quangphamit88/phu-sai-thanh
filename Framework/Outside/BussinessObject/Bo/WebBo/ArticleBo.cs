﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using EntitiesObject.Entities.WebEntities;

namespace BussinessObject.Bo.WebBo
{
    public class ArticleBo : BaseBo<Article>
    {
        public ArticleBo() : base(DaoFactory.Article) { }

        #region get top bài viết mới và view nhiều nhất xxx
        ///// <summary>
        ///// 1: moi nhat, 2: xem nhieu nhat
        ///// </summary>
        ///// <param name="top"></param>
        ///// <param name="type"></param>
        ///// <returns></returns>
        //public List<Out_Article_GetTopArticle_Result> GetTopArticle(int top, int type)
        //{
        //    return DaoFactory.Article.Out_Article_GetTopArticle(top, type);
        //}

        #endregion

        #region get danh sách bài viết theo CatagoryId xxx

        public List<Out_Article_GetDataByCatagoryId_Result> Out_Article_GetDataByCatagoryId(int catagoryId, int startIndex, int pageLength, int orderBy, int orderDirection, out int totalRow)
        {
            return DaoFactory.Article.Out_Article_GetDataByCatagoryId(catagoryId, startIndex, pageLength, orderBy,
                orderDirection, out totalRow);
        }

        #endregion

        #region Get Article Detail by Id xxx

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status">null: lấy bỏ qua status</param>
        /// <returns></returns>
        public List<Out_Article_GetDataById_Result> Out_Article_GetDataById(int id, int? status)
        {
            return DaoFactory.Article.Out_Article_GetDataById(id, status);
        }
        #endregion

        #region Get top bài viết liên quan

        public List<Out_Article_GetRelatedArticle_Result> Out_Article_GetRelatedArticle(int top, int categoryId, int articleId)
        {
            return DaoFactory.Article.Out_Article_GetRelatedArticle(top, categoryId, articleId);
        }
        #endregion

        #region Count View Article

        public int Out_Article_CountView(int articleId)
        {
            return DaoFactory.Article.Out_Article_CountView(articleId);
        }

        #endregion

        #region kiểm tra article có đủ điều kiện hay không xxx

        /// <summary>
        /// kiểm tra article có đủ điều kiện hay không
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool CheckIsExist(int id)
        {
            return DaoFactory.Article.CheckIsExist(id);
        }

        #endregion

        public int Article_Insert(Article input)
        {
            return DaoFactory.Article.Article_Insert(input);
        }

        public bool Article_UpdateContent(Article input)
        {
            return DaoFactory.Article.Article_UpdateContent(input);
        }

        public List<Out_Article_ListByUserId_Result> GetListByUserId(int userId, int pageIndex, int pagelengh, out int totalRow)
        {
            return DaoFactory.Article.GetListByUserId(userId, pageIndex, pagelengh, out totalRow);
        }

        public List<Out_Article_ListByUserIdComment_Result> GetListByUserIdAndComment(int userId, int pageIndex, int pagelengh, out int totalRow)
        {
            return DaoFactory.Article.GetListByUserIdAndComment(userId, pageIndex, pagelengh, out totalRow);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="modifyBy"></param>
        /// <param name="modifyByType"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public bool EditStatus(int id, int modifyBy, short modifyByType, short status)
        {
            return DaoFactory.Article.EditStatus(id, modifyBy, modifyByType, status);
        }

        /// <summary>
        /// lấy thông tin bài viết for EditStatus, CheckPermission...
        /// xxx
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Out_Article_GetInfoByID_Result GetInfoByID(int id)
        {
            return DaoFactory.Article.GetInfoByID(id);
        }

        /// <summary>
        /// đếm số bài viết đã tạo trong ngày 
        /// </summary>
        /// <returns></returns>
        public int GetNumberPostToday(int userId)
        {
            return DaoFactory.Article.Out_Article_CountArticleInDay(userId);
        }

        public bool CheckUserMemberGuild(int userId, int articleId)
        {
            return DaoFactory.Article.CheckUserMemberGuild(userId, articleId);
        }

        public  List<Out_Forum_GetArticleByGuild_Result> Forum_GetArticleByGuild(int guild, int top)
        {
            return DaoFactory.Article.Forum_GetArticleByGuild(guild, top);
        }

        public List<Out_Article_Search_Result> Search(int top, int status, int pageIndex, int pageSize, ref int totalRows)
        {
            return DaoFactory.Article.Search(top, status, pageIndex, pageSize, ref totalRows);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="top"></param>
        /// <param name="cateId">10: dich vu, 3: gioi thieu</param>
        /// <returns></returns>
        public List<Article> GetService(int top, int cateId)
        {
            return DaoFactory.Article.GetService(top, cateId);
        }
    }
}
