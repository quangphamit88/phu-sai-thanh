﻿/**********************************************************************
 * Author:      LongNP
 * DateCreate:  2017-05-04
 *********************************************************************/

using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using EntitiesObject.Entities.WebEntities;

namespace BussinessObject.Bo.WebBo
{
    public class CommentBo : BaseBo<Comment>
    {
        public CommentBo() : base(DaoFactory.Comment) { }

        /// <summary>
        /// Tạo mới bài viết
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public int Insert(Comment item)
        {
            return DaoFactory.Comment.Insert(item);
        }

        /// <summary>
        /// get danh sách comment theo ArticleID
        /// startDate = null, endDate = null sẽ lấy tất cả
        /// </summary>
        /// <param name="articleID"></param>
        /// <param name="accountID"></param>
        /// <param name="status">0: soạn thảo || 1: review || 2: hiển thị || 3: ẩn</param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="startIndex"></param>
        /// <param name="pageLength"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public List<Out_Comment_List_ArticleID_Paging_Result> GetList(int articleID, int? accountID, int? status, DateTime? startDate, DateTime? endDate, short? orderBy, int startIndex, int pageLength, out int totalRow)
        {
            return DaoFactory.Comment.GetList(articleID, accountID, status, startDate, endDate, orderBy, startIndex, pageLength, out totalRow);
        }

        /// <summary>
        /// Get Comment by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Out_Comment_GetById_Result GetInfo(int id)
        {
            return DaoFactory.Comment.GetInfo(id);
        }

        /// <summary>
        /// cập nhật trạng thái
        /// LongNP
        /// </summary>
        /// <param name="id"></param>
        /// <param name="modifyBy"></param>
        /// <param name="modifyByType"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public bool EditStatus(int id, int modifyBy, short modifyByType, short status)
        {
            return DaoFactory.Comment.EditStatus(id, modifyBy, modifyByType, status);
        }

        /// <summary>
        /// đếm số comment trong ngày
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="articleId"></param>
        /// <returns></returns>
        public int GetNumberPostToday(int userId, int? articleId)
        {
            return DaoFactory.Comment.Out_Comment_CountCommentInDay(userId, articleId);
        }
    }
}
