﻿/**********************************************************************
 * Author:      LongNP
 * DateCreate:  2017-05-05
 *********************************************************************/

using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using EntitiesObject.Entities.WebEntities;

namespace BussinessObject.Bo.WebBo
{
    public class ClanBo : BaseBo<Clan>
    {
        public ClanBo() : base(DaoFactory.Clan) { }

        /// <summary>
        /// Tạo mới clan và tạo mới record ClanMember (owner)
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Insert(Clan item)
        {
            return DaoFactory.Clan.Insert(item);
        }

        /// <summary>
        /// Get clan by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Out_Clan_GetByID_Result GetInfo(int id)
        {
            return DaoFactory.Clan.GetInfo(id);
        }

        /// <summary>
        /// Get clan by GameClanID
        /// </summary>
        /// <param name="gameClanID"></param>
        /// <param name="gameID">load GameID từ MyConfig</param>
        /// <returns></returns>
        public Out_Clan_GetByGameClanID_Result GetInfoByGameClanID(int gameClanID, int gameID)
        {
            return DaoFactory.Clan.GetInfoByGameClanID(gameClanID, gameID);
        }

        /// <summary>
        /// Get clan by account id
        /// </summary>
        /// <param name="accountID"></param>
        /// <returns></returns>
        public Out_Clan_GetByAccount_Result GetInfoByAccount(int accountID)
        {
            return DaoFactory.Clan.GetInfoByAccount(accountID);
        }

        /// <summary>
        /// kiểm tra và tạo mới clan member
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool CheckAndInsertMember(ClanMember item)
        {
            return DaoFactory.Clan.CheckAndInsertMember(item);
        }

        /// <summary>
        /// Kiểm tra clan đã tồn tại trong forum hay chưa
        /// </summary>
        /// <param name="gameID">load GameID từ MyConfig</param>
        /// <param name="clanID"></param>
        /// <returns></returns>
        public int CheckIsExist(int gameID, int clanID)
        {
            return DaoFactory.Clan.CheckIsExist(gameID, clanID);
        }

        /// <summary>
        /// Xóa tài khoản ra khỏi clan
        /// </summary>
        /// <param name="accountID"></param>
        /// <returns></returns>
        public bool DeleteMember(int accountID)
        {
            return DaoFactory.Clan.DeleteMember(accountID);
        }
    }
}
