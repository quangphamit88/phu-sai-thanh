﻿/**********************************************************************
 * Author:      LongNP
 * DateCreate:  2017-05-12
 *********************************************************************/

using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using EntitiesObject.Entities.WebEntities;

namespace BussinessObject.Bo.WebBo
{
    public class AccountInfoCommentBo : BaseBo<AccountInfoComment>
    {
        public AccountInfoCommentBo() : base(DaoFactory.AccountInfoComment) { }

        /// <summary>
        /// Tạo mới bài viết
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public int Insert(AccountInfoComment item)
        {
            return DaoFactory.AccountInfoComment.Insert(item);
        }

        /// <summary>
        /// get danh sách comment theo ArticleID
        /// startDate = null, endDate = null sẽ lấy tất cả
        /// </summary>
        /// <param name="articleID"></param>
        /// <param name="accountID"></param>
        /// <param name="status">0: soạn thảo || 1: review || 2: hiển thị || 3: ẩn</param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="startIndex"></param>
        /// <param name="pageLength"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public List<Out_AccountInfoComment_GetList_Result> GetList(int accountID, int? accountCommentID, int? status, DateTime? startDate, DateTime? endDate, short? orderBy, int startIndex, int pageLength, out int totalRow)
        {
            return DaoFactory.AccountInfoComment.GetList(accountID, accountCommentID, status, startDate, endDate, orderBy, startIndex, pageLength, out totalRow);
        }

        /// <summary>
        /// Get Comment by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Out_AccountInfoComment_GetById_Result GetInfo(int id)
        {
            return DaoFactory.AccountInfoComment.GetInfo(id);
        }

        /// <summary>
        /// cập nhật trạng thái
        /// LongNP
        /// </summary>
        /// <param name="id"></param>
        /// <param name="modifyBy"></param>
        /// <param name="modifyByType"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public bool EditStatus(int id, int modifyBy, short modifyByType, short status)
        {
            return DaoFactory.AccountInfoComment.EditStatus(id, modifyBy, modifyByType, status);
        }

        /// <summary>
        /// Đếm số comment trang tài khoản trong ngày
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="accountID"></param>
        /// <returns></returns>
        public int CountCommentToday(int userID, int accountID)
        {
            return DaoFactory.AccountInfoComment.CountCommentToday(userID, accountID);
        }
    }
}
