﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntitiesObject.Message.Enum;
using log4net.Util;
using Newtonsoft.Json;

namespace BussinessObject.Helper
{
    public class BoHelper
    {
        public static string SerialObject(object obj)
        {
            return obj == null ? "" : JsonConvert.SerializeObject(obj);
        }

        public static T DeserialObject<T>(string serial)
        {
            object obj = null;
            if (string.IsNullOrEmpty(serial)) return (T)obj;
            try
            {
                return JsonConvert.DeserializeObject<T>(serial);
            }
            catch (Exception)
            {
                return (T)obj;
            }
        }

        public static ClientPlatform GetClientPlatform(string userAgent)
        {
            if (userAgent.Contains("Android"))
            {
                return ClientPlatform.Android;
            }
            if (userAgent.Contains("iPhone") || userAgent.Contains("iPad") || userAgent.Contains("ios"))
            {
                return ClientPlatform.Ios;
            }
            if (userAgent.Contains("Windows Phone"))
            {
                return ClientPlatform.WindowPhone;
            }
            return ClientPlatform.Web;
        }
    }
}
