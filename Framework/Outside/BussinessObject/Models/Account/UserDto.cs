﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessObject.Models.Account
{
    /// <summary>
    /// Định nghĩa đối tượng user
    /// <para>Author: PhatVT</para>
    /// <para>Created Date: 18/12/2014</para>
    /// </summary>
    public class UserDto
    {
        public int UserID { get; set; }

        public string Email { get; set; }
        public string NickName { get; set; }
        public string Favorite { get; set; }
        public string FullName { get; set; }
        public  string Password { get; set; }
        public int? CategoryID { get; set; }
        public string Character { get; set; }
        public string Personal { get; set; }
        public string AvatarPath { get; set; }
        public string EmotionPath { get; set; }

        public DateTime? CreateDate { get; set; }
        public DateTime? LastLogin { get; set; }

        public decimal? Coin { get; set; }
        public decimal KenFootball { get; set; }

        public bool? IsActive { get; set; }

        public bool? Gender { get; set; }
        public bool HideInfo { get; set; }
        

        public DateTime? DateOfBirth { get; set; }
        public string Location { get; set; }
        public string Phone { get; set; }

        public int? UserTypeID { get; set; }

        public string Blast { get; set; }

        public int? PubUserID { get; set; }

        public int? Timeplay { get; set; }

        public string CMND { get; set; }

        public bool? InUse { get; set; }

        public int PartnerID { get; set; }

        public string Token { get; set; }

        public string CategoryName { get; set; }

        public decimal GoldUser { get; set; }

        public decimal Star { get; set; }
        public decimal GoldWallet { get; set; }
        public  int VipPoint { get; set; }
        public int CampaignId { get; set; }
        public int ChannelId { get; set; }
        public bool? HasMobile { get; set; }

        public string DisplayName { get; set; } 
        public int Level { get; set; }
        public long TotalExp { get; set; }

        public int AppId { get; set; }

        public long NextLevelExperience { get; set; }


    }
}
