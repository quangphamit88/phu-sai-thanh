﻿/**********************************************************************
 * Author: ThongNT
 * DateCreate: 06-25-2014 
 * Description: DaoFactory  
 * ####################################################################
 * Author:......................
 * DateModify: .................
 * Description: ................
 * 
 *********************************************************************/

using DataAccess.Dao;
using DataAccess.Dao.WebDao;

namespace DataAccess
{
    public class DaoFactory
    {
        #region Category
        public static ICategoryDao Category { get { return new CategoryDao(); } }

        #endregion

        #region Article

        public static IArticleDao Article { get { return new ArticleDao(); } }

        #endregion

        #region Comment

        public static ICommentDao Comment { get { return new CommentDao(); } }

        #endregion

        #region Account

        public static IAccountDao Account { get { return new AccountDao(); } }

        #endregion

        #region Clan

        public static IClanDao Clan { get { return new ClanDao(); } }

        #endregion

        #region AccountInfoComment

        public static IAccountInfoCommentDao AccountInfoComment { get { return new AccountInfoCommentDao(); } }

        #endregion

        #region RealEstate
        public static IRealEstateDao RealEstate { get { return new RealEstateDao(); } }
        public static IRealEstateAlbumDao RealEstateAlbum { get { return new RealEstateAlbumDao(); } }
        public static IRealEstateSeoDao RealEstateSeo { get { return new RealEstateSeoDao(); } }
        public static IRealEstateOnSaleDao RealEstateOnSale { get { return new RealEstateOnSaleDao(); } }

        public static IContactBookDao ContactBook { get { return new ContactBookDao(); } }
        public static IAddressBookDao AddressBook { get { return new AddressBookDao(); } }
        public static IBrokerAccountDao BrokerAccount { get { return new BrokerAccountDao(); } }
        public static IUserFeedbackDao UserFeedback { get { return new UserFeedbackDao(); } }
        public static IRtagKeywordDao TagKeyword { get { return new RtagKeywordDao(); } }
        public static IXcityDao Xcity { get { return new XcityDao(); } }
        public static IXdistrictDao Xdistrict { get { return new XdistrictDao(); } }
        public static IXwardDao Xward { get { return new XwardDao(); } }
        public static IXstreetDao Xstreet { get { return new XstreetDao(); } }
        public static IRealEstatePermissionDao RealEstatePermission { get { return new RealEstatePermissionDao(); } }
        public static ISystemConfigDao SystemConfig { get { return new SystemConfigDao();} }
        #endregion
    }
}
