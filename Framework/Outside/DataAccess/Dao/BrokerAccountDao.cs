﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DataAccess.EF;
using DataAccess.Interface;
using EntitiesObject.Entities.WebEntities;
using DataAccess.Extension;
using System.Data.Entity.Core.Objects;

namespace DataAccess.Dao.WebDao
{
    public interface IBrokerAccountDao : IBaseFactories<BrokerAccount>
    {

        Out_BrokerAccount_SelectById_Result GetById(int brokerId);

        BrokerAccount GetByAdmin(int adminId);
    }

    internal class BrokerAccountDao : DaoFactories<WebEntities, BrokerAccount>, IBrokerAccountDao
    {
        public Out_BrokerAccount_SelectById_Result GetById(int brokerId)
        {
            using (Uow)
            {
                return Uow.Context.Out_BrokerAccount_SelectById(brokerId).FirstOrDefault();
            }
        }


        public BrokerAccount GetByAdmin(int adminId)
        {
            using (Uow)
            {
                return Uow.Context.BrokerAccounts.FirstOrDefault(x => x.AdminId == adminId);
            }
        }
    }
}
