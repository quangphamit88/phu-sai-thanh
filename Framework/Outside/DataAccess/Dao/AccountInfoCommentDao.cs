﻿/**********************************************************************
 * Author:      LongNP
 * DateCreate:  2017-05-12
 *********************************************************************/

using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.EF;
using DataAccess.Interface;
using EntitiesObject.Entities.WebEntities;

namespace DataAccess.Dao
{
    public interface IAccountInfoCommentDao : IBaseFactories<AccountInfoComment>
    {
        /// <summary>
        /// Tạo mới bài viết
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        int Insert(AccountInfoComment item);

        /// <summary>
        /// get danh sách account info comment theo AccountID
        /// startDate = null, endDate = null sẽ lấy tất cả
        /// </summary>
        /// <param name="articleID"></param>
        /// <param name="accountID"></param>
        /// <param name="status">0: soạn thảo || 1: review || 2: hiển thị || 3: ẩn</param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="startIndex"></param>
        /// <param name="pageLength"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        List<Out_AccountInfoComment_GetList_Result> GetList(int accountID, int? accountCommentID, int? status, DateTime? startDate, DateTime? endDate, short? orderBy, int startIndex, int pageLength, out int totalRow);

        /// <summary>
        /// Get Comment by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Out_AccountInfoComment_GetById_Result GetInfo(int id);

        /// <summary>
        /// cập nhật trạng thái
        /// LongNP
        /// </summary>
        /// <param name="id"></param>
        /// <param name="modifyBy"></param>
        /// <param name="modifyByType"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        bool EditStatus(int id, int modifyBy, short modifyByType, short status);

        /// <summary>
        /// Đếm số comment trang tài khoản trong ngày
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="accountID"></param>
        /// <returns></returns>
        int CountCommentToday(int userID, int accountID);
    }

    internal class AccountInfoCommentDao : DaoFactories<WebEntities, AccountInfoComment>, IAccountInfoCommentDao
    {
        public int Insert(AccountInfoComment item)
        {
            using (Uow)
            {
                var commentID = new ObjectParameter("ID", typeof(int));
                var list = Uow.Context.Out_AccountInfoComment_Insert(item.AccountID, item.AccountCommentID, item.AccountDisplayName, item.AccountType, item.Content, item.Status, item.ModifyBy, item.ModifyByType, commentID);
                int result = 0;
                if (commentID.Value != null)
                {
                    int.TryParse(commentID.Value.ToString(), out result);
                }
                return result;
            }
        }

        public List<Out_AccountInfoComment_GetList_Result> GetList(int accountID, int? accountCommentID, int? status, DateTime? startDate, DateTime? endDate, short? orderBy, int startIndex, int pageLength, out int totalRow)
        {
            using (Uow)
            {
                var outTotalRow = new ObjectParameter("TotalRow", typeof(int));
                var list = Uow.Context.Out_AccountInfoComment_GetList(accountID, accountCommentID, status, startDate, endDate, orderBy, startIndex, pageLength, outTotalRow).ToList();
                totalRow = outTotalRow.Value == null ? 0 : (int)outTotalRow.Value;
                return list;
            }
        }

        public Out_AccountInfoComment_GetById_Result GetInfo(int id)
        {
            using (Uow)
            {
                return Uow.Context.Out_AccountInfoComment_GetById(id).FirstOrDefault();
            }
        }

        public bool EditStatus(int id, int modifyBy, short modifyByType, short status)
        {
            using (Uow)
            {
                var outOutResult = new ObjectParameter("OutResult", typeof(int));
                var list = Uow.Context.Out_AccountInfoComment_EditStatus(id, modifyBy, modifyByType, status, outOutResult);
                int result = 0;
                if (outOutResult.Value != null)
                {
                    int.TryParse(outOutResult.Value.ToString(), out result);
                }
                return result == 1;
            }
        }
        public int CountCommentToday(int userID, int accountID)
        {
            using (Uow)
            {
                var data = Uow.Context.Out_AccountInfoComment_CountCommentInDay(userID, accountID).FirstOrDefault();
                return data.GetValueOrDefault(0);
            }
        }
    }
}
