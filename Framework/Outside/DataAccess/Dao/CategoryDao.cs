﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DataAccess.EF;
using DataAccess.Interface;
using EntitiesObject.Entities.WebEntities;
using DataAccess.Extension;
using System.Data.Entity.Core.Objects;

namespace DataAccess.Dao.WebDao
{
    public interface ICategoryDao : IBaseFactories<Category>
    {
        List<Out_Category_Get_Result> Category_Get(int? gameID, bool? status);

        List<Out_Category_GetDataById_Result> Out_Category_GetDataById(int gameID, int id);

        List<Out_Category_GetByIsUserAccess_Result> GetByIsUserAccess(int gameID, int isUserAccess);
    }

    internal class CategoryDao : DaoFactories<WebEntities, Category>, ICategoryDao
    {
        public List<Out_Category_Get_Result> Category_Get(int? gameID, bool? status)
        {
            using (Uow)
            {
                return Uow.Context.Out_Category_Get(gameID, status).ToList();
            }
        }

        public List<Out_Category_GetDataById_Result> Out_Category_GetDataById(int gameID,int id)
        {
            using (Uow)
            {
                return Uow.Context.Out_Category_GetDataById(gameID, id).ToList();
            }
        }

        public List<Out_Category_GetByIsUserAccess_Result> GetByIsUserAccess(int gameID, int isUserAccess)
        {
            using (Uow)
            {
                return Uow.Context.Out_Category_GetByIsUserAccess(gameID, isUserAccess).ToList();
            }
        }
    }
}
