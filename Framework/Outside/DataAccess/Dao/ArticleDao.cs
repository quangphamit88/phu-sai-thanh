﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.EF;
using DataAccess.Interface;
using EntitiesObject.Entities.WebEntities;

namespace DataAccess.Dao
{
    public interface IArticleDao : IBaseFactories<Article>
    {
        #region get top bài viết mới và view nhiều nhất ThinhQHT

        //List<Out_Article_GetTopArticle_Result> Out_Article_GetTopArticle(int top, int type);

        #endregion

        #region get danh sách bài viết theo CatagoryId ThinhQHT

        List<Out_Article_GetDataByCatagoryId_Result> Out_Article_GetDataByCatagoryId(int catagoryId, int startIndex, int pageLength, int orderBy, int orderDirection, out int totalRow);

        #endregion

        #region Get Article Detail by Id ThinhQHT

        List<Out_Article_GetDataById_Result> Out_Article_GetDataById(int id, int? status);

        #endregion

        #region Get top bài viết liên quan

        List<Out_Article_GetRelatedArticle_Result> Out_Article_GetRelatedArticle(int top, int categoryId, int articleId);

        #endregion

        #region Count View Article

        int Out_Article_CountView(int articleId);

        #endregion

        #region kiểm tra article có đủ điều kiện hay không LongNP

        /// <summary>
        /// kiểm tra article có đủ điều kiện hay không LongNP
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool CheckIsExist(int id);

        #endregion

        int Article_Insert(Article input);

        bool Article_UpdateContent(Article input);

        List<Out_Article_ListByUserId_Result> GetListByUserId(int userId, int pageIndex, int pagelengh, out int totalRow);

        List<Out_Article_ListByUserIdComment_Result> GetListByUserIdAndComment(int userId, int pageIndex, int pagelengh, out int totalRow);

        /// <summary>
        /// cập nhật trạng thái bài viết
        /// LongNP
        /// </summary>
        /// <param name="id"></param>
        /// <param name="modifyBy"></param>
        /// <param name="modifyByType"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        bool EditStatus(int id, int modifyBy, short modifyByType, short status);

        /// <summary>
        /// lấy thông tin bài viết for EditStatus, CheckPermission...
        /// LongNP
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Out_Article_GetInfoByID_Result GetInfoByID(int id);

        #region đếm số bài viết của users theo ngày

        int Out_Article_CountArticleInDay(int userId);

        #endregion

        bool CheckUserMemberGuild(int userId, int articleId);

        List<Out_Forum_GetArticleByGuild_Result> Forum_GetArticleByGuild(int guild, int top);

        List<Out_Article_Search_Result> Search(int top, int status, int pageIndex, int pageSize, ref int totalRows);

        List<Article> GetService(int top, int cateId);
    }

    internal class ArticleDao : DaoFactories<WebEntities, Article>, IArticleDao
    {
        public List<Out_Forum_GetArticleByGuild_Result> Forum_GetArticleByGuild(int guild, int top)
        {
            using (Uow)
            {

                return Uow.Context.Out_Forum_GetArticleByGuild(guild, top).ToList() ;
           
            }
        }
        public bool CheckUserMemberGuild(int userId,int articleId)
        {
            using (Uow)
            {
                var outOutResult = new ObjectParameter("isMember", typeof(int));
                var data = Uow.Context.Out_Article_CheckUserMemberGuild(userId, articleId, outOutResult);
                if (outOutResult.Value != null)
                {
                    var totalRow = 0;
                    int.TryParse(outOutResult.Value.ToString(), out totalRow);
                    return totalRow == 1;
                }
                return false;
            }
        }
        public List<Out_Article_ListByUserIdComment_Result> GetListByUserIdAndComment(int userId, int pageIndex, int pagelengh, out int totalRow)
        {
            using (Uow)
            {
                var outTotalRow = new ObjectParameter("TotalRow", typeof(int));
                var list = Uow.Context.Out_Article_ListByUserIdComment(userId, pageIndex, pagelengh, outTotalRow).ToList();
                totalRow = 0;
                if (outTotalRow.Value != null)
                {
                    int.TryParse(outTotalRow.Value.ToString(), out totalRow);
                }
                return list;
            }
        }
        public List<Out_Article_ListByUserId_Result> GetListByUserId(int userId, int pageIndex, int pagelengh, out int totalRow)
        {
            using (Uow)
            {
                var outTotalRow = new ObjectParameter("TotalRow", typeof(int));
                var list = Uow.Context.Out_Article_ListByUserId(userId, pageIndex, pagelengh, outTotalRow).ToList();
                totalRow = 0;
                if (outTotalRow.Value != null)
                {
                    int.TryParse(outTotalRow.Value.ToString(), out totalRow);
                }
                return list;
            }
        }
        public bool Article_UpdateContent(Article input)
        {
            using (Uow)
            {
                var outResult = new ObjectParameter("OutResult", typeof(int));
                var list = Uow.Context.Out_Article_UpdateContent(input.ID, input.Status, input.CategoryID, input.Title, input.Avatar, input.Summary, input.Content, input.ViewType, input.IsComment, input.IsCommentFacebook, input.IsFollow, input.IsShare, input.IsLikeFacebook, input.IsNotify, input.IsHot, input.MetaTitle, input.MetaKeyword, input.MetaDescription, input.OgTitle, input.OgDescription, input.OgUrl, input.OgImage, input.TwitterTitle, input.TwitterDescription, input.TwitterUrl, input.TwitterImage, input.MetaH1, input.MetaH2, input.MetaH3, input.ModifyBy, input.ModifyByType, input.PublishDate, outResult);
                int result = 0;
                if (outResult.Value != null)
                {
                    int.TryParse(outResult.Value.ToString(), out result);
                }
                return result == 1;
            }
        }

        #region get top bài viết mới và view nhiều nhất ThinhQHT

        //public List<Out_Article_GetTopArticle_Result> Out_Article_GetTopArticle(int top, int type)
        //{
        //    using (Uow)
        //    {
        //        return Uow.Context.Out_Article_GetTopArticle(top, type).ToList();
        //    }
        //}
        #endregion

        #region get danh sách bài viết theo CatagoryId ThinhQHT

        public List<Out_Article_GetDataByCatagoryId_Result> Out_Article_GetDataByCatagoryId(int catagoryId, int startIndex, int pageLength, int orderBy, int orderDirection, out int totalRow)
        {
            using (Uow)
            {
                var outTotalRow = new ObjectParameter("TotalRow", typeof(int));
                var list = Uow.Context.Out_Article_GetDataByCatagoryId(catagoryId, startIndex, pageLength, orderBy,
                    orderDirection, outTotalRow).ToList();
                totalRow = outTotalRow.Value == null ? 0 : (int)outTotalRow.Value;
                return list;
            }
        }

        #endregion

        #region Get Article Detail by Id ThinhQHT

        public List<Out_Article_GetDataById_Result> Out_Article_GetDataById(int id, int? status)
        {
            using (Uow)
            {
                return Uow.Context.Out_Article_GetDataById(id, status).ToList();
            }
        }

        #endregion

        #region Get top bài viết liên quan

        public List<Out_Article_GetRelatedArticle_Result> Out_Article_GetRelatedArticle(int top, int categoryId,
            int articleId)
        {
            using (Uow)
            {
                return Uow.Context.Out_Article_GetRelatedArticle(top, categoryId, articleId).ToList();
            }
        }

        #endregion

        #region Count View Article

        public int Out_Article_CountView(int articleId)
        {
            using (Uow)
            {
                var result = Uow.Context.Out_Article_CountView(articleId).FirstOrDefault();
                return result.GetValueOrDefault(0);
            }
        }

        #endregion

        #region kiểm tra article có đủ điều kiện hay không LongNP

        public bool CheckIsExist(int id)
        {
            using (Uow)
            {
                var outIsExist = new ObjectParameter("IsExist", typeof(int));
                Uow.Context.Out_Article_IsExist(id, outIsExist);
                bool result = false;

                if (outIsExist.Value != null)
                {
                    bool.TryParse(outIsExist.Value.ToString(), out result);
                }

                return result;
            }
        }

        #endregion

        public int Article_Insert(Article input)
        {
            using (Uow)
            {
                var outIdArticle = new ObjectParameter("IdArticle", typeof(int));
                var list = Uow.Context.Out_Article_Insert(input.CategoryID, input.AccountID, input.AccountDisplayName, input.AccountType, input.Title, input.Avatar, input.Summary, input.Content, input.Status, input.ViewNumber, input.ViewType, input.IsComment, input.IsCommentFacebook, input.IsFollow, input.IsShare, input.IsLikeFacebook, input.IsNotify
                    , input.IsHot, input.MetaTitle, input.MetaKeyword, input.MetaDescription, input.OgTitle, input.OgDescription, input.OgUrl, input.OgImage, input.TwitterTitle, input.TwitterDescription, input.TwitterUrl, input.TwitterImage, input.MetaH1, input.MetaH2, input.MetaH3, input.ModifyBy, input.ModifyByType, input.PublishDate, outIdArticle);
                int result = 0;
                if (outIdArticle.Value != null)
                {
                    int.TryParse(outIdArticle.Value.ToString(), out result);
                }
                return result;
            }
        }

        public bool EditStatus(int id, int modifyBy, short modifyByType, short status)
        {
            using (Uow)
            {
                var outOutResult = new ObjectParameter("OutResult", typeof(int));
                var list = Uow.Context.Out_Article_EditStatus(id, modifyBy, modifyByType, status, outOutResult);
                int result = 0;
                if (outOutResult.Value != null)
                {
                    int.TryParse(outOutResult.Value.ToString(), out result);
                }
                return result == 1;
            }
        }

        /// <summary>
        /// lấy thông tin bài viết for EditStatus, CheckPermission...
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Out_Article_GetInfoByID_Result GetInfoByID(int id)
        {
            using (Uow)
            {
                return Uow.Context.Out_Article_GetInfoByID(id).FirstOrDefault();
            }
        }

        #region đếm số bài viết của users theo ngày

        public int Out_Article_CountArticleInDay(int userId)
        {
            using (Uow)
            {
                var data = Uow.Context.Out_Article_CountArticleInDay(userId).FirstOrDefault();
                return data.GetValueOrDefault(0);
            }
        }

        #endregion


        public List<Out_Article_Search_Result> Search(int top, int status, int pageIndex, int pageSize, ref int totalRows)
        {
            using (Uow)
            {
                //var outOutResult = new ObjectParameter("OutResult", typeof(int));
                var ls = Uow.Context.Out_Article_Search(top, status, pageIndex, pageSize, 0).ToList();
                totalRows = ls.Any() ? ls.FirstOrDefault().TotalRows.GetValueOrDefault() : 0;
                return ls;
            }
        }


        public List<Article> GetService(int top, int cateId)
        {
            using (Uow)
            {
                return Uow.Context.Articles.Where(x => x.CategoryID == cateId).Take(top).ToList();
            }
        }
    }
}
