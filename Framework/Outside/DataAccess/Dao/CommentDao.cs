﻿/**********************************************************************
 * Author:      LongNP
 * DateCreate:  2017-05-04
 *********************************************************************/

using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.EF;
using DataAccess.Interface;
using EntitiesObject.Entities.WebEntities;

namespace DataAccess.Dao
{
    public interface ICommentDao : IBaseFactories<Comment>
    {
        /// <summary>
        /// Tạo mới bài viết
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        int Insert(Comment item);

        /// <summary>
        /// get danh sách comment theo ArticleID
        /// startDate = null, endDate = null sẽ lấy tất cả
        /// </summary>
        /// <param name="articleID"></param>
        /// <param name="accountID"></param>
        /// <param name="status">0: soạn thảo || 1: review || 2: hiển thị || 3: ẩn</param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="startIndex"></param>
        /// <param name="pageLength"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        List<Out_Comment_List_ArticleID_Paging_Result> GetList(int articleID, int? accountID, int? status, DateTime? startDate, DateTime? endDate, short? orderBy, int startIndex, int pageLength, out int totalRow);

        /// <summary>
        /// Get Comment by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Out_Comment_GetById_Result GetInfo(int id);

        /// <summary>
        /// cập nhật trạng thái
        /// LongNP
        /// </summary>
        /// <param name="id"></param>
        /// <param name="modifyBy"></param>
        /// <param name="modifyByType"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        bool EditStatus(int id, int modifyBy, short modifyByType, short status);

        #region đếm số comment của users trong ngày ThinhQHT

        int Out_Comment_CountCommentInDay(int userId, int? articleId);

        #endregion
    }

    internal class CommentDao : DaoFactories<WebEntities, Comment>, ICommentDao
    {
        public int Insert(Comment item)
        {
            using (Uow)
            {
                var commentID = new ObjectParameter("IDComment", typeof(int));
                var list = Uow.Context.Out_Comment_Insert(item.ArticleID, item.AccountID, item.AccountDisplayName, item.AccountType, item.Content, item.Status, item.ModifyBy, item.ModifyByType, commentID);
                int result = 0;
                if (commentID.Value != null)
                {
                    int.TryParse(commentID.Value.ToString(), out result);
                }
                return result;
            }
        }

        public List<Out_Comment_List_ArticleID_Paging_Result> GetList(int articleID, int? accountID, int? status, DateTime? startDate, DateTime? endDate, short? orderBy, int startIndex, int pageLength, out int totalRow)
        {
            using (Uow)
            {
                var outTotalRow = new ObjectParameter("TotalRow", typeof(int));
                var list = Uow.Context.Out_Comment_List_ArticleID_Paging(articleID, accountID, status, startDate, endDate, orderBy, startIndex, pageLength, outTotalRow).ToList();
                totalRow = outTotalRow.Value == null ? 0 : (int)outTotalRow.Value;
                return list;
            }
        }

        public Out_Comment_GetById_Result GetInfo(int id)
        {
            using (Uow)
            {
                return Uow.Context.Out_Comment_GetById(id).FirstOrDefault();
            }
        }

        public bool EditStatus(int id, int modifyBy, short modifyByType, short status)
        {
            using (Uow)
            {
                var outOutResult = new ObjectParameter("OutResult", typeof(int));
                var list = Uow.Context.Out_Comment_EditStatus(id, modifyBy, modifyByType, status, outOutResult);
                int result = 0;
                if (outOutResult.Value != null)
                {
                    int.TryParse(outOutResult.Value.ToString(), out result);
                }
                return result == 1;
            }
        }

        #region đếm số comment của users trong ngày ThinhQHT

        public int Out_Comment_CountCommentInDay(int userId, int? articleId)
        {
            using (Uow)
            {
                var data = Uow.Context.Out_Comment_CountCommentInDay(userId, articleId).FirstOrDefault();
                return data.GetValueOrDefault(0);
            }
        }

        #endregion
    }
}
