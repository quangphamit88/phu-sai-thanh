﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using DataAccess.EF;
using DataAccess.Interface;
using EntitiesObject.Entities.WebEntities;
using DataAccess.Extension;
using System.Data.Entity.Core.Objects;

namespace DataAccess.Dao.WebDao
{
    public interface IRealEstatePermissionDao : IBaseFactories<RealEstatePermission>
    {

        int GetAdminRole(int adminId);
        RealEstatePermission GetPermission(long productId, int adminId);
    }

    internal class RealEstatePermissionDao : DaoFactories<WebEntities, RealEstatePermission>, IRealEstatePermissionDao
    {
        //public bool Article_UpdateActive(int id, bool isActive, int modifyBy)
        //{
        //    using (Uow)
        //    {
        //        var outOutResult = new ObjectParameter("OutResult", typeof(int));
        //        var list = Uow.Context.Ins_Game_UpdateActive(id, isActive, modifyBy, outOutResult);
        //        int result = 0;
        //        if (outOutResult.Value != null)
        //        {
        //            int.TryParse(outOutResult.Value.ToString(), out result);
        //        }
        //        return result == 1;
        //    }
        //}


        public List<Xcity> GetCityList()
        {
            using (Uow)
            {
                return Uow.Context.Xcities.Where(x => x.IsPublic == true).ToList();
            }
        }

        public int GetAdminRole(int adminId)
        {
            using (Uow)
            {
                var obj = Uow.Context.AdminRoles.FirstOrDefault(x => x.AdminId == adminId);
                return obj == null ? 0 : obj.RoleId.GetValueOrDefault();
            }
        }

        public RealEstatePermission GetPermission(long productId, int adminId)
        {
            using (Uow)
            {
                return Uow.Context.RealEstatePermissions.FirstOrDefault(x => x.ProductId == productId && x.AdminId==adminId);
            }
        }
    }
}
