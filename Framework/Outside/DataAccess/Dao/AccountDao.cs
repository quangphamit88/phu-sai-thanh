﻿/**********************************************************************
 * Author:      LongNP
 * DateCreate:  2017-05-04
 *********************************************************************/

using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.EF;
using DataAccess.Interface;
using EntitiesObject.Entities.WebEntities;

namespace DataAccess.Dao
{
    public interface IAccountDao : IBaseFactories<Account>
    {
        /// <summary>
        /// Tạo mới
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        int Insert(Account item);

        /// <summary>
        /// Get account by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="totalArticle"></param>
        /// <param name="totalComment"></param>
        /// <returns></returns>
        Out_Account_GetById_Result GetInfo(int id);

        /// <summary>
        /// Get full account info by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Out_Account_GetFullInfoById_Result GetFullInfo(int id);

        /// <summary>
        /// Get Account by GameAccountID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Out_Account_GetByGameAccountID_Result GetInfoByGameAccountID(int gameID, int gameAccountID);

        /// <summary>
        /// Kiểm tra tài khoản game đã tồn tại trong forum hay chưa
        /// </summary>
        /// <param name="gameID">load GameID từ MyConfig</param>
        /// <param name="gameAccountID"></param>
        /// <param name="gameAccountName"></param>
        /// <returns></returns>
        int CheckIsExist(int gameID, int gameAccountID, string gameAccountName);

        /// <summary>
        /// cập nhật thông tin tài khoản đăng nhập
        /// </summary>
        /// <param name="id"></param>
        /// <param name="avatar"></param>
        /// <param name="totalGold"></param>
        /// <param name="level"></param>
        /// <param name="playTime"></param>
        /// <returns></returns>
        bool UpdateInfo(int id, string avatar, decimal totalGold, int level, int playTime);

        /// <summary>
        /// Get account by GameId , AccountId
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        Out_Account_GetByGameIdAccountId_Result Out_Account_GetByGameIdAccountId(int gameId, int accountId);

        /// <summary>
        /// cập nhật avatar
        /// </summary>
        /// <param name="id"></param>
        /// <param name="avatar"></param>
        /// <returns></returns>
        bool UpdateAvatar(int id, string avatar);

        bool Login(string userName, string password, ref Admin admin);
    }

    internal class AccountDao : DaoFactories<WebEntities, Account>, IAccountDao
    {
        public int Insert(Account item)
        {
            using (Uow)
            {
                var accountID = new ObjectParameter("ID", typeof(int));
                Uow.Context.Out_Account_Insert(item.GameID, item.GameAccountID, item.GameAccountName, item.GameDisplayName, item.Avatar, item.TotalGold, item.AccountType, item.LockType, accountID);
                int result = 0;
                if (accountID.Value != null)
                {
                    int.TryParse(accountID.Value.ToString(), out result);
                }
                return result;
            }
        }

        public Out_Account_GetById_Result GetInfo(int id)
        {
            using (Uow)
            {
                return Uow.Context.Out_Account_GetById(id).FirstOrDefault();
            }
        }

        public Out_Account_GetFullInfoById_Result GetFullInfo(int id)
        {
            using (Uow)
            {
                return Uow.Context.Out_Account_GetFullInfoById(id).FirstOrDefault();
            }
        }

        public Out_Account_GetByGameAccountID_Result GetInfoByGameAccountID(int gameID, int gameAccountID)
        {
            using (Uow)
            {
                return Uow.Context.Out_Account_GetByGameAccountID(gameAccountID, gameID).FirstOrDefault();
            }
        }

        public int CheckIsExist(int gameID, int gameAccountID, string gameAccountName)
        {
            using (Uow)
            {
                var outID = new ObjectParameter("ID", typeof(int));
                Uow.Context.Out_Account_GameAccount_IsExist(gameID, gameAccountID, gameAccountName, outID);
                int result = 0;

                if (outID.Value != null)
                {
                    int.TryParse(outID.Value.ToString(), out result);
                }

                return result;
            }
        }

        public bool UpdateInfo(int id, string avatar, decimal totalGold, int level, int playTime)
        {
            using (Uow)
            {
                var outResult = new ObjectParameter("OutResult", typeof(int));
                Uow.Context.Out_Account_UpdateInfo(id, avatar, totalGold, level, playTime, outResult);
                int result = 0;

                if (outResult.Value != null)
                {
                    int.TryParse(outResult.Value.ToString(), out result);
                }

                return result == 1;
            }
        }

        public Out_Account_GetByGameIdAccountId_Result Out_Account_GetByGameIdAccountId(int gameId, int accountId)
        {
            using (Uow)
            {
                return Uow.Context.Out_Account_GetByGameIdAccountId(gameId, accountId).FirstOrDefault();
            }
        }

        public bool UpdateAvatar(int id, string avatar)
        {
            using (Uow)
            {
                var outResult = new ObjectParameter("OutResult", typeof(int));
                Uow.Context.Out_Account_Update_Avatar(id, avatar, outResult);
                int result = 0;

                if (outResult.Value != null)
                {
                    int.TryParse(outResult.Value.ToString(), out result);
                }

                return result == 1;
            }
        }


        public bool Login(string userName, string password, ref Admin admin)
        {
            using (Uow)
            {
                admin = Uow.Context.Admins.FirstOrDefault(x => x.LoginName == userName);
                return admin != null;
            }
        }
    }
}
