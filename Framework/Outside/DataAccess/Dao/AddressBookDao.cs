﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DataAccess.EF;
using DataAccess.Interface;
using EntitiesObject.Entities.WebEntities;
using DataAccess.Extension;
using System.Data.Entity.Core.Objects;

namespace DataAccess.Dao.WebDao
{
    public interface IAddressBookDao : IBaseFactories<AddressBook>
    {

        Out_AddressBook_SelectById_Result GetById(long id);
    }

    internal class AddressBookDao : DaoFactories<WebEntities, AddressBook>, IAddressBookDao
    {
        //public bool Article_UpdateActive(int id, bool isActive, int modifyBy)
        //{
        //    using (Uow)
        //    {
        //        var outOutResult = new ObjectParameter("OutResult", typeof(int));
        //        var list = Uow.Context.Ins_Game_UpdateActive(id, isActive, modifyBy, outOutResult);
        //        int result = 0;
        //        if (outOutResult.Value != null)
        //        {
        //            int.TryParse(outOutResult.Value.ToString(), out result);
        //        }
        //        return result == 1;
        //    }
        //}


        public Out_AddressBook_SelectById_Result GetById(long addressId)
        {
            using (Uow)
            {
                return Uow.Context.Out_AddressBook_SelectById(addressId).FirstOrDefault();
            }
        }
    }
}
