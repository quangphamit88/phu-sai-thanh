﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DataAccess.EF;
using DataAccess.Interface;
using EntitiesObject.Entities.WebEntities;
using DataAccess.Extension;
using System.Data.Entity.Core.Objects;

namespace DataAccess.Dao.WebDao
{
    public interface IXstreetDao : IBaseFactories<Xstreet>
    {

        List<Xstreet> GetStreetByWard(int wardId);
        List<Xstreet> GetStreetByDistrict(int districtId);
    }

    internal class XstreetDao : DaoFactories<WebEntities, Xstreet>, IXstreetDao
    {
        //public bool Article_UpdateActive(int id, bool isActive, int modifyBy)
        //{
        //    using (Uow)
        //    {
        //        var outOutResult = new ObjectParameter("OutResult", typeof(int));
        //        var list = Uow.Context.Ins_Game_UpdateActive(id, isActive, modifyBy, outOutResult);
        //        int result = 0;
        //        if (outOutResult.Value != null)
        //        {
        //            int.TryParse(outOutResult.Value.ToString(), out result);
        //        }
        //        return result == 1;
        //    }
        //}

        public List<Xstreet> GetStreetByDistrict(int districtId)
        {
            using (Uow)
            {
                return Uow.Context.Xstreets.Where(x => x.IsPublic == true && (x.DistrictId == districtId)).OrderBy(x => x.StreetName).ToList();
            }
        }

        public List<Xstreet> GetStreetByWard(int wardId)
        {
            using (Uow)
            {
                var ward = Uow.Context.Xwards.FirstOrDefault(x => x.Id == wardId);
                return ward != null
                    ? Uow.Context.Xstreets.Where(x => x.IsPublic == true && x.DistrictId == ward.DistrictId)
                        .OrderBy(x => x.StreetName)
                        .ToList()
                    : Uow.Context.Xstreets.Where(x => x.IsPublic == true && (x.WardId == wardId))
                        .OrderBy(x => x.StreetName)
                        .ToList();
            }
        }
    }
}
