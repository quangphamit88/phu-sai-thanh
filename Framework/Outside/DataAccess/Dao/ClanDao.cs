﻿/**********************************************************************
 * Author:      LongNP
 * DateCreate:  2017-05-04
 *********************************************************************/

using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.EF;
using DataAccess.Interface;
using EntitiesObject.Entities.WebEntities;

namespace DataAccess.Dao
{
    public interface IClanDao : IBaseFactories<Clan>
    {
        /// <summary>
        /// Tạo mới
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        int Insert(Clan item);

        /// <summary>
        /// Get clan by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Out_Clan_GetByID_Result GetInfo(int id);

        /// <summary>
        /// Get clan by account id
        /// </summary>
        /// <param name="accountID"></param>
        /// <returns></returns>
        Out_Clan_GetByAccount_Result GetInfoByAccount(int accountID);

        /// <summary>
        /// Get clan by GameClanID
        /// </summary>
        /// <param name="gameClanID"></param>
        /// <param name="gameID"></param>
        /// <returns></returns>
        Out_Clan_GetByGameClanID_Result GetInfoByGameClanID(int gameClanID, int gameID);

        /// <summary>
        /// kiểm tra và tạo mới clan member
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        bool CheckAndInsertMember(ClanMember item);

        /// <summary>
        /// Kiểm tra clan đã tồn tại trong forum hay chưa
        /// </summary>
        /// <param name="gameID">load GameID từ MyConfig</param>
        /// <param name="clanID"></param>
        /// <returns></returns>
        int CheckIsExist(int gameID, int clanID);

        /// <summary>
        /// Xóa tài khoản ra khỏi clan
        /// </summary>
        /// <param name="accountID"></param>
        /// <returns></returns>
        bool DeleteMember(int accountID);
    }

    internal class ClanDao : DaoFactories<WebEntities, Clan>, IClanDao
    {
        public int Insert(Clan item)
        {
            using (Uow)
            {
                var outID = new ObjectParameter("ID", typeof(int));
                Uow.Context.Out_Clan_Insert(item.GameID, item.ClanGameID, item.OwnerID, item.GuildName, item.GuildAlias, item.Avatar, item.IsActive, outID);
                int result = 0;
                if (outID.Value != null)
                {
                    int.TryParse(outID.Value.ToString(), out result);
                }
                return result;
            }
        }

        public Out_Clan_GetByID_Result GetInfo(int id)
        {
            using (Uow)
            {
                return Uow.Context.Out_Clan_GetByID(id).FirstOrDefault();
            }
        }

        public Out_Clan_GetByAccount_Result GetInfoByAccount(int accountID)
        {
            using (Uow)
            {
                return Uow.Context.Out_Clan_GetByAccount(accountID).FirstOrDefault();
            }
        }

        public Out_Clan_GetByGameClanID_Result GetInfoByGameClanID(int gameClanID, int gameID)
        {
            using (Uow)
            {
                return Uow.Context.Out_Clan_GetByGameClanID(gameClanID, gameID).FirstOrDefault();
            }
        }

        public bool CheckAndInsertMember(ClanMember item)
        {
            using (Uow)
            {
                var outIsSuccess = new ObjectParameter("IsSuccess", typeof(int));
                Uow.Context.Out_ClanMember_CheckAndInsert(item.ClanID, item.AccountID, item.IsOwner, outIsSuccess);
                bool result = false;

                if (outIsSuccess.Value != null)
                {
                    bool.TryParse(outIsSuccess.Value.ToString(), out result);
                }

                return result;
            }
        }

        public int CheckIsExist(int gameID, int clanID)
        {
            using (Uow)
            {
                var outID = new ObjectParameter("ID", typeof(int));
                Uow.Context.Out_Clan_GameClan_IsExist(gameID, clanID, outID);
                int result = 0;

                if (outID.Value != null)
                {
                    int.TryParse(outID.Value.ToString(), out result);
                }

                return result;
            }
        }

        public bool DeleteMember(int accountID)
        {
            using (Uow)
            {
                var outResult = new ObjectParameter("OutResult", typeof(int));
                Uow.Context.Out_ClanMember_DeleteMember(accountID, outResult);
                int result = 0;

                if (outResult.Value != null)
                {
                    int.TryParse(outResult.Value.ToString(), out result);
                }

                return result == 1;
            }
        }
    }
}
