﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DataAccess.EF;
using DataAccess.Interface;
using EntitiesObject.Custom;
using EntitiesObject.Entities.WebEntities;
using DataAccess.Extension;
using System.Data.Entity.Core.Objects;
using EntitiesObject.Message.Model;

namespace DataAccess.Dao.WebDao
{
    public interface IRealEstateDao : IBaseFactories<RealEstate>
    {
        /// <summary>
        /// Tim bat dong san
        /// </summary>
        /// <returns></returns>
        List<Out_RealEstate_Search_Result> Search(RealEstateSearchRequest request, ref int totalRow);

        List<Out_RealEstate_SearchSimilar_Result> SearchSimilar(int top, long productId, long price, int cityId, int districtId, int wardId, bool saleOrRent);
        List<Out_RealEstate_SelectTopView_Result> SearchTopView(int top);
        List<Out_RealEstate_RecentViewed_Result> RecentViewed(string listProductId);

        List<Out_RealEstate_CountByWard_Result> CountByWard(int districtId);

        List<Out_RealEstate_SelectTopHot_Result> SearchTopHot(int top, bool saleOrRent);

        //RealEstateInfo GetById(long id, bool includeAddress, int getType);

        RealEstateInfo GetById(long id, long productId, bool includeAddress, int getType);

        bool SaveProduct(RealEstateOnSale product);
    }

    internal class RealEstateDao : DaoFactories<WebEntities, RealEstate>, IRealEstateDao
    {
        /// <summary>
        /// Tim bat dong san
        /// </summary>
        /// <returns></returns>
        public List<Out_RealEstate_Search_Result> Search(RealEstateSearchRequest request, ref int totalRow)
        {
            using (Uow)
            {
                var rs =
                    Uow.Context.Out_RealEstate_Search(
                        request.Title,
                        request.RealEstateType,
                        request.SaleOrRent,
                        request.StatusSale,
                        request.StreetId,
                        request.WardId,
                        request.DistrictId,
                        request.CityId,
                        request.RoomNumber,
                        request.MinPrice,
                        request.MaxPrice,
                        request.ViewDirection,
                        request.LaneType,
                        request.SortType,
                        request.PageIndex,
                        request.PageSize,
                        request.IsHot).ToList();
                var outRealEstateSearchResult = rs.FirstOrDefault();
                if (outRealEstateSearchResult != null)
                    totalRow = rs.Any() ? outRealEstateSearchResult.TotalRow.GetValueOrDefault() : 0;
                return rs;
            }
        }


        public RealEstateInfo GetById(long id, long productId, bool includeAddress, int getType)
        {
            using (Uow)
            {
                var obj = new RealEstateInfo { RealEstate = GetOne(id) };
                // get BDS
                if (obj.RealEstate == null)
                {
                    return null;
                }
                if (includeAddress)
                {
                    obj.Address = DaoFactory.AddressBook.GetOne(obj.RealEstate.AddressId);
                }
                switch (getType)
                {
                    case 1:
                        // Get RealEstate and Album
                        obj.RealEstateAlbum = DaoFactory.RealEstateAlbum.GetOne(id);
                        break;
                    case 2:
                        // Get RealEstate and Seo 
                        obj.RealEstateSeo = DaoFactory.RealEstateSeo.GetOne(id);
                        break;
                    case 3:
                        // Get RealEstate Sale
                        obj.RealEstateOnSale = DaoFactory.RealEstateOnSale.GetOne(productId);
                        if (obj.RealEstateOnSale == null) return obj;
                        obj.Contact = DaoFactory.ContactBook.GetOne(obj.RealEstateOnSale.ContactId);
                        obj.Broker = DaoFactory.BrokerAccount.GetOne(obj.RealEstateOnSale.BrokerId);
                        break;
                    default:
                        // Get all things
                        // Get RealEstate and Album
                        obj.RealEstateAlbum = DaoFactory.RealEstateAlbum.GetOne(id);
                        // Get RealEstate and Seo 
                        obj.RealEstateSeo = DaoFactory.RealEstateSeo.GetOne(id);
                        // Get RealEstate Sale
                        obj.RealEstateOnSale = DaoFactory.RealEstateOnSale.GetByRealEstateId(id);
                        if (obj.RealEstateOnSale == null) return obj;
                        obj.Contact = DaoFactory.ContactBook.GetOne(obj.RealEstateOnSale.ContactId);
                        obj.Broker = DaoFactory.BrokerAccount.GetOne(obj.RealEstateOnSale.BrokerId);
                        break;
                }

                return obj;
            }
        }


        public List<Out_RealEstate_SelectTopHot_Result> SearchTopHot(int top, bool saleOrRent)
        {
            using (Uow)
            {
                return Uow.Context.Out_RealEstate_SelectTopHot(top, saleOrRent).ToList();
            }
        }


        public List<Out_RealEstate_SearchSimilar_Result> SearchSimilar(int top, long productId, long price, int cityId, int districtId, int wardId, bool saleOrRent)
        {
            using (Uow)
            {
                return Uow.Context.Out_RealEstate_SearchSimilar(top, productId, price, cityId, districtId, wardId, saleOrRent).ToList();
            }
        }

        public List<Out_RealEstate_RecentViewed_Result> RecentViewed(string listProductId)
        {
            using (Uow)
            {
                return Uow.Context.Out_RealEstate_RecentViewed(listProductId).ToList();
            }
        }

        public List<Out_RealEstate_CountByWard_Result> CountByWard(int districtId)
        {
            using (Uow)
            {
                return Uow.Context.Out_RealEstate_CountByWard(districtId).ToList();
            }
        }


        public List<Out_RealEstate_SelectTopView_Result> SearchTopView(int top)
        {
            using (Uow)
            {
                return Uow.Context.Out_RealEstate_SelectTopView(top).ToList();
            }
        }


        public bool SaveProduct(RealEstateOnSale product)
        {
            return DaoFactory.RealEstateOnSale.Save(product);
        }
    }
}
