﻿namespace EntitiesObject.Message.Enum
{
    public enum ClientPlatform
    {
        Web = 0,
        AppFacebook = 1,
        Android = 2,
        Ios = 3,
        WindowPhone = 4
    }
}