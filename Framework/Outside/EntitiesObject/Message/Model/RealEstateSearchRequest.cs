﻿using PhuSaiThanhLib.Enum;

namespace EntitiesObject.Message.Model
{
    public class RealEstateSearchRequest
    {
        public string Title { get; set; }
        public int RealEstateType { get; set; }
        public int SaleOrRent { get; set; }
        public int StatusSale { get; set; }
        public int StreetId { get; set; }
        public int WardId { get; set; }
        public int DistrictId { get; set; }
        public int CityId { get; set; }
        public decimal MinPrice { get; set; }
        public decimal MaxPrice { get; set; }
        public int ViewDirection { get; set; }
        /// <summary>
        /// Nha mat tien or hem
        /// </summary>
        public int LaneType { get; set; }
        public int SortType { get; set; }
        public bool OrderByDesc { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public bool IsHot { get; set; }
        public int RoomNumber { get; set; }
    }
}
