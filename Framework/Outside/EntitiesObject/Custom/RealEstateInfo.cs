﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntitiesObject.Entities.WebEntities;

namespace EntitiesObject.Custom
{
    public class RealEstateInfo
    {
        public RealEstateInfo()
        {
            RealEstate=new RealEstate();
            RealEstateAlbum=new RealEstateAlbum();
            RealEstateOnSale=new RealEstateOnSale();
            RealEstateSeo=new RealEstateSeo();
            Contact=new ContactBook();
            Address=new AddressBook();
            Broker=new BrokerAccount();
        }
        public RealEstate RealEstate { get; set; }

        public RealEstateAlbum RealEstateAlbum { get; set; }

        public RealEstateOnSale RealEstateOnSale { get; set; }

        public RealEstateSeo RealEstateSeo { get; set; }

        public ContactBook Contact { get; set; }

        public AddressBook Address { get; set; }

        public BrokerAccount Broker { get; set; }
    }
}
