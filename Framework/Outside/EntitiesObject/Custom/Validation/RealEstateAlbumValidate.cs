﻿using System.ComponentModel.DataAnnotations;

namespace EntitiesObject.Custom.Validation
{
    public class RealEstateAlbumValidate
    {
        [Required(ErrorMessage = "Vui lòng chọn hình")]
        public string ImagesUrl { get; set; }
    }
}
