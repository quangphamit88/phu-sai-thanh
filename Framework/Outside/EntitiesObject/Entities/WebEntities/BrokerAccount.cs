//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EntitiesObject.Entities.WebEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class BrokerAccount
    {
        public int Id { get; set; }
        public Nullable<long> ContactId { get; set; }
        public Nullable<long> AddressId { get; set; }
        public string UserName { get; set; }
        public string DisplayName { get; set; }
        public string Password { get; set; }
        public string PassworldSalt { get; set; }
        public string Email { get; set; }
        public string AvatarUrl { get; set; }
        public string OpenId { get; set; }
        public Nullable<int> OpenIdType { get; set; }
        public Nullable<bool> IsLock { get; set; }
        public Nullable<System.DateTime> LockTimeout { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<int> AdminId { get; set; }
        public string Phone { get; set; }
        public string KeySearch { get; set; }
    }
}
