//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EntitiesObject.Entities.WebEntities
{
    using System;
    
    public partial class Out_Article_GetDataByCatagoryId_Result
    {
        public Nullable<int> ID { get; set; }
        public Nullable<int> CategoryID { get; set; }
        public string CategoryName { get; set; }
        public Nullable<int> AccountID { get; set; }
        public Nullable<short> AccountType { get; set; }
        public Nullable<int> GameAccountID { get; set; }
        public string Title { get; set; }
        public Nullable<int> ViewNumber { get; set; }
        public string Publisher { get; set; }
        public Nullable<bool> IsHot { get; set; }
        public Nullable<System.DateTime> PublishDate { get; set; }
        public Nullable<long> RowNumber { get; set; }
    }
}
