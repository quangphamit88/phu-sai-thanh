﻿using System.Configuration;
using MyConfig.Authentication;

namespace MyConfig
{
    public class MyConfiguration
    {
        #region Instance
        private static MyConfigSection _instance;
        private static MyConfigSection Instance
        {
            get
            {
                return _instance ?? (_instance = (MyConfigSection)ConfigurationManager.GetSection("MyConfig"));
            }
        }        
        #endregion

        #region Authentication

        /// <summary>
        /// Cấu hình kết nối Google
        /// <para>Author: PhatVT</para>
        /// <para>Date Created: 23/12/2014</para>
        /// </summary>
        public static GoogleOpenAuthElement GoogleOpenAuth
        {
            get { return Instance.GoogleOpenAuthElement; }
        }

        /// <summary>
        /// Cấu hình kết nối Facebook
        /// <para>Author: PhatVT</para>
        /// <para>Date Created: 24/12/2014</para>
        /// </summary>
        public static FacebookOpenAuthElement FacebookOpenAuth
        {
            get { return Instance.FacebookOpenAuthElement; }
        }

        /// <summary>
        /// Cấu hình kết nối Yahoo
        /// <para>Author: PhatVT</para>
        /// <para>Date Created: 24/12/2014</para>
        /// </summary>
        public static YahooOpenAuthElement YahooOpenAuth
        {
            get { return Instance.YahooOpenAuthElement; }
        }

        #endregion

        #region default xxx

        public static DefaultElement Default
        {
            get { return Instance.DefaultElement; }
        }

        #endregion        

        #region SEO xxx

        public static SeoElement Seo
        {
            get { return Instance.SeoElement; }
        }

        #endregion        

        #region authen xxx

        public static OutsideAuthenElement OutsideAuthen
        {
            get { return Instance.OutsideAuthenElement; }
        }

        #endregion             

        #region rule xxx

        public static RuleElement Rule
        {
            get { return Instance.RuleElement; }
        }

        #endregion        
    }
}
