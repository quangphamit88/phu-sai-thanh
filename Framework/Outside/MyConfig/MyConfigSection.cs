﻿using System.Configuration;
using MyConfig.Authentication;

namespace MyConfig
{
    public class MyConfigSection : ConfigurationSection
    {
        #region Authentication

        /// <summary>
        /// Cấu hình kết nối Google
        /// <para>Author: PhatVT</para>
        /// <para>Date Created: 23/12/2014</para>
        /// </summary>
        [ConfigurationProperty("GoogleOpenAuth")]
        public GoogleOpenAuthElement GoogleOpenAuthElement { get { return (GoogleOpenAuthElement)this["GoogleOpenAuth"]; } }

        /// <summary>
        /// Cấu hình kết nối Facebook
        /// <para>Author: PhatVT</para>
        /// <para>Date Created: 24/12/2014</para>
        /// </summary>
        [ConfigurationProperty("FacebookOpenAuth")]
        public FacebookOpenAuthElement FacebookOpenAuthElement { get { return (FacebookOpenAuthElement)this["FacebookOpenAuth"]; } }

        /// <summary>
        /// Cấu hình kết nối Yahoo
        /// <para>Author: PhatVT</para>
        /// <para>Date Created: 24/12/2014</para>
        /// </summary>
        [ConfigurationProperty("YahooOpenAuth")]
        public YahooOpenAuthElement YahooOpenAuthElement { get { return (YahooOpenAuthElement)this["YahooOpenAuth"]; } }

        #endregion        

        #region Default xxx

        [ConfigurationProperty("Default")]
        public DefaultElement DefaultElement { get { return (DefaultElement)this["Default"]; } }

        #endregion        

        #region SEO xxx

        [ConfigurationProperty("Seo")]
        public SeoElement SeoElement { get { return (SeoElement)this["Seo"]; } }

        #endregion

        #region authen xxx

        /// <summary>
        /// key dùng để mã hóa thông tin admin đăng nhập và mã hóa mật khẩu
        /// </summary>
        [ConfigurationProperty("OutsideAuthen")]
        public OutsideAuthenElement OutsideAuthenElement { get { return (OutsideAuthenElement)this["OutsideAuthen"]; } }

        #endregion           

        #region rule xxx

        /// <summary>
        /// cấu hình rule post bài và comment
        /// </summary>
        [ConfigurationProperty("Rule")]
        public RuleElement RuleElement { get { return (RuleElement)this["Rule"]; } }

        #endregion
    }
}
