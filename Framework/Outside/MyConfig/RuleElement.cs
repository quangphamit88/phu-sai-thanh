﻿/**********************************************************************
 * Author: xxx
 * DateCreate: 2017-05-10
 * Description: cấu hình rule post bài
 * ####################################################################
 * Author:......................
 * DateModify: .................
 * Description: ................
 * 
 *********************************************************************/
using System.Configuration;

namespace MyConfig
{
    public class RuleElement : ConfigurationElement
    {
        [ConfigurationProperty("MaxCreateArticlePerDay", DefaultValue = 3)]
        public int MaxCreateArticlePerDay
        {
            get { return (int)this["MaxCreateArticlePerDay"]; }
        }

        [ConfigurationProperty("MaxCommentPerArticlePerDay", DefaultValue = 3)]
        public int MaxCommentPerArticlePerDay
        {
            get { return (int)this["MaxCommentPerArticlePerDay"]; }
        }
    }
}
