﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace MyConfig
{
    public class OutsideAuthenElement : ConfigurationElement
    {
        [ConfigurationProperty("KeySign", DefaultValue = "11cdc186a1da4467b34056261e7fd303")]
        public string KeySign
        {
            get { return (string)this["KeySign"]; }
        }
    }
}
