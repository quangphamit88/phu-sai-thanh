﻿/**********************************************************************
 * Author: ThongNT
 * DateCreate: 06-25-2014 
 * Description: Quan ly thong tin cau hinh chung cho project  
 * ####################################################################
 * Author:......................
 * DateModify: .................
 * Description: ................
 * 
 *********************************************************************/
using System.Configuration;

namespace MyConfig
{
    public class SeoElement : ConfigurationElement
    {
        [ConfigurationProperty("MetaTitle", DefaultValue = "Diễn đàn")]
        public string MetaTitle
        {
            get { return (string)this["MetaTitle"]; }
        }

        [ConfigurationProperty("MetaDescription", DefaultValue = "Diễn đàn game")]
        public string MetaDescription
        {
            get { return (string)this["MetaDescription"]; }
        }

        [ConfigurationProperty("MetaKeyword", DefaultValue = "Diễn đàn, dien dan, dien-dan")]
        public string MetaKeyword
        {
            get { return (string)this["MetaKeyword"]; }
        }

        [ConfigurationProperty("OgTitle", DefaultValue = "")]
        public string OgTitle
        {
            get { return (string)this["OgTitle"]; }
        }

        [ConfigurationProperty("OgDescription", DefaultValue = "")]
        public string OgDescription
        {
            get { return (string)this["OgDescription"]; }
        }

        [ConfigurationProperty("OgUrl", DefaultValue = "")]
        public string OgUrl
        {
            get { return (string)this["OgUrl"]; }
        }

        [ConfigurationProperty("OgImage", DefaultValue = "")]
        public string OgImage
        {
            get { return (string)this["OgImage"]; }
        }

        [ConfigurationProperty("TwitterTitle", DefaultValue = "")]
        public string TwitterTitle
        {
            get { return (string)this["TwitterTitle"]; }
        }

        [ConfigurationProperty("TwitterDescription", DefaultValue = "")]
        public string TwitterDescription
        {
            get { return (string)this["TwitterDescription"]; }
        }

        [ConfigurationProperty("TwitterUrl", DefaultValue = "")]
        public string TwitterUrl
        {
            get { return (string)this["TwitterUrl"]; }
        }

        [ConfigurationProperty("TwitterImage", DefaultValue = "")]
        public string TwitterImage
        {
            get { return (string)this["TwitterImage"]; }
        }

        [ConfigurationProperty("MetaH1", DefaultValue = "Diễn đàn game")]
        public string MetaH1
        {
            get { return (string)this["MetaH1"]; }
        }

        [ConfigurationProperty("MetaH2", DefaultValue = "Diễn đàn game")]
        public string MetaH2
        {
            get { return (string)this["MetaH2"]; }
        }

        [ConfigurationProperty("MetaH3", DefaultValue = "Diễn đàn game")]
        public string MetaH3
        {
            get { return (string)this["MetaH3"]; }
        }
    }
}
