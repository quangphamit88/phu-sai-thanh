﻿/**********************************************************************
 * Author: ThongNT
 * DateCreate: 06-25-2014 
 * Description: Quan ly thong tin cau hinh chung cho project  
 * ####################################################################
 * Author:......................
 * DateModify: .................
 * Description: ................
 * 
 *********************************************************************/
using System.Configuration;

namespace MyConfig
{
    public class DefaultElement : ConfigurationElement
    {
        
        #region PageSize

        [ConfigurationProperty("PageSize", DefaultValue = 30)]
        public int PageSize
        {
            get { return (int)this["PageSize"]; }
        }

        [ConfigurationProperty("HomeArticleTopSize", DefaultValue = 5)]
        public int HomeArticleTopSize
        {
            get { return (int)this["HomeArticleTopSize"]; }
        }

        [ConfigurationProperty("HomeArticleSize", DefaultValue = 10)]
        public int HomeArticleSize
        {
            get { return (int)this["HomeArticleSize"]; }
        }

        [ConfigurationProperty("PageSizeComment", DefaultValue = 10)]
        public int PageSizeComment
        {
            get { return (int)this["PageSizeComment"]; }
        }

        #endregion

        [ConfigurationProperty("DefaultLanguage", DefaultValue = "vn")]
        public string DefaultLanguage
        {
            get { return (string)this["DefaultLanguage"]; }
        }

        #region Domain

        /// <summary>
        /// Url full domain
        /// <para>Author: PhatVT</para>
        /// <para>Date Created: 12/22/2014</para>
        /// </summary>
        [ConfigurationProperty("FullDomain", DefaultValue = "")]
        public string FullDomain
        {
            get { return (string)this["FullDomain"]; }
        }

        /// <summary>
        /// Url soft domain
        /// <para>Author: PhatVT</para>
        /// <para>Date Created: 12/22/2014</para>
        /// </summary>
        [ConfigurationProperty("ShortDomain", DefaultValue = "")]
        public string ShortDomain
        {
            get { return (string)this["ShortDomain"]; }
        }

        #endregion

        /// <summary>
        /// Version xoa Cache CSS va Javascript
        /// </summary>
        [ConfigurationProperty("ContentVersion", DefaultValue = 1)]
        public int ContentVersion
        {
            get { return (int)this["ContentVersion"]; }
        }

        /// <summary>
        /// <para>Domain chua hinh cua user</para> 
        /// </summary>
        [ConfigurationProperty("ImageHost", DefaultValue = "http://192.168.5.167:8005/")]
        public string ImageHost
        {
            get { return (string)this["ImageHost"]; }
        }

        /// <summary>
        /// <para>Ten domain default: http://hoiquan52.com</para>
        /// </summary>
        [ConfigurationProperty("HostName", DefaultValue = "http://localhost:2967/")]
        public string HostName
        {
            get { return (string)this["HostName"]; }
        }

        /// <summary>
        /// <para>domain upload flash avatar</para>
        /// </summary>
        [ConfigurationProperty("AssImagePath", DefaultValue = "Images/Article/")]
        public string AssImagePath
        {
            get { return (string)this["AssImagePath"]; }
        }

        [ConfigurationProperty("RuleTotalPlayTime", DefaultValue = 60)]
        public int RuleTotalPlayTime
        {
            get { return (int)this["RuleTotalPlayTime"]; }
        }

        [ConfigurationProperty("CategoryRulePolicy", DefaultValue = 9)]
        public int CategoryRulePolicy
        {
            get { return (int)this["CategoryRulePolicy"]; }
        }
    }
}
