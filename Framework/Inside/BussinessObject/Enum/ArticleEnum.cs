﻿using System.ComponentModel;

namespace BussinessObject.Enum
{
    public enum ArticleStatusEnum
    {
        [Description("Soạn thảo")]
        SoanThao = 0,

        [Description("Đang chờ duyệt")]
        DangChoDuyet = 1,

        [Description("Hiển thị")]
        HienThi = 2,

        [Description("Ẩn")]
        An = 3
    }
}
