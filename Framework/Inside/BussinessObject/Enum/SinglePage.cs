﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessObject.Enum
{
    /// <summary>
    /// Định nghĩa các trang single page
    /// <para>Author: vietnhh</para>
    /// <para>Create Date: 2016/12/26</para>
    /// </summary>
    public enum SinglePage
    {
        [Description("Trang Chủ")]
        HomePage = 1,

        [Description("Danh sách tin tức")]
        ListTinTuc = 2,

        [Description("Danh sách bang hội")]
        ListBang = 3,
    }

    public enum PageType
    {
        [Description("Single Page ")]
        SinglePage  = 1,
        [Description("Game")]
        Game = 2,
        [Description("Bài viết")]
        Article = 3,
        
    }
}
