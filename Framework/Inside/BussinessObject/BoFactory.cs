﻿/*********************************************************************
* Author: ThongNT
* DateCreate: 06-25-2014 
* Description: BoFactory  
* ####################################################################
* Author:......................
* DateModify: .................
* Description: ................
* 
*********************************************************************/

using BussinessObject.Bo.WebBo;

namespace BussinessObject
{
    public class BoFactory
    {
        #region Admin
        //public static AdminBo Admin { get { return new AdminBo(); } }
        #endregion

        #region Category
        public static CategoryBo Category { get { return new CategoryBo(); } }
        #endregion

        #region Article
        public static ArticleBo Article { get { return new ArticleBo(); } }
        #endregion

        #region Category
        public static CommentBo Comment { get { return new CommentBo(); } }
        #endregion

        #region BatDongSan
        public static RealEstateBo RealEstate { get { return new RealEstateBo(); } }
        public static AddressBookBo AddressBook { get { return new AddressBookBo(); } }
        //public static BrokerAccountBo BrokerAccount { get { return new BrokerAccountBo(); } }
        public static RealEstatePermissionBo RealEstatePermission { get { return new RealEstatePermissionBo(); } }
        //public static AdminRoleBo AdminRole { get { return new AdminRoleBo(); } }
        public static ContactBookBo ContactBook { get { return new ContactBookBo(); } }
        public static XcityBo Xcity { get { return new XcityBo(); } }
        public static XdistrictBo Xdistrict { get { return new XdistrictBo(); } }
        public static XwardBo Xward { get { return new XwardBo(); } }
        public static XstreetBo Xstreet { get { return new XstreetBo(); } }
        public static CustomerRequestBo CustomerRequest { get { return new CustomerRequestBo(); } }
        public static UserFeedbackBo UserFeedback { get { return new UserFeedbackBo();} }
        public static SystemConfigBo SystemConfig { get { return new SystemConfigBo();} }
        #endregion
    }
}
