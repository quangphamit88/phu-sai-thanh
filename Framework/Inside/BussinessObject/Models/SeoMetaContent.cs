﻿/*
 * Author: Vietnhh
 * Date: 12/26/2016
 * Description: nội dung seo
 */

using System;
using System.ComponentModel;
using EntitiesObject.Message.Enum;

namespace BussinessObject.Models
{

    public class SeoMetaContent
    {
        public string TitleConfig { get; set; }
        public string Description { get; set; }
        public string Keyword { get; set; }
        public string Url { get; set; }
      
        // google
        public string OgTitle { get; set; }
        public string OgDescription { get; set; }
        public string OgUrl { get; set; }
        public string OgImage { get; set; }
        

        // twitter
        public string TwitterTitle { get; set; }
        public string TwitterDescription { get; set; }
        public string TwitterUrl { get; set; }
        public string TwitterImage { get; set; }
    }
    

}
