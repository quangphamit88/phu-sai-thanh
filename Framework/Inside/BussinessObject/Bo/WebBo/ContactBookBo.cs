﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using BussinessObject.Helper;
using BussinessObject.Models;
using DataAccess;
using EntitiesObject.Entities.WebEntities;
using MyUtility.Extensions;

namespace BussinessObject.Bo.WebBo
{
    public class ContactBookBo : BaseBo<ContactBook>
    {
        public ContactBookBo()
            : base(DaoFactory.ContactBook)
        {
        }

        public List<Ins_ContactBook_Search_Result> Search(string keyWord, string phone, string email, int cityId, int pageIndex, int pageSize, ref int totalRows)
        {
            return DaoFactory.ContactBook.Search(keyWord, phone, email, cityId, pageIndex, pageSize, ref totalRows);
        }

    }
}
