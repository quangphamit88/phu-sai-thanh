﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using BussinessObject.Helper;
using BussinessObject.Models;
using DataAccess;
using EntitiesObject.Entities.WebEntities;
using MyUtility.Extensions;

namespace BussinessObject.Bo.WebBo
{
    public class CustomerRequestBo : BaseBo<CustomerRequest>
    {
        public CustomerRequestBo()
            : base(DaoFactory.CustomerRequest)
        {
        }
        public List<Ins_CustomerRequest_Search_Result> Search(int adminId, string keyword, DateTime fromDate, DateTime toDate, int status, int pageIndex, int pageSize, int customerType, ref int totalRow)
        {
            return DaoFactory.CustomerRequest.Search(adminId, keyword, fromDate, toDate, status, pageIndex, pageSize, customerType, ref totalRow);

        }
    }
}
