﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using BussinessObject.Helper;
using BussinessObject.Models;
using DataAccess;
using EntitiesObject.Entities.WebEntities;
using MyUtility.Extensions;

namespace BussinessObject.Bo.WebBo
{
    public class BrokerAccountBo : BaseBo<NewBroker>
    {
        public BrokerAccountBo()
            : base(DaoFactory.NewBroker)
        {
        }

        public List<Ins_BrokerAccount_SearchAutoComplete_Result> SearchAutoComplete(string keyword)
        {
            return DaoFactory.NewBroker.SearchAutoComplete(keyword);
        }
        public NewBroker GetByAdminId(int adminId)
        {
            return DaoFactory.NewBroker.GetByAdminId(adminId);
        }

        ///// <summary>
        ///// Thong tai khoan moi gioi, neu exist thi update
        ///// </summary>
        ///// <param name="broker"></param>
        ///// <returns></returns>
        //public bool AddBroker(NewBroker broker)
        //{
        //    return DaoFactory.BrokerAccount.AddBroker(broker);
        //}
    }
}
