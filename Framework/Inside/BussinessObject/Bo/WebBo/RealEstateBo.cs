﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using BussinessObject.Helper;
using BussinessObject.Models;
using DataAccess;
using EntitiesObject.Custom;
using EntitiesObject.Entities.WebEntities;
using EntitiesObject.Message;
using EntitiesObject.Message.DtoResponse;
using MyUtility.Extensions;

namespace BussinessObject.Bo.WebBo
{
    public class RealEstateBo : BaseBo<RealEstate>
    {
        public RealEstateBo()
            : base(DaoFactory.RealEstate)
        {
        }

        public BaseDtoResponse AddRealEstate(RealEstateInfo realEstateInfo)
        {
            var rs = new BaseDtoResponse();
            // Step 0. Them thong tin address
            var isSuccess = DaoFactory.AddressBook.Add(realEstateInfo.Address);
            if (!isSuccess)
            {
                rs.Code = CommonCode.InvalidData.Value();
                rs.Message = "Lỗi tạo AddressBook";
                return rs;
            }
            // Step 1. Them thong tin bat dong san
            realEstateInfo.RealEstate.AddressId = realEstateInfo.Address.Id;
            isSuccess = Add(realEstateInfo.RealEstate);
            if (!isSuccess)
            {
                rs.Code = CommonCode.InvalidData.Value();
                rs.Message = "Lỗi thêm thông tin bất động sản";
                return rs;
            }
            // Step 2. Them album batdongsan
            realEstateInfo.RealEstateAlbum.RealEstateId = realEstateInfo.RealEstate.Id;
            isSuccess = DaoFactory.RealEstateAlbum.Add(realEstateInfo.RealEstateAlbum);
            if (!isSuccess)
            {
                rs.Code = CommonCode.InvalidData.Value();
                rs.Message = "Lỗi thêm thông tin album bất động sản";
            }
            // Step 3. Them SEO batdongsan
            realEstateInfo.RealEstateSeo.RealEstateId = realEstateInfo.RealEstate.Id;
            isSuccess = DaoFactory.RealEstateSeo.Add(realEstateInfo.RealEstateSeo);
            if (!isSuccess)
            {
                rs.Code = CommonCode.InvalidData.Value();
                rs.Message += ",Lỗi thêm thông tin SEO bất động sản";
            }
            // Step 4. Them ContactBook
            realEstateInfo.Contact.AddressId = realEstateInfo.Address.Id;
            isSuccess = DaoFactory.ContactBook.Add(realEstateInfo.Contact);
            if (!isSuccess)
            {
                rs.Code = CommonCode.InvalidData.Value();
                rs.Message += ",Lỗi thêm thông tin khách hàng";
                return rs;
            }
            // Step 5. Them batdongsan Sale
            realEstateInfo.RealEstateOnSale.RealEstateId = realEstateInfo.RealEstate.Id;
            realEstateInfo.RealEstateOnSale.ContactId = realEstateInfo.Contact.Id;
            isSuccess = DaoFactory.RealEstateOnSale.Add(realEstateInfo.RealEstateOnSale);
            if (!isSuccess)
            {
                rs.Code = CommonCode.InvalidData.Value();
                rs.Message += ",Lỗi đăng thông tin giao dịch bất động sản";
                return rs;
            }
            rs.Code = CommonCode.Success.Value();
            rs.Message = "Tạo bất động sản thành công";
            return rs;
        }

        public BaseDtoResponse UpdateRealEstate(RealEstateInfo realEstateInfo)
        {
            var rs = new BaseDtoResponse();            
            // Step 1. Luu thong tin bat dong san
            var isSuccess = Save(realEstateInfo.RealEstate);
            if (!isSuccess)
            {
                rs.Code = CommonCode.InvalidData.Value();
                rs.Message = "Lỗi lưu thông tin bất động sản";
                return rs;
            }
            // Step 2. Luu thong tin address
            if (realEstateInfo.Address != null)
            {
                isSuccess = DaoFactory.AddressBook.Save(realEstateInfo.Address);
                if (!isSuccess)
                {
                    rs.Code = CommonCode.InvalidData.Value();
                    rs.Message = "Lỗi lưu AddressBook";
                    return rs;
                }
            }
            // Step 3. Them album batdongsan
            if (realEstateInfo.RealEstateAlbum != null)
            {
                isSuccess = DaoFactory.RealEstateAlbum.Save(realEstateInfo.RealEstateAlbum);
                if (!isSuccess)
                {
                    rs.Code = CommonCode.InvalidData.Value();
                    rs.Message = "Lỗi lưu thông tin album bất động sản";
                    return rs;
                }
            }
            // Step 3. Them SEO batdongsan
            if (realEstateInfo.RealEstateSeo != null)
            {
                isSuccess = DaoFactory.RealEstateSeo.Save(realEstateInfo.RealEstateSeo);
                if (!isSuccess)
                {
                    rs.Code = CommonCode.InvalidData.Value();
                    rs.Message = "Lỗi lưu thông tin SEO bất động sản";
                    return rs;
                }
            }
            // Step 4. Them ContactBook
            if (realEstateInfo.Contact != null)
            {
                isSuccess = DaoFactory.ContactBook.Save(realEstateInfo.Contact);
                if (!isSuccess)
                {
                    rs.Code = CommonCode.InvalidData.Value();
                    rs.Message = "Lỗi lưu thông tin khách hàng";
                    return rs;
                }
            }
            // Step 5. Them batdongsan Sale
            if (realEstateInfo.RealEstateOnSale != null)
            {
                isSuccess = DaoFactory.RealEstateOnSale.Save(realEstateInfo.RealEstateOnSale);
                if (!isSuccess)
                {
                    rs.Code = CommonCode.InvalidData.Value();
                    rs.Message = "Lỗi đăng thông tin giao dịch bất động sản";
                    return rs;
                }
            }
            rs.Code = CommonCode.Success.Value();
            rs.Message = "Tạo bất động sản thành công";
            return rs;
        }

        public List<Ins_RealEstate_Search_Result> Search(int adminId, int productType, string productName, string soNha, int streetId,
            int wardId, int districtId, int cityId, DateTime fromDate, DateTime toDate, int saleOrRent, int status, string sold, int pageIndex,
            int pageSize, ref int totalRow)
        {
            return DaoFactory.RealEstate.Search(adminId, productType, productName, soNha, streetId, wardId, districtId,
                    cityId, fromDate, toDate, saleOrRent, status,sold, pageIndex, pageSize,ref totalRow).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="includeAddress"></param>
        /// <param name="getType">
        /// 0. Get all things
        /// 1. Get RealEstate and Album
        /// 2. Get RealEstate and Seo 
        /// 3. Get RealEstate Sale
        /// </param>
        /// <returns></returns>
        public RealEstateInfo GetById(long id, long productId, bool includeAddress, int getType)
        {
            return DaoFactory.RealEstate.GetById(id, productId, includeAddress, getType);
        }

        public bool DeleteRealEstate(long id, long productId, int deleteType)
        {
            return DaoFactory.RealEstate.DeleteRealEstate(id, productId, deleteType);
        }
    }
}
