﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using BussinessObject.Helper;
using BussinessObject.Models;
using DataAccess;
using EntitiesObject.Entities.WebEntities;
using MyUtility.Extensions;

namespace BussinessObject.Bo.WebBo
{
    public class AdminBo : BaseBo<Admin>
    {
        public AdminBo()
            : base(DaoFactory.Admin)
        {
        }

        public List<Admin> Admin_GetAll()
        {
            return DaoFactory.Admin.Admin_GetAll();
        }

        public Ins_Admin_GetById_Result Admin_GetById(int id)
        {
            return DaoFactory.Admin.Admin_GetById(id);
        }


        public bool Admin_CheckIsExist(string keyWord)
        {
            return DaoFactory.Admin.Admin_CheckIsExist(keyWord);
        }

        public Ins_Admin_Login_Result Admin_Login(string loginName, string pass)
        {
            return DaoFactory.Admin.Admin_Login(loginName,pass);
        }

        public List<Ins_Admin_Search_Paging_Result> Admin_Search(string keyword, int isAtive, int pageIndex, int pagelengh, out int totalRow)
        {
            return DaoFactory.Admin.Admin_Search(keyword, isAtive, pageIndex, pagelengh, out totalRow);
        }

        public int Admin_Insert(Admin input)
        {
            return DaoFactory.Admin.Admin_Insert(input);

        }

        public bool Admin_UpdateInfo(Admin input)
        {
            return DaoFactory.Admin.Admin_UpdateInfo(input);
        }

        public bool Admin_UpdatePass(int id, string oldPPassword, string newPassword)
        {
            return DaoFactory.Admin.Admin_UpdatePass(id, oldPPassword, newPassword);
        }

        public  bool Admin_UpdateActive(int id, bool isActive, int modifyBy)
        {
            return DaoFactory.Admin.Admin_UpdateActive(id, isActive, modifyBy);
        }

        public bool DeleteAdmin(int adminId)
        {
            var obj = DaoFactory.Admin.GetOne(adminId);
            if (obj == null) return true;

            // delete broker
            var broker = DaoFactory.BrokerAccount.GetByAdminId(adminId);
            if (broker != null)
            {
                DaoFactory.BrokerAccount.Delete(broker);
            }
            // delete role
            DaoFactory.AdminRole.DeleteByAdmin(adminId);
            return DaoFactory.Admin.Delete(obj);
        }
    }
}
