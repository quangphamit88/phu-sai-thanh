﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using BussinessObject.Helper;
using BussinessObject.Models;
using DataAccess;
using EntitiesObject.Entities.WebEntities;
using MyUtility.Extensions;

namespace BussinessObject.Bo.WebBo
{
    public class AddressBookBo : BaseBo<AddressBook>
    {
        public AddressBookBo()
            : base(DaoFactory.AddressBook)
        {
        }

        public bool CheckExist(int id, int cityId, int districtId, int wardId, int streetId, string addressNo)
        {
            return DaoFactory.AddressBook.CheckExist(id, cityId, districtId, wardId, streetId, addressNo);
        }

        public List<Xcity> GetCityList()
        {
            return DaoFactory.Xcity.GetCityList();
        }

        public List<Xdistrict> GetDistrictByCity(int cityId)
        {
            return DaoFactory.Xdistrict.GetDistrictByCity(cityId);
        }

        public List<Xward> GetWardByDistrict(int districtId)
        {
            return DaoFactory.Xward.GetWardByDistrict(districtId);
        }

        public List<Xstreet> GetStreetByWard(int wardId)
        {
            return DaoFactory.Xstreet.GetStreetByWard(wardId);
        }

        public List<Xstreet> GetStreetByDistrict(int districtId)
        {
            return DaoFactory.Xstreet.GetStreetByDistrict(districtId);
        }

        public Xcity GetCityById(int cityId)
        {
            return DaoFactory.Xcity.GetOne(cityId);
        }

        public Xdistrict GetDistrictById(int districtId)
        {
            return DaoFactory.Xdistrict.GetOne(districtId);
        }

        public Xward GetWardById(int wardId)
        {
            return DaoFactory.Xward.GetOne(wardId);
        }
        public Xstreet GetStreetById(int streetId)
        {
            return DaoFactory.Xstreet.GetOne(streetId);
        }
    }
}
