﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using BussinessObject.Helper;
using BussinessObject.Models;
using DataAccess;
using EntitiesObject.Entities.WebEntities;
using MyUtility.Extensions;

namespace BussinessObject.Bo.WebBo
{
    public class RealEstatePermissionBo : BaseBo<RealEstatePermission>
    {
        public RealEstatePermissionBo()
            : base(DaoFactory.RealEstatePermission)
        {
        }
        public List<Ins_RealEstatePermission_SelectByProductId_Result> GetByProductId(long productId)
        {
            return DaoFactory.RealEstatePermission.GetByProductId(productId);
        }

        public RealEstatePermission GetByKeyIds(long productId, int adminId)
        {
            return DaoFactory.RealEstatePermission.GetByKeyIds(productId, adminId);
        }
        public List<RealEstatePermission> GetByAdminId(int adminId)
        {
            return DaoFactory.RealEstatePermission.GetByAdminId(adminId);
        }
    }
}
