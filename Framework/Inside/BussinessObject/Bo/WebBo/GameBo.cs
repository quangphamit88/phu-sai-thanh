﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using BussinessObject.Helper;
using BussinessObject.Models;
using DataAccess;
using EntitiesObject.Entities.WebEntities;
using MyUtility.Extensions;

namespace BussinessObject.Bo.WebBo
{
    public class GameBo : BaseBo<Game>
    {
        public GameBo()
            : base(DaoFactory.Game)
        {
        }

        public List<Game> Game_GetAll()
        {
            return DaoFactory.Game.Game_GetAll();
        }

        public Ins_Game_GetById_Result Game_GetById(int id)
        {
            return DaoFactory.Game.Game_GetById(id);
        }
        public int Game_Insert(Game input)
        {
            return DaoFactory.Game.Game_Insert(input);
        }

        public bool Article_UpdateActive(int id, bool isActive, int modifyBy)
        {
            return DaoFactory.Game.Article_UpdateActive(id, isActive, modifyBy);
        }
        public bool Article_UpdateInfo(Game input)
        {
            return DaoFactory.Game.Article_UpdateInfo(input);
        }

        public List<Ins_Game_Get_Result> Game_Search(bool IsActive, string Name)
        {
            return DaoFactory.Game.Game_Search(IsActive, Name);
        }

    }
}
