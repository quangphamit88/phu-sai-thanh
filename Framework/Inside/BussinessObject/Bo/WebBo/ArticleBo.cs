﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using BussinessObject.Helper;
using BussinessObject.Models;
using DataAccess;
using EntitiesObject.Entities.WebEntities;
using MyUtility.Extensions;

namespace BussinessObject.Bo.WebBo
{
    public class ArticleBo : BaseBo<Article>
    {
        public ArticleBo()
            : base(DaoFactory.Article)
        {
        }

        public List<Article> Article_GetAll()
        {
            return DaoFactory.Article.Article_GetAll();
        }

        public Ins_Article_GetById_Result Article_GetById(int id)
        {
            return DaoFactory.Article.Article_GetById(id);
        }


        /// <summary>
        /// // cập nhật trạng thái bài viết, status = -1 là xóa luôn bài viết khỏi DB
        /// </summary>
        /// <returns></returns>
        public bool Article_EditStatus(int id, int modifyBy, short modifyByType, short status)
        {
            return DaoFactory.Article.Article_EditStatus(id, modifyBy, modifyByType, status);
        }

        public int Article_Insert(Article input)
        {
            return DaoFactory.Article.Article_Insert(input);
        }

        public bool Article_UpdateContent(Article input)
        {
            return DaoFactory.Article.Article_UpdateContent(input);
        }

        public bool Article_UpdateViewNumber(int id, int viewNumber)
        {
            return DaoFactory.Article.Article_UpdateViewNumber(id, viewNumber);
        }

        public List<Ins_Article_List_Paging_Result> Article_List_Paging(int categoryID, int accountID, string accountDisplayName, string title, int status, int isComment, int isCommentFacebook, int isFollow, int isShare, int isLikeFacebook, int isNotify, int isHot, DateTime createDateFrom, DateTime createDateTo, int pageIndex, int pagelengh, out int totalRow)
        {
            return DaoFactory.Article.Article_List_Paging(categoryID, accountID, accountDisplayName, title, status, isComment, isCommentFacebook, isFollow, isShare, isLikeFacebook, isNotify, isHot, createDateFrom, createDateTo, pageIndex, pagelengh, out totalRow);
        }


    }
}