﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using BussinessObject.Helper;
using BussinessObject.Models;
using DataAccess;
using EntitiesObject.Entities.WebEntities;
using MyUtility.Extensions;

namespace BussinessObject.Bo.WebBo
{
    public class AdminRoleBo : BaseBo<AdminRole>
    {
        public AdminRoleBo()
            : base(DaoFactory.AdminRole)
        {
        }
        public bool AddUserRole(int adminId, int roleId)
        {
            return DaoFactory.AdminRole.AddUserRole(adminId, roleId);
        }

        public AdminRole GetByAdminId(int adminId)
        {
            return DaoFactory.AdminRole.GetByAdminId(adminId);
        }
    }
}
