﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using BussinessObject.Helper;
using BussinessObject.Models;
using DataAccess;
using EntitiesObject.Entities.WebEntities;
using MyUtility.Extensions;

namespace BussinessObject.Bo.WebBo
{
    public class CategoryBo : BaseBo<Category>
    {
        public CategoryBo()
            : base(DaoFactory.Category)
        {
        }

        public List<Ins_Category_GetAll_Result> Category_GetAll()
        {
            return DaoFactory.Category.Category_GetAll();
        }

        public Ins_Category_GetById_Result Category_GetById(int id)
        {
            return DaoFactory.Category.Category_GetById(id);
        }

        public bool Category_UpdateStaus(int id, bool isActive, int modifyBy)
        {
            return DaoFactory.Category.Category_UpdateStaus(id, isActive, modifyBy);
        }

        public bool Category_Update(Category input)
        {
            return DaoFactory.Category.Category_Update(input);
        }

        public int Category_Insert(Category input)
        {
            return DaoFactory.Category.Category_Insert(input);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameID"> -1 for get all</param>
        /// <param name="isActive">-1 for get all</param>
        /// <param name="position">-1 for get all</param>
        /// <param name="name">'' for get all</param>
        /// <returns></returns>
        public List<Ins_Category_Search_Result> Category_Search(int gameID, int isActive, int position, string name)
        {
            return DaoFactory.Category.Category_Search(gameID, isActive, position, name);
        }

        public bool Category_UpdateUserAccess(int id, bool isUserAccess, int modifyBy)
        {
            return DaoFactory.Category.Category_UpdateUserAccess(id, isUserAccess, modifyBy);
        }
    }
}
