﻿/**********************************************************************
 * Author: xxxx
 * DateCreate: 06-25-2014 
 * Description: DaoFactory  
 * ####################################################################
 * Author:......................
 * DateModify: .................
 * Description: ................
 * 
 *********************************************************************/

using DataAccess.Dao.WebDao;

namespace DataAccess
{
    public class DaoFactory
    {
        #region Admin
        //public static IAdminDao Admin { get { return new AdminDao(); } }
        #endregion

        #region Category
        public static ICategoryDao Category { get { return new CategoryDao(); } }
        #endregion

        #region Article
        public static IArticleDao Article { get { return new ArticleDao(); } }
        #endregion

        #region Comment

        public static ICommentDao Comment { get { return new CommentDao(); } }

        #endregion

        #region Game
        public static IGameDao Game { get { return new GameDao(); } }        
        #endregion

        #region RealEstate
        public static IRealEstateDao RealEstate { get { return new RealEstateDao(); } }
        public static IRealEstateAlbumDao RealEstateAlbum { get { return new RealEstateAlbumDao(); } }
        public static IRealEstateSeoDao RealEstateSeo { get { return new RealEstateSeoDao(); } }
        public static IRealEstateOnSaleDao RealEstateOnSale { get { return new RealEstateOnSaleDao(); } }

        public static IContactBookDao ContactBook { get { return new ContactBookDao(); } }
        public static IAddressBookDao AddressBook { get { return new AddressBookDao(); } }
        //public static INewBrokerDao NewBroker { get { return new NewBrokerDao(); } }

        public static IXcityDao Xcity { get { return new XcityDao(); } }
        public static IXdistrictDao Xdistrict { get { return new XdistrictDao(); } }
        public static IXwardDao Xward { get { return new XwardDao(); } }
        public static IXstreetDao Xstreet { get { return new XstreetDao(); } }
        public static IRealEstatePermissionDao RealEstatePermission { get { return new RealEstatePermissionDao(); } }
        //public static IAdminRoleDao AdminRole { get { return new AdminRoleDao(); } }
        public static ICustomerRequestDao CustomerRequest { get { return new CustomerRequestDao(); } }
        public static IUserFeedbackDao UserFeedback { get { return new UserFeedbackDao();} }
        public static ISystemConfigDao SystemConfig { get { return new SystemConfigDao();} }

        #endregion
    }
}
