﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.EF;
using DataAccess.Interface;
using EntitiesObject.Entities.WebEntities;

namespace DataAccess.Dao.WebDao
{

    public interface IUserFeedbackDao : IBaseFactories<UserFeedback>
    {
        List<Ins_UserFeedback_Search_Result> Search(string keyword, int adminId, int isReply, int pageIndex, int pageSize, out int totalRows);
    }
    internal class UserFeedbackDao : DaoFactories<WebEntities, UserFeedback>, IUserFeedbackDao
    {

        public List<Ins_UserFeedback_Search_Result> Search(string keyword, int adminId, int isReply, int pageIndex, int pageSize, out int totalRows)
        {
            using (Uow)
            {
                var totalR = new ObjectParameter("TotalRows", typeof(int));                
                var rs = Uow.Context.Ins_UserFeedback_Search(keyword, isReply, adminId, pageIndex, pageSize, totalR).ToList();
                totalRows = totalR.Value == null ? 0 : Convert.ToInt32(totalR.Value.ToString());
                return rs;
            }
        }
    }
}
