﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DataAccess.EF;
using DataAccess.Interface;
using EntitiesObject.Entities.WebEntities;
using DataAccess.Extension;
using System.Data.Entity.Core.Objects;

namespace DataAccess.Dao.WebDao
{
    public interface ICategoryDao : IBaseFactories<Category>
    {
        /// <summary>
        /// Lấy tất cả bài viết
        /// </summary>
        /// <returns></returns>
        List<Ins_Category_GetAll_Result> Category_GetAll();

        bool Category_UpdateUserAccess(int id, bool isUserAccess, int modifyBy);

        bool Category_UpdateStaus(int id, bool isActive, int modifyBy);

        bool Category_Update(Category input);

        int Category_Insert(Category input);

        List<Ins_Category_Search_Result> Category_Search(int gameID, int isActive, int position, string name);

        Ins_Category_GetById_Result Category_GetById(int id);
    }

    internal class CategoryDao : DaoFactories<WebEntities, Category>, ICategoryDao
    {

        public bool Category_UpdateUserAccess(int id, bool isUserAccess, int modifyBy)
        {
            using (Uow)
            {
                var outOutResult = new ObjectParameter("OutResult", typeof(int));
                var list = Uow.Context.Ins_Category_UpdateUserAccess(id, isUserAccess, modifyBy, outOutResult);
                int result = 0;
                if (outOutResult.Value != null)
                {
                    int.TryParse(outOutResult.Value.ToString(), out result);
                }
                return result == 1;
            }


        }
        /// <summary>
        /// Lấy tất cả bài viết
        /// </summary>
        /// <returns></returns>
        public List<Ins_Category_GetAll_Result> Category_GetAll()
        {
            using (Uow)
            {
                return Uow.Context.Ins_Category_GetAll().ToList();
            }
        }

        public Ins_Category_GetById_Result Category_GetById(int id)
        {
            using (Uow)
            {
                return Uow.Context.Ins_Category_GetById(id).FirstOrDefault();
            }
        }

        public bool Category_UpdateStaus(int id,  bool isActive,int modifyBy )
        {
            using (Uow)
            {
                var outOutResult = new ObjectParameter("OutResult", typeof(int));
                var list = Uow.Context.Ins_Category_UpdateStaus(id, isActive, modifyBy, outOutResult);
                int result = 0;
                if (outOutResult.Value != null)
                {
                    int.TryParse(outOutResult.Value.ToString(), out result);
                }
                return result == 1;
            }
        }

        public bool Category_Update(Category input)
        {
            using (Uow)
            {
                var outResult = new ObjectParameter("IdCategory", typeof(int));
                var list = Uow.Context.Ins_Category_Update(input.ID, input.GameID, 
                    input.Name, input.Avatar, input.Position, input.MetaTitle,input.MetaKeyword, input.MetaDescription, input.OgTitle, input.OgDescription,
                    input.OgUrl, input.OgImage, input.TwitterTitle, input.TwitterDescription, input.TwitterUrl, input.TwitterImage, input.MetaH1, input.MetaH2, input.MetaH3, input.ModifyBy, input.IsActive, input.IsUserAccess, outResult);
                int result = 0;
                if (outResult.Value != null)
                {
                    int.TryParse(outResult.Value.ToString(), out result);
                }
                return result == 1;
            }
        }

        public int Category_Insert(Category input)
        {
            using (Uow)
            {
                var outResult = new ObjectParameter("IdCategory", typeof(int));
                var list = Uow.Context.Ins_Category_Insert(input.GameID,
                    input.Name, input.Avatar, input.Position, input.MetaTitle, input.MetaKeyword, input.MetaDescription, input.OgTitle, input.OgDescription,
                    input.OgUrl, input.OgImage, input.TwitterTitle, input.TwitterDescription, input.TwitterUrl, input.TwitterImage, input.CreateBy, input.MetaH1, input.MetaH2, input.MetaH3, input.IsActive, input.IsUserAccess, outResult);
                int result = 0;
                if (outResult.Value != null)
                {
                    int.TryParse(outResult.Value.ToString(), out result);
                }
                return result;
            }
        }

        public List<Ins_Category_Search_Result> Category_Search(int gameID, int isActive, int position, string name)
        {
            using (Uow)
            {
                return Uow.Context.Ins_Category_Search(gameID,isActive,position,name).ToList();
            }
        }
    }
}
