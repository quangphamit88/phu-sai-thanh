﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DataAccess.EF;
using DataAccess.Interface;
using EntitiesObject.Entities.WebEntities;
using DataAccess.Extension;
using System.Data.Entity.Core.Objects;

namespace DataAccess.Dao.WebDao
{
    public interface IArticleDao : IBaseFactories<Article>
    {
        /// <summary>
        /// Lấy tất cả bài viết
        /// </summary>
        /// <returns></returns>
        List<Article> Article_GetAll();

        /// <summary>
        /// // cập nhật trạng thái bài viết, status = -1 là xóa luôn bài viết khỏi DB
        /// </summary>
        /// <returns></returns>
        bool Article_EditStatus(int id, int modifyBy, short modifyByType, short status);

        int Article_Insert(Article input);

        bool Article_UpdateContent(Article input);

        bool Article_UpdateViewNumber(int id, int viewNumber);

        List<Ins_Article_List_Paging_Result> Article_List_Paging(int categoryID, int accountID,string accountDisplayName, string title, int status, int isComment, int isCommentFacebook, int isFollow, int isShare, int isLikeFacebook, int isNotify, int isHot, DateTime createDateFrom, DateTime createDateTo, int pageIndex, int pagelengh,  out int totalRow);

        Ins_Article_GetById_Result Article_GetById(int id);
    }

    internal class ArticleDao : DaoFactories<WebEntities, Article>, IArticleDao
    {
        /// <summary>
        /// Lấy tất cả bài viết
        /// </summary>
        /// <returns></returns>
        public List<Article> Article_GetAll()
        {
            using (Uow)
            {
                return Uow.Context.Articles.ToList();
            }
        }

        public Ins_Article_GetById_Result Article_GetById(int id)
        {
            using (Uow)
            {
                return Uow.Context.Ins_Article_GetById(id).FirstOrDefault();
            }
        }


        public bool Article_EditStatus(int id, int modifyBy, short modifyByType, short status)
        {
            using (Uow)
            {
                var outOutResult = new ObjectParameter("OutResult", typeof(int));
                var list = Uow.Context.Ins_Article_EditStatus(id, modifyBy, modifyByType, status, outOutResult);
                int result = 0;
                if (outOutResult.Value != null)
                {
                    int.TryParse(outOutResult.Value.ToString(), out result);
                }
                return result == 1;
            }
        }

        public int Article_Insert(Article input)
        {
            using (Uow)
            {
                var outIdArticle = new ObjectParameter("IdArticle", typeof(int));
                var list = Uow.Context.Ins_Article_Insert(input.CategoryID, input.AccountID, input.AccountDisplayName, input.AccountType, input.Title, input.Avatar, input.Summary, input.Content, input.Status, input.ViewNumber, input.ViewType, input.IsComment, input.IsCommentFacebook, input.IsFollow, input.IsShare, input.IsLikeFacebook, input.IsNotify
                    , input.IsHot, input.MetaTitle, input.MetaKeyword, input.MetaDescription, input.OgTitle, input.OgDescription, input.OgUrl, input.OgImage, input.TwitterTitle, input.TwitterDescription, input.TwitterUrl, input.TwitterImage, input.MetaH1, input.MetaH2, input.MetaH3, input.ModifyBy, input.ModifyByType, input.PublishDate, outIdArticle);
                int result = 0;
                if (outIdArticle.Value != null)
                {
                    int.TryParse(outIdArticle.Value.ToString(), out result);
                }
                return result ;
            }
        }

        public bool Article_UpdateContent(Article input)
        {
            using (Uow)
            {
                var outResult = new ObjectParameter("OutResult", typeof(int));
                var list = Uow.Context.Ins_Article_UpdateContent(input.ID, input.Status, input.CategoryID, input.Title, input.Avatar, input.Summary, input.Content, input.ViewType, input.IsComment, input.IsCommentFacebook, input.IsFollow, input.IsShare, input.IsLikeFacebook, input.IsNotify, input.IsHot, input.MetaTitle, input.MetaKeyword, input.MetaDescription, input.OgTitle, input.OgDescription, input.OgUrl, input.OgImage, input.TwitterTitle, input.TwitterDescription, input.TwitterUrl, input.TwitterImage, input.MetaH1, input.MetaH2, input.MetaH3, input.ModifyBy, input.ModifyByType, input.PublishDate, outResult);
                int result = 0;
                if (outResult.Value != null)
                {
                    int.TryParse(outResult.Value.ToString(), out result);
                }
                return result == 1;
            }
        }

        public bool Article_UpdateViewNumber(int id, int viewNumber)
        {
            using (Uow)
            {
                var outResult = new ObjectParameter("OutResult", typeof(int));
                var list = Uow.Context.Ins_Article_UpdateViewNumber(id,viewNumber ,outResult);
                    int result = 0;
                if (outResult.Value != null)
                {
                    int.TryParse(outResult.Value.ToString(), out result);
                }
                return result ==1;
            }
        }

        public List<Ins_Article_List_Paging_Result> Article_List_Paging(int categoryID, int accountID, string accountDisplayName, string title, int status, int isComment, int isCommentFacebook, int isFollow, int isShare, int isLikeFacebook, int isNotify, int isHot, DateTime createDateFrom, DateTime createDateTo, int pageIndex, int pagelengh, out int totalRow)
        {
            using (Uow)
            {

                using (Uow)
                {
                    var outTotalRow = new ObjectParameter("TotalRow", typeof(int));

                    var list = Uow.Context.Ins_Article_List_Paging(categoryID, accountID,accountDisplayName, title, status, isComment, isCommentFacebook, isFollow, isShare, isLikeFacebook, isNotify, isHot, createDateFrom, createDateTo, pageIndex, pagelengh, outTotalRow).ToList();

                    if (list.Count > 0)
                    {
                        totalRow = outTotalRow.Value == null ? 0 : (int)outTotalRow.Value;
                    }
                    else
                    {
                        totalRow = 0;
                    }
                    return list;
                }

              
            }
        }
    }
}
