﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DataAccess.EF;
using DataAccess.Interface;
using EntitiesObject.Entities.WebEntities;
using DataAccess.Extension;
using System.Data.Entity.Core.Objects;

namespace DataAccess.Dao.WebDao
{
    public interface IGameDao : IBaseFactories<Game>
    {
        /// <summary>
        /// Lấy tất cả bài viết
        /// </summary>
        /// <returns></returns>
        List<Game> Game_GetAll();
        int Game_Insert(Game input);

        bool Article_UpdateActive(int id, bool isActive, int modifyBy);
        bool Article_UpdateInfo(Game input);

        List<Ins_Game_Get_Result> Game_Search(bool IsActive, string Name);

        Ins_Game_GetById_Result Game_GetById(int id);
    }

    internal class GameDao : DaoFactories<WebEntities, Game>, IGameDao
    {
        /// <summary>
        /// Lấy tất cả bài viết
        /// </summary>
        /// <returns></returns>
        public List<Game> Game_GetAll()
        {
            using (Uow)
            {
                return Uow.Context.Games.ToList();
            }
        }

        public Ins_Game_GetById_Result Game_GetById(int id)
        {
            using (Uow)
            {
                return Uow.Context.Ins_Game_GetById(id).FirstOrDefault();
            }
        }

        public int Game_Insert(Game input)
        {
            using (Uow)
            {
                var outIdGame = new ObjectParameter("IdGame", typeof(int));
                var list = Uow.Context.Ins_Game_Insert(input.Name, input.Avatar, input.Description, input.LinkWebsite, input.LinkiOS, input.LinkAndroid, input.IsActive, input.CreateBy, outIdGame);
                int result = 0;
                if (outIdGame.Value != null)
                {
                    int.TryParse(outIdGame.Value.ToString(), out result);
                }
                return result;
            }
        }

        public bool Article_UpdateActive(int id, bool isActive, int modifyBy)
        {
            using (Uow)
            {
                var outOutResult = new ObjectParameter("OutResult", typeof(int));
                var list = Uow.Context.Ins_Game_UpdateActive(id, isActive, modifyBy, outOutResult);
                int result = 0;
                if (outOutResult.Value != null)
                {
                    int.TryParse(outOutResult.Value.ToString(), out result);
                }
                return result == 1;
            }
        }

        public bool Article_UpdateInfo(Game input)
        {

            using (Uow)
            {
                var outOutResult = new ObjectParameter("OutResult", typeof(int));
                var list = Uow.Context.Ins_Game_UpdateInfo(input.ID, input.Name, input.Avatar, input.Description, input.LinkWebsite, input.LinkiOS, input.LinkAndroid, input.IsActive, input.ModifyBy, outOutResult);
                int result = 0;
                if (outOutResult.Value != null)
                {
                    int.TryParse(outOutResult.Value.ToString(), out result);
                }
                return result == 1;
            }
        }

        public List<Ins_Game_Get_Result> Game_Search(bool IsActive, string Name)
        {   
            using (Uow)
            {
                var list = Uow.Context.Ins_Game_Get(IsActive, Name);
                return list.ToList();
            }
        }
    }
}
