﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DataAccess.EF;
using DataAccess.Interface;
using EntitiesObject.Custom;
using EntitiesObject.Entities.WebEntities;
using DataAccess.Extension;
using System.Data.Entity.Core.Objects;

namespace DataAccess.Dao.WebDao
{
    public interface IRealEstateDao : IBaseFactories<RealEstate>
    {
        /// <summary>
        /// Tim bat dong san
        /// </summary>
        /// <returns></returns>
        List<Ins_RealEstate_Search_Result> Search(int adminId, int productType, string ProductName, string soNha, int streetId, int wardId, int districtId, int cityId, DateTime fromDate, DateTime toDate, int saleOrRent, int status, string sold, int pageIndex, int pageSize, ref int totalRow);

        bool DeleteRealEstate(long id, long productId, int deleteType);
        //RealEstateInfo GetById(long id, bool includeAddress, int getType);

        RealEstateInfo GetById(long id, long productId, bool includeAddress, int getType);
    }

    internal class RealEstateDao : DaoFactories<WebEntities, RealEstate>, IRealEstateDao
    {
        /// <summary>
        /// Tim bat dong san
        /// </summary>
        /// <returns></returns>
        public List<Ins_RealEstate_Search_Result> Search(int adminId, int productType, string productName, string soNha, int streetId, int wardId, int districtId, int cityId, DateTime fromDate, DateTime toDate, int saleOrRent, int status, string sold, int pageIndex, int pageSize, ref int totalRow)
        {
            using (Uow)
            {
                var totalRows = new ObjectParameter("TotalRow", typeof(int));
                var rs = Uow.Context.Ins_RealEstate_Search(adminId,productType, productName, soNha, streetId, wardId, districtId,
                    cityId, fromDate, toDate, saleOrRent, status,sold, pageIndex, pageSize, totalRows).ToList();
                //if (totalRows.Value != null)
                //{
                //    int.TryParse(totalRows.Value.ToString(), out totalRow);
                //}
                totalRow = rs.Any() ? rs.FirstOrDefault().TotalRow.GetValueOrDefault() : 0;
                return rs;
            }
        }

        public bool DeleteRealEstate(long id, long productId, int deleteType)
        {
            using (Uow)
            {
                return Uow.Context.Ins_RealEstate_Delete(id, productId, deleteType) > 0;
            }
        }


        public RealEstateInfo GetById(long id, bool includeAddress, int getType)
        {
            using (Uow)
            {
                var obj = new RealEstateInfo { RealEstate = GetOne(id) };
                // get BDS
                if (obj.RealEstate == null)
                {
                    return null;
                }
                if (includeAddress)
                {
                    obj.Address = DaoFactory.AddressBook.GetOne(obj.RealEstate.AddressId);
                }
                switch (getType)
                {                    
                    case 1:
                        // Get RealEstate and Album
                        obj.RealEstateAlbum = DaoFactory.RealEstateAlbum.GetOne(id);
                        break;
                    case 2:
                        // Get RealEstate and Seo 
                        obj.RealEstateSeo = DaoFactory.RealEstateSeo.GetOne(id);
                        break;
                    case 3:
                        // Get RealEstate Sale
                        obj.RealEstateOnSale = DaoFactory.RealEstateOnSale.GetOne(id);
                        if (obj.RealEstateOnSale == null) return obj;
                        obj.Contact = DaoFactory.ContactBook.GetOne(obj.RealEstateOnSale.ContactId);
                        //obj.Broker = DaoFactory.BrokerAccount.GetOne(obj.RealEstateOnSale.BrokerId);
                        break;
                    default:
                        // Get all things
                        // Get RealEstate and Album
                        obj.RealEstateAlbum = DaoFactory.RealEstateAlbum.GetOne(id);
                        // Get RealEstate and Seo 
                        obj.RealEstateSeo = DaoFactory.RealEstateSeo.GetOne(id);
                        // Get RealEstate Sale
                        obj.RealEstateOnSale = DaoFactory.RealEstateOnSale.GetByRealEstateId(id);
                        if (obj.RealEstateOnSale == null) return obj;
                        obj.Contact = DaoFactory.ContactBook.GetOne(obj.RealEstateOnSale.ContactId);
                        //obj.Broker = DaoFactory.BrokerAccount.GetOne(obj.RealEstateOnSale.BrokerId);
                        break;                        
                }                

                return obj;
            }
        }


        public RealEstateInfo GetById(long id, long productId, bool includeAddress, int getType)
        {
            using (Uow)
            {
                var obj = new RealEstateInfo { RealEstate = GetOne(id) };
                // get BDS
                if (obj.RealEstate == null)
                {
                    return null;
                }
                if (includeAddress)
                {
                    obj.Address = DaoFactory.AddressBook.GetOne(obj.RealEstate.AddressId);
                }
                switch (getType)
                {                    
                    case 1:
                        // Get RealEstate and Album
                        obj.RealEstateAlbum = DaoFactory.RealEstateAlbum.GetOne(id);
                        break;
                    case 2:
                        // Get RealEstate and Seo 
                        obj.RealEstateSeo = DaoFactory.RealEstateSeo.GetOne(id);
                        break;
                    case 3:
                        // Get RealEstate Sale
                        obj.RealEstateOnSale = DaoFactory.RealEstateOnSale.GetOne(productId);
                        if (obj.RealEstateOnSale == null) return obj;
                        obj.Contact = DaoFactory.ContactBook.GetOne(obj.RealEstateOnSale.ContactId);
                        //obj.Broker = DaoFactory.BrokerAccount.GetOne(obj.RealEstateOnSale.BrokerId);
                        break;
                    default:
                        // Get all things
                        // Get RealEstate and Album
                        obj.RealEstateAlbum = DaoFactory.RealEstateAlbum.GetOne(id);
                        // Get RealEstate and Seo 
                        obj.RealEstateSeo = DaoFactory.RealEstateSeo.GetOne(id);
                        // Get RealEstate Sale
                        obj.RealEstateOnSale = DaoFactory.RealEstateOnSale.GetByRealEstateId(id);
                        if (obj.RealEstateOnSale == null) return obj;
                        obj.Contact = DaoFactory.ContactBook.GetOne(obj.RealEstateOnSale.ContactId);
                        //obj.Broker = DaoFactory.BrokerAccount.GetOne(obj.RealEstateOnSale.BrokerId);
                        break;                        
                }                

                return obj;
            }
        }
    }
}
