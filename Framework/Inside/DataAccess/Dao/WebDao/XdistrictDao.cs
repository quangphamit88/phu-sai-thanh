﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DataAccess.EF;
using DataAccess.Interface;
using EntitiesObject.Entities.WebEntities;
using DataAccess.Extension;
using System.Data.Entity.Core.Objects;

namespace DataAccess.Dao.WebDao
{
    public interface IXdistrictDao : IBaseFactories<Xdistrict>
    {

        List<Xdistrict> GetDistrictByCity(int cityId);
    }

    internal class XdistrictDao : DaoFactories<WebEntities, Xdistrict>, IXdistrictDao
    {
        //public bool Article_UpdateActive(int id, bool isActive, int modifyBy)
        //{
        //    using (Uow)
        //    {
        //        var outOutResult = new ObjectParameter("OutResult", typeof(int));
        //        var list = Uow.Context.Ins_Game_UpdateActive(id, isActive, modifyBy, outOutResult);
        //        int result = 0;
        //        if (outOutResult.Value != null)
        //        {
        //            int.TryParse(outOutResult.Value.ToString(), out result);
        //        }
        //        return result == 1;
        //    }
        //}


        public List<Xdistrict> GetDistrictByCity(int cityId)
        {
            using (Uow)
            {
                return Uow.Context.Xdistricts.Where(x => x.IsPublic == true && x.CityId == cityId).ToList();
            }
        }
    }
}
