﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using DataAccess.EF;
using DataAccess.Interface;
using EntitiesObject.Entities.WebEntities;
using DataAccess.Extension;
using System.Data.Entity.Core.Objects;

namespace DataAccess.Dao.WebDao
{
    public interface IAdminRoleDao : IBaseFactories<AdminRole>
    {
        bool AddUserRole(int adminId, int roleId);

        AdminRole GetByAdminId(int adminId);

        bool DeleteByAdmin(int adminId);
    }

    internal class AdminRoleDao : DaoFactories<WebEntities, AdminRole>, IAdminRoleDao
    {
        //public bool Article_UpdateActive(int id, bool isActive, int modifyBy)
        //{
        //    using (Uow)
        //    {
        //        var outOutResult = new ObjectParameter("OutResult", typeof(int));
        //        var list = Uow.Context.Ins_Game_UpdateActive(id, isActive, modifyBy, outOutResult);
        //        int result = 0;
        //        if (outOutResult.Value != null)
        //        {
        //            int.TryParse(outOutResult.Value.ToString(), out result);
        //        }
        //        return result == 1;
        //    }
        //}

        public bool AddUserRole(int adminId, int roleId)
        {
            using (Uow)
            {
                var obj = Uow.Context.AdminRoles.FirstOrDefault(x => x.AdminId == adminId);
                if (obj != null)
                {
                    obj.RoleId = roleId;
                    return Save(obj);
                }
                else
                {
                    obj = new AdminRole { AdminId = adminId, RoleId = roleId, CreateDate = DateTime.Now };
                    return Add(obj);
                }
            }
        }


        public AdminRole GetByAdminId(int adminId)
        {
            using (Uow)
            {
                return Uow.Context.AdminRoles.FirstOrDefault(x => x.AdminId == adminId);
            }
        }


        public bool DeleteByAdmin(int adminId)
        {
            using (Uow)
            {
                var obj = GetByAdminId(adminId);
                if (obj != null)
                {
                    return DaoFactory.AdminRole.Delete(obj);
                }
                return false;
            }
        }
    }
}
