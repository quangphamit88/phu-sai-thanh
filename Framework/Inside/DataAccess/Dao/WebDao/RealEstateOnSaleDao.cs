﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DataAccess.EF;
using DataAccess.Interface;
using EntitiesObject.Entities.WebEntities;
using DataAccess.Extension;
using System.Data.Entity.Core.Objects;

namespace DataAccess.Dao.WebDao
{
    public interface IRealEstateOnSaleDao : IBaseFactories<RealEstateOnSale>
    {

        RealEstateOnSale GetByRealEstateId(long realEstateId);
        IEnumerable<RealEstateOnSale> GetListByRealEstateId(long realEstateId);
        RealEstateOnSale GetOne(long productId);
    }

    internal class RealEstateOnSaleDao : DaoFactories<WebEntities, RealEstateOnSale>, IRealEstateOnSaleDao
    {
        //public bool Article_UpdateActive(int id, bool isActive, int modifyBy)
        //{
        //    using (Uow)
        //    {
        //        var outOutResult = new ObjectParameter("OutResult", typeof(int));
        //        var list = Uow.Context.Ins_Game_UpdateActive(id, isActive, modifyBy, outOutResult);
        //        int result = 0;
        //        if (outOutResult.Value != null)
        //        {
        //            int.TryParse(outOutResult.Value.ToString(), out result);
        //        }
        //        return result == 1;
        //    }
        //}

        public RealEstateOnSale GetOne(long productId)
        {
            using (Uow)
            {
                return
                    Uow.Context.RealEstateOnSales
                        .FirstOrDefault(x => x.ProductId == productId);
            }
        }

        public RealEstateOnSale GetByRealEstateId(long realEstateId)
        {
            using (Uow)
            {
                return
                    Uow.Context.RealEstateOnSales.OrderByDescending(x => x.ProductId)
                        .FirstOrDefault(x => x.RealEstateId == realEstateId);
            }
        }


        public IEnumerable<RealEstateOnSale> GetListByRealEstateId(long realEstateId)
        {
            using (Uow)
            {
                return
                    Uow.Context.RealEstateOnSales.Where(x => x.RealEstateId == realEstateId).AsEnumerable();
            }
        }
    }
}
