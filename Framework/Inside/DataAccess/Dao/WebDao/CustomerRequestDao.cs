﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using DataAccess.EF;
using DataAccess.Interface;
using EntitiesObject.Entities.WebEntities;
using DataAccess.Extension;
using System.Data.Entity.Core.Objects;

namespace DataAccess.Dao.WebDao
{
    public interface ICustomerRequestDao : IBaseFactories<CustomerRequest>
    {

        List<Ins_CustomerRequest_Search_Result> Search(int adminId, string keyword, DateTime fromDate, DateTime toDate, int status, int pageIndex, int pageSize, int customerType, ref int totalRow);
    }

    internal class CustomerRequestDao : DaoFactories<WebEntities, CustomerRequest>, ICustomerRequestDao
    {
        //public bool Article_UpdateActive(int id, bool isActive, int modifyBy)
        //{
        //    using (Uow)
        //    {
        //        var outOutResult = new ObjectParameter("OutResult", typeof(int));
        //        var list = Uow.Context.Ins_Game_UpdateActive(id, isActive, modifyBy, outOutResult);
        //        int result = 0;
        //        if (outOutResult.Value != null)
        //        {
        //            int.TryParse(outOutResult.Value.ToString(), out result);
        //        }
        //        return result == 1;
        //    }
        //}

        public List<Ins_CustomerRequest_Search_Result> Search(int adminId, string keyword, DateTime fromDate, DateTime toDate, int status, int pageIndex, int pageSize, int customerType, ref int totalRow)
        {
            using (Uow)
            {
                var outOutResult = new ObjectParameter("TotalRow", typeof(int));
                var list = Uow.Context.Ins_CustomerRequest_Search(adminId,keyword, fromDate, toDate,status, pageIndex,pageSize,customerType, outOutResult).ToList();
                //int result = 0;
                //if (outOutResult.Value != null)
                //{
                //    int.TryParse(outOutResult.Value.ToString(), out result);
                //}
                totalRow = list.Any() ? list[0].TotalRow.GetValueOrDefault() : 0;
                return list;
            }
        }
    }
}
