﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DataAccess.EF;
using DataAccess.Interface;
using EntitiesObject.Entities.WebEntities;
using DataAccess.Extension;
using System.Data.Entity.Core.Objects;

namespace DataAccess.Dao.WebDao
{
    public interface IContactBookDao : IBaseFactories<ContactBook>
    {

        List<Ins_ContactBook_Search_Result> Search(string keyWord, string phone, string email, int cityId, int pageIndex, int pageSize, ref int totalRows);
    }

    internal class ContactBookDao : DaoFactories<WebEntities, ContactBook>, IContactBookDao
    {
        //public bool Article_UpdateActive(int id, bool isActive, int modifyBy)
        //{
        //    using (Uow)
        //    {
        //        var outOutResult = new ObjectParameter("OutResult", typeof(int));
        //        var list = Uow.Context.Ins_Game_UpdateActive(id, isActive, modifyBy, outOutResult);
        //        int result = 0;
        //        if (outOutResult.Value != null)
        //        {
        //            int.TryParse(outOutResult.Value.ToString(), out result);
        //        }
        //        return result == 1;
        //    }
        //}


        public List<Ins_ContactBook_Search_Result> Search(string keyWord, string phone, string email, int cityId, int pageIndex, int pageSize, ref int totalRows)
        {
            using (Uow)
            {
                var outOutResult = new ObjectParameter("TotalRows", typeof(int));
                var list = Uow.Context.Ins_ContactBook_Search(keyWord, phone, email, cityId, pageIndex, pageSize, outOutResult);
                totalRows = 0;
                if (outOutResult.Value != null)
                {
                    int.TryParse(outOutResult.Value.ToString(), out totalRows);
                }
                return list.ToList();
            }
        }
    }
}
