﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DataAccess.EF;
using DataAccess.Interface;
using EntitiesObject.Entities.WebEntities;
using DataAccess.Extension;
using System.Data.Entity.Core.Objects;

namespace DataAccess.Dao.WebDao
{
    public interface INewBrokerDao : IBaseFactories<NewBroker>
    {

        List<Ins_BrokerAccount_SearchAutoComplete_Result> SearchAutoComplete(string keyword);
        NewBroker GetByAdminId(int adminId);

        //Boolean AddBroker(NewBroker broker);
    }

    internal class NewBrokerDao : DaoFactories<WebEntities, NewBroker>, INewBrokerDao
    {
        //public bool Article_UpdateActive(int id, bool isActive, int modifyBy)
        //{
        //    using (Uow)
        //    {
        //        var outOutResult = new ObjectParameter("OutResult", typeof(int));
        //        var list = Uow.Context.Ins_Game_UpdateActive(id, isActive, modifyBy, outOutResult);
        //        int result = 0;
        //        if (outOutResult.Value != null)
        //        {
        //            int.TryParse(outOutResult.Value.ToString(), out result);
        //        }
        //        return result == 1;
        //    }
        //}


        public List<Ins_BrokerAccount_SearchAutoComplete_Result> SearchAutoComplete(string keyword)
        {
            using (Uow)
            {
                return Uow.Context.Ins_BrokerAccount_SearchAutoComplete(keyword).ToList();
            }
        }


        public NewBroker GetByAdminId(int adminId)
        {
            using (Uow)
            {
                var obj = Uow.Context.NewBrokers.FirstOrDefault(x => x.ID == adminId);
                return obj;
            }
        }


        //public bool AddBroker(NewBroker broker)
        //{
        //    using (Uow)
        //    {
        //        var obj = Uow.Context.NewBrokers.FirstOrDefault(c => c.ID == broker.ID);
        //        if (obj != null)
        //        {
        //            obj.Username = broker.Username;
        //            obj.DisplayName = broker.DisplayName;
        //            obj.Email = broker.Email;
        //            obj.Phone = broker.Phone;
        //            obj.Password = broker.Password;
        //            //obj.Status = broker.Status;
        //            obj.Avatar = broker.Avatar;
        //            obj.Keyword = broker.Keyword;
        //            return DaoFactory.BrokerAccount.Save(obj);
        //        }
        //        else
        //        {
        //            return DaoFactory.BrokerAccount.Add(broker);
        //        }
        //    }
        //}
    }
}
