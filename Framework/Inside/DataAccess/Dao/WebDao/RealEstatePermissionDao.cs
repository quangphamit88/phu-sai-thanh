﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DataAccess.EF;
using DataAccess.Interface;
using EntitiesObject.Entities.WebEntities;
using DataAccess.Extension;
using System.Data.Entity.Core.Objects;

namespace DataAccess.Dao.WebDao
{
    public interface IRealEstatePermissionDao : IBaseFactories<RealEstatePermission>
    {
        List<Ins_RealEstatePermission_SelectByProductId_Result> GetByProductId(long productId);
        RealEstatePermission GetByKeyIds(long productId, int adminId);

        List<RealEstatePermission> GetByAdminId(int adminId);
    }

    internal class RealEstatePermissionDao : DaoFactories<WebEntities, RealEstatePermission>, IRealEstatePermissionDao
    {
        //public bool Article_UpdateActive(int id, bool isActive, int modifyBy)
        //{
        //    using (Uow)
        //    {
        //        var outOutResult = new ObjectParameter("OutResult", typeof(int));
        //        var list = Uow.Context.Ins_Game_UpdateActive(id, isActive, modifyBy, outOutResult);
        //        int result = 0;
        //        if (outOutResult.Value != null)
        //        {
        //            int.TryParse(outOutResult.Value.ToString(), out result);
        //        }
        //        return result == 1;
        //    }
        //}


        public List<Ins_RealEstatePermission_SelectByProductId_Result> GetByProductId(long productId)
        {
            using (Uow)
            {
                return Uow.Context.Ins_RealEstatePermission_SelectByProductId(productId).ToList();
            }
        }

        public RealEstatePermission GetByKeyIds(long productId, int adminId)
        {
            using (Uow)
            {
                return Uow.Context.RealEstatePermissions.Where(x => x.ProductId == productId && x.AdminId == adminId).FirstOrDefault();
            }
        }


        public List<RealEstatePermission> GetByAdminId(int adminId)
        {
            using (Uow)
            {
                return Uow.Context.RealEstatePermissions.Where(x => x.AdminId == adminId).ToList();
            }
        }
    }
}
