﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DataAccess.EF;
using DataAccess.Interface;
using EntitiesObject.Entities.WebEntities;
using DataAccess.Extension;
using System.Data.Entity.Core.Objects;

namespace DataAccess.Dao.WebDao
{
    public interface IAdminDao : IBaseFactories<Admin>
    {
        /// <summary>
        /// Lấy tất cả Admin
        /// </summary>
        /// <returns></returns>
        List<Admin> Admin_GetAll();

        //kiểm tra Admin đã tồn tại chưa
        bool Admin_CheckIsExist(string keyWord);

        Ins_Admin_Login_Result Admin_Login(string loginName, string pass);

        List<Ins_Admin_Search_Paging_Result> Admin_Search(string keyword, int isAtive, int pageIndex, int pagelengh, out int totalRow);

        int Admin_Insert(Admin input);

        bool Admin_UpdateInfo(Admin input);

        bool Admin_UpdatePass(int id, string oldPPassword, string newPassword);

        bool Admin_UpdateActive(int id, bool isActive, int modifyBy);
        Ins_Admin_GetById_Result Admin_GetById(int id);
    }

    internal class AdminDao : DaoFactories<WebEntities, Admin>, IAdminDao
    {
        /// <summary>
        /// Lấy tất cả Admin
        /// </summary>
        /// <returns></returns>
        public List<NewBrokers> Admin_GetAll()
        {
            using (Uow)
            {
                return Uow.Context.NewBrokers.ToList();
            }
        }

        public Ins_Admin_GetById_Result Admin_GetById(int id)
        {
            using (Uow)
            {
                return Uow.Context.Ins_Admin_GetById(id).FirstOrDefault();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool Admin_CheckIsExist(string keyWord)
        {
            using (Uow)
            {
                var outIsExist = new ObjectParameter("IsExist", typeof(int));
                var list = Uow.Context.Ins_Admin_IsExist(keyWord, outIsExist);
                bool result = false;
                if (outIsExist.Value != null)
                {
                    bool.TryParse(outIsExist.Value.ToString(), out result);
                }

                return result;
            }
        }

        public Ins_Admin_Login_Result Admin_Login(string loginName, string pass)
        {
            using (Uow)
            {
                return Uow.Context.Ins_Admin_Login(loginName, pass).FirstOrDefault();
            }
        }

        public List<Ins_Admin_Search_Paging_Result> Admin_Search(string keyword, int isAtive, int pageIndex, int pagelengh, out int totalRow)
        {
            using (Uow)
            {
                var outTotalRow = new ObjectParameter("TotalRow", typeof(int));
                var list = Uow.Context.Ins_Admin_Search_Paging(keyword, isAtive, pageIndex, pagelengh, outTotalRow);
                totalRow = 0;
                if (outTotalRow.Value != null)
                {
                    int.TryParse(outTotalRow.Value.ToString(), out totalRow);
                }
                return list.ToList();
            }
        }

        //public int Admin_Insert(Admin input)
        //{
        //    using (Uow)
        //    {
        //        var outIdAdmin = new ObjectParameter("IdAdmin", typeof(int));
        //        var list = Uow.Context.Ins_Admin_Insert(input.LoginName, input.Password, input.DisplayName, input.Avatar, input.IsActive, input.CreateBy, outIdAdmin);
        //        int result = 0;
        //        if (outIdAdmin.Value != null)
        //        {
        //            int.TryParse(outIdAdmin.Value.ToString(), out result);
        //        }
        //        return result;
        //    }
        //}

        //public bool Admin_UpdateInfo(Admin input)
        //{
        //    using (Uow)
        //    {

        //        var outOutResult = new ObjectParameter("OutResult", typeof(int));
        //        var list = Uow.Context.Ins_Admin_UpdateInfo(input.ID, input.DisplayName, input.Avatar, input.IsActive, input.ModifyBy, outOutResult);
        //        int result = 0;
        //        if (outOutResult.Value != null)
        //        {
        //            int.TryParse(outOutResult.Value.ToString(), out result);
        //        }
        //        return result == 1;
        //    }
        //}

        public bool Admin_UpdatePass(int id, string oldPPassword, string newPassword)
        {
            using (Uow)
            {

                var outOutResult = new ObjectParameter("OutResult", typeof(int));
                var list = Uow.Context.Ins_Admin_UpdatePass(id, oldPPassword, newPassword, outOutResult);
                int result = 0;
                if (outOutResult.Value != null)
                {
                    int.TryParse(outOutResult.Value.ToString(), out result);
                }

                return result == 1;
            }
        }

        public bool Admin_UpdateActive(int id, bool isActive, int modifyBy)
        {
            using (Uow)
            {

                var outOutResult = new ObjectParameter("OutResult", typeof(int));
                var list = Uow.Context.Ins_Admin_UpdateActive(id, isActive, modifyBy, outOutResult);
                int result = 0;
                if (outOutResult.Value != null)
                {
                    int.TryParse(outOutResult.Value.ToString(), out result);
                }

                return result == 1;
            }
        }


    }
}
