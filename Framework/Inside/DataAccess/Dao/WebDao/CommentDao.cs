﻿/**********************************************************************
 * Author:      VIetnhh
 * DateCreate:  2017-05-04
 *********************************************************************/

using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.EF;
using DataAccess.Interface;
using EntitiesObject.Entities.WebEntities;

namespace  DataAccess.Dao.WebDao
{
    public interface ICommentDao : IBaseFactories<Comment>
    {

        /// <summary>
        /// get danh sách comment theo ArticleID
        /// startDate = null, endDate = null sẽ lấy tất cả
        /// </summary>
        /// <param name="articleID"></param>
        /// <param name="accountID"></param>
        /// <param name="status">0: soạn thảo || 1: review || 2: hiển thị || 3: ẩn</param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="startIndex"></param>
        /// <param name="pageLength"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        List<Ins_Comment_List_ArticleID_Paging_Result> GetList(int articleID, int? accountID, int? status, DateTime? startDate, DateTime? endDate, short? orderBy, int startIndex, int pageLength, out int totalRow);

        bool Comment_EditStatus(int id, int isActive, int modifyBy);

        Ins_Comment_GetById_Result Comment_GetById(int id);

        bool Comment_UpdateContent(Comment input);
    }

    internal class CommentDao : DaoFactories<WebEntities, Comment>, ICommentDao
    {
        public bool Comment_UpdateContent(Comment input)
        {
            using (Uow)
            {
                var outResult = new ObjectParameter("OutResult", typeof(int));
                var list = Uow.Context.Ins_Comment_UpdateContent(input.ID, input.Content, input.ModifyBy, input.ModifyByType, outResult);
                int result = 0;
                if (outResult.Value != null)
                {
                    int.TryParse(outResult.Value.ToString(), out result);
                }
                return result == 1;
            }
        }
        public Ins_Comment_GetById_Result Comment_GetById(int id)
        {
            using (Uow)
            {
                return Uow.Context.Ins_Comment_GetById(id).FirstOrDefault();
            }
        }



        public bool Comment_EditStatus(int id, int isActive, int modifyBy)
        {
            using (Uow)
            {
                var outOutResult = new ObjectParameter("OutResult", typeof(int));
                var list = Uow.Context.Ins_Comment_UpdateStaus(id, isActive, modifyBy, outOutResult);
                int result = 0;
                if (outOutResult.Value != null)
                {
                    int.TryParse(outOutResult.Value.ToString(), out result);
                }
                return result == 1;
            }
        }
        public List<Ins_Comment_List_ArticleID_Paging_Result> GetList(int articleID, int? accountID, int? status, DateTime? startDate, DateTime? endDate, short? orderBy, int startIndex, int pageLength, out int totalRow)
        {
            using (Uow)
            {
                var outTotalRow = new ObjectParameter("TotalRow", typeof(int));
                var list = Uow.Context.Ins_Comment_List_ArticleID_Paging(articleID, accountID, status, startDate, endDate, orderBy, startIndex, pageLength, outTotalRow).ToList();
                totalRow = outTotalRow.Value == null ? 0 : (int)outTotalRow.Value;
                return list;
            }
        }

    }
}
