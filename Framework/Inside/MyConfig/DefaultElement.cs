﻿/**********************************************************************
 * Author: ThongNT
 * DateCreate: 06-25-2014 
 * Description: Quan ly thong tin cau hinh chung cho project  
 * ####################################################################
 * Author:......................
 * DateModify: .................
 * Description: ................
 * 
 *********************************************************************/
using System.Configuration;

namespace MyConfig
{
    public class DefaultElement : ConfigurationElement
    {
        [ConfigurationProperty("AdminRoleId", DefaultValue = 1)]
        public int AdminRoleId
        {
            get { return (int)this["AdminRoleId"]; }
        }

        [ConfigurationProperty("GameID", DefaultValue = 1)]
        public int GameID
        {
            get { return (int)this["GameID"]; }
        }

        [ConfigurationProperty("PageSize", DefaultValue = 30)]
        public int PageSize
        {
            get { return (int)this["PageSize"]; }
        }

        [ConfigurationProperty("DefaultLanguage", DefaultValue = "vn")]
        public string DefaultLanguage
        {
            get { return (string)this["DefaultLanguage"]; }
        }
    }
}
