﻿using System.Configuration;

namespace MyConfig.Authentication
{
    /// <summary>
    /// Định nghĩa cấu hình kết nối Google
    /// <para>Author: PhatVT</para>
    /// <para>Created Date: 23/12/2014</para>
    /// </summary>
    public class GoogleOpenAuthElement : ConfigurationElement
    {
        [ConfigurationProperty("Url", DefaultValue = "https://accounts.google.com/o/oauth2/auth")]
        public string Url
        {
            get { return (string)this["Url"]; }
        }

        [ConfigurationProperty("Scope", DefaultValue = "https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email")]
        public string Scope
        {
            get { return (string)this["Scope"]; }
        }

        [ConfigurationProperty("ReturnUrl", DefaultValue = "http://p111.us/dang-nhap/google")]
        public string ReturnUrl
        {
            get { return (string)this["ReturnUrl"]; }
        }

        [ConfigurationProperty("ClientId", DefaultValue = "729883332587-5k8pjito5rcc8tp6osrdpq4u3a6car35.apps.googleusercontent.com")]
        public string ClientId
        {
            get { return (string)this["ClientId"]; }
        }
    }
}
