﻿using System.Configuration;

namespace MyConfig.Authentication
{
    /// <summary>
    /// Định nghĩa cấu hình kết nối Yahoo
    /// <para>Author: PhatVT</para>
    /// <para>Created Date: 24/12/2014</para>
    /// </summary>
    public class YahooOpenAuthElement : ConfigurationElement
    {
        [ConfigurationProperty("ReturnUrl", DefaultValue = "http://p111.us/dang-nhap/xac-nhan/yahoo")]
        public string ReturnUrl
        {
            get { return (string)this["ReturnUrl"]; }
        }
    }
}
