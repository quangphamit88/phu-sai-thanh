﻿using System.Configuration;

namespace MyConfig.Authentication
{
    /// <summary>
    /// Định nghĩa cấu hình kết nối Facebook
    /// <para>Author: PhatVT</para>
    /// <para>Created Date: 24/12/2014</para>
    /// </summary>
    public class FacebookOpenAuthElement : ConfigurationElement
    {
        [ConfigurationProperty("Url", DefaultValue = "https://www.facebook.com/v2.8/dialog/oauth?")]
        public string Url
        {
            get { return (string)this["Url"]; }
        }

        [ConfigurationProperty("AccessTokenUrl", DefaultValue = "https://graph.facebook.com/v2.8/oauth/access_token?")]
        public string AccessTokenUrl
        {
            get { return (string)this["AccessTokenUrl"]; }
        }

        [ConfigurationProperty("AppId", DefaultValue = "")]
        public string AppId
        {
            get { return (string)this["AppId"]; }
        }

        [ConfigurationProperty("AppSecret", DefaultValue = "")]
        public string AppSecret
        {
            get { return (string)this["AppSecret"]; }
        }

        [ConfigurationProperty("AppNamespace", DefaultValue = "")]
        public string AppNamespace
        {
            get { return (string)this["AppNamespace"]; }
        }
        [ConfigurationProperty("ReturnUrl", DefaultValue = "http://3cay.net/dang-nhap/xac-nhan/facebook")]
        public string ReturnUrl
        {
            get { return (string)this["ReturnUrl"]; }
        }

        [ConfigurationProperty("ProfileUrl", DefaultValue = "https://graph.facebook.com/v2.8/me")]
        public string ProfileUrl
        {
            get { return (string)this["ProfileUrl"]; }
        }

        [ConfigurationProperty("AppLink", DefaultValue = "")]
        public string AppLink
        {
            get { return (string)this["AppLink"]; }
        }

        [ConfigurationProperty("Scope", DefaultValue = "")]
        public string Scope
        {
            get { return (string)this["Scope"]; }
        }
    }
}
