﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesObject.Message.DtoResponse
{
    public class BaseDtoResponse
    {
        /// <summary>
        /// Ma loi tra ve cho client
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        /// Thong diep tra ve cho client
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Du lieu tra ve neu co
        /// </summary>
        public object Data { get; set; }
    
    }
}
