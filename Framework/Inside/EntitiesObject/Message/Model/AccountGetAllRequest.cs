﻿namespace EntitiesObject.Message.Model
{
    public class AccountGetAllModel
    {
        public int? Count { get; set; }
        public int? FromUserId { get; set; }
    }
}
