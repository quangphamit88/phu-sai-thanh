﻿using System.ComponentModel.DataAnnotations;

namespace EntitiesObject.Custom.Validation
{
    public class RealEstateOnSaleValidate
    {
        [Required(ErrorMessage = "Vui lòng chọn ngày")]
        public string PublicDate { get; set; }
    }
}
