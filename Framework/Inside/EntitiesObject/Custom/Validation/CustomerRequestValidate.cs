﻿using System.ComponentModel.DataAnnotations;

namespace EntitiesObject.Custom.Validation
{
    public class CustomerRequestValidate
    {
        [Required]
        public string CustomerName { get; set; }

        [EmailAddress]
        public string Email { get; set; }

    }
}
