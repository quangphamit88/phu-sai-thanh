﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EntitiesObject.Custom.Validation
{
    public class ContactBookValidate
    {
        public long Id { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập")]
        public string ContactName { get; set; }

        public string CompanyName { get; set; }
        public string CardId { get; set; }
        public string TaxNo { get; set; }

        //[Required(ErrorMessage = "Vui lòng nhập")]
        public string Mobile { get; set; }

        //[Required(ErrorMessage = "Vui lòng nhập")]
        public string Email { get; set; }

        //public Nullable<bool> IsPersonal { get; set; }
        //public Nullable<long> AddressId { get; set; }
        public string Notes { get; set; }
    }
}
