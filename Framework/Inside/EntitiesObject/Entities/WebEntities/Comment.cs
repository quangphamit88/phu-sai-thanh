//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EntitiesObject.Entities.WebEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class Comment
    {
        public int ID { get; set; }
        public int ArticleID { get; set; }
        public int AccountID { get; set; }
        public string AccountDisplayName { get; set; }
        public short AccountType { get; set; }
        public string Content { get; set; }
        public short Status { get; set; }
        public Nullable<int> ModifyBy { get; set; }
        public Nullable<short> ModifyByType { get; set; }
        public System.DateTime CreateDate { get; set; }
        public System.DateTime ModifyDate { get; set; }
    }
}
